close 
close all
%clc
clear

im = imread('/home/pika/Infurnia/opencvCodes/pano_test/sample_images/test/IMG_20160520_172523.jpeg');
%imshow(im);
%im_new = im;
%im_new = zeros(480,640,3);
%im_new = im2uint8(im_new);
H_fov = [-61.1/2 61.1/2];
V_fov = [-47.3/2 47.3/2];

%d_H = (H_fov(2)- H_fov(1))/640;
%d_V = (V_fov(2)- V_fov(1))/480;

focal_H = 640/(2*tand(H_fov(2)));
focal_V = 480/(2*tand(V_fov(2)));

%focal_H = 513;
init_x = 0;
init_y = 0;

phi = H_fov(1);
theta = V_fov(1);

init_x = round(focal_H*tand(phi))
init_y = round(focal_V*tand(theta)*secd(phi))

im_new = zeros(2*abs(init_y)+1,2*abs(init_x)+1,3);
im_new = im2uint8(im_new);

d_H = (H_fov(2)- H_fov(1))/(2*abs(init_x)+1);
d_V = (V_fov(2)- V_fov(1))/(2*abs(init_y)+1);


for cols = 1 : 2*abs(init_x)+1
  for rows = 1 : 2*abs(init_y)+1
    phi = H_fov(1) + (cols-1)*d_H;
    theta = V_fov(1) + (rows-1)*d_V;
    
    p_x = focal_H*tand(phi);
    p_y = focal_V*tand(theta)*secd(phi)

%    round(p_y)-init_y+1 
%    round(p_x)-init_x+1
    if( round(p_y+240+1) <= 480 && round(p_y+240+1) >= 1 && round(p_x+320+1) <= 640  && round(p_x+320+1) >= 1)
      im_new(rows,cols,:) = im(round(p_y+240+1),round(p_x+320+1),:);
    end
  end
end

imshow(im);
figure,imshow(im_new);