im = imread('/home/pika/Infurnia/opencvCodes/pano_test/sample_images/test/IMG_20160520_172523.jpeg');
im_new = zeros(480,640);
H_fov = [-61.1/2 61.1/2];
V_fov = [-47.3/2 47.3/2];

d_H = (H_fov(2)- H_fov(1))/640
d_V = (V_fov(2)- V_fov(1))/480

focal = 640/(2*tand(H_fov(2)))

for cols = 1 : 640
  for rows = 1: 480
    phi = H_fov(1) + (cols-1)*d_H
    theta = V_fov(1) + (rows-1)*d_V
    p_x = focal*tand(phi)
    p_y = focal*tand(theta)*secd(theta)
    im_new(rows,cols) = im(round(p_x+1),round(p_y+1));
  end
end