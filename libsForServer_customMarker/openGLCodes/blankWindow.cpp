#include <bits/stdc++.h>
#include <GL/glut.h>
#include <GL/gl.h>

using namespace std;
void renderer(void);
void initialSetup(void);
void changeSize(GLsizei w, GLsizei h); //New Function Prototype

static int prev_x = 0;
static int prev_y = 0;

float translate_x = 0;
float translate_y = 0;
float translate_z = -200;

float scale = 0.125;
static GLfloat theta[] = {0.0, 0.0, 0.0}; // Rotation (X,Y,Z)
static GLint axis = 2; // Changed by the Mouse Callback Routine


void keyboard (unsigned char key, int x, int y)
{
   switch (key) {
      case 27:
         exit(0);
         break;
      default:
         break;
   }
}


void argConvGlpara( float para[3][4], float gl_para[16] )
{
    int     i, j;

    for( j = 0; j < 3; j++ ) {
        for( i = 0; i < 4; i++ ) {
            gl_para[i*4+j] = para[j][i];
        }
    }
    gl_para[0*4+3] = gl_para[1*4+3] = gl_para[2*4+3] = 0.0;
    gl_para[3*4+3] = 1.0;
}
void motionCallback(int x, int y){
	
	//~ printf("\nMouse Pos : %d %d",x,y);
	//~ theta[axis] += 0.5;
	//~ if( theta[axis] > 360.0 ) theta[axis] -= 360.0;

	if(prev_x == prev_y && prev_x == 0){ prev_x = x; prev_y = y;}
	else{
		if(x - prev_x > 0){
			theta[axis] += 0.25;
			if( theta[axis] > 360.0 ) theta[axis] -= 360.0;
		}
		if(x - prev_x < 0){
			theta[axis] -= 0.25;
			//~ if( theta[axis] > 360.0 ) theta[axis] -= 360.0;
			if( theta[axis] < 360.0 ) theta[axis] += 360.0;
		}
	}

}
void mouseCallBack(int btn, int state, int x, int y)
{
	// Changes the rotation axis depending on the mouse button pressed.
	if(btn==GLUT_LEFT_BUTTON && state == GLUT_UP) {
		
		//~ axis = 0;
		prev_x = prev_y = 0;
		//~ printf("\nMouse Pos : %d %d",x,y);
	}
	//~ printf("\nMouse Pos : %d %d",x,y);
	if(btn==GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) translate_z += 50;
	if(btn==GLUT_RIGHT_BUTTON && state == GLUT_UP) {
		//~ axis = 2;
		translate_z -= 50;
	}
	if(btn == 3) {
		scale += 0.025; 
	}
	if(btn == 4){
		scale -= 0.025;
		if(scale <= 0.025) scale =0.025;
	}
}

void keyBoardCallBack(int key,int x, int y){
	if(key == GLUT_KEY_UP) translate_y -= 50;
	else if(key == GLUT_KEY_DOWN) translate_y += 50;
	else if(key == GLUT_KEY_RIGHT) translate_x += 50;
	else if(key == GLUT_KEY_LEFT) translate_x -= 50;
}
void idleCallBack()
{
	//~ // Spins the cube around the set axis.
	//~ theta[axis] += 0.5;
	//~ if( theta[axis] > 360.0 ) theta[axis] -= 360.0;
	glutPostRedisplay();
}


//~ void renderer(void)
//~ {
	//~ glClear(GL_COLOR_BUFFER_BIT);
	//~ glColor3f(1.0,0.0,0.0);
	//~ //Set the color to red
	//~ glRectf(100.0, 150.0, 150.0, 100.0);
	//~ //Draw a rectangle from 100,150 to 150, 100
	//~ glFlush();
//~ }

void renderer()
{
	int last=0;
	float ang;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//~ glRotatef(45,1,1,0);
	//~ glColor3f(0.0,1.0,0.0);
	glPushMatrix();
	//Draw a Cube Centered at origin
	//~ glutWireCube(80);
	//~ //Draw a Cube Centered at -80, -90, 0
	//~ glPushMatrix();
	//~ glTranslatef(0, 0, 0);
	//~ glutWireCube(70);
	//~ glPopMatrix();
	//~ //Draw a Cube Centered at -80, 90, 0
	//~ glPushMatrix();
	//~ glTranslatef(-80, 90, 0);
	//~ glutWireCube(60);
	//~ glPopMatrix();
	glRotatef(theta[0], 1.0, 0.0, 0.0);
	glRotatef(theta[1], 0.0, 1.0, 0.0);
	glRotatef(theta[2], 0.0, 0.0, 1.0);

	glTranslatef(translate_x, translate_y, translate_z);
	glScalef(scale, scale, scale);
	glColor3f(1,0,0);
	glBegin(GL_LINES);
	//~ 
//~ glVertex3f(-1442.362202,0.000000,2040.000000);
 //~ glVertex3f(-550.000000,0.000000,2040.000000);
 //~ glVertex3f(700.000000,0.000000,1195.000000);
 //~ glVertex3f(-210.000000,0.000000,1195.000000);
 //~ glVertex3f(700.000000,0.000000,2166.000000);
 //~ glVertex3f(-210.000000,0.000000,2166.000000);
 //~ glVertex3f(-550.000000,0.000000,0.000000);
 //~ glVertex3f(-550.000000,0.000000,2040.000000);
 //~ glVertex3f(700.000000,0.000000,1195.000000);
 //~ glVertex3f(700.000000,0.000000,2166.000000);
 //~ glVertex3f(-210.000000,0.000000,1195.000000);
 //~ glVertex3f(-210.000000,0.000000,2166.000000);
 //~ glVertex3f(-1460.753401,-1518.675873,2060.000000);
 //~ glVertex3f(-1471.530530,-2408.610619,2060.000000);
 //~ glVertex3f(-1460.753401,-1518.675873,-0.000000);
 //~ glVertex3f(-1460.753401,-1518.675873,2060.000000);
 //~ glVertex3f(-1471.530530,-2408.610619,-0.000000);
 //~ glVertex3f(-1471.530530,-2408.610619,2060.000000);
 //~ glVertex3f(-2603.363248,-2643.685006,892.000000);
 //~ glVertex3f(-1603.636547,-2667.062848,892.000000);
 //~ glVertex3f(-2603.363248,-2643.685006,1432.000000);
 //~ glVertex3f(-1603.636547,-2667.062848,1432.000000);
 //~ glVertex3f(-2603.363248,-2643.685006,892.000000);
 //~ glVertex3f(-2603.363248,-2643.685006,1432.000000);
 //~ glVertex3f(-1603.636547,-2667.062848,892.000000);
 //~ glVertex3f(-1603.636547,-2667.062848,1432.000000);
 //~ 
 //~ 
 //~ 
 //~ 
 //~ glVertex3f(-1442.362202,0.000000,0.000000);
 //~ glVertex3f(-1442.362202,0.000000,2631.000000);
 //~ glVertex3f(1056.182026,-0.000000,0.000000);
 //~ glVertex3f(1056.182026,-0.000000,2631.000000);
 //~ glVertex3f(-1474.696906,-2670.078002,0.000000);
 //~ glVertex3f(-1474.696906,-2670.078002,2631.000000);
 //~ 
 //~ glVertex3f(-1442.362202,0.000000,0.000000);
 //~ glVertex3f(1056.182026,-0.000000,0.000000);
 //~ glVertex3f(-1442.362202,0.000000,2631.000000);
 //~ glVertex3f(1056.182026,-0.000000,2631.000000);
//~ 
 //~ glVertex3f(-1474.696906,-2670.078002,0.000000);
 //~ glVertex3f(-1442.362202,0.000000,0.000000);
 //~ glVertex3f(-1474.696906,-2670.078002,2631.000000);
 //~ glVertex3f(-1442.362202,0.000000,2631.000000);
//~ 
 //~ glVertex3f(1056.182026,-0.000000,0.000000);
 //~ glVertex3f(1043.949350,-3391.410818,0.000000);
 //~ glVertex3f(1056.182026,-0.000000,2631.000000);
 //~ glVertex3f(1043.949350,-3391.410818,2631.000000);
//~ 
 //~ glVertex3f(-2812.306129,-2638.799037,0.000000);
 //~ glVertex3f(-1474.696906,-2670.078002,0.000000);
 //~ glVertex3f(-2812.306129,-2638.799037,2631.000000);
 //~ glVertex3f(-1474.696906,-2670.078002,2631.000000);
 //~ 
	ifstream myfile ("/home/pika/Infurnia/libsForServer_customMarker/featureVectors_bglr_frnd.txt");
	float ZVec[3] = {0};
	string l;
	if (myfile.is_open())
	{
		while ( getline (myfile,l) )
		{
				//~ cout << l << '\n';
				size_t strt = l.find('(');
				size_t end = l.find(')');
				
				//~ cout << " \n strt : " << strt << " end : " << end << endl;
				
				char temp[50] = ""; 
				for(int i = strt+1,j = 0,n = 0; i <= end; i++,j++){
					
					if(l[i] != ',' && l[i] != ')') temp[j] = l[i];
					else{
						temp[j] = '\0';
						//~ cout << temp<< " " << atof(temp) << endl;
						j = -1;
						ZVec[n] = atof(temp);
						n++;
						temp[0] = '\0';
					}
				}
			glVertex3f(ZVec[0],ZVec[1],ZVec[2]);
		}
		myfile.close();
	}

	//~ else cout << "Unable to open file";

	glEnd();
	glPopMatrix();
	//~ glPushMatrix();
	//~ glScalef(scale, scale, scale);
	//~ glTranslatef(translate_x, translate_y, translate_z);
	//~ //Draw a Cube Centered at origin
	//~ glutWireCube(80);
	//~ glPopMatrix();
	glutSwapBuffers();
}

void initialSetup(void)
{
	glClearColor(0.0, 0.0, .5, 1.0);
}

//~ void changeSize(GLsizei w, GLsizei h) //New Function for resizing
//~ {
	//~ if(h==0)
	//~ h = 1;
	//~ glViewport(0,0,w,h);
	//~ glMatrixMode(GL_PROJECTION);
	//~ glLoadIdentity();
	//~ if(w <= h)
	//~ glOrtho(0, 250, 0, 250.0 * h/w, 1, -1);
	//~ else
	//~ glOrtho(0,250.0 * w/h, 0, 250, 1, -1);
	//~ glMatrixMode(GL_MODELVIEW);
	//~ glLoadIdentity();
//~ }

void changeSize(GLsizei w, GLsizei h) {
	//~ GLfloat sideRange = 200;
	glViewport(0,0,1640,1230);
	//~ glMatrixMode(GL_PROJECTION);
	//~ glLoadIdentity();
	//~ glOrtho(-sideRange, sideRange, -sideRange, sideRange, sideRange, -sideRange);
	//~ glMatrixMode(GL_MODELVIEW);
	//~ glLoadIdentity();
	//~ 
	
	float glCameraMatrix[16] = {0};

	float near = 50,far = 5000,right = 3280*(float)1.12/(2*1000), left = -3280*(float)1.12/(2*1000), top = -2460*(float)1.12/(2*1000), bottom = 2460*(float)1.12/(2*1000);
	glCameraMatrix[0*4+0] = (float)2767/(float)1640/*(2*near)/((right-left)*10)*/;
	glCameraMatrix[2*4+0] = -(right+left)/(right-left);
	glCameraMatrix[1*4+1] = (float)-2767/(float)1230/*(2*near)/((top-bottom)*10)*/;
	glCameraMatrix[2*4+1] = -(top+bottom)/(top-bottom);
	glCameraMatrix[2*4+2] = (far+near)/(far-near);
	glCameraMatrix[3*4+2] = -2*far*near/(far-near);
	glCameraMatrix[2*4+3] = 1;

	//~ float transMat[3][4] = {{0.73808,0.67402,0.03061,-703.58578},{0.43628,-0.44216,-0.78368,185.50222},{-0.51468,0.59177,-0.62041,1985.46434}};
	float transMat[3][4] = {{1,0,0,0},{0,0,-1,0},{0,1,0,1985.46434}};
	//~ float transMat[3][4] = {{+0.67463,-0.73752,+0.03061,-266.96900},{-0.44179,-0.43665,-0.78368,+343.57436},{+0.59134,+0.51517,-0.62041,+1850.99456},};
	float glPara[16] = {0};
	//~ memset(glPara,0,sizeof(glPara[0][0]*4*4));
	argConvGlpara(transMat,glPara);
	//argDrawMode3D
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//argDraw3dCamera
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
//            GLU.gluPerspective(gl,80.0f,(float)2048/(float)1440,0.1f,100.0f);
	glLoadMatrixf( glCameraMatrix );
//		    gl.gl
	glMatrixMode(GL_MODELVIEW);
	
	glLoadMatrixf(glPara);

}


int main (int argc, const char * argv[]){
	

		glutInit(&argc, (char **)argv);
		glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
		glutCreateWindow("First Program");
		glutDisplayFunc(renderer);
		glutReshapeFunc(changeSize);
		glutMouseFunc(mouseCallBack);
		glutSpecialFunc(keyBoardCallBack);
		//~ glutMouseWheelFunc(mouseWheel);
		glutMotionFunc(motionCallback);
		glutIdleFunc(idleCallBack);
		glutKeyboardFunc(keyboard);
		initialSetup();
		glutMainLoop();
		return 0;
	}
