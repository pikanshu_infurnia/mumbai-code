#include "thresholdingGlobal.h"
void FindBlobs(const Mat &binary,vector<vector<Point2i> > &blobs)
{
    blobs.clear();

    // Fill the label_image with the blobs
    // 0  - background
    // 1  - unlabelled foreground
    // 2+ - labelled foreground

    Mat label_image;
    binary.convertTo(label_image, CV_32SC1);

    int label_count = 2; // starts at 2 because 0,1 are used already

    for(int y=0; y < label_image.rows; y++) {
        int *row = (int*)label_image.ptr(y);
        for(int x=0; x < label_image.cols; x++) {
            if(row[x] != 1) {
                continue;
            }

            Rect rect;
            floodFill(label_image, Point(x,y), label_count, &rect, 0, 0, 8);

            vector <Point2i> blob;

            for(int i=rect.y; i < (rect.y+rect.height); i++) {
                int *row2 = (int*)label_image.ptr(i);
                for(int j=rect.x; j < (rect.x+rect.width); j++) {
                    if(row2[j] != label_count) {
                        continue;
                    }

                    blob.push_back(Point2i(j,i));
                }
            }

            blobs.push_back(blob);

            label_count++;
        }
    }
}

void thresholding(const Mat &imGray ,const char path[100],int *CandidateNum,int thresholdingType, bool PRINT_ANDROID_LOG ){
	Mat imBW,binary;
	char str[50];
	char strWrite[200];
	
	int len;
	int height,width;
	int h = 201,w = 201;
	height = imGray.rows; width = imGray.cols;                    // dimension of input image

	printf("\nReached thresholding\n");
	if(! imGray.data ) {                                                   // checking image data
		if (PRINT_ANDROID_LOG)
			cout <<  "Could not open or find the image" << std::endl ;
			//~ __android_log_print(ANDROID_LOG_INFO,"THRESHOLDING","Could not open or find the image");
		
		return;
	}
	len = sprintf(str,"debugging/Gray_%d.jpg",thresholdingType);
	imwrite(str,imGray);
	
	if(thresholdingType == 0)	adaptiveThreshold(imGray,imBW,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY_INV,21,0); // adaptive thresholding or local thresholding for background = light , marker = dark
	else if(thresholdingType == 1)	adaptiveThreshold(imGray,imBW,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,21,0); // adaptive thresholding or local thresholding for background = dark , marker = light

	printf("\nReached after adaptive thresholding\n");
	
	//~ adaptiveThreshold(imGray,imBW,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY_INV,21,0); // adaptive thresholding or local thresholding for background = light , marker = dark
	// loop for erasing the unused part of image , top one-third rows and , first and last one-fourth columns
	for(int i = 0 ; i < height; i++)
		for(int j = 0; j < width; j++)
			if((i < height/3) || (j < width/4) || (j > (width*3)/4)) imBW.data[i*width+j] = 0;

	len = sprintf(str,"debugging/adaptiveThresh_%d.jpg",thresholdingType);
	imwrite(str,imBW);

	vector < vector<Point2i > > blobs;                          // output variable for blob detection

	threshold(imBW, binary, 0, 1.0, THRESH_BINARY);             // converting binary image 0-255 to 0-1
	
	//~ imBW = Mat(binary.size(),CV_8UC1,Scalar(255));
	FindBlobs(binary, blobs);	                                // blob detection

	//~ imBW.release();
	imBW = Mat(binary.size(),CV_8UC1,Scalar(255));
	binary.release();

	vector<Point2i> centroids;                    // centroids for blobs

	Mat output = Mat::zeros(imGray.size(), CV_8UC1);           // image for output connected component
	int candidateNum = *CandidateNum; // counter for final candidate 

	for(int i=0 ; i < blobs.size(); i++) {
		double tempx = 0,tempy = 0;
		Point2i centre;

		if(blobs[i].size() < 9000 && blobs[i].size() > 500){
			Mat crpIMGray,crpIMBW,crpEdge,crpBinary;
			crpIMGray = Mat(Size(w,h),CV_8UC1); crpIMBW = Mat::zeros(Size(w,h),CV_8UC1);      // initializing

			for(int j=0; j < blobs[i].size(); j++) {
				int x = blobs[i][j].x; int y = blobs[i][j].y;
				tempx += x; tempy += y;
				output.at<uchar>(y,x) = 255;
			}

			centre.x = int(tempx/blobs[i].size()); centre.y = int(tempy/blobs[i].size());
			bool flag = true;
			for(int j = 0; j < blobs[i].size(); j++){
				int x = blobs[i][j].x; int y = blobs[i][j].y;
				if(abs(x-centre.x)>60 || abs(y-centre.y)>60){
					flag = false;
					break;
				} 
			}
			if(flag == false) continue;

			centroids.push_back(centre);
			// x = col = width, y = row = height;

			int offsetRow = centre.y - 100, offsetCol = centre.x - 100;
			for(int j=0; j < blobs[i].size(); j++) {
				int x = blobs[i][j].x; int y = blobs[i][j].y;
				//~ tempx += x; tempy += y;
				if((x-offsetCol) >= 0 && (y-offsetRow) >= 0)
					crpIMBW.data[(y-offsetRow)*w+(x-offsetCol)] = 255;
			}

			for(int m = 0; m < h ; m++){
				for(int n = 0; n < w; n++){
					if(m+offsetRow >=0 && m+offsetRow < height && n+offsetCol >=0 && n+offsetCol < width){
						crpIMGray.data[m*w + n] = imGray.data[(m+offsetRow)*width+(n+offsetCol)];
						//~ crpIMBW.data[m*w+n] = output.data[((m+offsetRow)*width+(n+offsetCol))];
					}
					else crpIMGray.data[m*w + n] /*= crpIMBW.data[m*w+n]*/ = 0;
				}
			}
			//~ len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_1.jpg",count,int(centroids.size()));
			//~ strcat(strWrite,path); // adding path to write string
			//~ len = sprintf(str,"Crop_%03d.jpg",int(centroids.size()));
			//~ strcat(strWrite,str);
			//~ imwrite(strWrite,crpIMGray);
			//~ strWrite[0] = '\0';
			len = sprintf(str,"debugging/crpIMGray_%03d_%d.jpg",int(centroids.size()),thresholdingType);
			imwrite(str,crpIMGray);

			GaussianBlur(crpIMGray,crpIMGray, Size(3,3),sqrt(2));
			Canny(crpIMGray,crpEdge, 0.1*255, 0.3*255, 3,false);
			crpIMGray.release();
			
			len = sprintf(str,"debugging/Edge_%03d_%d.jpg",int(centroids.size()),thresholdingType);
			imwrite(str,crpEdge);

			vector < vector<Point2i > > cannyBlobs;
			threshold(crpEdge, crpBinary, 0, 1.0, THRESH_BINARY);             // converting binary image 0-255 to 0-1
			vector<Point2i> cannyCentroids;                                        // centroids for cannyblobs
			FindBlobs(crpBinary, cannyBlobs);
			crpBinary.release();
			int totalSize = 0;
			for(int a=0 ; a < cannyBlobs.size(); a++) {
				double cannyTempx = 0,cannyTempy = 0;
				Point2i cannyCentre;
				bool flag2 = true;
				for(int b=0; b < cannyBlobs[a].size(); b++) {
					int cannyX = cannyBlobs[a][b].x;
					int cannyY = cannyBlobs[a][b].y;
					if(abs(cannyX - 100) > 60 || abs(cannyY - 100) > 60) {
						flag2 = false;
						break;
					}
					cannyTempx += cannyX; cannyTempy += cannyY;
				}
				cannyCentre.x = int(cannyTempx/cannyBlobs[a].size()); cannyCentre.y = int(cannyTempy/cannyBlobs[a].size());

				if(((cannyCentre.x < 50 || cannyCentre.x > 150) || (cannyCentre.y < 50 || cannyCentre.y > 150) || flag2 == false)){
					for(int b=0; b < cannyBlobs[a].size(); b++) {
						crpEdge.data[cannyBlobs[a][b].y*w + cannyBlobs[a][b].x] = 0;
					}
				}
				else totalSize += (int)cannyBlobs[a].size();
			}
			//~ if (PRINT_ANDROID_LOG)
				//~ __android_log_print(ANDROID_LOG_INFO,"THRESHOLDING","\n Count %d,totalSize %d \n",int(centroids.size()),totalSize);
			if(totalSize > 20){
				//~ strcat(strWrite,path); // adding path to write string
				len = sprintf(str,"debugging/edge_%03d_%d.jpg",int(centroids.size()),thresholdingType);
				//~ strcat(strWrite,str);
				imwrite(str,crpEdge);
				//~ strWrite[0] = '\0';
			}
			else continue;
			for(int m = 0; m < h ; m++){
				for(int n = 0; n < w; n++){
					crpIMBW.data[m*w + n] += crpEdge.data[m*w+n];
				}
			}
			crpEdge.release();

			threshold(crpIMBW, crpIMBW, 0, 255, THRESH_BINARY_INV);

			Mat tmpImg = Mat :: ones(427,320,CV_8UC1);
			for(int i = 0 ,m = 0; i < h; i++,m++){
				for(int j = 0,n = 0; j < w; j++,n++){
					tmpImg.data[m*320+n] = crpIMBW.data[i*w+j];
				}
			}
			//~ strcat(strWrite,path);
			//~ len = sprintf(str,"CrpBin_%03d.jpg",int(centroids.size()));
			//~ strcat(strWrite,str);
			//~ imwrite(strWrite,crpIMBW);
			//~ strWrite[0] = '\0';

			//~ crpIMBW.release();

			threshold(tmpImg, tmpImg, 0, 255, THRESH_BINARY);
			
			strWrite[0] = '\0';
			strcat(strWrite,path);
			len = sprintf(str,"candidate_%02d_%04d_%04d.jpg",++candidateNum,centre.x,centre.y);
			strcat(strWrite,str);
			imwrite(strWrite,tmpImg);
			//~ if (PRINT_ANDROID_LOG)
				//~ __android_log_print(ANDROID_LOG_INFO,"THRESHOLDING","\n path : %s \n str : %s \n strWrite : %s\n",path,str,strWrite);
			for(int m = 0; m < h ; m++){
				for(int n = 0; n < w; n++){
					if(m+offsetRow >=0 && m+offsetRow < height && n+offsetCol >=0 && n+offsetCol < width){
						imBW.data[(m+offsetRow)*width+(n+offsetCol)] = crpIMBW.data[m*w+n] ;
					}
				}
			}
			crpIMBW.release();
			tmpImg.release();
		}
	}
	//~ strWrite[0] = '\0';
	//~ strcat(strWrite,path);
	len = sprintf(str,"debugging/final_%d.jpg",thresholdingType);
	imwrite(str,imBW);

	//~ strWrite[0] = '\0';
	//~ strcat(strWrite,path);
	len = sprintf(str,"debugging/output_%d.jpg",thresholdingType);
	imwrite(str,output);

	*CandidateNum = candidateNum;
	output.release();
}
