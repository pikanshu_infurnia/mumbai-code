#include "mainFunc.h"
#include "object.h"
#include <bits/stdc++.h>

using namespace cv;
using namespace std;

///////////////////////////////////////////////////// transformation relationship variables 
struct relativeTransMat{
	int id[2];
	double transMat[4][4];
};

double roomHeight = 0;

vector<int> markerIdx_id;
vector<vector<relativeTransMat> > imageWiseElements;
vector<relativeTransMat> initialElements;
vector<relativeTransMat> intermediateElements;
vector<relativeTransMat> finalElements;
///////////////////////////////////////////////////////////////////////////////

vector<vector<int> > extremePoints; //  extreme ends of baselines on walls (size = no. of detected markers
vector<vector<int> > baseVec; // stored as vectors : id S2 E2 (S1=S2=500)  // baselines stored for floor plan
vector<vector<vector<int> > > featureVec; // feature elements stored as strtPt and endPt (size = no. of distinct markers)
vector<vector<vector<int> > > featureVec1; // same as featureVec but the feature line's end points are arranged in an order (from low to high in value)
vector<vector<vector<int> > > rectVec; // same as featureVec but only feature elements that qualifiy as window or door 
 


//~ int	dist[12][12];
/////////////////////////////////// variables for flloyd warshall 
int intermediateMarkerRelationMatrix[12][12]; // matrix used for marker connections
vector<vector<int> > dist;
vector<vector<int> > pathMat;
static int n = 0; // no. of distinct markers detected
//~ int finalMarkerRelationArray[12];
///////////////////////////////////

/////////////////////////////////// Intersection of base lines ///////////////////////
vector<double> intersection(vector<double> line1, vector<double> line2){
	double x1,x2,x3,x4,y1,y2,y3,y4,x,y;
	vector<double> vec(2,0);
	x1 = line1[0]; y1 = line1[1];
	x2 = line1[2]; y2 = line1[3];
	x3 = line2[0]; y3 = line2[1];
	x4 = line2[2]; y4 = line2[3];
	double isParallel = (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4);
	if(isParallel != 0){
		x = ((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/isParallel;
		y = ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/isParallel;
		vec[0] = x; vec[1] = y;
	}
	return vec;
}
///////////////////////////////// identifying rectangular objects ////////////////////////////////
double calcOverlappingPart(double S1,double S2,double S3,double S4){
	double temp;
	double middle[2];
	double s[4] = {S1,S2,S3,S4};
	for(int i = 0; i < 4; i++){
		for(int j = i+1; j < 4; j++){
			if(s[j] < s[i]){
				temp = s[j];
				s[j] = s[i];
				s[i] = temp;
			}  
		}
	}
	middle[0] = s[1];
	middle[1] = s[2];
	
	if((S2-S3)*(S2-S4) < 0 || (S1-S3)*(S2-S4) < 0 || (S3-S1)*(S3-S2) < 0 || (S4-S1)*(S4-S2) < 0){ 
		temp = (fabs(s[2]-s[1])/fabs(S2-S1)) < (fabs(s[2]-s[1])/fabs(S4-S3)) ? (fabs(s[2]-s[1])/fabs(S2-S1)):(fabs(s[2]-s[1])/fabs(S4-S3));
		return(temp);
	}
	else return 0;
}
double calcCf(ProjectedLine line1, ProjectedLine line2, int mode){ // calculating conf. factor between a pair of lines
	if(mode == 0){
		//~ printf("\n line1 %f line2 %f ", line1.len,line2.len);
		double ratio = (line1.len > line2.len ? line2.len:line1.len)/(line1.len > line2.len ? line1.len:line2.len);
		double overlappingPart = calcOverlappingPart(line1.strtPt[0],line1.endPt[0],line2.strtPt[0],line2.endPt[0]);
		//~ printf("\n ratio %f over %f ", ratio,overlappingPart); 
		return(ratio*overlappingPart);
	}
	if(mode == 1){
		//~ printf("\n line1 %f line2 %f ", line1.len,line2.len);
		double ratio = (line1.len > line2.len ? line2.len:line1.len)/(line1.len > line2.len ? line1.len:line2.len);
		double overlappingPart = calcOverlappingPart(line1.strtPt[1],line1.endPt[1],line2.strtPt[1],line2.endPt[1]);
		//~ printf("\n ratio %f over %f ", ratio,overlappingPart); 
		return(ratio*overlappingPart);
	}

}

double calcCfRect(ProjectedLine Hline1,ProjectedLine Hline2,ProjectedLine Vline1,ProjectedLine Vline2){ // calculating conf. factor between a pair of horizontal and vertical pair
	//~ Point rectCt1,rectCt2;
	//~ double Vwd,Vht,Hwd,Hht;
	double temp;
	double s[4];
	s[0] = Hline1.strtPt[0]; s[1] = Hline1.endPt[0];s[2] = Hline2.strtPt[0]; s[3] = Hline2.endPt[0]; 
	for(int i = 0; i < 4; i++){
		for(int j = i+1; j < 4; j++){
			if(s[j] < s[i]){
				temp = s[j];
				s[j] = s[i];
				s[i] = temp;
			}  
		}
	}
	double overlappingPart1 = calcOverlappingPart(s[0],s[3],Vline2.strtPt[0],Vline1.strtPt[0]);
	//~ printf("\n\n (%f,%f) (%f,%f) : overlappingPart1 : %f ", s[0],s[3],Vline2.strtPt[0],Vline1.strtPt[0],overlappingPart1);
	//~ Hwd = fabs(s[0]-s[3]);
	//~ Hht = fabs(Hline1.strtPt[1] - Hline2.strtPt[1]);
	//~ 
	//~ rectCt1.x = (s[0]+s[3])/2;
	//~ rectCt1.y = (Hline1.strtPt[1] + Hline2.strtPt[1])/2;
	//~ 
	s[0] = Vline1.strtPt[1]; s[1] = Vline1.endPt[1];s[2] = Vline2.strtPt[1]; s[3] = Vline2.endPt[1]; 
	for(int i = 0; i < 4; i++){
		for(int j = i+1; j < 4; j++){
			if(s[j] < s[i]){
				temp = s[j];
				s[j] = s[i];
				s[i] = temp;
			}  
		}
	}
	double overlappingPart2 = calcOverlappingPart(s[0],s[3],Hline2.strtPt[1],Hline1.strtPt[1]);
	//~ printf("\n (%f,%f) (%f,%f) : overlappingPart2 : %f ", s[0],s[3],Hline2.strtPt[1],Hline1.strtPt[1],overlappingPart2);
	//~ rectCt2.y = (s[0]+s[3])/2;
	//~ rectCt2.x = (Vline1.strtPt[0] + Vline2.strtPt[0])/2;
//~ 
	//~ Vwd = fabs(Vline1.strtPt[0] - Vline2.strtPt[0]);
	//~ Vht = fabs(s[0]-s[3]);
//~ 
	//~ double dis = sqrt((rectCt1.x - rectCt2.x)*(rectCt1.x - rectCt2.x)+(rectCt1.y - rectCt2.y)*(rectCt1.y - rectCt2.y))/4000;
	//~ double ratioWd = ((Vwd < Hwd)? Vwd:Hwd)/((Vwd > Hwd)? Vwd:Hwd);
	//~ double ratioHt = ((Vht < Hht)? Vht:Hht)/((Vht > Hht)? Vht:Hht);
	//~ return (ratioHt*ratioWd*dis);
	//~ double overlappingPart = calcOverlappingPart(Hline1.strtPt[1],Hline1.endPt[1],line2.strtPt[1],line2.endPt[1]);
	return overlappingPart1*overlappingPart2;
	
}

void completeRectWindow(ProjectedLine &H1,ProjectedLine &H2,ProjectedLine &V1,ProjectedLine &V2){ // completing lines that comprise a window
	if(V1.strtPt[0] < V2.strtPt[0]){
		H1.strtPt[0] = V1.strtPt[0]; H1.endPt[0] = V2.strtPt[0];
		H2.strtPt[0] = V1.strtPt[0]; H2.endPt[0] = V2.strtPt[0];
	}
	else{
		H1.strtPt[0] = V2.strtPt[0]; H1.endPt[0] = V1.strtPt[0];
		H2.strtPt[0] = V2.strtPt[0]; H2.endPt[0] = V1.strtPt[0];
	}
	if(H1.strtPt[1] < H2.strtPt[1]){
		V1.strtPt[1] = H1.strtPt[1]; V1.endPt[1] = H2.strtPt[1];
		V2.strtPt[1] = H1.strtPt[1]; V2.endPt[1] = H2.strtPt[1];
	}
	else{
		V1.strtPt[1] = H2.strtPt[1]; V1.endPt[1] = H1.strtPt[1];
		V2.strtPt[1] = H2.strtPt[1]; V2.endPt[1] = H1.strtPt[1];
	}

}

void completeRectDoorVertical(ProjectedLine &V1,ProjectedLine &V2){ // completing vertical lines that comprise a door
	double max;
	max = (V1.endPt[1] > V2.endPt[1]) ? V1.endPt[1] : V2.endPt[1];
	V1.endPt[1] = V2.endPt[1] = max;
	V1.strtPt[1] = V2.strtPt[1] = 0;
}
void completeRectDoorHorizontal(ProjectedLine &H1,ProjectedLine &V1,ProjectedLine &V2){ // completing horizontal line associated with a door
	
	if(V1.strtPt[0] != V2.strtPt[0]){
		if(V1.strtPt[0] < V2.strtPt[0]){
			H1.strtPt[0] = V1.strtPt[0]; H1.endPt[0] = V2.strtPt[0];
		}
		else{
			H1.strtPt[0] = V2.strtPt[0]; H1.endPt[0] = V1.strtPt[0];
		}
	}
	else{
		if(fabs(H1.strtPt[0]-V1.strtPt[0]) < fabs(H1.endPt[0]-V1.strtPt[0])){
			H1.strtPt[0] = V1.strtPt[0];
		}
		else{
			H1.endPt[0] = V1.strtPt[0];
		}
	}
	H1.strtPt[1] = H1.endPt[1] = V1.endPt[1];
}

double rectOverlap(vector<ProjectedLine> rect1,vector<ProjectedLine> rect2){ // overlapping area between two rectangles
	ProjectedLine H1 = rect1[0],H2 = rect1[1];
	ProjectedLine H3 = rect2[0],H4 = rect2[1];
	double XA1,XA2,YA1,YA2,XB1,XB2,YB1,YB2;
	XA2 = std::min(H1.strtPt[0],H1.endPt[0]);
	XA1 = std::max(H1.strtPt[0],H1.endPt[0]);
	YA2 = std::min(H1.strtPt[1],H2.strtPt[1]);
	YA1 = std::max(H1.strtPt[1],H2.strtPt[1]);
          
	XB2 = std::min(H3.strtPt[0],H3.endPt[0]);
	XB1 = std::max(H3.strtPt[0],H3.endPt[0]);
	YB2 = std::min(H3.strtPt[1],H4.strtPt[1]);
	YB1 = std::max(H3.strtPt[1],H4.strtPt[1]);
	
	double SA = fabs(H1.strtPt[0]-H1.endPt[0])*fabs(H1.strtPt[1]-H2.strtPt[1]);
	double SB = fabs(H3.strtPt[0]-H3.endPt[0])*fabs(H3.strtPt[1]-H4.strtPt[1]);
	double SI = fabs(std::max(XA2, XB2) - std::min(XA1, XB1)) * fabs(std::max(YA2, YB2) - std::min(YA1, YB1));
	double SU = SA + SB - SI;
	
	return std::max(SI/SA,SI/SB);

}

void rectElimination(vector<vector<ProjectedLine> > &rect){ // eliminating rectangles that are completely inside of another rectangle
	for(int i = 0; i < rect.size(); i++){
		for(int j = 0; j < rect.size(); j++){
			if(i != j){
				ProjectedLine H1 = rect[i][0],H2 = rect[i][1],V1 = rect[i][2], V2 = rect[i][3];
				ProjectedLine H3 = rect[j][0],H4 = rect[j][1],V3 = rect[j][2], V4 = rect[j][3];
				vector<Point> P1(5);
				Vector<Point> P2(5);
				P1[0] = Point(H1.strtPt[0],H1.strtPt[1]);P1[1] = Point(H1.endPt[0],H1.endPt[1]);
				P1[2] = Point(H2.endPt[0],H2.endPt[1]); P1[3] = Point(H2.strtPt[0],H2.strtPt[1]);
				P1[4] = Point(H1.strtPt[0],H1.strtPt[1]);

				P2[0] = Point(H3.strtPt[0],H3.strtPt[1]);P2[1] = Point(H3.endPt[0],H3.endPt[1]);
				P2[2] = Point(H4.endPt[0],H4.endPt[1]); P2[3] = Point(H4.strtPt[0],H4.strtPt[1]);
				P2[4] = Point(H3.strtPt[0],H3.strtPt[1]);
				int count_inside = 0;
				double A,B,C,D;

				for(int m = 0; m < 4; m++){
					A = -(P1[m+1].y - P1[m].y);
					B = P1[m+1].x - P1[m].x;
					C = -(A * P1[m].x + B * P1[m].y);
					for(int p = 0; p < 4; p++){
						D = A*P2[p].x + B*P2[p].y + C;
						if(floor(D) <= 0 && (A !=0 || B != 0) ) count_inside++;
						//~ printf("\n count_inside : %d A : %f B : %f C : %f D : %f",count_inside, A,B,C,D);
					}
				}
				
				
				if(!(count_inside > 0 && count_inside < 16)){
					
					printf("\n block: \n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H1.strtPt[0],H1.strtPt[1],H1.endPt[0],H1.endPt[1],H2.endPt[0],H2.endPt[1],H2.strtPt[0],H2.strtPt[1] );
					printf("\n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H3.strtPt[0],H3.strtPt[1],H3.endPt[0],H3.endPt[1],H4.endPt[0],H4.endPt[1],H4.strtPt[0],H4.strtPt[1] );
					printf("\n (%d,%d),(%d,%d),(%d,%d),(%d,%d)",P1[0].x,P1[0].y,P1[1].x,P1[1].y,P1[2].x,P1[2].y,P1[3].x,P1[3].y);
					printf("\n (%d,%d),(%d,%d),(%d,%d),(%d,%d)",P2[0].x,P2[0].y,P2[1].x,P2[1].y,P2[2].x,P2[2].y,P2[3].x,P2[3].y);
					printf("\n count_inside : %d",count_inside);
					
				}

				if(count_inside == 16){
					rect.erase(rect.begin()+j);
					//~ i--;
					j--;
					if(i > j) i--;
					//~ break;
				}
			}
		}
	}
}

// identifying window and doors in features detected
vector<ProjectedLine> identifyRect(vector<ProjectedLine> projectedHSegments, vector<ProjectedLine> projectedVSegments,vector<ProjectedLine>& rectHSeg, vector<ProjectedLine>& rectVSeg){

	vector<ProjectedLine> rect;
	vector<vector<ProjectedLine> > rectWindow;
	vector<vector<ProjectedLine> > rectDoor;
	
	vector<vector<double> > Hcf(projectedHSegments.size()); // confidence factor 
	vector<vector<double> > Vcf(projectedVSegments.size()); // confidence factor
	vector<bool> DoneH(projectedHSegments.size(),false);
	vector<bool> DoneV(projectedVSegments.size(),false);
	
	
	int mode = 0 ;
	for(int i = 0; i < projectedHSegments.size(); i++){
		for(int j = 0; j < projectedHSegments.size(); j++){
			if(i == j) Hcf[i].push_back(-1);
			else{
				double cf = calcCf(projectedHSegments[i],projectedHSegments[j],mode);
				//~ if(cf < 0.6) cf = 0;
				Hcf[i].push_back(cf); // mode = 0 = horizontal ; mode = 1 = vertical
			}
		}
	}
	
	cout << " \n Confidence Factor Horizontal: \n";
	for(int i = 0; i < projectedHSegments.size(); i++){
		cout << endl;
		for(int j = 0; j < projectedHSegments.size(); j++)
			printf(" %f ", Hcf[i][j]);
	}

	mode = 1 ;
	for(int i = 0; i < projectedVSegments.size(); i++){
		for(int j = 0; j < projectedVSegments.size(); j++){
			if(i == j) Vcf[i].push_back(-1);
			else{
				double cf = calcCf(projectedVSegments[i],projectedVSegments[j],mode);
				//~ if(cf < 0.6) cf = 0;
				Vcf[i].push_back(cf); // mode = 0 = horizontal ; mode = 1 = vertical
			}
		}
	}
	
	cout << " \n Confidence Factor Vertical : \n";
	for(int i = 0; i < projectedVSegments.size(); i++){
		cout << endl;
		for(int j = 0; j < projectedVSegments.size(); j++)
			printf(" %f ", Vcf[i][j]);
	}
// detecting windows and doors
	for(int i = 0; i < Hcf.size(); i++){
		for(int j = i+1; j < Hcf.size(); j++){
			for(int k = 0; k < Vcf.size(); k++){
				for(int l = k+1; l < Vcf.size(); l++){
					if(Hcf[i][j] > 0.5 && Vcf[k][l] > 0.5){
						double cf = calcCfRect(projectedHSegments[i],projectedHSegments[j],projectedVSegments[k],projectedVSegments[l]);
						if(cf > 0.75){
							rect.clear();
							completeRectWindow(projectedHSegments[i],projectedHSegments[j],projectedVSegments[k],projectedVSegments[l]);
							
							if(!DoneH[i]){
								//~ rectHSeg.push_back(projectedHSegments[i]);
								DoneH[i] = true;
							}
							
							if(projectedHSegments[i].strtPt[1] > projectedHSegments[j].strtPt[1]){
								rect.push_back(projectedHSegments[i]);
								rect.push_back(projectedHSegments[j]);
							}
							else{
								rect.push_back(projectedHSegments[j]);
								rect.push_back(projectedHSegments[i]);
							}
							
							if(!DoneH[j]){
								//~ rectHSeg.push_back(projectedHSegments[j]);
								DoneH[j] = true;
							}
							rect.push_back(projectedVSegments[k]);
							if(!DoneV[k]){
								//~ rectVSeg.push_back(projectedVSegments[k]);
								DoneV[k] = true;
							}
							rect.push_back(projectedVSegments[l]);
							if(!DoneV[l]){
								//~ rectVSeg.push_back(projectedVSegments[l]);
								DoneV[l] = true;
							}
							
							rectWindow.push_back(rect);
						}
						//~ printf("\n(%d,%d) (%d,%d) : %f",i,j,k,l,cf);
						
					}
					
					if(projectedVSegments[k].len > 1500 && projectedVSegments[l].len > 1500 && Vcf[k][l] > 0.5 && fabs(projectedVSegments[l].strtPt[0] - projectedVSegments[k].strtPt[0]) > 500 && fabs(projectedVSegments[l].strtPt[0] - projectedVSegments[k].strtPt[0]) < 1200 ){
						completeRectDoorVertical(projectedVSegments[k],projectedVSegments[l]);
						ProjectedLine V1,V2;
						
						
						for(int m = 0; m < projectedHSegments.size(); m++){
							double S1,S2,S3,S4;
							S1 = projectedVSegments[l].strtPt[0];
							S2 = projectedVSegments[k].strtPt[0];
							S3 = projectedHSegments[m].strtPt[0];
							S4 = projectedHSegments[m].endPt[0];
							
							double overlappingPart2 = calcOverlappingPart(S1,S2,S3,S4);
							//~ printf("\n m : %d k : %d l : %d overlappingPart2 : %f",m,k,l,overlappingPart2);
							if(overlappingPart2 > 0.75){
								rect.clear();
								completeRectDoorHorizontal(projectedHSegments[m],projectedVSegments[k],projectedVSegments[l]);
								rect.push_back(projectedHSegments[m]);
								if(!DoneH[m]){
									//~ rectHSeg.push_back(projectedHSegments[m]);
									DoneH[m] = true;
								}
								ProjectedLine line = projectedHSegments[m];
								line.strtPt[1] = line.endPt[1] = 0;
								rect.push_back(line);
								rect.push_back(projectedVSegments[k]);
								if(!DoneV[k]){
									DoneV[k] = true;
									//~ rectVSeg.push_back(projectedVSegments[k]);
								}
								rect.push_back(projectedVSegments[l]);
								if(!DoneV[l]){
									DoneV[l] = true;
									//~ rectVSeg.push_back(projectedVSegments[l]);
								}
								rectDoor.push_back(rect);
							}
						}
						
					}
					
				}
			}
		}
	}


// detecting doors with only one vertical and horizontal line 
	for(int l = 0; l < Vcf.size(); l++){
		if(projectedVSegments[l].len > 1500 && !DoneV[l]){
			int crspHrz = 0;
			double minDis = 9999;
			int minDisLine = -1;
			double S1,S2;
			S1 = projectedVSegments[l].endPt[0];
			S2 = projectedVSegments[l].endPt[1];
			for(int m = 0; m < projectedHSegments.size(); m++){
				double dis1 = fabs(S1-projectedHSegments[m].strtPt[0])+fabs(S2-projectedHSegments[m].strtPt[1]);
				double dis2 = fabs(S1-projectedHSegments[m].endPt[0])+fabs(S2-projectedHSegments[m].endPt[1]);
				if((dis1 < dis2 ? dis1 : dis2) < minDis){
					minDis = (dis1 < dis2 ? dis1 : dis2);
					minDisLine = m;
				}
			}
			printf("\n mindis is %f .\n",minDis);
			if(minDis < 200){
				completeRectDoorVertical(projectedVSegments[l],projectedVSegments[l]);
				if(!DoneV[l]){
					DoneV[l] = true;
					cout << "\npushed Vertical line:";
					//~ rectVSeg.push_back(projectedVSegments[l]);
				}
				completeRectDoorHorizontal(projectedHSegments[minDisLine],projectedVSegments[l],projectedVSegments[l]);
				if(!DoneH[minDisLine]){
					cout << "\npushed Horizontal line:";
					//~ rectHSeg.push_back(projectedHSegments[minDisLine]);
					DoneH[minDisLine] = true;
				}
				rect.clear();
				rect.push_back(projectedHSegments[minDisLine]);
				ProjectedLine line = projectedHSegments[minDisLine];
				line.strtPt[1] = line.endPt[1] = 0;
				rect.push_back(line);
				rect.push_back(projectedVSegments[l]);
				line = projectedVSegments[l];
				if(fabs(projectedVSegments[l].strtPt[0] - projectedHSegments[minDisLine].strtPt[0]) > fabs(projectedVSegments[l].strtPt[0] - projectedHSegments[minDisLine].endPt[0]))
					line.strtPt[0] = line.endPt[0] = projectedHSegments[minDisLine].strtPt[0];
				else line.strtPt[0] = line.endPt[0] = projectedHSegments[minDisLine].endPt[0];
				rect.push_back(line);
				rectDoor.push_back(rect);
			}
		}
		
	}

// elimination of unrequired rectangles
	cout << "\n size of rectWindow : " << rectWindow.size() << endl;
	rectElimination(rectWindow);
	cout << "\n size of rectDoor : " << rectDoor.size() << endl;
	rectElimination(rectDoor);
	cout << "\n size of rectWindow : " << rectWindow.size() << endl;
	cout << "\n size of rectDoor : " << rectDoor.size() << endl;
	for(int i = 0; i < rectWindow.size(); i++){
		for(int j = 0; j < rectWindow.size(); j++){
			if(i != j){
				double overlapPortion = rectOverlap(rectWindow[i],rectWindow[j]);
				printf("\noverlapPortion : %f ",overlapPortion);
				if(overlapPortion > 0.7){
					ProjectedLine H1 = rectWindow[i][0],H2 = rectWindow[i][1];
					ProjectedLine H3 = rectWindow[j][0],H4 = rectWindow[j][1];
					double XA1,XA2,YA1,YA2;
					XA2 = std::min(std::min(H1.strtPt[0],H1.endPt[0]),std::min(H3.strtPt[0],H3.endPt[0]));
					XA1 = std::max(std::max(H1.strtPt[0],H1.endPt[0]),std::max(H3.strtPt[0],H3.endPt[0]));
					YA2 = std::min(std::min(H1.strtPt[1],H2.strtPt[1]),std::min(H3.strtPt[1],H4.strtPt[1]));
					YA1 = std::max(std::max(H1.strtPt[1],H2.strtPt[1]),std::max(H3.strtPt[1],H4.strtPt[1]));
					rectWindow[i][0].strtPt[0] = XA2; rectWindow[i][0].strtPt[1] = YA1;
					rectWindow[i][0].endPt[0]  = XA1; rectWindow[i][0].endPt[1]  = YA1;
					rectWindow[i][1].strtPt[0] = XA2; rectWindow[i][1].strtPt[1] = YA2;
					rectWindow[i][1].endPt[0]  = XA1; rectWindow[i][1].endPt[1]  = YA2;




					rectWindow[i][2].strtPt[0] = XA2; rectWindow[i][2].strtPt[1] = YA2;
					rectWindow[i][2].endPt[0]  = XA2; rectWindow[i][2].endPt[1]  = YA1;
					rectWindow[i][3].strtPt[0] = XA1; rectWindow[i][3].strtPt[1] = YA2;
					rectWindow[i][3].endPt[0]  = XA1; rectWindow[i][3].endPt[1]  = YA1;
					
					rectWindow.push_back(rectWindow[i]);
					rectWindow.erase(rectWindow.begin()+i);
					rectWindow.erase(rectWindow.begin()+j);
					i--;j--;
					break;
				}
			}
		}
	}

// pushing remaining rectangular features
	for(int i = 0; i < rectWindow.size(); i++){
		ProjectedLine H1 = rectWindow[i][0],H2 = rectWindow[i][1];
		//~ if(rectWindow[i][0].strtPt[1] != 0 && rectWindow[i][1].strtPt[1] != 0){
			printf("\n block: \n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H1.strtPt[0],H1.strtPt[1],H1.endPt[0],H1.endPt[1],H2.endPt[0],H2.endPt[1],H2.strtPt[0],H2.strtPt[1] );
			rectHSeg.push_back(rectWindow[i][0]);
			rectHSeg.push_back(rectWindow[i][1]);
			rectVSeg.push_back(rectWindow[i][2]);
			rectVSeg.push_back(rectWindow[i][3]);
		//~ }
		//~ else cout << "\n eliminated window \n";
	}
	for(int i = 0; i < rectDoor.size(); i++){
		ProjectedLine H1 = rectDoor[i][0],H2 = rectDoor[i][1];
		//~ if(rectDoor[i][0].strtPt[1] != 0 && rectDoor[i][1].strtPt[1] != 0){
			printf("\n block: \n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H1.strtPt[0],H1.strtPt[1],H1.endPt[0],H1.endPt[1],H2.endPt[0],H2.endPt[1],H2.strtPt[0],H2.strtPt[1] );
			rectHSeg.push_back(rectDoor[i][0]);
			rectHSeg.push_back(rectDoor[i][1]);
			rectVSeg.push_back(rectDoor[i][2]);
			rectVSeg.push_back(rectDoor[i][3]);
		//~ }
		//~ else cout << "\n eliminated door \n";
	}
	//~ rectElimination(rectDoor);

	return rect;
}




///////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////// Floyd-Warshall//////////////////////////////////
void printDist() {
    int i, j;
    printf("\n DistMat:\n");
    printf("    ");
    for (i = 0; i < n; ++i)
        printf("%4c", 'A' + i);
    printf("\n");
    for (i = 0; i < n; ++i) {
        printf("%4c", 'A' + i);
        for (j = 0; j < n; ++j)
            printf("%4d", dist[i][j]);
        printf("\n");
    }
    printf("\n");
}
void printPath() {
    int i, j;
    printf("\n PathMat:\n");
    printf("    ");
    for (i = 0; i < n; ++i)
        printf("%4c", 'A' + i);
    printf("\n");
    for (i = 0; i < n; ++i) {
        printf("%4c", 'A' + i);
        for (j = 0; j < n; ++j)
            printf("%4d", pathMat[i][j]);
        printf("\n");
    }
    printf("\n");
}
/*
    floyd_warshall()
 
    after calling this function dist[i][j] will the the minimum distance
    between i and j if it exists (i.e. if there's a path between i and j)
    or 0, otherwise
*/
void floyd_warshall() {
    int i, j, k;
    for (k = 0; k < n; ++k) {
        printDist();
        printPath();
        for (i = 0; i < n; ++i)
            for (j = 0; j < n; ++j)
                /* If i and j are different nodes and if 
                    the paths between i and k and between
                    k and j exist, do */
                if ((dist[i][k] * dist[k][j] != 0) && (i != j))
                    /* See if you can't get a shorter path
                        between i and j by interspacing
                        k somewhere along the current
                        path */
                    if ((dist[i][k] + dist[k][j] < dist[i][j]) ||
                        (dist[i][j] == 0)){
                        dist[i][j] = dist[i][k] + dist[k][j];
                        pathMat[i][j] = pathMat[i][j] = k+1;
					}
    }
    printDist();
    printPath();
}

void pathExtractor(int src, int dst,vector<int> &path){
	//~ printf("\n path : %d",src);
	path.push_back(src);
	int temp = dst;
	while(temp != pathMat[src-1][temp-1]){
		//~ printf(" --> %d", pathMat[src-1][temp-1]);
		temp = pathMat[src-1][temp-1];
		path.push_back(temp);
	}
	//~ printf(" --> %d", dst);
	path.push_back(dst);
}
////////////////////////////////////////////////////// transformation matrix manipulation
void matMul(double newRotMat[3][3],double rotMat1[3][3],double rotMat2[3][3]){
	for(int a = 0 ;a < 3; a++){
		for(int b = 0; b < 3; b++){
			newRotMat[a][b] = rotMat1[a][0]*rotMat2[0][b] + rotMat1[a][1]*rotMat2[1][b] + rotMat1[a][2]*rotMat2[2][b];
		}
	}
}

void matTranspose(double rotMat[3][3]){
	double temp[3][3];
	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 3; j++)
			temp[j][i] = rotMat[i][j];

	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 3; j++)
			rotMat[i][j] = temp[i][j];
}


void calcNewTranslation1(double newTranslation[3], double rotation1[3][3],double translation1[3],double translation2[3]){
	newTranslation[0] = translation1[0]+(rotation1[0][0]*translation2[0] + rotation1[0][1]*translation2[1] + rotation1[0][2]*translation2[2]);
	newTranslation[1] = translation1[1]+(rotation1[1][0]*translation2[0] + rotation1[1][1]*translation2[1] + rotation1[1][2]*translation2[2]);
	newTranslation[2] = translation1[2]+(rotation1[2][0]*translation2[0] + rotation1[2][1]*translation2[1] + rotation1[2][2]*translation2[2]); 
}

void calcNewTranslation2(double newTranslation[3], double rotation1[3][3],double translation1[3],double translation2[3]){
	newTranslation[0] = translation1[0]-(rotation1[0][0]*translation2[0] + rotation1[0][1]*translation2[1] + rotation1[0][2]*translation2[2]);
	newTranslation[1] = translation1[1]-(rotation1[1][0]*translation2[0] + rotation1[1][1]*translation2[1] + rotation1[1][2]*translation2[2]);
	newTranslation[2] = translation1[2]-(rotation1[2][0]*translation2[0] + rotation1[2][1]*translation2[1] + rotation1[2][2]*translation2[2]); 
}

void copyRotNTranslMat(double srcTranslation[3],double srcRotation[3][3],double dstTranslation[3],double dstRotation[3][3]){
	for(int i = 0 ; i < 3 ; i++){
		for(int j = 0; j < 3; j++)
			dstRotation[i][j] = srcRotation[i][j];
		dstTranslation[i] = srcTranslation[i];
	}
}

void calcNewTransMat(double newTransMat[4][4],double transMat1[4][4],double transMat2[4][4]){
	double rotation1[3][3],rotation2[3][3],rotation3[3][3];
	memset(rotation1,0,sizeof(rotation1[0][0])*3*3);
	memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
	memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
	double translation1[3] = {0},translation2[3]={0},translation3[3] = {0};
	transMatSplitter(transMat2,rotation2,translation2);
	transMatSplitter(transMat1,rotation1,translation1);
	matMul(rotation3,rotation1,rotation2);
	calcNewTranslation1(translation3,rotation1,translation1,translation2);
	getTransMat(newTransMat,rotation3,translation3);

}

void getIntermediateElements(){
	double rotation1[3][3],rotation2[3][3],rotation3[3][3];
	memset(rotation1,0,sizeof(rotation1[0][0])*3*3);
	memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
	memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
	double translation1[3] = {0},translation2[3]={0},translation3[3] = {0};
	
	for(int i = 0; i < initialElements.size(); i++){
		if(initialElements[i].id[0] == initialElements[i].id[1]){
			transMatSplitter(initialElements[i].transMat,rotation1,translation1);
			int x = initialElements[i].id[0];
			for(int j = i+1; j < initialElements.size(); j++){
				if(initialElements[j].id[0] == initialElements[j].id[1]){
					int y = initialElements[j].id[0];
					if(intermediateMarkerRelationMatrix[x][y] == -1){
						transMatSplitter(initialElements[j].transMat,rotation2,translation2);
						calcNewRotMat(rotation3,rotation1,rotation2);
						calcNewTranslation(translation3,rotation1,translation1,translation2);
						relativeTransMat r;
						r.id[0] = x; r.id[1] = y;
						getTransMat(r.transMat,rotation3,translation3);
						intermediateElements.push_back(r);
						intermediateMarkerRelationMatrix[x][y] = intermediateElements.size() -1;
						memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
						memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
						memset(translation2,0,sizeof(translation2[0]*3));
						memset(translation3,0,sizeof(translation3[0]*3));
					}
					if(intermediateMarkerRelationMatrix[y][x] == -1){
						transMatSplitter(initialElements[j].transMat,rotation2,translation2);
						calcNewRotMat(rotation3,rotation2,rotation1);
						calcNewTranslation(translation3,rotation2,translation2,translation1);
						relativeTransMat r;
						r.id[0] = y; r.id[1] = x;
						getTransMat(r.transMat,rotation3,translation3);
						intermediateElements.push_back(r);
						intermediateMarkerRelationMatrix[y][x] = intermediateElements.size() -1;
						memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
						memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
						memset(translation2,0,sizeof(translation2[0]*3));
						memset(translation3,0,sizeof(translation3[0]*3));
					}
				}
			}
			memset(rotation1,0,sizeof(rotation1[0][0])*3*3);
			memset(translation1,0,sizeof(translation1[0]*3));
		}
		//~ else perror("\n initialElements[i].id[0] and initialElements[i].id[1] not equal\n");
	}
}

void getFinalElements(){
	for(int i = 2;i < n+1; i++){ 
		vector<int> path;
		pathExtractor(1,i,path);
		printf("\n path : %d",path[0]);
		for(int j = 1; j < path.size(); j++)
			printf(" --> %d", path[j]);
		
		int count = path.size();
		double rotation1[3][3],rotation2[3][3],rotation3[3][3];
		memset(rotation1,0,sizeof(rotation1[0][0])*3*3);
		memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
		memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
		double translation1[3] = {0},translation2[3]={0},translation3[3] = {0};
		
		//~ relativeTransMat r2 =  intermediateElements[(intermediateMarkerRelationMatrix[path[count-2]-1][path[count-1]-1])];
		//~ transMatSplitter(r2.transMat,rotation2,translation2); 
		//~ for(int j = count-2; j > 0; j--){
			//~ relativeTransMat r1 =  intermediateElements[(intermediateMarkerRelationMatrix[path[j-1]-1][path[j]-1])];
			//~ transMatSplitter(r1.transMat,rotation1,translation1);
			//~ calcNewRotMatFinal(rotation3,rotation1,rotation2);
			//~ calcNewTranslationFinal(translation3,rotation1,translation1,translation2);
			//~ copyRotNTranslMat(translation3,rotation3,translation2,rotation2);
		//~ }
		//~ r2.id[0] = path[0]-1; r2.id[1] = path[count-1]-1;
		//~ getTransMat(r2.transMat,rotation2,translation2);
		
		
		//~ relativeTransMat r2 =  intermediateElements[(intermediateMarkerRelationMatrix[path[1]-1][path[0]-1])];
		//~ transMatSplitter(r2.transMat,rotation2,translation2);
		//~ for(int j = 2; j < path.size(); j++){
			//~ relativeTransMat r1 =  intermediateElements[(intermediateMarkerRelationMatrix[path[j]-1][path[j-1]-1])];
			//~ transMatSplitter(r1.transMat,rotation1,translation1);
			//~ calcNewRotMatFinal(rotation3,rotation1,rotation2);
			//~ calcNewTranslationFinal(translation3,rotation1,translation1,translation2);
			//~ copyRotNTranslMat(translation3,rotation3,translation2,rotation2);
		//~ }
		//~ r2.id[0] = path[count-1]-1; r2.id[1] = path[0]-1; 
		
		if(count > 2){
			relativeTransMat r2 =  intermediateElements[(intermediateMarkerRelationMatrix[path[2]-1][path[1]-1])];
			transMatSplitter(r2.transMat,rotation2,translation2);
			if(count > 3){
				for(int j = 3; j < count; j++){
					relativeTransMat r1 =  intermediateElements[(intermediateMarkerRelationMatrix[path[j]-1][path[j-1]-1])];
					transMatSplitter(r1.transMat,rotation1,translation1);
					matMul(rotation3,rotation1,rotation2);
					calcNewTranslation1(translation3,rotation1,translation1,translation2);
					copyRotNTranslMat(translation3,rotation3,translation2,rotation2);
				}
			}
			relativeTransMat r1 =  intermediateElements[(intermediateMarkerRelationMatrix[path[0]-1][path[1]-1])];
			transMatSplitter(r1.transMat,rotation1,translation1);

			matTranspose(rotation2);
			matMul(rotation3,rotation1,rotation2);
			calcNewTranslation2(translation3,rotation3,translation1,translation2);
			getTransMat(r2.transMat,rotation3,translation3);
			r2.id[0] = path[0]-1; r2.id[1] = path[count-1]-1;
			r2.transMat[2][3] = 0;
			finalElements.push_back(r2);
			
		}
		else{
			relativeTransMat r1 =  intermediateElements[(intermediateMarkerRelationMatrix[path[0]-1][path[1]-1])];
			r1.transMat[2][3] = 0;
			finalElements.push_back(r1);
		}
	}

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int main(int argc , char **argv){
	DIR *d;
	string l;
	char originalImagePath[] = "/home/pika/Infurnia/libsForServer_customMarker/testImage/";
	
	initialElements.clear();
	intermediateElements.clear();
	finalElements.clear();
	int alpha = 0;
	memset(intermediateMarkerRelationMatrix,-1,sizeof(intermediateMarkerRelationMatrix[0][0])*12*12);
	//~ memset(finalMarkerRelationArray,-1,sizeof(finalMarkerRelationArray[0])*12);

	if(argc == 1){
		struct dirent *dir;
		d = opendir(originalImagePath);
		if(d){
			while((dir = readdir(d)) != NULL){
				char *imgName = dir->d_name;
				printf("\noriginalfile : %s \n",imgName);
				if(imgName[0] != '.'){
					char path[200]="";
					strcat(path,originalImagePath);
					strcat(path,imgName);
					//~ printf("\noriginalfile : %s %s\n",imgName,dir->d_type);
					Mat imRgb = imread(path,CV_LOAD_IMAGE_COLOR);
					if(!imRgb.data){
						printf("\n could not read original image\n");
						return -1;
					}
					double ZVec[3] = {0};
					ifstream myfile ("/home/pika/Infurnia/libsForServer_customMarker/gravityVectors");
					if (myfile.is_open())
					{
						while ( getline (myfile,l) )
						{
							size_t found = l.find(imgName);
							if(found != string::npos) {
								cout << l << '\n';
								size_t strt = l.find('{');
								size_t end = l.find('}');
								
								//~ cout << " \n strt : " << strt << " end : " << end << endl;
								
								char temp[50] = ""; 
								for(int i = strt+1,j = 0,n = 0; i <= end; i++,j++){
									
									if(l[i] != ',' && l[i] != '}') temp[j] = l[i];
									else{
										temp[j] = '\0';
										//~ cout << temp<< " " << atof(temp) << endl;
										j = -1;
										ZVec[n] = atof(temp);
										n++;
										temp[0] = '\0';
									}
								}
							}
						}
						myfile.close();
					}

					else cout << "Unable to open file";
					
					cout << "\n ZVec : " << ZVec[0] << "," << ZVec[1] << "," << ZVec[2] << endl;
					//~ double ZVec[3] = {0.03470388798398862,-0.9706881741157941,-0.2378236926602152};
					//~ double t = sqrt(ZVec[0]*ZVec[0]+ZVec[1]*ZVec[1]+ZVec[2]*ZVec[2]);
					//~ ZVec[0] /= t;
					//~ ZVec[1] /= t;
					//~ ZVec[2] /= t;
					double **TransMat = detectMarker(imgName, imRgb, ZVec,"/home/pika/Infurnia/libsForServer/candidateImage1/");
					
					
					//~ printf("\n Final transformation matrix : \n");
					//~ for(int i = 0; i < 4; i++){
						//~ 
							//~ printf("\n %+4.5f %+4.5f %+4.5f %+8.5f \n", transMat[i][0], transMat[i][1], transMat[i][2], transMat[i][3]);
						//~ 
					//~ }
					//~ int dist[5][5];
					
					//~ double transMat[4][4];
					
					if(true){
						for(int beta = 0; beta< imageWiseElements[alpha].size(); beta++){
							//~ for(int i = 0; i < 4; i++)
								//~ for(int j = 0; j < 4; j++)
									//~ transMat[i][j] = imageWiseElements[alpha][beta].transMat[i][j];
							double temp = 0;
							for(int i = 0;i<4;i++)
								for(int j = 0;j<4;j++)
									temp += (imageWiseElements[alpha][beta].transMat[i][j]);

							if(round(temp) != 0){
								double baselineParameter[2] = {0,imageWiseElements[alpha][beta].id[0]};
								Mat imRgbResized;
								Mat imGrResized;

								resize(imRgb,imRgbResized,Size(imRgb.cols/2,imRgb.rows/2));

								cvtColor(imRgbResized,imGrResized,CV_BGR2GRAY);
								//~ imshow("RGB",imRgbResized);
								//~ imshow("GRAY",imGrResized);
								detectlineonWall(imGrResized,imRgbResized, imageWiseElements[alpha][beta].transMat,baselineParameter,2767,imgName);
							}
						}
						alpha++;
					}
					//~ printf("\n\n intermediateMarkerRelationMatrix :\n");
					//~ for(int i = 0; i < 12; i++){
						//~ for(int j = 0 ; j < 12; j++){
							//~ printf("%3d",intermediateMarkerRelationMatrix[i][j]);
						//~ }
						//~ printf("\n");
					//~ } 
				}
			}
		}
	}
	
		/////////////////////////////////////// Floyd Warshall ////////////////////////////////////////////
	
	printf("\n\n imageWiseElements :\n");
	for(int j = 0; j < imageWiseElements.size(); j++){
		printf("\n Image %d ", j+1);
		//~ for(int i = 0; i < imageWiseElements[j].size(); i++){
			//~ printf("\n\n TransMat %d wrt %d\n",imageWiseElements[j][i].id[0],imageWiseElements[j][i].id[1]);
			//~ for(int count = 0 ; count < 4; count++){
				//~ printf("%+4.5f %+4.5f %+4.5f %+8.5f\n", imageWiseElements[j][i].transMat[count][0], imageWiseElements[j][i].transMat[count][1], imageWiseElements[j][i].transMat[count][2], imageWiseElements[j][i].transMat[count][3]);
			//~ }
		//~ }
		initialElements = imageWiseElements[j];
		
		for(int i = 0; i < initialElements.size(); i++){
			printf("\n\n TransMat %d wrt %d\n",initialElements[i].id[0],initialElements[i].id[1]);
			for(int count = 0 ; count < 4; count++){
				printf("%+4.5f %+4.5f %+4.5f %+8.5f\n", initialElements[i].transMat[count][0], initialElements[i].transMat[count][1], initialElements[i].transMat[count][2], initialElements[i].transMat[count][3]);
			}
		}

		getIntermediateElements();

		printf("\n\n intermediateElements :\n");
		for(int i = 0; i < intermediateElements.size(); i++){
			printf("\n\n TransMat %d wrt %d\n",intermediateElements[i].id[0],intermediateElements[i].id[1]);
			for(int count = 0 ; count < 4; count++){
				printf("%+4.5f %+4.5f %+4.5f %+8.5f\n", intermediateElements[i].transMat[count][0], intermediateElements[i].transMat[count][1], intermediateElements[i].transMat[count][2], intermediateElements[i].transMat[count][3]);
			}
		}
	}

	n = markerIdx_id.size();
	
	dist.resize(n);
	
	for(int i = 0; i < n; i++){
		dist[i].resize(n,0);
	}
	
	pathMat.resize(n);
	for(int i = 0; i < n; i++){
		pathMat[i].resize(n,0);
	}

	printf("\n\n intermediateMarkerRelationMatrix :\n");
	for(int i = 0; i < 12; i++){
		for(int j = 0 ; j < 12; j++){
			printf("%3d",intermediateMarkerRelationMatrix[i][j]);
			if(i < n && j < n){
				if(intermediateMarkerRelationMatrix[i][j] != -1 ) dist[i][j] = 1;
				else dist[i][j] = 0;
			}
		}
		printf("\n");
	} 
	
	for(int i = 0; i < n; i++){
		for(int j = 0 ; j < n ; j++){
			if(i!=j) pathMat[i][j] = j+1;
			else pathMat[i][j] = 0;
		}
	}

	floyd_warshall();

	getFinalElements();
	
	printf("\n\n finalElements :\n");
	for(int i = 0; i < finalElements.size(); i++){
		printf("\n\n TransMat %d wrt %d\n",finalElements[i].id[0],finalElements[i].id[1]);
		for(int count = 0 ; count < 4; count++){
			printf("%+4.5f %+4.5f %+4.5f %+8.5f\n", finalElements[i].transMat[count][0], finalElements[i].transMat[count][1], finalElements[i].transMat[count][2], finalElements[i].transMat[count][3]);
		}
	}

	Mat out = Mat::ones(1000,1000,CV_8UC1)*255;
	for(int i = 0; i < baseVec.size(); i++){
		baseVec[i][3] = 0;
		printf("\n Marker : %d compare : %d End points : (%d,%d,0),(%d,%d,0) Vector : (%d,0,0) ", baseVec[i][0],finalElements[baseVec[i][0]].id[1],baseVec[i][1],baseVec[i][3],baseVec[i][2],baseVec[i][3],baseVec[i][2]-baseVec[i][1]);
		double vec[3];
		double pt1[3],pt2[3];
		
		if(finalElements[baseVec[i][0]].id[1] == baseVec[i][0] && baseVec[i][0] != 0){
			printf("\n\nEntered 1.");
			pt1[0] = finalElements[baseVec[i][0]].transMat[0][3]+finalElements[baseVec[i][0]].transMat[0][0] * (baseVec[i][1])+finalElements[baseVec[i][0]].transMat[0][1] * (baseVec[i][3]);
			pt1[1] = finalElements[baseVec[i][0]].transMat[1][3]+finalElements[baseVec[i][0]].transMat[1][0] * (baseVec[i][1])+finalElements[baseVec[i][0]].transMat[1][1] * (baseVec[i][3]);
			pt1[2] = finalElements[baseVec[i][0]].transMat[2][3]+finalElements[baseVec[i][0]].transMat[2][0] * (baseVec[i][1])+finalElements[baseVec[i][0]].transMat[2][1] * (baseVec[i][3]);

			pt2[0] = finalElements[baseVec[i][0]].transMat[0][3]+finalElements[baseVec[i][0]].transMat[0][0] * (baseVec[i][2])+finalElements[baseVec[i][0]].transMat[0][1] * (baseVec[i][3]);
			pt2[1] = finalElements[baseVec[i][0]].transMat[1][3]+finalElements[baseVec[i][0]].transMat[1][0] * (baseVec[i][2])+finalElements[baseVec[i][0]].transMat[1][1] * (baseVec[i][3]);
			pt2[2] = finalElements[baseVec[i][0]].transMat[2][3]+finalElements[baseVec[i][0]].transMat[2][0] * (baseVec[i][2])+finalElements[baseVec[i][0]].transMat[2][1] * (baseVec[i][3]);
			vec[0] = (pt2[0]-pt1[0]); vec[1]= (pt2[1]-pt1[1]); vec[2]=(pt2[2]-pt1[2]);
			line(out,Point(round(pt2[1])/10+500,round(pt2[0])/10+500),Point(round(pt1[1])/10+500,round(pt1[0])/10+500),Scalar(0,0,255),5,8);
		}
		else {
			printf("\n\nEntered 2.");
			pt1[0] = baseVec[i][1]; pt1[1] = baseVec[i][3];pt1[2] = 0;
			pt2[0] = baseVec[i][2]; pt2[1] = baseVec[i][3];pt2[2] = 0;
			vec[0] = (pt2[0]-pt1[0]); vec[1]=vec[2]=0;
			line(out,Point(round(pt2[1])/10+500,round(pt2[0])/10+500),Point(round(pt1[1])/10+500,round(pt1[0])/10+500),Scalar(0,0,255),5,8);
			
		}
		printf("\n New End Points (%f,%f,%f),(%f,%f,%f) vector (%f,%f,%f)",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2],vec[0],vec[1],vec[2]);
	}
	
	imwrite("floorPlan.png",out);
	out.release();
	////////////////////////////////////////////////////////////// FloorPlan Extraction Complete ////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////// Feature Extraction /////////////////////////////////////////////////////
	featureVec.resize(n);
	sprintf(originalImagePath,"/home/pika/Infurnia/libsForServer_customMarker/featureImage/");
	
	if(argc == 1){
		struct dirent *dir;
		d = opendir(originalImagePath);
		if(d){
			while((dir = readdir(d)) != NULL){
				char*imgName = dir->d_name;
				printf("\n FeatureFile : %s \n",imgName);
				if(imgName[0] != '.'){
					char path[200]="";
					strcat(path,originalImagePath);
					strcat(path,imgName);
					
					Mat imRgb = imread(path,CV_LOAD_IMAGE_COLOR);
					if(!imRgb.data){
						printf("\n could not read original Image\n");
						return -1;
					}
					double ZVec[3] = {0};
					ifstream myfile ("/home/pika/Infurnia/libsForServer_customMarker/gravityVectors");
					if (myfile.is_open())
					{
						while ( getline (myfile,l) )
						{
							size_t found = l.find(imgName);
							if(found != string::npos) {
								cout << l << '\n';
								size_t strt = l.find('{');
								size_t end = l.find('}');
								
								//~ cout << " \n strt : " << strt << " end : " << end << endl;
								
								char temp[50] = ""; 
								for(int i = strt+1,j = 0,n = 0; i <= end; i++,j++){
									
									if(l[i] != ',' && l[i] != '}') temp[j] = l[i];
									else{
										temp[j] = '\0';
										//~ cout << temp<< " " << atof(temp) << endl;
										j = -1;
										ZVec[n] = atof(temp);
										n++;
										temp[0] = '\0';
									}
								}
							}
						}
						myfile.close();
					}

					else cout << "Unable to open file";
					

					cout<< "\n ZVec : " << ZVec[0] << "," << ZVec[1] << "," << ZVec[2] << endl;
					double **TransMat = detectMarker(imgName, imRgb, ZVec,"/home/pika/Infurnia/libsForServer/candidateImage1/");
					
					if(true){
						for(int beta = 0; beta< imageWiseElements[alpha].size(); beta++){
							//~ for(int i = 0; i < 4; i++)
								//~ for(int j = 0; j < 4; j++)
									//~ transMat[i][j] = imageWiseElements[alpha][beta].transMat[i][j];
							double temp = 0;
							for(int i = 0;i<4;i++)
								for(int j = 0;j<4;j++)
									temp += (imageWiseElements[alpha][beta].transMat[i][j]);

							if(round(temp) != 0){
								double baselineParameter[2] = {1,imageWiseElements[alpha][beta].id[0]};
								Mat imRgbResized;
								Mat imGrResized;

								resize(imRgb,imRgbResized,Size(imRgb.cols/2,imRgb.rows/2));

								cvtColor(imRgbResized,imGrResized,CV_BGR2GRAY);
								//~ imshow("RGB",imRgbResized);
								//~ imshow("GRAY",imGrResized);
								detectlineonWall(imGrResized,imRgbResized, imageWiseElements[alpha][beta].transMat,baselineParameter,2767,imgName);
							}
						}
						alpha++;
					}

				}
			}
		}
		
	}
	featureVec1.resize(featureVec.size());
	rectVec.resize(featureVec.size());
	vector<vector<double> > baselines;
	baselines.resize(featureVec.size());

	cout << "\n\n FEATURES MARKERWISE \n\n";
	for(int i = 0; i < featureVec.size();i++){
		cout<<"\n Marker ID : " << i;
		//~ featureVec1.resize(featureVec.size());
		vector<ProjectedLine> projectedHSegments;
		vector<ProjectedLine> projectedVSegments;
		double S1,S2,E1,E2;
		ProjectedLine p;
		for(int j = 0; j < featureVec[i].size();j++){
			if(abs(featureVec[i][j][0]-featureVec[i][j][2]) > abs(featureVec[i][j][1]-featureVec[i][j][3])){
				S1 = featureVec[i][j][1]; S2 = featureVec[i][j][0];
				E1 = featureVec[i][j][3]; E2 = featureVec[i][j][2];
				p.strtPt[0] = S2; p.strtPt[1] = S1;
				p.endPt[0] = E2; p.endPt[1] = E1;
				p.slop = 0;
				p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
				projectedHSegments.push_back(p);
				//~ printf("\n\nProjection End points : (%d,%d),(%d,%d)",(int)round(S2),(int)round(S1),(int)round(E2),(int)round(E1));
				
			}
			if(abs(featureVec[i][j][0]-featureVec[i][j][2]) < abs(featureVec[i][j][1]-featureVec[i][j][3])){
				S1 = featureVec[i][j][1]; S2 = featureVec[i][j][0];
				E1 = featureVec[i][j][3]; E2 = featureVec[i][j][2];
				p.strtPt[0] = S2; p.strtPt[1] = S1;
				p.endPt[0] = E2; p.endPt[1] = E1;
				p.slop = 0;
				p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
				projectedVSegments.push_back(p);
			}
		}
		
		vector<ProjectedLine> mergedProjectedHSegments = mergeProjectedLines(projectedHSegments,0,1,true);
		vector<ProjectedLine> mergedProjectedVSegments = mergeProjectedLines(projectedVSegments,0,2,true);
		
		vector<ProjectedLine> newProjectedHSegments;
		vector<ProjectedLine> newProjectedVSegments;
		
		vector<int> baseLineInfo;
		for(int A = 0 ; A < mergedProjectedHSegments.size(); A++){
			baseLineInfo.clear();
			if(mergedProjectedHSegments[A].len > 200){
				S1 = mergedProjectedHSegments[A].strtPt[1]; S2 = mergedProjectedHSegments[A].strtPt[0];
				E1 = mergedProjectedHSegments[A].endPt[1];  E2 = mergedProjectedHSegments[A].endPt[0];
				if(S2 < E2){
					mergedProjectedHSegments[A].strtPt[0] = S2;
					mergedProjectedHSegments[A].endPt[0] = E2;
				}
				else{
					mergedProjectedHSegments[A].strtPt[0] = E2;
					mergedProjectedHSegments[A].endPt[0] = S2;
				}
				mergedProjectedHSegments[A].strtPt[1] = (S1+E1)/2;
				mergedProjectedHSegments[A].endPt[1] = (S1+E1)/2;

				if(S2 < E2){
					baseLineInfo.push_back((int)round(S2));
					baseLineInfo.push_back((int)round(S1));
					baseLineInfo.push_back((int)round(E2));
					baseLineInfo.push_back((int)round(S1));
				}
				else{
					baseLineInfo.push_back((int)round(E2));
					baseLineInfo.push_back((int)round(S1));
					baseLineInfo.push_back((int)round(S2));
					baseLineInfo.push_back((int)round(S1));
				}
				featureVec1[i].push_back(baseLineInfo);
				if(S1 > 150) newProjectedHSegments.push_back(mergedProjectedHSegments[A]);
				//~ if(S1 < 80) baselines[i] = mergedProjectedHSegments[A];
			}
			//~ printf("\n\nProjection End points : (%d,%d),(%d,%d)",(int)round(S2),(int)round(S1),(int)round(E2),(int)round(E1));
		}

		for(int A = 0 ; A < mergedProjectedVSegments.size(); A++){
			baseLineInfo.clear();
			if(mergedProjectedVSegments[A].len > 200){
				S1 = mergedProjectedVSegments[A].strtPt[1]; S2 = mergedProjectedVSegments[A].strtPt[0];
				E1 = mergedProjectedVSegments[A].endPt[1];  E2 = mergedProjectedVSegments[A].endPt[0];
				mergedProjectedVSegments[A].strtPt[0] = (S2+E2)/2;
				mergedProjectedVSegments[A].endPt[0] = (S2+E2)/2;

				if(S1 < E1){
					mergedProjectedVSegments[A].strtPt[1] = S1;
					mergedProjectedVSegments[A].endPt[1] = E1;
				}
				else{
					mergedProjectedVSegments[A].strtPt[1] = E1;
					mergedProjectedVSegments[A].endPt[1] = S1;
				}

				if(S1 < E1){
					baseLineInfo.push_back((int)round(S2));
					baseLineInfo.push_back((int)round(S1));
					baseLineInfo.push_back((int)round(E2));
					baseLineInfo.push_back((int)round(E1));
				}
				else{
					baseLineInfo.push_back((int)round(E2));
					baseLineInfo.push_back((int)round(E1));
					baseLineInfo.push_back((int)round(S2));
					baseLineInfo.push_back((int)round(S1));

				}

				featureVec1[i].push_back(baseLineInfo);
				newProjectedVSegments.push_back(mergedProjectedVSegments[A]);
			}
		}
		
		vector<ProjectedLine> rectHSeg,rectVSeg;
		identifyRect(newProjectedHSegments,newProjectedVSegments,rectHSeg,rectVSeg);

		
		for(int A = 0 ; A < rectHSeg.size(); A++){
			baseLineInfo.clear();
			S1 = rectHSeg[A].strtPt[1]; S2 = rectHSeg[A].strtPt[0];
			E1 = rectHSeg[A].endPt[1];  E2 = rectHSeg[A].endPt[0];
			baseLineInfo.push_back((int)round(S2));
			baseLineInfo.push_back((int)round(S1));
			baseLineInfo.push_back((int)round(E2));
			baseLineInfo.push_back((int)round(S1));
			rectVec[i].push_back(baseLineInfo);
		}

		for(int A = 0 ; A < rectVSeg.size(); A++){
			baseLineInfo.clear();
			S1 = rectVSeg[A].strtPt[1]; S2 = rectVSeg[A].strtPt[0];
			E1 = rectVSeg[A].endPt[1];  E2 = rectVSeg[A].endPt[0];
			baseLineInfo.push_back((int)round(S2));
			baseLineInfo.push_back((int)round(S1));
			baseLineInfo.push_back((int)round(E2));
			baseLineInfo.push_back((int)round(E1));
			rectVec[i].push_back(baseLineInfo);
		}
		//~ for(int A = 0 ; A < mergedProjectedHSegments.size(); A++){
			//~ baseLineInfo.clear();
			//~ S1 = mergedProjectedHSegments[A].strtPt[1]; S2 = mergedProjectedHSegments[A].strtPt[0];
			//~ E1 = mergedProjectedHSegments[A].endPt[1];  E2 = mergedProjectedHSegments[A].endPt[0];
			//~ if(S1 < 150){
				//~ baseLineInfo.push_back((int)round(S2));
				//~ baseLineInfo.push_back((int)round(S1));
				//~ baseLineInfo.push_back((int)round(E2));
				//~ baseLineInfo.push_back((int)round(S1));
				//~ featureVec1[i].push_back(baseLineInfo);
				//~ rectVec[i].push_back(baseLineInfo);
			//~ }
		//~ }

		cout << "\n allfeature : " << endl;
		for(int j = 0; j < featureVec1[i].size();j++){
			//~ printf("\n (%d,0,%d) (%d,0,%d)",featureVec[i][j][0],(featureVec[i][j][1]),featureVec[i][j][2],(featureVec[i][j][3]));
			double vec[3];
			double pt1[3],pt2[3];
			
			if(finalElements[i].id[1] == i && i != 0){
				pt1[0] = finalElements[i].transMat[0][3] +finalElements[i].transMat[0][0]*(featureVec1[i][j][0]) +finalElements[i].transMat[0][2]*(featureVec1[i][j][1]);
				pt1[1] = finalElements[i].transMat[1][3] +finalElements[i].transMat[1][0]*(featureVec1[i][j][0]) +finalElements[i].transMat[1][2]*(featureVec1[i][j][1]);
				pt1[2] = finalElements[i].transMat[2][3] +finalElements[i].transMat[2][0]*(featureVec1[i][j][0]) +finalElements[i].transMat[2][2]*(featureVec1[i][j][1]);

				pt2[0] = finalElements[i].transMat[0][3] +finalElements[i].transMat[0][0]*(featureVec1[i][j][2]) +finalElements[i].transMat[0][2]*(featureVec1[i][j][3]);
				pt2[1] = finalElements[i].transMat[1][3] +finalElements[i].transMat[1][0]*(featureVec1[i][j][2]) +finalElements[i].transMat[1][2]*(featureVec1[i][j][3]);
				pt2[2] = finalElements[i].transMat[2][3] +finalElements[i].transMat[2][0]*(featureVec1[i][j][2]) +finalElements[i].transMat[2][2]*(featureVec1[i][j][3]);
				vec[0] = (pt2[0]-pt1[0]); vec[1]= (pt2[1]-pt1[1]); vec[2]=(pt2[2]-pt1[2]);
				//~ line(out,Point(round(pt2[1])/10+500,round(pt2[0])/10+500),Point(round(pt1[1])/10+500,round(pt1[0])/10+500),Scalar(0,0,255),5,8);
			}
			else {
				pt1[0] = featureVec1[i][j][0]; pt1[2] = featureVec1[i][j][1];pt1[1] = 0;
				pt2[0] = featureVec1[i][j][2]; pt2[2] = featureVec1[i][j][3];pt2[1] = 0;
				vec[0] = (pt2[0]-pt1[0]); vec[1]=vec[2]=0;
				//~ line(out,Point(round(pt2[1])/10+500,round(pt2[0])/10+500),Point(round(pt1[1])/10+500,round(pt1[0])/10+500),Scalar(0,0,255),5,8);
				
			}
			printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
			roomHeight = roomHeight > pt1[2] ? roomHeight : pt1[2]; 
			roomHeight = roomHeight > pt2[2] ? roomHeight : pt2[2]; 
			if(pt1[2] < 105 && pt2[2] < 105){ // threshold should be greater than merging threshold
				baselines[i].push_back(pt1[0]); baselines[i].push_back(pt1[1]);
				baselines[i].push_back(pt2[0]); baselines[i].push_back(pt2[1]);
			}
		}
	}
	
	cout << "\n\n Extreme Points :  \n\n";
	for(int i = 0; i < extremePoints.size();i++)
		printf("\n MarkerId : %d points %d %d",extremePoints[i][0],extremePoints[i][1],extremePoints[i][2]);
		
	cout << "\n\n Base lines : \n\n";
	for(int i = 0; i < baselines.size();i++){
		if(i != 3 && baselines[i].size() !=0){
			printf("\n");
			printf(" marker %d : (%f,%f) (%f,%f)",i,baselines[i][0],baselines[i][1],baselines[i][2],baselines[i][3]);
		}
	}
	cout << "\n\nintersection points : \n";
	vector<vector<double> > intersectionPoints;
	for(int i = 0 ; i < intermediateElements.size(); i = i + 2){
		int id1,id2;
		id1 = intermediateElements[i].id[0]; id2 = intermediateElements[i].id[1];
		if(id1 != 3 && id2 != 3 && baselines[id1].size() != 0 && baselines[id2].size() != 0){
			vector<double> vec = intersection(baselines[id1],baselines[id2]);
			printf(" line1 : %d line2 : %d Point(%f,%f)\n",id1,id2,vec[0],vec[1]);
			if(fabs(baselines[id1][0]-vec[0])+fabs(baselines[id1][1]-vec[1]) > fabs(baselines[id1][2]-vec[0])+fabs(baselines[id1][3]-vec[1])){
				baselines[id1][2] = vec[0]; baselines[id1][3] = vec[1];
			}
			else{
				baselines[id1][0] = vec[0]; baselines[id1][1] = vec[1];
			}
			if(fabs(baselines[id2][0]-vec[0])+fabs(baselines[id2][1]-vec[1]) > fabs(baselines[id2][2]-vec[0])+fabs(baselines[id2][3]-vec[1])){
				baselines[id2][2] = vec[0]; baselines[id2][3] = vec[1];
			}
			else{
				baselines[id2][0] = vec[0]; baselines[id2][1] = vec[1];
			}
			intersectionPoints.push_back(vec);
		}
	}
	ofstream myfile ("/home/pika/Infurnia/libsForServer_customMarker/featureVectors_bglr_frnd.txt");
	if (myfile.is_open())
	{
		char str[200];
		for(int i = 0; i < intersectionPoints.size(); i++){
			//~ vector<double> vec = intersectionPoints[i];
			printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",intersectionPoints[i][0],intersectionPoints[i][1],0.0,intersectionPoints[i][0],intersectionPoints[i][1],roomHeight);
			sprintf(str,"(%f,%f,%f)\n(%f,%f,%f)\n",intersectionPoints[i][0],intersectionPoints[i][1],0.0,intersectionPoints[i][0],intersectionPoints[i][1],roomHeight);
			myfile << str;

		}
		cout << "\n new baselines :\n";
		for(int i = 0; i < baselines.size();i++){
			if(i != 3){
				printf("\n");
				//~ printf(" marker %d : (%f,%f) (%f,%f)",i,baselines[i][0],baselines[i][1],baselines[i][2],baselines[i][3]);
				printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",baselines[i][0],baselines[i][1],0.0,baselines[i][2],baselines[i][3],0.0);
				printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",baselines[i][0],baselines[i][1],roomHeight,baselines[i][2],baselines[i][3],roomHeight);
				sprintf(str,"(%f,%f,%f)\n(%f,%f,%f)\n",baselines[i][0],baselines[i][1],0.0,baselines[i][2],baselines[i][3],0.0);
				myfile << str;
				sprintf(str,"(%f,%f,%f)\n(%f,%f,%f)\n",baselines[i][0],baselines[i][1],roomHeight,baselines[i][2],baselines[i][3],roomHeight);
				myfile << str;
			}
		}



		cout << "\n rectfeature : " << endl;
		for(int i = 0; i < rectVec.size(); i++){
			
			if(i != 3){
				cout << "\n MarkerId : " << i << endl;
				double min,max;
				bool baselineOrientation; // true : oriented in parallel to refrence marker(means x axis) else perpendicular(means Y axix)
				
				if(fabs(baselines[i][0] - baselines[i][2]) > fabs(baselines[i][1] - baselines[i][3])){ 
					min = baselines[i][0] < baselines[i][2] ? baselines[i][0] : baselines[i][2];
					max = baselines[i][0] > baselines[i][2] ? baselines[i][0] : baselines[i][2];
					baselineOrientation = true;
				}
				else{
					min = baselines[i][1] < baselines[i][3] ? baselines[i][1] : baselines[i][3];
					max = baselines[i][1] > baselines[i][3] ? baselines[i][1] : baselines[i][3];
					baselineOrientation = false;
				}
				
				for(int j = 0; j < rectVec[i].size();j++){
					//~ printf("\n (%d,0,%d) (%d,0,%d)",featureVec[i][j][0],(featureVec[i][j][1]),featureVec[i][j][2],(featureVec[i][j][3]));
					double vec[3];
					double pt1[3],pt2[3];
					
					if(finalElements[i].id[1] == i && i != 0){
						pt1[0] = finalElements[i].transMat[0][3] +finalElements[i].transMat[0][0]*(rectVec[i][j][0]) +finalElements[i].transMat[0][2]*(rectVec[i][j][1]);
						pt1[1] = finalElements[i].transMat[1][3] +finalElements[i].transMat[1][0]*(rectVec[i][j][0]) +finalElements[i].transMat[1][2]*(rectVec[i][j][1]);
						pt1[2] = finalElements[i].transMat[2][3] +finalElements[i].transMat[2][0]*(rectVec[i][j][0]) +finalElements[i].transMat[2][2]*(rectVec[i][j][1]);

						pt2[0] = finalElements[i].transMat[0][3] +finalElements[i].transMat[0][0]*(rectVec[i][j][2]) +finalElements[i].transMat[0][2]*(rectVec[i][j][3]);
						pt2[1] = finalElements[i].transMat[1][3] +finalElements[i].transMat[1][0]*(rectVec[i][j][2]) +finalElements[i].transMat[1][2]*(rectVec[i][j][3]);
						pt2[2] = finalElements[i].transMat[2][3] +finalElements[i].transMat[2][0]*(rectVec[i][j][2]) +finalElements[i].transMat[2][2]*(rectVec[i][j][3]);
						vec[0] = (pt2[0]-pt1[0]); vec[1]= (pt2[1]-pt1[1]); vec[2]=(pt2[2]-pt1[2]);
						//~ line(out,Point(round(pt2[1])/10+500,round(pt2[0])/10+500),Point(round(pt1[1])/10+500,round(pt1[0])/10+500),Scalar(0,0,255),5,8);
					}
					else {
						pt1[0] = rectVec[i][j][0]; pt1[2] = rectVec[i][j][1];pt1[1] = 0;
						pt2[0] = rectVec[i][j][2]; pt2[2] = rectVec[i][j][3];pt2[1] = 0;
						vec[0] = (pt2[0]-pt1[0]); vec[1]=vec[2]=0;
						//~ line(out,Point(round(pt2[1])/10+500,round(pt2[0])/10+500),Point(round(pt1[1])/10+500,round(pt1[0])/10+500),Scalar(0,0,255),5,8);
						
					}
					if(!baselineOrientation){
						if(fabs(pt1[2]-pt2[2]) < 0.001 &&((pt1[1] > min-100 && pt1[1] < max+100) || (pt2[1] > min-100 && pt2[1] < max+100))){
							if(pt1[1] > pt2[1]){
								pt1[1] = pt1[1] > max ? max:pt1[1];
								pt2[1] = pt2[1] < min ? min:pt2[1];
							}
							else{
								pt2[1] = pt2[1] > max ? max:pt2[1];
								pt1[1] = pt1[1] < min ? min:pt1[1];
							}
							printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							sprintf(str,"(%f,%f,%f)\n(%f,%f,%f)\n",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							myfile << str;
						}
						else if(fabs(pt1[1]-pt2[1]) < 0.001 && pt1[1] > min-100 && pt1[1] < max+100){
							pt2[1] = pt1[1] = pt1[1] > max ? max:pt1[1];
							pt2[1] = pt1[1] = pt2[1] < min ? min:pt2[1];
							printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							sprintf(str,"(%f,%f,%f)\n(%f,%f,%f)\n",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							myfile << str;
						}
					}
					else{
						if(fabs(pt1[2]-pt2[2]) < 0.001 &&((pt1[0] > min-100 && pt1[0] < max+100) || (pt2[0] > min-100 && pt2[0] < max+100))){
							if(pt1[0] > pt2[0]){
								pt1[0] = pt1[0] > max ? max:pt1[0];
								pt2[0] = pt2[0] < min ? min:pt2[0];
							}
							else{
								pt2[0] = pt2[0] > max ? max:pt2[0];
								pt1[0] = pt1[0] < min ? min:pt1[0];
							}
							printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							sprintf(str,"(%f,%f,%f)\n(%f,%f,%f)\n",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							myfile << str;
						}
						else if(fabs(pt1[0]-pt2[0]) < 0.001 && pt1[0] > min-100 && pt1[0] < max+100){
							pt2[0] = pt1[0] = pt1[0] > max ? max:pt1[0];
							pt2[0] = pt1[0] = pt2[0] < min ? min:pt2[0];
							printf("\n glVertex3f(%f,%f,%f);\n glVertex3f(%f,%f,%f);",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							sprintf(str,"(%f,%f,%f)\n(%f,%f,%f)\n",pt1[0],pt1[1],pt1[2],pt2[0],pt2[1],pt2[2]);
							myfile << str;
	
						}
					}
				}
			}
		}
		myfile.close();
	}
	return 0;
}

