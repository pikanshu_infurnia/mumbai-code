///////////////////////////////// identifying rectangular objects ////////////////////////////////
double calcOverlappingPart(double S1,double S2,double S3,double S4){
	double temp;
	double middle[2];
	double s[4] = {S1,S2,S3,S4};
	for(int i = 0; i < 4; i++){
		for(int j = i+1; j < 4; j++){
			if(s[j] < s[i]){
				temp = s[j];
				s[j] = s[i];
				s[i] = temp;
			}  
		}
	}
	middle[0] = s[1];
	middle[1] = s[2];
	
	if((S2-S3)*(S2-S4) < 0 || (S1-S3)*(S2-S4) < 0 || (S3-S1)*(S3-S2) < 0 || (S4-S1)*(S4-S2) < 0){ 
		temp = (fabs(s[2]-s[1])/fabs(S2-S1)) < (fabs(s[2]-s[1])/fabs(S4-S3)) ? (fabs(s[2]-s[1])/fabs(S2-S1)):(fabs(s[2]-s[1])/fabs(S4-S3));
		return(temp);
	}
	else return 0;
}
double calcCf(ProjectedLine line1, ProjectedLine line2, int mode){ // calculating conf. factor between a pair of lines
	double ratio = 0, overlappingPart = 0;
	if(mode == 0){
		//~ printf("\n line1 %f line2 %f ", line1.len,line2.len);
		ratio = (line1.len > line2.len ? line2.len:line1.len)/(line1.len > line2.len ? line1.len:line2.len);
		overlappingPart = calcOverlappingPart(line1.strtPt[0],line1.endPt[0],line2.strtPt[0],line2.endPt[0]);
		//~ printf("\n ratio %f over %f ", ratio,overlappingPart); 
	}
	if(mode == 1){
		//~ printf("\n line1 %f line2 %f ", line1.len,line2.len);
		ratio = (line1.len > line2.len ? line2.len:line1.len)/(line1.len > line2.len ? line1.len:line2.len);
		overlappingPart = calcOverlappingPart(line1.strtPt[1],line1.endPt[1],line2.strtPt[1],line2.endPt[1]);
		//~ printf("\n ratio %f over %f ", ratio,overlappingPart); 
	}
	return(ratio*overlappingPart);
}

double calcCfRect(ProjectedLine Hline1,ProjectedLine Hline2,ProjectedLine Vline1,ProjectedLine Vline2){ // calculating conf. factor between a pair of horizontal and vertical pair
	//~ Point rectCt1,rectCt2;
	//~ double Vwd,Vht,Hwd,Hht;
	double temp;
	double s[4];
	s[0] = Hline1.strtPt[0]; s[1] = Hline1.endPt[0];s[2] = Hline2.strtPt[0]; s[3] = Hline2.endPt[0]; 
	for(int i = 0; i < 4; i++){
		for(int j = i+1; j < 4; j++){
			if(s[j] < s[i]){
				temp = s[j];
				s[j] = s[i];
				s[i] = temp;
			}  
		}
	}
	double overlappingPart1 = calcOverlappingPart(s[0],s[3],Vline2.strtPt[0],Vline1.strtPt[0]);
	//~ printf("\n\n (%f,%f) (%f,%f) : overlappingPart1 : %f ", s[0],s[3],Vline2.strtPt[0],Vline1.strtPt[0],overlappingPart1);
	//~ Hwd = fabs(s[0]-s[3]);
	//~ Hht = fabs(Hline1.strtPt[1] - Hline2.strtPt[1]);
	//~ 
	//~ rectCt1.x = (s[0]+s[3])/2;
	//~ rectCt1.y = (Hline1.strtPt[1] + Hline2.strtPt[1])/2;
	//~ 
	s[0] = Vline1.strtPt[1]; s[1] = Vline1.endPt[1];s[2] = Vline2.strtPt[1]; s[3] = Vline2.endPt[1]; 
	for(int i = 0; i < 4; i++){
		for(int j = i+1; j < 4; j++){
			if(s[j] < s[i]){
				temp = s[j];
				s[j] = s[i];
				s[i] = temp;
			}  
		}
	}
	double overlappingPart2 = calcOverlappingPart(s[0],s[3],Hline2.strtPt[1],Hline1.strtPt[1]);
	//~ printf("\n (%f,%f) (%f,%f) : overlappingPart2 : %f ", s[0],s[3],Hline2.strtPt[1],Hline1.strtPt[1],overlappingPart2);
	//~ rectCt2.y = (s[0]+s[3])/2;
	//~ rectCt2.x = (Vline1.strtPt[0] + Vline2.strtPt[0])/2;
//~ 
	//~ Vwd = fabs(Vline1.strtPt[0] - Vline2.strtPt[0]);
	//~ Vht = fabs(s[0]-s[3]);
//~ 
	//~ double dis = sqrt((rectCt1.x - rectCt2.x)*(rectCt1.x - rectCt2.x)+(rectCt1.y - rectCt2.y)*(rectCt1.y - rectCt2.y))/4000;
	//~ double ratioWd = ((Vwd < Hwd)? Vwd:Hwd)/((Vwd > Hwd)? Vwd:Hwd);
	//~ double ratioHt = ((Vht < Hht)? Vht:Hht)/((Vht > Hht)? Vht:Hht);
	//~ return (ratioHt*ratioWd*dis);
	//~ double overlappingPart = calcOverlappingPart(Hline1.strtPt[1],Hline1.endPt[1],line2.strtPt[1],line2.endPt[1]);
	return overlappingPart1*overlappingPart2;
	
}

void completeRectWindow(ProjectedLine &H1,ProjectedLine &H2,ProjectedLine &V1,ProjectedLine &V2){ // completing lines that comprise a window
	if(V1.strtPt[0] < V2.strtPt[0]){
		H1.strtPt[0] = V1.strtPt[0]; H1.endPt[0] = V2.strtPt[0];
		H2.strtPt[0] = V1.strtPt[0]; H2.endPt[0] = V2.strtPt[0];
	}
	else{
		H1.strtPt[0] = V2.strtPt[0]; H1.endPt[0] = V1.strtPt[0];
		H2.strtPt[0] = V2.strtPt[0]; H2.endPt[0] = V1.strtPt[0];
	}
	if(H1.strtPt[1] < H2.strtPt[1]){
		V1.strtPt[1] = H1.strtPt[1]; V1.endPt[1] = H2.strtPt[1];
		V2.strtPt[1] = H1.strtPt[1]; V2.endPt[1] = H2.strtPt[1];
	}
	else{
		V1.strtPt[1] = H2.strtPt[1]; V1.endPt[1] = H1.strtPt[1];
		V2.strtPt[1] = H2.strtPt[1]; V2.endPt[1] = H1.strtPt[1];
	}

}

void completeRectDoorVertical(ProjectedLine &V1,ProjectedLine &V2){ // completing vertical lines that comprise a door
	double max;
	max = (V1.endPt[1] > V2.endPt[1]) ? V1.endPt[1] : V2.endPt[1];
	V1.endPt[1] = V2.endPt[1] = max;
	V1.strtPt[1] = V2.strtPt[1] = 0;
}
void completeRectDoorHorizontal(ProjectedLine &H1,ProjectedLine &V1,ProjectedLine &V2){ // completing horizontal line associated with a door
	
	if(V1.strtPt[0] != V2.strtPt[0]){
		if(V1.strtPt[0] < V2.strtPt[0]){
			H1.strtPt[0] = V1.strtPt[0]; H1.endPt[0] = V2.strtPt[0];
		}
		else{
			H1.strtPt[0] = V2.strtPt[0]; H1.endPt[0] = V1.strtPt[0];
		}
	}
	else{
		if(fabs(H1.strtPt[0]-V1.strtPt[0]) < fabs(H1.endPt[0]-V1.strtPt[0])){
			H1.strtPt[0] = V1.strtPt[0];
		}
		else{
			H1.endPt[0] = V1.strtPt[0];
		}
	}
	H1.strtPt[1] = H1.endPt[1] = V1.endPt[1];
}

double rectOverlap(vector<ProjectedLine> rect1,vector<ProjectedLine> rect2){ // overlapping area between two rectangles
	ProjectedLine H1 = rect1[0],H2 = rect1[1];
	ProjectedLine H3 = rect2[0],H4 = rect2[1];
	double XA1,XA2,YA1,YA2,XB1,XB2,YB1,YB2;
	XA2 = std::min(H1.strtPt[0],H1.endPt[0]);
	XA1 = std::max(H1.strtPt[0],H1.endPt[0]);
	YA2 = std::min(H1.strtPt[1],H2.strtPt[1]);
	YA1 = std::max(H1.strtPt[1],H2.strtPt[1]);
          
	XB2 = std::min(H3.strtPt[0],H3.endPt[0]);
	XB1 = std::max(H3.strtPt[0],H3.endPt[0]);
	YB2 = std::min(H3.strtPt[1],H4.strtPt[1]);
	YB1 = std::max(H3.strtPt[1],H4.strtPt[1]);
	
	double SA = fabs(H1.strtPt[0]-H1.endPt[0])*fabs(H1.strtPt[1]-H2.strtPt[1]);
	double SB = fabs(H3.strtPt[0]-H3.endPt[0])*fabs(H3.strtPt[1]-H4.strtPt[1]);
	double SI = fabs(std::max(XA2, XB2) - std::min(XA1, XB1)) * fabs(std::max(YA2, YB2) - std::min(YA1, YB1));
	
	return std::max(SI/SA,SI/SB);

}

void rectElimination(vector<vector<ProjectedLine> > &rect){ // eliminating rectangles that are completely inside of another rectangle
	for(int i = 0; i < rect.size(); i++){
		for(int j = 0; j < rect.size(); j++){
			if(i != j){
				ProjectedLine H1 = rect[i][0],H2 = rect[i][1],V1 = rect[i][2], V2 = rect[i][3];
				ProjectedLine H3 = rect[j][0],H4 = rect[j][1],V3 = rect[j][2], V4 = rect[j][3];
				vector<Point> P1(5);
				Vector<Point> P2(5);
				P1[0] = Point(H1.strtPt[0],H1.strtPt[1]);P1[1] = Point(H1.endPt[0],H1.endPt[1]);
				P1[2] = Point(H2.endPt[0],H2.endPt[1]); P1[3] = Point(H2.strtPt[0],H2.strtPt[1]);
				P1[4] = Point(H1.strtPt[0],H1.strtPt[1]);

				P2[0] = Point(H3.strtPt[0],H3.strtPt[1]);P2[1] = Point(H3.endPt[0],H3.endPt[1]);
				P2[2] = Point(H4.endPt[0],H4.endPt[1]); P2[3] = Point(H4.strtPt[0],H4.strtPt[1]);
				P2[4] = Point(H3.strtPt[0],H3.strtPt[1]);
				int count_inside = 0;
				double A,B,C,D;

				for(int m = 0; m < 4; m++){
					A = -(P1[m+1].y - P1[m].y);
					B = P1[m+1].x - P1[m].x;
					C = -(A * P1[m].x + B * P1[m].y);
					for(int p = 0; p < 4; p++){
						D = A*P2[p].x + B*P2[p].y + C;
						if(floor(D) <= 0 && (A !=0 || B != 0) ) count_inside++;
						//~ printf("\n count_inside : %d A : %f B : %f C : %f D : %f",count_inside, A,B,C,D);
					}
				}
				
				
				if(!(count_inside > 0 && count_inside < 16)){
					
					printf("\n block: \n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H1.strtPt[0],H1.strtPt[1],H1.endPt[0],H1.endPt[1],H2.endPt[0],H2.endPt[1],H2.strtPt[0],H2.strtPt[1] );
					printf("\n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H3.strtPt[0],H3.strtPt[1],H3.endPt[0],H3.endPt[1],H4.endPt[0],H4.endPt[1],H4.strtPt[0],H4.strtPt[1] );
					printf("\n (%d,%d),(%d,%d),(%d,%d),(%d,%d)",P1[0].x,P1[0].y,P1[1].x,P1[1].y,P1[2].x,P1[2].y,P1[3].x,P1[3].y);
					printf("\n (%d,%d),(%d,%d),(%d,%d),(%d,%d)",P2[0].x,P2[0].y,P2[1].x,P2[1].y,P2[2].x,P2[2].y,P2[3].x,P2[3].y);
					printf("\n count_inside : %d",count_inside);
					
				}

				if(count_inside == 16){
					rect.erase(rect.begin()+j);
					//~ i--;
					j--;
					if(i > j) i--;
					//~ break;
				}
			}
		}
	}
}
void rectEliminationDoorWindow(vector<vector<ProjectedLine> > &rect1,vector<vector<ProjectedLine> > &rect2){ // eliminating rectangles that are completely inside of another rectangle
	for(int i = 0; i < rect1.size(); i++){
		for(int j = 0; j < rect2.size(); j++){
			if(i != j){
				ProjectedLine H1 = rect1[i][0],H2 = rect1[i][1],V1 = rect1[i][2], V2 = rect1[i][3];
				ProjectedLine H3 = rect2[j][0],H4 = rect2[j][1],V3 = rect2[j][2], V4 = rect2[j][3];
				vector<Point> P1(5);
				Vector<Point> P2(5);
				P1[0] = Point(H1.strtPt[0],H1.strtPt[1]);P1[1] = Point(H1.endPt[0],H1.endPt[1]);
				P1[2] = Point(H2.endPt[0],H2.endPt[1]); P1[3] = Point(H2.strtPt[0],H2.strtPt[1]);
				P1[4] = Point(H1.strtPt[0],H1.strtPt[1]);

				P2[0] = Point(H3.strtPt[0],H3.strtPt[1]);P2[1] = Point(H3.endPt[0],H3.endPt[1]);
				P2[2] = Point(H4.endPt[0],H4.endPt[1]); P2[3] = Point(H4.strtPt[0],H4.strtPt[1]);
				P2[4] = Point(H3.strtPt[0],H3.strtPt[1]);
				int count_inside = 0;
				double A,B,C,D;

				for(int m = 0; m < 4; m++){
					A = -(P1[m+1].y - P1[m].y);
					B = P1[m+1].x - P1[m].x;
					C = -(A * P1[m].x + B * P1[m].y);
					for(int p = 0; p < 4; p++){
						D = A*P2[p].x + B*P2[p].y + C;
						if(floor(D) <= 0 && (A !=0 || B != 0) ) count_inside++;
						//~ printf("\n count_inside : %d A : %f B : %f C : %f D : %f",count_inside, A,B,C,D);
					}
				}
				
				
				if(!(count_inside > 0 && count_inside < 16)){
					
					printf("\n block: \n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H1.strtPt[0],H1.strtPt[1],H1.endPt[0],H1.endPt[1],H2.endPt[0],H2.endPt[1],H2.strtPt[0],H2.strtPt[1] );
					printf("\n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H3.strtPt[0],H3.strtPt[1],H3.endPt[0],H3.endPt[1],H4.endPt[0],H4.endPt[1],H4.strtPt[0],H4.strtPt[1] );
					printf("\n (%d,%d),(%d,%d),(%d,%d),(%d,%d)",P1[0].x,P1[0].y,P1[1].x,P1[1].y,P1[2].x,P1[2].y,P1[3].x,P1[3].y);
					printf("\n (%d,%d),(%d,%d),(%d,%d),(%d,%d)",P2[0].x,P2[0].y,P2[1].x,P2[1].y,P2[2].x,P2[2].y,P2[3].x,P2[3].y);
					printf("\n count_inside : %d",count_inside);
					
				}

				if(count_inside == 16){
					rect2.erase(rect.begin()+j);
					//~ i--;
					j--;
					//~ if(i > j) i--;
					//~ break;
				}
			}
		}
	}
}
// identifying window and doors in features detected
vector<ProjectedLine> identifyRect(vector<ProjectedLine> projectedHSegments, vector<ProjectedLine> projectedVSegments,vector<ProjectedLine>& rectHSeg, vector<ProjectedLine>& rectVSeg){

	vector<ProjectedLine> rect;
	vector<vector<ProjectedLine> > rectWindow;
	vector<vector<ProjectedLine> > rectDoor;
	
	vector<vector<double> > Hcf(projectedHSegments.size()); // confidence factor 
	vector<vector<double> > Vcf(projectedVSegments.size()); // confidence factor
	vector<bool> DoneH(projectedHSegments.size(),false);
	vector<bool> DoneV(projectedVSegments.size(),false);
	
	
	int mode = 0 ;
	for(int i = 0; i < projectedHSegments.size(); i++){
		for(int j = 0; j < projectedHSegments.size(); j++){
			if(i == j) Hcf[i].push_back(-1);
			else{
				double cf = calcCf(projectedHSegments[i],projectedHSegments[j],mode);
				//~ if(cf < 0.6) cf = 0;
				Hcf[i].push_back(cf); // mode = 0 = horizontal ; mode = 1 = vertical
			}
		}
	}
	
	cout << " \n Confidence Factor Horizontal: \n";
	for(int i = 0; i < projectedHSegments.size(); i++){
		cout << endl;
		for(int j = 0; j < projectedHSegments.size(); j++)
			printf(" %f ", Hcf[i][j]);
	}

	mode = 1 ;
	for(int i = 0; i < projectedVSegments.size(); i++){
		for(int j = 0; j < projectedVSegments.size(); j++){
			if(i == j) Vcf[i].push_back(-1);
			else{
				double cf = calcCf(projectedVSegments[i],projectedVSegments[j],mode);
				//~ if(cf < 0.6) cf = 0;
				Vcf[i].push_back(cf); // mode = 0 = horizontal ; mode = 1 = vertical
			}
		}
	}
	
	cout << " \n Confidence Factor Vertical : \n";
	for(int i = 0; i < projectedVSegments.size(); i++){
		cout << endl;
		for(int j = 0; j < projectedVSegments.size(); j++)
			printf(" %f ", Vcf[i][j]);
	}
// detecting windows and doors
	for(int i = 0; i < Hcf.size(); i++){
		for(int j = i+1; j < Hcf.size(); j++){
			for(int k = 0; k < Vcf.size(); k++){
				for(int l = k+1; l < Vcf.size(); l++){
					if(Hcf[i][j] > 0.5 && Vcf[k][l] > 0.5){
						double cf = calcCfRect(projectedHSegments[i],projectedHSegments[j],projectedVSegments[k],projectedVSegments[l]);
						if(cf > 0.75){
							rect.clear();
							completeRectWindow(projectedHSegments[i],projectedHSegments[j],projectedVSegments[k],projectedVSegments[l]);
							
							if(!DoneH[i]){
								//~ rectHSeg.push_back(projectedHSegments[i]);
								DoneH[i] = true;
							}
							
							if(projectedHSegments[i].strtPt[1] > projectedHSegments[j].strtPt[1]){
								rect.push_back(projectedHSegments[i]);
								rect.push_back(projectedHSegments[j]);
							}
							else{
								rect.push_back(projectedHSegments[j]);
								rect.push_back(projectedHSegments[i]);
							}
							
							if(!DoneH[j]){
								//~ rectHSeg.push_back(projectedHSegments[j]);
								DoneH[j] = true;
							}
							rect.push_back(projectedVSegments[k]);
							if(!DoneV[k]){
								//~ rectVSeg.push_back(projectedVSegments[k]);
								DoneV[k] = true;
							}
							rect.push_back(projectedVSegments[l]);
							if(!DoneV[l]){
								//~ rectVSeg.push_back(projectedVSegments[l]);
								DoneV[l] = true;
							}
							
							rectWindow.push_back(rect);
						}
						//~ printf("\n(%d,%d) (%d,%d) : %f",i,j,k,l,cf);
						
					}
					
					if(projectedVSegments[k].len > 1500 && projectedVSegments[l].len > 1500 && Vcf[k][l] > 0.5 && fabs(projectedVSegments[l].strtPt[0] - projectedVSegments[k].strtPt[0]) > 500 && fabs(projectedVSegments[l].strtPt[0] - projectedVSegments[k].strtPt[0]) < 1200 ){
						completeRectDoorVertical(projectedVSegments[k],projectedVSegments[l]);
						ProjectedLine V1,V2;
						
						
						for(int m = 0; m < projectedHSegments.size(); m++){
							double S1,S2,S3,S4;
							S1 = projectedVSegments[l].strtPt[0];
							S2 = projectedVSegments[k].strtPt[0];
							S3 = projectedHSegments[m].strtPt[0];
							S4 = projectedHSegments[m].endPt[0];
							
							double overlappingPart2 = calcOverlappingPart(S1,S2,S3,S4);
							//~ printf("\n m : %d k : %d l : %d overlappingPart2 : %f",m,k,l,overlappingPart2);
							if(overlappingPart2 > 0.75){
								rect.clear();
								completeRectDoorHorizontal(projectedHSegments[m],projectedVSegments[k],projectedVSegments[l]);
								rect.push_back(projectedHSegments[m]);
								if(!DoneH[m]){
									//~ rectHSeg.push_back(projectedHSegments[m]);
									DoneH[m] = true;
								}
								ProjectedLine line = projectedHSegments[m];
								line.strtPt[1] = line.endPt[1] = 0;
								rect.push_back(line);
								rect.push_back(projectedVSegments[k]);
								if(!DoneV[k]){
									DoneV[k] = true;
									//~ rectVSeg.push_back(projectedVSegments[k]);
								}
								rect.push_back(projectedVSegments[l]);
								if(!DoneV[l]){
									DoneV[l] = true;
									//~ rectVSeg.push_back(projectedVSegments[l]);
								}
								rectDoor.push_back(rect);
							}
						}
						
					}
					
				}
			}
		}
	}


// detecting doors with only one vertical and horizontal line 
/*
	for(int l = 0; l < Vcf.size(); l++){
		if(projectedVSegments[l].len > 1500 && !DoneV[l]){
			int crspHrz = 0;
			double minDis = 9999;
			int minDisLine = -1;
			double S1,S2;
			S1 = projectedVSegments[l].endPt[0];
			S2 = projectedVSegments[l].endPt[1];
			for(int m = 0; m < projectedHSegments.size(); m++){
				double dis1 = fabs(S1-projectedHSegments[m].strtPt[0])+fabs(S2-projectedHSegments[m].strtPt[1]);
				double dis2 = fabs(S1-projectedHSegments[m].endPt[0])+fabs(S2-projectedHSegments[m].endPt[1]);
				if((dis1 < dis2 ? dis1 : dis2) < minDis){
					minDis = (dis1 < dis2 ? dis1 : dis2);
					minDisLine = m;
				}
			}
			printf("\n mindis is %f .\n",minDis);
			if(minDis < 200){
				completeRectDoorVertical(projectedVSegments[l],projectedVSegments[l]);
				if(!DoneV[l]){
					DoneV[l] = true;
					cout << "\npushed Vertical line:";
					//~ rectVSeg.push_back(projectedVSegments[l]);
				}
				completeRectDoorHorizontal(projectedHSegments[minDisLine],projectedVSegments[l],projectedVSegments[l]);
				if(!DoneH[minDisLine]){
					cout << "\npushed Horizontal line:";
					//~ rectHSeg.push_back(projectedHSegments[minDisLine]);
					DoneH[minDisLine] = true;
				}
				rect.clear();
				rect.push_back(projectedHSegments[minDisLine]);
				ProjectedLine line = projectedHSegments[minDisLine];
				line.strtPt[1] = line.endPt[1] = 0;
				rect.push_back(line);
				rect.push_back(projectedVSegments[l]);
				line = projectedVSegments[l];
				if(fabs(projectedVSegments[l].strtPt[0] - projectedHSegments[minDisLine].strtPt[0]) > fabs(projectedVSegments[l].strtPt[0] - projectedHSegments[minDisLine].endPt[0]))
					line.strtPt[0] = line.endPt[0] = projectedHSegments[minDisLine].strtPt[0];
				else line.strtPt[0] = line.endPt[0] = projectedHSegments[minDisLine].endPt[0];
				rect.push_back(line);
				rectDoor.push_back(rect);
			}
		}
		
	} */

// elimination of unrequired rectangles
	cout << "\n size of rectWindow : " << rectWindow.size() << endl;
	rectElimination(rectWindow);
	cout << "\n size of rectDoor : " << rectDoor.size() << endl;
	rectElimination(rectDoor);
	rectEliminationDoorWindow(rectDoor,rectWindow);
	cout << "\n size of rectWindow : " << rectWindow.size() << endl;
	cout << "\n size of rectDoor : " << rectDoor.size() << endl;
	for(int i = 0; i < rectWindow.size(); i++){
		for(int j = 0; j < rectWindow.size(); j++){
			if(i != j){
				double overlapPortion = rectOverlap(rectWindow[i],rectWindow[j]);
				printf("\noverlapPortion : %f ",overlapPortion);
				if(overlapPortion > 0.7){
					ProjectedLine H1 = rectWindow[i][0],H2 = rectWindow[i][1];
					ProjectedLine H3 = rectWindow[j][0],H4 = rectWindow[j][1];
					double XA1,XA2,YA1,YA2;
					XA2 = std::min(std::min(H1.strtPt[0],H1.endPt[0]),std::min(H3.strtPt[0],H3.endPt[0]));
					XA1 = std::max(std::max(H1.strtPt[0],H1.endPt[0]),std::max(H3.strtPt[0],H3.endPt[0]));
					YA2 = std::min(std::min(H1.strtPt[1],H2.strtPt[1]),std::min(H3.strtPt[1],H4.strtPt[1]));
					YA1 = std::max(std::max(H1.strtPt[1],H2.strtPt[1]),std::max(H3.strtPt[1],H4.strtPt[1]));
					rectWindow[i][0].strtPt[0] = XA2; rectWindow[i][0].strtPt[1] = YA1;
					rectWindow[i][0].endPt[0]  = XA1; rectWindow[i][0].endPt[1]  = YA1;
					rectWindow[i][1].strtPt[0] = XA2; rectWindow[i][1].strtPt[1] = YA2;
					rectWindow[i][1].endPt[0]  = XA1; rectWindow[i][1].endPt[1]  = YA2;




					rectWindow[i][2].strtPt[0] = XA2; rectWindow[i][2].strtPt[1] = YA2;
					rectWindow[i][2].endPt[0]  = XA2; rectWindow[i][2].endPt[1]  = YA1;
					rectWindow[i][3].strtPt[0] = XA1; rectWindow[i][3].strtPt[1] = YA2;
					rectWindow[i][3].endPt[0]  = XA1; rectWindow[i][3].endPt[1]  = YA1;
					
					rectWindow.push_back(rectWindow[i]);
					rectWindow.erase(rectWindow.begin()+i);
					rectWindow.erase(rectWindow.begin()+j);
					i--;j--;
					break;
				}
			}
		}
	}

// pushing remaining rectangular features
	for(int i = 0; i < rectWindow.size(); i++){
		ProjectedLine H1 = rectWindow[i][0],H2 = rectWindow[i][1];
		//~ if(rectWindow[i][0].strtPt[1] != 0 && rectWindow[i][1].strtPt[1] != 0){
			printf("\n block: \n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H1.strtPt[0],H1.strtPt[1],H1.endPt[0],H1.endPt[1],H2.endPt[0],H2.endPt[1],H2.strtPt[0],H2.strtPt[1] );
			rectHSeg.push_back(rectWindow[i][0]);
			rectHSeg.push_back(rectWindow[i][1]);
			rectVSeg.push_back(rectWindow[i][2]);
			rectVSeg.push_back(rectWindow[i][3]);
		//~ }
		//~ else cout << "\n eliminated window \n";
	}
	for(int i = 0; i < rectDoor.size(); i++){
		ProjectedLine H1 = rectDoor[i][0],H2 = rectDoor[i][1];
		//~ if(rectDoor[i][0].strtPt[1] != 0 && rectDoor[i][1].strtPt[1] != 0){
			printf("\n block: \n (%f,%f),(%f,%f),(%f,%f),(%f,%f)",H1.strtPt[0],H1.strtPt[1],H1.endPt[0],H1.endPt[1],H2.endPt[0],H2.endPt[1],H2.strtPt[0],H2.strtPt[1] );
			rectHSeg.push_back(rectDoor[i][0]);
			rectHSeg.push_back(rectDoor[i][1]);
			rectVSeg.push_back(rectDoor[i][2]);
			rectVSeg.push_back(rectDoor[i][3]);
		//~ }
		//~ else cout << "\n eliminated door \n";
	}
	//~ rectElimination(rectDoor);

	return rect;
}


/////////////////////////////////////// Drawing Final Extracted doors and windows on th image//////////////////////////
    private void draw3DQuadsonImg(double transMat[4][4],Mat& image,double r,double g,double b){

		int imageW = image.cols;
		int imageH = image.rows;
		double focal= 2767;
		vector<double> fVec;
		for (int j = 0; j < rectWindow.size(); j++) {
			double pt1[3],pt2[3];
			for (int k = 0; k < 4; k++) {
				ProjectedLine p = rectWindow[i][j];
				fVec.clear();
				fVec.push_back(p.strtPt[0]);
				fVec.push_back(p.strtPt[1]);
				fVec.push_back(p.endPt[0]);
				fVec.push_back(p.endPt[1]);

				pt1[0] = transMat[0][3] + transMat[0][0] * (fVec[0]) + transMat[0][2] * (fVec[1]);
				pt1[1] = transMat[1][3] + transMat[1][0] * (fVec[0]) + transMat[1][2] * (fVec[1]);
				pt1[2] = transMat[2][3] + transMat[2][0] * (fVec[0]) + transMat[2][2] * (fVec[1]);

				pt2[0] = transMat[0][3] + transMat[0][0] * (fVec[2]) + transMat[0][2] * (fVec[3]);
				pt2[1] = transMat[1][3] + transMat[1][0] * (fVec[2]) + transMat[1][2] * (fVec[3]);
				pt2[2] = transMat[2][3] + transMat[2][0] * (fVec[2]) + transMat[2][2] * (fVec[3]);
				
				double S1,S2,E1,E2;
//                    Log.d("CompileProject", "(" + doors.get(i).get(j).pt[0] + " ," + doors.get(i).get(j).pt[1] + " ," + doors.get(i).get(j).pt[2] + ")");
				S2 = (pt1[0]*focal)/pt1[2];
				S1 = (pt1[1]*focal)/pt1[2];
				E2 = (pt2[0]*focal)/pt2[2];
				E1 = (pt2[1]*focal)/pt2[2];
				S2 += imageW/2;
				S1 += imageH/2;
				E2 += imageW/2;
				E1 += imageH/2;
				line(image,Point(round(S2),round(S1)),Point(round(E2),round(E1)),Scalar(r,g,b),1,8);
			}
		}
		for (int j = 0; j < rectDoor.size(); j++) {
			double pt1[3],pt2[3];
			for (int k = 0; k < 4; k++) {
				ProjectedLine p = rectDoor[i][j];
				fVec.clear();
				fVec.push_back(p.strtPt[0]);
				fVec.push_back(p.strtPt[1]);
				fVec.push_back(p.endPt[0]);
				fVec.push_back(p.endPt[1]);

				pt1[0] = transMat[0][3] + transMat[0][0] * (fVec[0]) + transMat[0][2] * (fVec[1]);
				pt1[1] = transMat[1][3] + transMat[1][0] * (fVec[0]) + transMat[1][2] * (fVec[1]);
				pt1[2] = transMat[2][3] + transMat[2][0] * (fVec[0]) + transMat[2][2] * (fVec[1]);

				pt2[0] = transMat[0][3] + transMat[0][0] * (fVec[2]) + transMat[0][2] * (fVec[3]);
				pt2[1] = transMat[1][3] + transMat[1][0] * (fVec[2]) + transMat[1][2] * (fVec[3]);
				pt2[2] = transMat[2][3] + transMat[2][0] * (fVec[2]) + transMat[2][2] * (fVec[3]);
				
				double S1,S2,E1,E2;
//                    Log.d("CompileProject", "(" + doors.get(i).get(j).pt[0] + " ," + doors.get(i).get(j).pt[1] + " ," + doors.get(i).get(j).pt[2] + ")");
				S2 = (pt1[0]*focal)/pt1[2];
				S1 = (pt1[1]*focal)/pt1[2];
				E2 = (pt2[0]*focal)/pt2[2];
				E1 = (pt2[1]*focal)/pt2[2];
				S2 += imageW/2;
				S1 += imageH/2;
				E2 += imageW/2;
				E1 += imageH/2;
				line(image,Point(round(S2),round(S1)),Point(round(E2),round(E1)),Scalar(r,g,b),1,8);
			}
		}
    }

///////////////////////////////////////////////////////////////////////////////////////////////////
