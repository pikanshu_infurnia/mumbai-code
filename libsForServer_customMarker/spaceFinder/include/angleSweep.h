double* HorzLineRange(Vector3d XVec, Vector3d ZVec, Vector3d MarkerPt1Camera,double M,double focal);
double* VertLineRange(Vector3d ZVec,Vector3d ShiftVec,Vector3d LineVec,Vector3d MarkerPt1Camera,double Theta0,Matrix2d AllPoints,double focal);
double AngleEndPts(Vector3d ZVec,Vector3d PtRefVec,bool flag,double focal);
