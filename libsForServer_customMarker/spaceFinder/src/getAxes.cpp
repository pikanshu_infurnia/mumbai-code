#include <global_spaceFinder.h>

void getMarkerAxes(double x[], Vector3d& XVec, Vector3d& YVec, Vector3d& ZVec, Vector3d& MarkerPt1Camera){
	XVec[0] = x[3]-x[0];XVec[1] = x[4]-x[1];XVec[2] = x[5]-x[2];
	YVec[0] = x[9]-x[0];YVec[1] = x[10]-x[1];YVec[2] = x[11]-x[2];	
	cout<<endl<<"Norm of XVec "<<XVec.norm();
	XVec.normalize();YVec.normalize();
	ZVec = -XVec.cross(YVec);
	cout<<endl<<"XVec = "<<XVec;
	cout<<endl<<"YVec = "<<YVec;
	cout<<endl<<"ZVec = "<<ZVec;
	cout<<endl;
// Got XVec,YVec and ZVec
	MarkerPt1Camera[0] = x[0];MarkerPt1Camera[1] = x[1];MarkerPt1Camera[2] = x[2];
}




void getWallAxes(Vector3d XVec,Vector3d YVec,Vector3d ZVec,Vector3d MarkerPt1Camera,double Rho0,double Theta0,Vector3d& LineVec,Vector3d& LinePVec,Vector3d& ShiftVec, double M, double N,double focal){
	double Theta = Theta0 - PI/2;
	double A = MarkerPt1Camera[2]*XVec[1]-MarkerPt1Camera[1]*XVec[2];
	double B = MarkerPt1Camera[2]*YVec[1]-MarkerPt1Camera[1]*YVec[2];
	double C = MarkerPt1Camera[2]*XVec[0]-MarkerPt1Camera[0]*XVec[2];
	double D = MarkerPt1Camera[2]*YVec[0]-MarkerPt1Camera[0]*YVec[2];
	double RatioS = sin(Theta);
	double RatioC = cos(Theta);
	
	printf(" \n A : %5f B : %5f C : %5f D : %5f \n", A,B,C,D);
	printf(" \n MarkerPt1Camera : (%f,%f,%f)",MarkerPt1Camera[0],MarkerPt1Camera[1],MarkerPt1Camera[2] );
	
	printf(" \n XVec : (%f,%f,%f)",XVec[0],XVec[1],XVec[2] );
	
	printf(" \n YVec : (%f,%f,%f)",YVec[0],YVec[1],YVec[2] );	
	
	cout<<endl<<sin(Theta)<<" "<<cos(Theta)<<" "<<Theta<<endl;
	double AbyB = (RatioC*B-RatioS*D)/(RatioS*C-RatioC*A);
	Vector3d TVec = ((RatioS/RatioC)*XVec+YVec);
	double Beta = 1/TVec.norm();
	double Alpha = Beta*AbyB;
	//cout<<endl<<"Beta = "<<Beta<<"; Alpha = "<<Alpha;

	LineVec = Alpha*XVec + Beta*YVec;
	LineVec.normalize();
	cout<<endl<<"LineVec "<<LineVec;

	D = XVec.dot(YVec);
	double Ratio = -(Beta + Alpha*D)/(Alpha + Beta*D);
	LinePVec = Ratio*XVec + YVec;
	LinePVec.normalize();
	cout<<endl<<"LinePVec "<<LinePVec;
	
// Code Done till Line 144 of test.m
	double Rho = (Rho0 - cos(Theta0)*(N/2-1) - sin(Theta0)*(M/2-1))/focal;
	//cout<<endl<<"Rho "<<Rho;

	Vector3d VecTRho;
	VecTRho[0] = cos(Theta0);VecTRho[1] = sin(Theta0);VecTRho[2] = -Rho;
	//cout<<endl<<"VecTRho "<<VecTRho;

	Alpha = -VecTRho.dot(MarkerPt1Camera)/VecTRho.dot(LinePVec);
	cout<<endl<<"MarkerPt 1 to Camera "<<MarkerPt1Camera;

	ShiftVec = Alpha*LinePVec;
	cout<<endl<<"Alpha = "<<Alpha<<"\n ShiftVec \n"<<ShiftVec<<endl;
}



void GetWallAxes(Vector3d XVec,Vector3d YVec,Vector3d ZVec,Vector3d MarkerPt1Camera,double Rho0,double Theta0,Vector3d& LineVec,Vector3d& LinePVec,Vector3d& ShiftVec, double M, double N,double focal){


	double Alpha;
	double Theta = Theta0 - PI/2;

	double Rho = (Rho0 - cos(Theta0)*(N/2) - sin(Theta0)*(M/2))/focal;
	//cout<<endl<<"Rho "<<Rho;

	Vector3d VecTRho;
	VecTRho[0] = cos(Theta0);VecTRho[1] = sin(Theta0);VecTRho[2] = -Rho;
	//cout<<endl<<"VecTRho "<<VecTRho;

	Alpha = -VecTRho.dot(MarkerPt1Camera)/VecTRho.dot(YVec);
	ShiftVec = Alpha*YVec;
	cout<<endl<<"Alpha = "<<Alpha<<"\n ShiftVec \n"<<ShiftVec<<endl;
	
	Vector3d linePt1Camera = ShiftVec + MarkerPt1Camera;
	
	double A = linePt1Camera[2]*XVec[1]-linePt1Camera[1]*XVec[2];
	double B = linePt1Camera[2]*YVec[1]-linePt1Camera[1]*YVec[2];
	double C = linePt1Camera[2]*XVec[0]-linePt1Camera[0]*XVec[2];
	double D = linePt1Camera[2]*YVec[0]-linePt1Camera[0]*YVec[2];
	double RatioS = sin(Theta);
	double RatioC = cos(Theta);
	
	printf(" \n A : %5f B : %5f C : %5f D : %5f ", A,B,C,D);
	printf(" \n MarkerPt1Camera : (%f,%f,%f)",MarkerPt1Camera[0],MarkerPt1Camera[1],MarkerPt1Camera[2] );
	
	printf(" \n linePt1Camera : (%f,%f,%f)",linePt1Camera[0],linePt1Camera[1],linePt1Camera[2] );
	
	
	printf(" \n XVec : (%f,%f,%f)",XVec[0],XVec[1],XVec[2] );
	
	printf(" \n YVec : (%f,%f,%f)",YVec[0],YVec[1],YVec[2] );	
	
	cout<<endl<<sin(Theta)<<" "<<cos(Theta)<<" "<<Theta<<endl;
	double AbyB = (RatioC*B-RatioS*D)/(RatioS*C-RatioC*A);
	Vector3d TVec = (AbyB*XVec+YVec);
	double Beta = 1/TVec.norm();
	Alpha = Beta*AbyB;
	cout<<endl<<"Beta = "<<Beta<<"; Alpha = "<<Alpha;

	LineVec = Alpha*XVec + Beta*YVec;
	LineVec.normalize();
	cout<<endl<<"LineVec "<<LineVec;

	D = XVec.dot(YVec);
	double Ratio = -(Beta + Alpha*D)/(Alpha + Beta*D);
	LinePVec = Ratio*XVec + YVec;
	LinePVec.normalize();
	cout<<endl<<"LinePVec "<<LinePVec;
	
	LinePVec = LineVec.cross(ZVec);


}


