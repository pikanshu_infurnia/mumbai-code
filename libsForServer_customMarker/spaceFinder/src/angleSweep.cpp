#include <global_spaceFinder.h>

double* HorzLineRange(Vector3d XVec, Vector3d ZVec, Vector3d MarkerPt1Camera,double M,double focal){
	double AngleV1 = AngleEndPts(XVec,MarkerPt1Camera,false,focal);
	cout<<"AngleV1 = "<<AngleV1*180/PI<<endl;
	// Calculating height of the image
	double Qr = -M/(2*focal);
	double VertLen = (MarkerPt1Camera[2]*Qr-MarkerPt1Camera[1])/(ZVec[1] - Qr*ZVec[2]);
	cout<<"Vertical Length = "<<VertLen<<endl;
	if(VertLen>4000)VertLen = 4000;

	Vector3d TVec = MarkerPt1Camera + 1.5*VertLen*ZVec;
	double AngleV2 = AngleEndPts(XVec,TVec,false,focal);
	cout<<"AngleV2 = "<<AngleV2*180/PI<<endl;
	// Final AngleRange
	double *AngleHRange = (double *) malloc(sizeof(double) * 2);
	if(AngleV1<AngleV2){
	AngleHRange[0] = AngleV1-15*PI/180;AngleHRange[1] = AngleV2+10*PI/180;}
	else{
	AngleHRange[0] = AngleV2-10*PI/180;AngleHRange[1] = AngleV1+15*PI/180;}
	cout<<"Angle Range for Horizontal Lines = ("<<AngleHRange[0]*180/PI<<","<<AngleHRange[1]*180/PI<<")\n";
	return AngleHRange;
// 
}



double* VertLineRange(Vector3d ZVec,Vector3d ShiftVec,Vector3d LineVec,Vector3d MarkerPt1Camera,double Theta0,Matrix2d AllPoints,double focal){
	Vector2d VecTh;VecTh[0] = -sin(Theta0);VecTh[1] = cos(Theta0);
	Vector2d Value = AllPoints*VecTh/focal;
	Vector3d VecRef = ShiftVec + MarkerPt1Camera;
	Vector3d TVec;TVec[0] = -sin(Theta0);TVec[1] = cos(Theta0);TVec[2] = -Value[0];

	double BetaS = -TVec.dot(VecRef)/TVec.dot(LineVec);
	TVec[2] = -Value[1];

	double BetaE = -TVec.dot(VecRef)/TVec.dot(LineVec);
	cout<<endl<<"BetaS = "<<BetaS<<"\n BetaE = "<<BetaE;
	
// Finding ThetaRange for Vertical Line Detection
	Vector3d StartPtRefVec = BetaS*LineVec + ShiftVec + MarkerPt1Camera;
	double AngleS = AngleEndPts(ZVec,StartPtRefVec,true,focal);
	cout<<endl<<"AngleS = "<<AngleS*180/PI<<endl;

	Vector3d EndPtRefVec = BetaE*LineVec + ShiftVec + MarkerPt1Camera;
	double AngleE = AngleEndPts(ZVec,EndPtRefVec,true,focal);
	cout<<endl<<"AngleE = "<<AngleE*180/PI<<endl;
	if(AngleE>AngleS){
		double Temp = AngleE;
		AngleE = AngleS;
		AngleS = Temp;}
	double *AngleVRange = (double *) malloc(sizeof(double) * 2);;
	AngleVRange[0] = AngleE-5*PI/180;
	AngleVRange[1] = AngleS+5*PI/180;
	cout<<"Angle Range for Vertical Lines = ("<<AngleVRange[0]*180/PI<<","<<AngleVRange[1]*180/PI<<")\n";
	return AngleVRange;
}


double AngleEndPts(Vector3d ZVec,Vector3d PtRefVec,bool flag,double focal){
	double Angle;
	// Don't generate sampled data, avoid SVD method, Use only 2 samples
	// Sample 1
	Vector3d TVec = PtRefVec;
	double X1 = TVec[0]*focal/TVec[2];double Y1 = TVec[1]*focal/TVec[2];
	TVec = PtRefVec + 50*ZVec;
	cout<<endl<<"(X1,Y1) = ("<<X1<<","<<Y1<<")\n";
	double X2 = TVec[0]*focal/TVec[2];double Y2 = TVec[1]*focal/TVec[2];
	cout<<endl<<"(X2,Y2) = ("<<X2<<","<<Y2<<")\n";
	
	Angle = atan((Y1 - Y2) / (X1- X2));
	cout<<endl;
	cout<<Angle<<endl;
//	Angle = PI/2 - Angle;
	Angle = PI/2 + Angle;

	cout<<Angle<<endl;
	if(flag == false){}
	else{
		if(Angle > PI/2){
		Angle = PI - Angle;
		}
	}
	cout<<Angle<<endl;
	return Angle;
}


