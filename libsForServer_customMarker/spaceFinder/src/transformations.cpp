#include <global_spaceFinder.h>

MatrixXd Transform2D_3D(MatrixXd InputPts,Vector3d inpZVec,Vector3d inpXVec,Vector3d OriginRef,double focal){
	double Qx,Qy,A1,B1,A2,B2,C1,C2,Alpha,Beta;
	MatrixXd OutPts(InputPts.rows(),2);
	for(int i=0;i<InputPts.rows();++i){
		Qx = InputPts(i,1)/focal;Qy = InputPts(i,0)/focal;
		A1 = inpZVec[0]-inpZVec[2]*Qx;A2 = inpZVec[1]-inpZVec[2]*Qy;
		B1 = inpXVec[0]-inpXVec[2]*Qx;B2 = inpXVec[1]-inpXVec[2]*Qy;
		C1 = -(OriginRef[0]-OriginRef[2]*Qx);
		C2 = -(OriginRef[1]-OriginRef[2]*Qy);
		Alpha = (B2*C1-B1*C2)/(B2*A1-B1*A2);
		Beta = (A2*C1-A1*C2)/(B1*A2-B2*A1);
		OutPts(i,0) = Alpha;OutPts(i,1) = Beta;
	}
	return OutPts;
}

