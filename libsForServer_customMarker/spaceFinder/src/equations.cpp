#include <global_spaceFinder.h>


using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

double Marker[4][2];
double focal;

//===========================================================================================
struct F1 {
  template <typename T> bool operator()(const T* const x1,
                                        const T* const x3,
                                        T* residual) const {
    // f1 = x1 - x3*Marker(1,1)/focal
    residual[0] = x1[0] - x3[0]*T(Marker[0][0]/focal);
    return true;
  }
};

struct F2 {
  template <typename T> bool operator()(const T* const x2,
                                        const T* const x3,
                                        T* residual) const {
    // f2 = x2 - x3*Marker(1,2)/focal
    residual[0] = x2[0] - x3[0]*T(Marker[0][1]/focal);
    return true;
  }
};

struct F3 {
  template <typename T> bool operator()(const T* const x4,
                                        const T* const x6,
                                        T* residual) const {
    // f3 = x4 - x6*Marker(2,1)/focal
    residual[0] = x4[0] - x6[0]*T(Marker[1][0]/focal);
    return true;
  }
};

struct F4 {
  template <typename T> bool operator()(const T* const x5,
                                        const T* const x6,
                                        T* residual) const {
    // f4 = x5 - x6*Marker(2,2)/focal
    residual[0] = x5[0] - x6[0]*T(Marker[1][1]/focal);
    return true;
  }
};



//////////////////////////////    Length and Breadth perpendicular                        
struct F5 {
  template <typename T> bool operator()(const T* const x4,
                                        const T* const x1,
                                        const T* const x10,
                                        const T* const x5,
                                        const T* const x2,
                                        const T* const x11,
                                        const T* const x6,
                                        const T* const x3,
                                        const T* const x12,
                                        T* residual) const {
    // f5 = x7 - x9*Marker(3,1)/focal
    //residual[0] = x7[0] - x9[0]*T(Marker[2][0]/focal);
    residual[0] = (x4[0]-x1[0])*(x10[0]-x1[0]) + (x5[0]-x2[0])*(x11[0]-x2[0])+ (x6[0]-x3[0])*(x12[0]-x3[0]);
    return true;
  }
};


////////////////    Side height
struct F6 {
  template <typename T> bool operator()(const T* const x10,
                                        const T* const x1,
                                        const T* const x11,
                                        const T* const x2,
                                        const T* const x12,
                                        const T* const x3,
                                        T* residual) const {
    // f6 = x8 - x9*Marker(3,2)/focal
    //residual[0] = x8[0] - x9[0]*T(Marker[2][1]/focal);
    residual[0] = (x10[0] - x1[0]) * (x10[0] - x1[0]) + (x11[0] - x2[0]) * (x11[0] - x2[0]) + (x12[0] - x3[0]) * (x12[0] - x3[0]) - T(73.48);
    return true;
  }
};

struct F7 {
  template <typename T> bool operator()(const T* const x10,
                                        const T* const x12,
                                        T* residual) const {
    // f7 = x10 - x12*Marker(4,1)/focal
    residual[0] = x10[0] - x12[0]*T(Marker[3][0]/focal);
    return true;
  }
};


struct F8 {
  template <typename T> bool operator()(const T* const x11,
                                        const T* const x12,
                                        T* residual) const {
    // f8 = x11 - x12*Marker(4,2)/focal
    residual[0] = x11[0] - x12[0]*T(Marker[3][1]/focal);
    return true;
  }
};


////////////     Diagonal midpoint
struct F9 {
  template <typename T> bool operator()(const T* const x1,
                                        const T* const x7,
                                        const T* const x4,
                                        const T* const x10,
                                        T* residual) const {
    // f9 = x(1) + x(7) - x(4) - x(10)
    residual[0] = x1[0] + x7[0] - x4[0] - x10[0];
    return true;
  }
};


////////////     Diagonal midpoint
struct F10 {
  template <typename T> bool operator()(const T* const x2,
                                        const T* const x8,
                                        const T* const x5,
                                        const T* const x11,
                                        T* residual) const {
    // f10 = x(2) + x(8) - x(5) - x(11)
    residual[0] = x2[0] + x8[0] - x5[0] - x11[0];
    return true;
  }
};

////////////     Diagonal midpoint
struct F11 {
  template <typename T> bool operator()(const T* const x3,
                                        const T* const x9,
                                        const T* const x6,
                                        const T* const x12,
                                        T* residual) const {
    // f11 = x(3) + x(9) - x(6) - x(12)
    residual[0] = x3[0] + x9[0] - x6[0] - x12[0];
    return true;
  }
};


/////////////////////side width
struct F12 {
  template <typename T> bool operator()(const T* const x4,
                                        const T* const x1,
                                        const T* const x5,
                                        const T* const x2,
                                        const T* const x6,
                                        const T* const x3,
                                        T* residual) const {
    // f12 = (x(4)-x(1))*(x(4)-x(1)) + (x(5)-x(2))*(x(5)-x(2)) + (x(6)-x(3))*(x(6)-x(3)) - 256]
    residual[0] = (x4[0] - x1[0]) * (x4[0] - x1[0]) + (x5[0] - x2[0]) * (x5[0] - x2[0]) + (x6[0] - x3[0]) * (x6[0] - x3[0]) - T(29.14);
    return true;
  }
};

//DEFINE_string(minimizer, "trust_region", "Minimizer type to use, choices are: line_search & trust_region");

double* getPoints(double FOCAL, double MARKER[4][2]) {
//  google::ParseCommandLineFlags(&argc, &argv, true);
//  google::InitGoogleLogging(argv[0]);

  double x1 =  1.0;
  double x2 = -1.0;
  double x3 =  100.0;
  double x4 =  1.0;
  double x5 =  1.0;
  double x6 =  100.0;
  double x7 =  1.0;
  double x8 =  1.0;
  double x9 =  100.0;
  double x10 =  1.0;
  double x11 =  1.0;
  double x12 =  100.0;
  
  focal = FOCAL;
  
  for(int i = 0 ; i< 4; i++){
  Marker[i][0] = MARKER[i][0];
  Marker[i][1] = MARKER[i][1];
  }

  Problem problem;
  // Add residual terms to the problem using the using the autodiff
  // wrapper to get the derivatives automatically. The parameters, x1 through
  // x4, are modified in place.
  problem.AddResidualBlock(new AutoDiffCostFunction<F1, 1, 1, 1>(new F1),
                           NULL,
                           &x1, &x3);
  problem.AddResidualBlock(new AutoDiffCostFunction<F2, 1, 1, 1>(new F2),
                           NULL,
                           &x2, &x3);
  problem.AddResidualBlock(new AutoDiffCostFunction<F3, 1, 1, 1>(new F3),
                           NULL,
                           &x4, &x6);
  problem.AddResidualBlock(new AutoDiffCostFunction<F4, 1, 1, 1>(new F4),
                           NULL,
                           &x5, &x6);
  problem.AddResidualBlock(new AutoDiffCostFunction<F5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1>(new F5),
                           NULL,
                           &x4, &x1, &x10, &x5, &x2, &x11, &x6, &x3, &x12);
  problem.AddResidualBlock(new AutoDiffCostFunction<F6, 1, 1, 1, 1, 1, 1, 1>(new F6),
                           NULL,
                           &x10, &x1, &x11, &x2, &x12, &x3);
  problem.AddResidualBlock(new AutoDiffCostFunction<F7, 1, 1, 1>(new F7),
                           NULL,
                           &x10, &x12);
  problem.AddResidualBlock(new AutoDiffCostFunction<F8, 1, 1, 1>(new F8),
                           NULL,
                           &x11, &x12);
  problem.AddResidualBlock(new AutoDiffCostFunction<F9, 1, 1, 1, 1, 1>(new F9),
                           NULL,
                           &x1, &x7, &x4, &x10);
  problem.AddResidualBlock(new AutoDiffCostFunction<F10, 1, 1, 1, 1, 1>(new F10),
                           NULL,
                           &x2, &x8, &x5, &x11);
  problem.AddResidualBlock(new AutoDiffCostFunction<F11, 1, 1, 1, 1, 1>(new F11),
                           NULL,
                           &x3, &x9, &x6, &x12);
  problem.AddResidualBlock(new AutoDiffCostFunction<F12, 1, 1, 1, 1, 1, 1, 1>(new F12),
                           NULL,
                           &x4, &x1, &x5, &x2, &x6, &x3);

  Solver::Options options;
  //LOG_IF(FATAL, !ceres::StringToMinimizerType(FLAGS_minimizer, &options.minimizer_type)) << "Invalid minimizer: " << FLAGS_minimizer << ", valid options are: trust_region and line_search.";

  options.max_num_iterations = 1000;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = true;

  cout << "Initial x1 = " << x1
            << ", x2 = " << x2
            << ", x3 = " << x3
            << ", x4 = " << x4
            << "\n";

  // Run the solver!
  Solver::Summary summary;
  Solve(options, &problem, &summary);

  cout << summary.FullReport() << "\n";
  cout << "Final x1 = " << x1
            << ", x2 = " << x2
            << ", x3 = " << x3
            << ", x4 = " << x4
            << ", x5 = " << x5
            << ", x6 = " << x6
            << ", x7 = " << x7
            << ", x8 = " << x8
            << ", x9 = " << x9
            << ", x10 = " << x10
            << ", x11 = " << x11
            << ", x12 = " << x12
            << "\n";
  static double x[12];
  x[0] = x1;
  x[1] = x2;
  x[2] = x3;
  x[3] = x4;
  x[4] = x5;
  x[5] = x6;
  x[6] = x7;
  x[7] = x8;
  x[8] = x9;
  x[9] = x10;
  x[10] = x11;
  x[11] = x12;
  return x;
}
