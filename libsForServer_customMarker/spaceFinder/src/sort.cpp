#include <global_spaceFinder.h>

void bubbleSort(vector <double>& Angle,vector <int>& indices){
	int Len = Angle.size();
	int temp2;double temp1;
	for(int ax=0;ax<Len;++ax){
		for(int sx=Len-1;sx>=ax+1;--sx){
			if(Angle[sx]<Angle[sx-1]){
				temp1 = Angle[sx];
				temp2 = indices[sx];
				Angle[sx] = Angle[sx-1];
				indices[sx] = indices[sx-1];
				Angle[sx-1] = temp1;
				indices[sx-1] = temp2;
			}
		}
	}
}
