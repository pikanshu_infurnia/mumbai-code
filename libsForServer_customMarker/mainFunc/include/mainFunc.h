#include "globalmainFunc.h"
struct ProjectedLine{
	double strtPt[2],endPt[2];
	double len;
	double slop;
};

void calcNewRotMat(double newRotMat[3][3],double rotMat1[3][3],double rotMat2[3][3]);
void calcNewTranslation(double newTranslation[3], double rotation1[3][3],double translation1[3],double translation2[3]);
void transMatSplitter(double transMat[4][4],double rotation[3][3],double translation[3]);
void getTransMat(double transMat[4][4],double rotation[3][3], double translation[3]);
void QuickSort(vector<ProjectedLine>& Segments, int const len);
vector<ProjectedLine> mergeProjectedLines(vector<ProjectedLine> &projectedSegments,double disThresh,int mode,bool featureMerging);

double** detectMarker(char imgName[],Mat &mRgb, double ZVec[3], char path_Thresholding[]);
vector<vector<double> > detectlineonWall(Mat &mGr,Mat &mRgb, double transMat[4][4], double baselineParameter[],double focal,char *imgName);
