#include "lineDetection.h"
#include "spaceFinder.h"
#include "thresholding.h"
#include "vertexVerification.h"


#include <GL/gl.h>
#include <GL/glut.h>
#include <AR/gsub.h>
#include <AR/video.h>
#include <AR/param.h>
#include <AR/ar.h>
#include <bits/stdc++.h>
#include <dirent.h>

using namespace std;

