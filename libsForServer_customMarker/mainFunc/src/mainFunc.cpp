//~ #include "lineDetection.h"
//~ #include "spaceFinder.h"
//~ #include "thresholding.h"
//~ #include "vertexVerification.h"
//~ 
//~ #include <stdio.h>
//~ #include <stdlib.h>
//~ #include <GL/gl.h>
//~ #include <GL/glut.h>
//~ #include <AR/gsub.h>
//~ #include <AR/video.h>
//~ #include <AR/param.h>
//~ #include <AR/ar.h>
//~ #include <bits/stdc++.h>
//~ #include <cstdlib>
//~ #include <cstdio>
//~ #include <cstring>
//~ #include <dirent.h>

#include "globalmainFunc.h"
#include "object.h"

#define COLLIDE_DIST 30000.0

using namespace std;

struct ProjectedLine{
	double strtPt[2],endPt[2];
	double len;
	double slop;
};

/* Object Data */
char            *model_name = "Data/object_data2";
ObjectData_T    *object;
int             objectnum;


static int             xsize, ysize;
static int             thresh = 100;
static int             count1 = 0;

static char           cparam_name[]    = "Data/camera_para.dat";
static ARParam         cparam;

static bool ARInitialised = false;


static char *get_buff( char *buf, int n, FILE *fp );
ObjectData_T *read_ObjData( char *name, int *objectnum);

void getMarkerVertices_2Dn3D(double x[12],double Marker[4][2],double transMat[4][4],int imWidth,int imHeight,double focal);
void rearranngeVertices(double vertices[4][2],int dir);
double callVerify(int imWidth, int imHeight,double focal, double transMat[4][4], double vertices[4][2], double ZVec[3]);

static void   init(void);
static void   cleanup(void);
static void   mainLoop(ARMarkerInfo    **marker_info,int *marker_num,ARUint8 *img);
static char* strCopy(char src[],int strt,int end);
double callVerify(int imWidth, int imHeight,double focal, double transMat[4][4], double vertices[4][2],int dir, double ZVec[3]);

///////////////////////////////////////// Relative Transformation variables ////////////////////////////////////////////

struct relativeTransMat{
	int id[2];
	double transMat[4][4];
};

extern vector<int> markerIdx_id;
extern vector<vector<relativeTransMat> > imageWiseElements;
extern vector<relativeTransMat> initialElements;
extern vector<relativeTransMat> intermediateElements;
extern vector<relativeTransMat> finalElements;
extern vector<vector<int> > extremePoints;
extern vector<vector<int> > baseVec; // stored as vectors : id S2 E2 (S1=S2=0)
extern vector<vector<vector<int> > > featureVec; // (3D points but y will be 0 since all points are on wall) so points will be stored as x1,z1,x2,z2
extern int intermediateMarkerRelationMatrix[12][12];
//~ extern int finalMarkerRelationArray[12];



////////////////////////////////////////////////////// Merging //////////////////////////////////////////////////////
void QuickSort(vector<ProjectedLine>& Segments, int const len)
{
  int const lenD = len;
  ProjectedLine pivot;
  int ind = lenD/2;
  int i,j = 0,k = 0;
  if(lenD>1){
	vector<ProjectedLine> L(lenD), R(lenD);
	pivot = Segments[ind];
	for(i=0;i<lenD;i++){
	  if(i!=ind){
		if(Segments[i].len > pivot.len){
		  L[j] = Segments[i];
		  j++;
		}
		else{
		  R[k] = Segments[i];
		  k++;
		}
	  }
	}
	QuickSort(L,j);
	QuickSort(R,k);
	for(int cnt=0;cnt<lenD;cnt++){
	  if(cnt<j){
		Segments[cnt] = L[cnt];
	  }
	  else if(cnt==j){
		Segments[cnt] = pivot;
	  }
	  else{
		Segments[cnt] = R[cnt-(j+1)];
	  }
	}
  }
}

vector<ProjectedLine> mergeProjectedLines(vector<ProjectedLine> &projectedSegments,double disThresh,int mode,bool featureMerging){
	vector<ProjectedLine> mergedProjectedSegments;
	int countSeg = projectedSegments.size();
	vector<bool> Done (countSeg,false);
	QuickSort(projectedSegments,countSeg);
	
	for(int i = 0; i < countSeg; i++){
		if(Done[i] != true){
			//~ double S1 = projectedSegments[i].strtPt[1], S2 = projectedSegments[i].strtPt[0];
			//~ double E1 = projectedSegments[i].endPt[1], E2 = projectedSegments[i].endPt[0];
			//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),1,8);
			//~ cout << "\n len : " << projectedSegments[i].len;
			//~ imshow("aisehi2",out);
			//~ waitKey(0);

			Done[i] = true;
			Point P1,P2;
			P1.x = projectedSegments[i].strtPt[0]; P1.y = projectedSegments[i].strtPt[1];
			P2.x = projectedSegments[i].endPt[0]; P2.y = projectedSegments[i].endPt[1];
			for(int j = i+1; j < countSeg; j++){
				if(Done[j] != true){
					Point pt;
					pt.x = (projectedSegments[j].strtPt[0] + projectedSegments[j].endPt[0])/2;
					pt.y = (projectedSegments[j].strtPt[1] + projectedSegments[j].endPt[1])/2;
					double len;
					double dis = distance(P1,P2,pt,len);
					bool flag = false;
					if(mode == 1){
						if(((projectedSegments[j].strtPt[0] - projectedSegments[i].strtPt[0])*(projectedSegments[j].strtPt[0] - projectedSegments[i].endPt[0])) < 0) flag = true;
						else if(((projectedSegments[j].endPt[0] - projectedSegments[i].strtPt[0])*(projectedSegments[j].endPt[0] - projectedSegments[i].endPt[0])) < 0) flag = true;
					}
					else if(mode == 2){
						if(((projectedSegments[j].strtPt[1] - projectedSegments[i].strtPt[1])*(projectedSegments[j].strtPt[1] - projectedSegments[i].endPt[1])) < 0) flag = true;
						if(((projectedSegments[j].endPt[1] - projectedSegments[i].strtPt[1])*(projectedSegments[j].endPt[1] - projectedSegments[i].endPt[1])) < 0) flag = true;
					}
					if(dis < disThresh && flag){
						Done[j] = true;
						//~ double max = -9999,min = 9999;
						double max,min = 9999;
						if(featureMerging) max = -9999;
						else max = -1;
						if(mode == 1){
							double arr[4] = {projectedSegments[j].strtPt[0],projectedSegments[j].endPt[0],projectedSegments[i].strtPt[0],projectedSegments[i].endPt[0]};
							for(int m = 0; m < 4; m++){
								if(max < arr[m]) max = arr[m];
								if(min > arr[m]) min = arr[m];
							}
							projectedSegments[i].strtPt[0] = min;
							projectedSegments[i].endPt[0] = max;
							projectedSegments[i].len = fabs(max-min);
						}
						if(mode == 2){
							double arr[4] = {projectedSegments[j].strtPt[1],projectedSegments[j].endPt[1],projectedSegments[i].strtPt[1],projectedSegments[i].endPt[1]};
							for(int m = 0; m < 4; m++){
								if(max < arr[m]) max = arr[m];
								if(min > arr[m]) min = arr[m];
							}
							projectedSegments[i].strtPt[1] = max;
							projectedSegments[i].endPt[1] = min;
							projectedSegments[i].len = fabs(max-min);
						}
					} 
				}
			}
			//~ S1 = projectedSegments[i].strtPt[1]; S2 = projectedSegments[i].strtPt[0];
			//~ E1 = projectedSegments[i].endPt[1]; E2 = projectedSegments[i].endPt[0];
			//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),1,8);
			//~ cout << "\n len : " << projectedSegments[i].len;
			//~ imshow("aisehi2",out);
			//~ waitKey(0);
			mergedProjectedSegments.push_back(projectedSegments[i]);
		}
		
	}
	return mergedProjectedSegments;
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void calcNewRotMat(double newRotMat[3][3],double rotMat1[3][3],double rotMat2[3][3]){
	for(int a = 0 ;a < 3; a++){
		for(int b = 0; b < 3; b++){
			newRotMat[a][b] = rotMat1[0][a]*rotMat2[0][b] + rotMat1[1][a]*rotMat2[1][b] + rotMat1[2][a]*rotMat2[2][b];
		}
	}
}
void calcNewTranslation(double newTranslation[3], double rotation1[3][3],double translation1[3],double translation2[3]){
	//~ newTranslation[0] = translation1[0]-(rotation1[0][0]*translation2[0] + rotation1[0][1]*translation2[1] + rotation1[0][2]*translation2[2]);
	//~ newTranslation[1] = translation1[1]-(rotation1[1][0]*translation2[0] + rotation1[1][1]*translation2[1] + rotation1[1][2]*translation2[2]);
	//~ newTranslation[2] = translation1[2]-(rotation1[2][0]*translation2[0] + rotation1[2][1]*translation2[1] + rotation1[2][2]*translation2[2]);
	
	newTranslation[0] = rotation1[0][0]*(translation2[0]-translation1[0]) + rotation1[1][0]*(translation2[1]-translation1[1]) + rotation1[2][0]*(translation2[2]-translation1[2]);
	newTranslation[1] = rotation1[0][1]*(translation2[0]-translation1[0])+rotation1[1][1]*(translation2[1]-translation1[1])+rotation1[2][1]*(translation2[2]-translation1[2]);
	newTranslation[2] = rotation1[0][2]*(translation2[0]-translation1[0])+rotation1[1][2]*(translation2[1]-translation1[1])+rotation1[2][2]*(translation2[2]-translation1[2]); 
}
void transMatSplitter(double transMat[4][4],double rotation[3][3],double translation[3]){
	for(int i = 0; i < 3; i++){
		rotation[i][0] = transMat[i][0]; rotation[i][1] = transMat[i][1];rotation[i][2] = transMat[i][2];
		translation[i] = transMat[i][3];
	}
}

void getTransMat(double transMat[4][4],double rotation[3][3], double translation[3]){
	for(int i = 0; i < 3; i++){
		transMat[i][0] = rotation[i][0]; transMat[i][1] = rotation[i][1];transMat[i][2] = rotation[i][2];
		transMat[i][3] = translation[i];
	}
	transMat[3][0] = transMat[3][1] = transMat[3][2] = 0 ;
	transMat[3][3] = 1;
}
//~ 
//~ void getIntermediateElements(){
	//~ double rotation1[3][3],rotation2[3][3],rotation3[3][3];
	//~ memset(rotation1,0,sizeof(rotation1[0][0])*3*3);
	//~ memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
	//~ memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
	//~ double translation1[3] = {0},translation2[3]={0},translation3[3] = {0};
	//~ 
	//~ for(int i = 0; i < initialElements.size(); i++){
		//~ if(initialElements[i].id[0] == initialElements[i].id[1]){
			//~ transMatSplitter(initialElements[i].transMat,rotation1,translation1);
			//~ int x = initialElements[i].id[0];
			//~ for(int j = i+1; j < initialElements.size(); j++){
				//~ if(initialElements[j].id[0] == initialElements[j].id[1]){
					//~ int y = initialElements[j].id[0];
					//~ if(intermediateMarkerRelationMatrix[x][y] == -1){
						//~ transMatSplitter(initialElements[j].transMat,rotation2,translation2);
						//~ calcNewRotMat(rotation3,rotation1,rotation2);
						//~ calcNewTranslation(translation3,rotation1,translation1,translation2);
						//~ relativeTransMat r;
						//~ r.id[0] = x; r.id[1] = y;
						//~ getTransMat(r.transMat,rotation3,translation3);
						//~ intermediateElements.push_back(r);
						//~ intermediateMarkerRelationMatrix[x][y] = intermediateElements.size() -1;
						//~ memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
						//~ memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
						//~ memset(translation2,0,sizeof(translation2[0]*3));
						//~ memset(translation3,0,sizeof(translation3[0]*3));
					//~ }
					//~ if(intermediateMarkerRelationMatrix[y][x] == -1){
						//~ transMatSplitter(initialElements[j].transMat,rotation2,translation2);
						//~ calcNewRotMat(rotation3,rotation2,rotation1);
						//~ calcNewTranslation(translation3,rotation2,translation2,translation1);
						//~ relativeTransMat r;
						//~ r.id[0] = y; r.id[1] = x;
						//~ getTransMat(r.transMat,rotation3,translation3);
						//~ intermediateElements.push_back(r);
						//~ intermediateMarkerRelationMatrix[y][x] = intermediateElements.size() -1;
						//~ memset(rotation2,0,sizeof(rotation2[0][0])*3*3);
						//~ memset(rotation3,0,sizeof(rotation3[0][0])*3*3);
						//~ memset(translation2,0,sizeof(translation2[0]*3));
						//~ memset(translation3,0,sizeof(translation3[0]*3));
					//~ }
				//~ }
			//~ }
			//~ memset(rotation1,0,sizeof(rotation1[0][0])*3*3);
			//~ memset(translation1,0,sizeof(translation1[0]*3));
		//~ }
	//~ }
//~ }

vector<vector<double> > detectlineonWall(Mat &mGr,Mat &mRgb, double transMat[4][4], double baselineParameter[2],double focal,char *imgName){
	
	// since the input image is half of the size of original image the focal used for this image linedetection will be halved
	// but transformation matrix is calculated assuming original image dimension and focal so getMarkerVertices will be using original focal and
	// imWidth,imHeight provided to this function will also be doubled
	double extremePt[2]; 
	vector<vector<int> > featureVecElement;
	double shiftLen;
	int mode = (int)baselineParameter[0];
	int markerId = baselineParameter[1];
	vector<int> baseLineInfo;
	//~ baseLineInfo.push_back(baselineParameter[1]);
	double x[12];
	double Marker[4][2];
	double imWidth = mRgb.cols,imHeight = mRgb.rows;
	double N = imWidth, M = imHeight;
	char str[200];
	
	sprintf(str,"debugging/Marker_%s.jpg",imgName);


	////////////////////////// Thresholds /////////////////////////////
	int Threshold = 40*1;
	float FillGap = 10*1;
	int PERCENTILE = 1;
	double Theta_Margin = 15 ,Rho_Margin= 8*1;
	int seg_gap_thresh = 25*4;
	double Threshold_Factor = (N)/(320);
	/////////////////////////////////////////////////////////////////////
	
	double S1,S2,E1,E2,SLOP;
	double RhoRes = 1;
	double ThetaRes = 1;
	vector<SCell> Segment;
	Mat out = Mat::zeros(500*2,500*2,CV_8UC1);


	getMarkerVertices_2Dn3D(x,Marker,transMat,imWidth*2,imHeight*2,focal);
	
	focal = focal/2;

	printf("\n 3D Marker coordinates : \n");
	for(int i = 0; i < 4; i++)
		printf(" %4.5f , %4.5f , %4.5f \n",x[i*3+0],x[i*3+1],x[i*3+2]);
		
	//~ baselineParameter[1] = (x[0*3+2]+x[1*3+2]+x[2*3+2]+x[3*3+2])/4;

	printf("\n 2D Marker coordinates : \n");
	for(int i = 0; i < 4; i++)
		printf(" %4.5f , %4.5f\n",Marker[i][0],Marker[i][1]);

	for(int i = 0;i<4;i++){
		//~ Marker[i][0] = (round(Marker[i][0])-1)/2; Marker[i][1] = (round(Marker[i][1])-1)/2; // marker coordinate adjust due to resizing of original image
		if((int)round(Marker[i][0]) % 2 != 0) Marker[i][0] = (round(Marker[i][0])-1)/2;
		else Marker[i][0] = round(Marker[i][0])/2;
		if((int)round(Marker[i][1]) % 2 != 0) Marker[i][1] = (round(Marker[i][1])-1)/2;
		else Marker[i][1] = round(Marker[i][1])/2;

		Marker[i][0] -= N/2; Marker[i][1] -= M/2; // marker coordinates modified according to image center
		printf("\nMarker Coordinates after shifting: %5f %5f \n",Marker[i][0],Marker[i][1]);
	}

	circle(mRgb,Point(round(Marker[0][0]+N/2),round(Marker[0][1]+M/2)),1,Scalar(255,0,0),1,8);
	circle(mRgb,Point(round(Marker[1][0]+N/2),round(Marker[1][1]+M/2)),1,Scalar(0,255,0),1,8);
	circle(mRgb,Point(round(Marker[2][0]+N/2),round(Marker[2][1]+M/2)),1,Scalar(0,0,255),1,8);
	circle(mRgb,Point(round(Marker[3][0]+N/2),round(Marker[3][1]+M/2)),1,Scalar(255,0,255),1,8);
	
	imwrite(str,mRgb);

	Vector3d XVec,YVec,ZVec,temp1,temp2,temp3,temp4;
	Vector3d MarkerPt1Camera;

	getMarkerAxes(x,XVec,YVec,ZVec,MarkerPt1Camera);

	Point PTX,PTY,PTZ,PT1,PT2;
	int vecLen = 500;

	temp1 = vecLen * XVec + MarkerPt1Camera;
	temp2 = vecLen * YVec + MarkerPt1Camera;
	temp3 = vecLen * ZVec + MarkerPt1Camera;

	PTX.x =round(temp1[0]*focal/temp1[2]+N/2); PTX.y = round(temp1[1]*focal/temp1[2]+M/2);
	PTY.x =round(temp2[0]*focal/temp2[2]+N/2); PTY.y = round(temp2[1]*focal/temp2[2]+M/2);
	PTZ.x =round(temp3[0]*focal/temp3[2]+N/2); PTZ.y = round(temp3[1]*focal/temp3[2]+M/2);

	PT1.x =round(MarkerPt1Camera[0]*focal/MarkerPt1Camera[2]+N/2); PT1.y = round(MarkerPt1Camera[1]*focal/MarkerPt1Camera[2]+M/2);

	printf("\nPTZ.y : %d PT1.y : %d\n" ,PTZ.y,PT1.y );


	if(PTZ.y > PT1.y){
		ZVec = -1 * ZVec;
		printf("\nWrong Solution\n");
		//~ imwrite(str1,mRgb);
	}
	else printf("\nRight Solution\n");

	line(mRgb,PT1,PTX,Scalar(0,255,0),1,8);
	line(mRgb,PT1,PTY,Scalar(255,0,0),1,8);
	line(mRgb,PT1,PTZ,Scalar(0,0,255),1,8);
	circle(mRgb,PT1,1,Scalar(0,0,255),1,8);
	//////////////////////////////////// vertex plotting//////////////
	//~ PT2.x = round(x[6]*focal/x[8]+N/2); PT1.y = round(x[7]*focal/x[8]+M/2);
	//~ circle(mRgb,Point(round(x[6]*focal/x[8]+N/2),round(x[7]*focal/x[8]+M/2)),1,Scalar(255,0,0),1,8);
	//~ circle(mRgb,Point(round(Marker[0][0]+N/2),round(Marker[0][1]+M/2)),2,Scalar(255,0,0),1,8);
	//~ circle(mRgb,Point(round(Marker[1][0]+N/2),round(Marker[1][1]+M/2)),2,Scalar(0,255,0),1,8);
	//~ circle(mRgb,Point(round(Marker[2][0]+N/2),round(Marker[2][1]+M/2)),2,Scalar(0,0,255),1,8);
	//~ circle(mRgb,Point(round(Marker[3][0]+N/2),round(Marker[3][1]+M/2)),2,Scalar(255,0,255),1,8);
	/////////////////////////////////////////////////////////////////////////////////////////////

	double* AngleHRange;

	AngleHRange = HorzLineRange(XVec,ZVec,MarkerPt1Camera,M,focal);

	double Theta0 =(AngleHRange[0]*180/PI + AngleHRange[1]*180/PI )/2;
	int ThetaRng =(int)(abs(AngleHRange[0]*180/PI) >= abs(AngleHRange[1]*180/PI) ? AngleHRange[0]*180/PI : AngleHRange[1]*180/PI)-Theta0;

	printf("\nAngle Range is %5f to %5f", AngleHRange[0]*180/PI,AngleHRange[1]*180/PI);
	int Margin = 5;

//	vector<SCell> FinalSegments =GetLineSeg(mGr,Theta0,ThetaRng,Margin,"output.png",Segment,Threshold,FillGap,RhoRes,ThetaRes,PERCENTILE);
	vector<SCell> FinalSegments =GetLineSeg(mGr,Theta0,ThetaRng,Margin,"output.png",Segment,Threshold/**Threshold_Factor*/,FillGap/**Threshold_Factor*/,RhoRes,ThetaRes,PERCENTILE);
	//(Theta0,abs(ThetaRng),Margin,argv);
	printf("\nThe size of Segment is %d",(int) Segment.size());
	printf("\nThe size of Final segments is %d and thresholdFactor : %f",(int)	 FinalSegments.size(),Threshold_Factor);


	//~ imwrite("/home/pika/Infurnia/libsForServer/Feature1.jpg",mRgb);

	if(FinalSegments.size() != 0){
		quickSort(FinalSegments, FinalSegments.size());
		//~ plotSeg(FinalSegments,mRgb,255,0,0);
		//~ plotSeg(Segment,mRgb,0,255,0);

		vector<double> SegLen,dis;
		double Rho0;

		vector<int> done_horizontal(FinalSegments.size(),0);
		vector<int> done_Segments(Segment.size(),0);
		vector<SCell> Line;
		vector<SCell> Seg_in_range;
	
		joinSeg_modified(Line,FinalSegments,Segment,done_horizontal,Theta_Margin,Rho_Margin/**Threshold_Factor*/,seg_gap_thresh/**Threshold_Factor*/,Seg_in_range,1,0);
		printf("\nThe size of Line is %d", (int)Line.size());
		//~ plotSeg(Line,mRgb,0,0,255);
		//~ imwrite("debug.png",mRgb);
		quickSort(Line,Line.size());
		

		vector<SCell> Line_for_raw_Segments;
	
		joinSeg_modified(Line_for_raw_Segments,Line,Segment,done_Segments,Theta_Margin,Rho_Margin/**Threshold_Factor*/,seg_gap_thresh/**Threshold_Factor*/,Seg_in_range,1,1);
		//~ plotSeg(Line_for_raw_Segments,mRgb,0,0,255);
		
		double lenThresh = 300 ;

		Point pt;
		pt.x = (Marker[0][0]+Marker[1][0]+N)/2; pt.y = (Marker[0][1]+Marker[1][1]+M)/2;

		S1 = Marker[0][1],S2 = Marker[0][0];
		E1 = Marker[1][1],E2 = Marker[1][0];
		SLOP = atan((S1 - E1)/(S2 - E2));
	
		SCell mainBaseSegment = nearestSeg(Line_for_raw_Segments,SegLen,dis,pt,lenThresh,SLOP);
		
		vector<SCell> baseline;
		baseline.push_back(mainBaseSegment);
		//~ plotSeg(baseline,mRgb,255,0,0);
		baselineParameter[0] = mainBaseSegment.rho_new;
		//~ baselineParameter[1] = mainBaseSegment.slop;
		
		double slop_baseline ;

		S1 = mainBaseSegment.StartPt[0],S2 = mainBaseSegment.StartPt[1];
		E1 = mainBaseSegment.EndPt[0],E2 = mainBaseSegment.EndPt[1];
		slop_baseline = atan((round(S1) - round(E1))/(round(S2) - round(E2)));

		Matrix2d AllPoints;

		AllPoints(0,0) = mainBaseSegment.StartPt[1]-N/2;AllPoints(0,1) = mainBaseSegment.StartPt[0]-M/2;
		AllPoints(1,0) = mainBaseSegment.EndPt[1]-N/2;AllPoints(1,1) = mainBaseSegment.EndPt[0]-M/2;

		Vector3d LineVec,LinePVec,ShiftVec;
		
		Rho0=mainBaseSegment.rho_new ;
		double Rho1,Rho2;
		Rho1 = S2*cos(slop_baseline- PI/2)+S1*sin(slop_baseline- PI/2);
		Rho2 = S2*cos(slop_baseline+ PI/2)+S1*sin(slop_baseline+ PI/2);
		Theta0 = fabs(Rho1-Rho0)>fabs(Rho2-Rho0)?(slop_baseline+ PI/2):(slop_baseline- PI/2);
		//~ Theta0=slop_baseline- PI/2;// mainBaseSegment.slop;
		cout << "\nRho 1: " << S2*cos(Theta0)+S1*sin(Theta0) << endl;
		cout << "\nRho 2: " << S2*cos(Theta0+PI)+S1*sin(Theta0+PI) << endl;
	//	Theta0 = mainBaseSegment.slop;
		

		printf("\nRho %f Theta %f slop_baseline %f", Rho0,Theta0,slop_baseline);

		//~ cout << "\n\n getWallAxes : \n\n";
		//~ getWallAxes(XVec,YVec,ZVec,MarkerPt1Camera,Rho0,Theta0,LineVec,LinePVec,ShiftVec,M,N,focal);
		cout << "\n\n GetWallAxes : \n\n";
		GetWallAxes(XVec,YVec,ZVec,MarkerPt1Camera,Rho0,Theta0,LineVec,LinePVec,ShiftVec,M,N,focal);
		//~ cout<<"LinePvec : " << LineVec.cross(ZVec) << endl;
		//~ printf("\n XVec.YVec : %5f LineVec.LinePVec : %5f XVec.LineVec %f LinePVec.YVec %f" , XVec.dot(YVec),LineVec.dot(LinePVec),XVec.dot(LineVec),LinePVec.dot(YVec));

		if(LineVec.dot(XVec) < 0){LineVec = -1 * LineVec;LinePVec = LineVec.cross(ZVec);}
		shiftLen = sqrt(ShiftVec[0]*ShiftVec[0]+ShiftVec[1]*ShiftVec[1]+ShiftVec[2]*ShiftVec[2]);
		temp3 = MarkerPt1Camera + ShiftVec;
		temp1 = vecLen * LineVec + temp3;
		temp2 = vecLen * LineVec.cross(ZVec) + temp3;
		temp4 = vecLen *ZVec + temp3;
		
		transMat[0][0] = LineVec[0]; transMat[0][1] = -LinePVec[0]; transMat[0][3] = temp3[0];
		transMat[1][0] = LineVec[1]; transMat[1][1] = -LinePVec[1]; transMat[1][3] = temp3[1];
		transMat[2][0] = LineVec[2]; transMat[2][1] = -LinePVec[2]; transMat[2][3] = temp3[2];
		
		PTX.x =round(temp1[0]*focal/temp1[2]+N/2); PTX.y = round(temp1[1]*focal/temp1[2]+M/2);
		PTY.x =round(temp2[0]*focal/temp2[2]+N/2); PTY.y = round(temp2[1]*focal/temp2[2]+M/2);
		PT1.x =round(temp3[0]*focal/temp3[2]+N/2); PT1.y = round(temp3[1]*focal/temp3[2]+M/2);
		PTZ.x =round(temp4[0]*focal/temp4[2]+N/2); PTZ.y = round(temp4[1]*focal/temp4[2]+M/2);
		
		
		line(mRgb,PT1,PTX,Scalar(0,255,0),1,8);
		line(mRgb,PT1,PTY,Scalar(255,0,0),1,8);
		line(mRgb,PT1,PTZ,Scalar(0,0,255),1,8);
		circle(mRgb,PT1,1,Scalar(0,0,255),1,8);
		printf("%\n\n Baseline Reference Point (%d,%d)\n\n ",PT1.x,PT1.y);
		//~ imwrite("feature.jpg",mRgb);
		///////////////////////////// VERTICAL LINES////////////////////////////
		double* AngleVRange;
		AngleVRange = VertLineRange(ZVec,ShiftVec,LineVec,MarkerPt1Camera,Theta0,AllPoints,focal);
		ThetaRng =(int)(abs(AngleVRange[0]*180/PI) >= abs(AngleVRange[1]*180/PI) ? abs(AngleVRange[0]*180/PI) : abs(AngleVRange[1]*180/PI));
		Margin = 5;
		Theta0 = 0;
		vector<SCell> FinalSegments_V = GetLineSeg(mGr,Theta0,ThetaRng,Margin,"output.png",Segment,Threshold/**Threshold_Factor*/,FillGap/**Threshold_Factor*/,RhoRes,ThetaRes,PERCENTILE);
		
		if(FinalSegments_V.size() != 0){
			//~ plotSeg(FinalSegments_V,mRgb,255,0,0);
			vector<int> done_vertical(FinalSegments_V.size(),0);
			Line.clear();
			joinSeg_modified(Line,FinalSegments_V,Segment,done_vertical,Theta_Margin,Rho_Margin/**Threshold_Factor*/,seg_gap_thresh/**Threshold_Factor*/,Seg_in_range,0,0);
			if(Line.size() != 0){
				vector<int> done_Segments_V(Segment.size(),0);
				joinSeg_modified(Line_for_raw_Segments,Line,Segment,done_Segments_V,Theta_Margin,Rho_Margin/**Threshold_Factor*/,seg_gap_thresh/**Threshold_Factor*/,Seg_in_range,0,1);
				plotSeg(Line_for_raw_Segments,mRgb,0,0,255);
				imwrite("feature.jpg",mRgb);
				MatrixXd StartPts((Line_for_raw_Segments.size())+2,2),EndPts((Line_for_raw_Segments.size())+2,2);
				int it;
				for(it = 0 ; it < Line_for_raw_Segments.size();it++){
					StartPts(it,0) = Line_for_raw_Segments[it].StartPt[0]-M/2;	StartPts(it,1) = Line_for_raw_Segments[it].StartPt[1]-N/2;
					EndPts(it,0) = Line_for_raw_Segments[it].EndPt[0]-M/2;	EndPts(it,1) = Line_for_raw_Segments[it].EndPt[1]-N/2;
				}
				StartPts(it,0) = PT1.y - M/2;	StartPts(it,1) = PT1.x - N/2;
				EndPts(it,0) = PT1.y - M/2;	EndPts(it,1) = PT1.x - N/2;
				it++;
				StartPts(it,1) = mainBaseSegment.StartPt[1]-N/2;StartPts(it,0) = mainBaseSegment.StartPt[0]-M/2;
				EndPts(it,1) = mainBaseSegment.EndPt[1]-N/2;EndPts(it,0) = mainBaseSegment.EndPt[0]-M/2;

				it++;
				Vector3d OriginRef = MarkerPt1Camera + ShiftVec;
				MatrixXd StartPt3D = Transform2D_3D(StartPts,ZVec,LineVec,OriginRef,focal);
				MatrixXd EndPt3D = Transform2D_3D(EndPts,ZVec,LineVec,OriginRef,focal);
				PT1.x = StartPt3D(it-1,1)/10+1000/2; PT1.y = -StartPt3D(it-1,0)/10 + 1000/2;
				printf("\n main reference point (%d,%d)",PT1.y,PT1.x);
				printf("\nvalue of it is %d",it);
				
				double count_H = 0,count_V = 0;

				vector<ProjectedLine> projectedHSegments;
				vector<ProjectedLine> projectedVSegments;
				ProjectedLine p;
				
				out = 255 - out;
				if(mode == 0){
					int M = 500*2;
					int N = 500*2;

					S2 = (StartPt3D(it-1,1)/10+N/2); E2 = (EndPt3D(it-1,1)/10+N/2);
					extremePt[0] = S2 > E2 ? E2:S2;
					extremePt[1] = S2 > E2 ? S2:E2;
					printf("\n\nExtreme points : (%f,%f),(%f,%f)",extremePt[0],extremePt[1],round(S2),round(E2));
					S2 = (StartPt3D(it-2,1)/10+N/2); E2 = (EndPt3D(it-2,1)/10+N/2);
					printf("\n\nExtreme points : (%f,%f),(%f,%f)",extremePt[0],extremePt[1],S2,E2);

					baseLineInfo.push_back(baselineParameter[1]);
					for(int j = it-2 ;j<it;j++){
						S1 = (-StartPt3D(j,0)/10+M/2);S2 = (StartPt3D(j,1)/10+N/2);
						E1 = (-EndPt3D(j,0)/10+M/2);E2 = (EndPt3D(j,1)/10+N/2);
						if(S2 != E2) {
							SLOP = atan((S1 - E1)/(S2 - E2));
							if(SLOP < -1){ SLOP += PI;}
							if(((SLOP > (0-5*PI/180)) && (SLOP < (0+5*PI/180))) && (round(S1) < 505 && round(E1) < 505)){
								//~ temp_stickyArray[j*3] = (S1+E1)/2-PT1.y; temp_stickyArray[1+j*3] = abs(S2 - E2); temp_stickyArray[j*3+2] = 0;
								count_H++;
								//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),1,8);
								if(S2 < E2){
									baseLineInfo.push_back(((int)round(S2) -500)*10);
									baseLineInfo.push_back(((int)round(E2) -500)*10);
									p.strtPt[0] = S2;
									p.endPt[0] = E2;
								}
								else{
									baseLineInfo.push_back(((int)round(E2) -500)*10);
									baseLineInfo.push_back(((int)round(S2) -500)*10);
									p.strtPt[0] = E2;
									p.endPt[0] = S2;
								}

								p.strtPt[1] = (S1+E1)/2;
								p.endPt[1] = (S1+E1)/2;
								p.slop = SLOP;
								p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
								line(out,Point(round(S2)/1,round(p.strtPt[1])/1),Point(round(E2)/1,round(p.endPt[1])/1),Scalar(0,255,0),1,8);
								projectedHSegments.push_back(p);
								printf("\n\nProjection End points : (%f,%f),(%f,%f)",round(S2),round(p.strtPt[1]),round(E2)/1,round(p.endPt[1]));
							}
							else if(((SLOP > (PI/2-5*PI/180)) && (SLOP < (PI/2+5*PI/180))) && (round(S1) < 505 && round(E1) < 505)){
								//~ temp_stickyArray[j*3] = (S2+E2)/2-PT1.x; temp_stickyArray[1+j*3] = abs(S1 - E1); temp_stickyArray[j*3+2] = 1;
								p.strtPt[0] = (S2+E2)/2; p.strtPt[1] = S1;
								p.endPt[0] = (S2+E2)/2; p.endPt[1] = E1;
								p.slop = SLOP;
								p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
								projectedVSegments.push_back(p);
								count_V++;
								//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),1,8);
								line(out,Point(round(p.strtPt[0])/1,round(S1)/1),Point(round(p.endPt[0])/1,round(E1)/1),Scalar(0,255,0),1,8);
							}
							else{
								//~ temp_stickyArray[j*3] = 99; temp_stickyArray[1+j*3] = 99; temp_stickyArray[j*3+2] = 2;
							}

						}
						else{
							//~ temp_stickyArray[j*3] = (S2+E2)/2-PT1.x; temp_stickyArray[1+j*3] = abs(S1 - E1); temp_stickyArray[j*3+2] = 1;
							p.strtPt[0] = (S2+E2)/2; p.strtPt[1] = S1;
							p.endPt[0] = (S2+E2)/2; p.endPt[1] = E1;
							p.slop = SLOP;
							p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
							projectedVSegments.push_back(p);

							count_V++;
							//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),3,8);
							line(out,Point(round(p.strtPt[0])/1,round(S1)/1),Point(round(p.endPt[0])/1,round(E1)/1),Scalar(0,255,0),5,8);
						}
					}
					baseLineInfo.push_back((int)shiftLen);
					baseVec.push_back(baseLineInfo);
				}
				if(mode == 1 || mode ==0){
					int M = 500*2;
					int N = 500*2;

					S2 = (StartPt3D(it-1,1)/10+N/2); E2 = (EndPt3D(it-1,1)/10+N/2);
					extremePt[0] = S2 > E2 ? E2:S2;
					extremePt[1] = S2 > E2 ? S2:E2;
					//~ printf("\n\nExtreme points : (%f,%f),(%f,%f)",extremePt[0],extremePt[1],S2,E2);
					S2 = (StartPt3D(it-2,1)/10+N/2); E2 = (EndPt3D(it-2,1)/10+N/2);
					//~ printf("\n\nExtreme points : (%f,%f),(%f,%f)",extremePt[0],extremePt[1],S2,E2);
					//~ baseLineInfo.push_back(baselineParameter[1]);
					for(int j = 0 ;j<it-2;j++){
						baseLineInfo.clear();
						S1 = (-StartPt3D(j,0)/10+M/2);S2 = (StartPt3D(j,1)/10+N/2);
						E1 = (-EndPt3D(j,0)/10+M/2);E2 = (EndPt3D(j,1)/10+N/2);
						if(S2 != E2) {
							SLOP = atan((S1 - E1)/(S2 - E2));
							if(SLOP < -1){ SLOP += PI;}
							if(((SLOP > (0-5*PI/180)) && (SLOP < (0+5*PI/180))) && (round(S1) < 505 && round(E1) < 505)){
								//~ temp_stickyArray[j*3] = (S1+E1)/2-PT1.y; temp_stickyArray[1+j*3] = abs(S2 - E2); temp_stickyArray[j*3+2] = 0;
								count_H++;
								//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),1,8);
								
								if(S2 < E2){
									p.strtPt[0] = S2;
									p.endPt[0] = E2;
								}
								else{
									p.strtPt[0] = E2;
									p.endPt[0] = S2;
								}
								p.strtPt[1] = (S1+E1)/2;
								p.endPt[1] = (S1+E1)/2;
								p.slop = SLOP;
								p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
								//~ line(out,Point(round(S2)/1,round(p.strtPt[1])/1),Point(round(E2)/1,round(p.endPt[1])/1),Scalar(0,255,0),1,8);
								projectedHSegments.push_back(p);
								//~ printf("\n\nProjection End points : (%f,%f),(%f,%f)",extremePt[0],extremePt[1],round(E2)/1,round(p.endPt[1]));
								//~ printf("\n\nProjection End points : (%f,%f),(%f,%f)",S2,S1,E2,E1);
								//~ if(S2 > extremePt[1]+10) S2 = extremePt[1];
								//~ else if(S2 < extremePt[0]-10) S2 = extremePt[0];
								//~ if(E2 > extremePt[1]+10) E2 = extremePt[1];
								//~ else if(E2 < extremePt[0]-10) E2 = extremePt[0];
//~ 
								//~ printf("\n\nExtreme points : (%f,%f),(%f,%f)",extremePt[0],extremePt[1],S2,E2);
								
								//~ baseLineInfo.push_back(((int)round(S2) -500)*10);
								//~ baseLineInfo.push_back(((int)round(p.strtPt[1]) -500)*-10);
								//~ baseLineInfo.push_back(((int)round(E2) -500)*10);
								//~ baseLineInfo.push_back(((int)round(p.endPt[1]) -500)*-10);
								//~ featureVecElement.push_back(baseLineInfo);

							}
							else if(((SLOP > (PI/2-5*PI/180)) && (SLOP < (PI/2+5*PI/180))) && (round(S1) < 500 || round(E1) < 500)){
								if(round(S1) > 500) S1 = 500.0;
								if(round(E1) > 500) E1 = 500.0;
								//~ temp_stickyArray[j*3] = (S2+E2)/2-PT1.x; temp_stickyArray[1+j*3] = abs(S1 - E1); temp_stickyArray[j*3+2] = 1;
								p.strtPt[0] = (S2+E2)/2;
								p.endPt[0] = (S2+E2)/2;

								if(S1 < E1){
									p.strtPt[1] = S1;
									p.endPt[1] = E1;
								}
								else{
									p.strtPt[1] = E1;
									p.endPt[1] = S1;
								}
								p.slop = SLOP;
								p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
								projectedVSegments.push_back(p);
								count_V++;
								//~ line(out,Point(round(p.strtPt[0])/1,round(S1)/1),Point(round(p.endPt[0])/1,round(E1)/1),Scalar(0,255,0),1,8);
								
								//~ if(S2 > extremePt[1]) S2 = extremePt[1];
								//~ if(S2 < extremePt[0]) S2 = extremePt[0];
								//~ if(E2 > extremePt[1]) E2 = extremePt[1];
								//~ if(E2 < extremePt[0]) E2 = extremePt[0];
								//~ if(p.strtPt[0] < extremePt[0]-10 || p.strtPt[0] > extremePt[1]+10) continue;
								
								//~ baseLineInfo.push_back(((int)round(p.strtPt[0]) -500)*10);
								//~ baseLineInfo.push_back(((int)round(S1) -500)*-10);
								//~ baseLineInfo.push_back(((int)round(p.endPt[0]) -500)*10);
								//~ baseLineInfo.push_back(((int)round(E1) -500)*-10);
								//~ featureVecElement.push_back(baseLineInfo);
								
								//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),1,8);

							}
							else{
								//~ temp_stickyArray[j*3] = 99; temp_stickyArray[1+j*3] = 99; temp_stickyArray[j*3+2] = 2;
							}

						}
						else{
							//~ temp_stickyArray[j*3] = (S2+E2)/2-PT1.x; temp_stickyArray[1+j*3] = abs(S1 - E1); temp_stickyArray[j*3+2] = 1;
							p.strtPt[0] = (S2+E2)/2; p.strtPt[1] = S1;
							p.endPt[0] = (S2+E2)/2; p.endPt[1] = E1;
							p.slop = SLOP;
							p.len = sqrt((S1-E1)*(S1-E1)+(S2-E2)*(S2-E2));
							projectedVSegments.push_back(p);

							count_V++;
							//~ line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),3,8);
							//~ line(out,Point(round(p.strtPt[0])/1,round(S1)/1),Point(round(p.endPt[0])/1,round(E1)/1),Scalar(0,255,0),5,8);
						}
					}
					
					vector<ProjectedLine> mergedProjectedHSegments = mergeProjectedLines(projectedHSegments,7,1,false);
					vector<ProjectedLine> mergedProjectedVSegments = mergeProjectedLines(projectedVSegments,7,2,false);

					if(mode == 1){
						int MIN,MAX;
						for(int Bt = 0; Bt < extremePoints.size(); Bt++){
							if(extremePoints[Bt][0] == markerId){
								MIN = extremePoints[Bt][1]; MAX = extremePoints[Bt][2];
								//~ found = true;
								break;
							}
						}
						extremePt[0] = extremePt[0] > MIN ? MIN:extremePt[0];
						extremePt[1] = extremePt[1] < MAX ? MAX:extremePt[1];
						for(int A = 0 ; A < mergedProjectedHSegments.size(); A++){
							baseLineInfo.clear();
							S1 = mergedProjectedHSegments[A].strtPt[1]; S2 = mergedProjectedHSegments[A].strtPt[0];
							E1 = mergedProjectedHSegments[A].endPt[1];  E2 = mergedProjectedHSegments[A].endPt[0];
							line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),5,8);
							if(S2 > extremePt[1]+10) S2 = extremePt[1];
							else if(S2 < extremePt[0]-10) S2 = extremePt[0];
							if(E2 > extremePt[1]+10) E2 = extremePt[1];
							else if(E2 < extremePt[0]-10) E2 = extremePt[0];
							int B = abs(((int)round(S1*100)-50000)/10) < 10 ? 0 : ((int)round(S1*100)-50000)/10; 
							baseLineInfo.push_back(((int)round(S2*100)-50000)/10);
							baseLineInfo.push_back(-B);
							baseLineInfo.push_back(((int)round(E2*100)-50000)/10);
							baseLineInfo.push_back(-B);
							//~ featureVecElement.push_back(baseLineInfo);
							if(markerId != 3) featureVec[markerId].push_back(baseLineInfo);
							printf("\nProjection End points H: (%d,%d),(%d,%d)",(((int)round(S2*100)-50000)/10),B,(((int)round(E2*100)-50000)/10),B);
						}
						
						for(int A = 0 ; A < mergedProjectedVSegments.size(); A++){
							baseLineInfo.clear();
							S1 = mergedProjectedVSegments[A].strtPt[1]; S2 = mergedProjectedVSegments[A].strtPt[0];
							E1 = mergedProjectedVSegments[A].endPt[1];  E2 = mergedProjectedVSegments[A].endPt[0];
							line(out,Point(round(S2)/1,round(S1)/1),Point(round(E2)/1,round(E1)/1),Scalar(0,255,0),5,8);
							if(S2 < extremePt[0]-10 || E2 > extremePt[1]+10) continue;
							baseLineInfo.push_back(((int)round(S2) -500)*10);
							baseLineInfo.push_back(((int)round(S1) -500)*-10);
							baseLineInfo.push_back(((int)round(E2) -500)*10);
							baseLineInfo.push_back(((int)round(E1) -500)*-10);
							//~ featureVecElement.push_back(baseLineInfo);
							if(markerId != 3) featureVec[markerId].push_back(baseLineInfo);
							printf("\nProjection End points V: (%d,%d),(%d,%d)",(((int)round(S2*100)-50000)/10),(((int)round(S1) -500)*-10),(((int)round(E2*100)-50000)/10),(((int)round(E1) -500)*-10));
						}
						
						cout << "\n Compare : " << featureVec[markerId].empty();
						cout << "\n Compare : " << featureVec[markerId].size();// << " " << featureVecElement.size() << " " << featureVecElement[0].size();
					}
					if(mode == 0){
						vector<int> vect(3);
						int MIN = 99999,MAX = -99999;
						int Bt;
						bool found = false;
						if(!extremePoints.empty()){
							for(Bt = 0; Bt < extremePoints.size(); Bt++){
								if(extremePoints[Bt][0] == markerId){
									MIN = extremePoints[Bt][1]; MAX = extremePoints[Bt][2];
									found = true;
									break;
								}
							}
						}
						if(found == false) vect[0] = markerId;
						for(int A = 0 ; A < mergedProjectedHSegments.size(); A++){
							baseLineInfo.clear();
							S1 = mergedProjectedHSegments[A].strtPt[1]; S2 = mergedProjectedHSegments[A].strtPt[0];
							E1 = mergedProjectedHSegments[A].endPt[1];  E2 = mergedProjectedHSegments[A].endPt[0];
							extremePt[0] = S2 > E2 ? E2:S2;
							extremePt[1] = S2 > E2 ? S2:E2;
							
							//~ if(S2 > extremePt[1]+10) S2 = extremePt[1];
							//~ else if(S2 < extremePt[0]-10) S2 = extremePt[0];
							//~ if(E2 > extremePt[1]+10) E2 = extremePt[1];
							//~ else if(E2 < extremePt[0]-10) E2 = extremePt[0];
							int B = abs(((int)round(S1*100)-50000)/10) < 10 ? 0 : ((int)round(S1*100)-50000)/10; 
							baseLineInfo.push_back(((int)round(S2*100)-50000)/10);
							baseLineInfo.push_back(-B);
							baseLineInfo.push_back(((int)round(E2*100)-50000)/10);
							baseLineInfo.push_back(-B);
							if(abs(B) < 130){
								if(found){
									extremePoints[Bt][1] = MIN = extremePt[0] < MIN ? extremePt[0]:MIN;
									extremePoints[Bt][2] = MAX = extremePt[1] > MAX ? extremePt[1]:MAX;
								}
								else {
									vect[1] = MIN = extremePt[0] < MIN ? extremePt[0]:MIN;
									vect[2] = MAX = extremePt[1] > MAX ? extremePt[1]:MAX;
								}

							}
						}
						if(!found) extremePoints.push_back(vect);

					}
					//~ if(featureVec[markerId].empty()) featureVec[markerId] = featureVecElement;
					//~ else if(featureVec[markerId].size() < featureVecElement.size()) featureVec[markerId] = featureVecElement;
					
					
				}
				
				imwrite("projection.jpg",out);
				S1 = mainBaseSegment.StartPt[0],S2 = mainBaseSegment.StartPt[1];
				E1 = mainBaseSegment.EndPt[0],E2 = mainBaseSegment.EndPt[1];

				slop_baseline = atan((round(S1) - round(E1))/(round(S2) - round(E2)));

				S1 = Marker[0][1]+M/2,S2 = Marker[0][0]+N/2;
				E1 = Marker[1][1]+M/2,E2 = Marker[1][0]+N/2;

				SLOP = atan((round(S1) - round(E1))/(round(S2) - round(E2)));

				baselineParameter[0] = SLOP - slop_baseline;
				baselineParameter[1] = -acos(XVec.dot(LineVec));

				//~ temp_stickyArray[i*3] = count_H;temp_stickyArray[i*3+1] = count_V;
				printf("\nvalue of count_H and count_V is %f & %f",count_H,count_V);
				//~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"value of count_H and count_V is %f & %f",temp_stickyArray[i*3],temp_stickyArray[i*3+1]);

				//~ printf("\nbefore  Calculating slop difference between baseline and marker XVec");
				//~ baselineParameter[0] = -acos(XVec.dot(LineVec));
				//~ printf("\nanglediff3D : %f 3Ddistance of Marker center from camera : %f ",baselineParameter[0],baselineParameter[1]);
				printf("\nslopBaseline : %f slopMarker : %f slopDiff : %f anglediff3D : %f \n",slop_baseline,SLOP,baselineParameter[0],baselineParameter[1]);
				/////////////////////// adding main reference point on the wall for projection
			}

		}
		//~ imwrite("/home/pika/Infurnia/libsForServer/Feature1.jpg",mRgb);
	}
	//~ char str[200];
	str[0] = '\0';
	sprintf(str,"/home/pika/Infurnia/libsForServer_customMarker/outputImage1/Feature_%s_%d",imgName,markerId);
	cout<<"\nFeature Image: " << str<<endl;
	imwrite(str,mRgb);
	sprintf(str,"/home/pika/Infurnia/libsForServer_customMarker/outputImage1/FrontProjection_%s_%d",imgName,markerId);
	cout<<"\nProjection Image: " << str<<endl;
	imwrite(str,out);
	//~ imwrite("projection.jpg",out);
	vector<vector<double> > vec(1);
	
	return vec;
}


double** detectMarker(char imgName[],Mat &mRgb, double ZVec[3], char path_Thresholding[]){

	
	Mat mGr;
	ARMarkerInfo    *marker_info;
	//~ ARMarkerInfo    *marker_info2;
	int marker_num = 0;
	cvtColor(mRgb,mGr,CV_RGB2GRAY);
	
	int imWidth = mGr.cols,imHeight = mGr.rows;
	xsize = mGr.cols; ysize = mGr.rows;
	
	double **TransMat = (double**)malloc(sizeof(double*)*4);
	for(int i = 0; i < 4; i++)
		TransMat[i] = (double*)malloc(sizeof(double)*4);
	if(!ARInitialised){
		//~ glutInit(&argc, argv);
		init();
		ARInitialised = true;
	}
	ARUint8 *img = (ARUint8*)malloc(sizeof(ARUint8)*xsize*ysize);

	for(int i = 0 ; i < ysize ; i++)
		for(int j = 0; j < xsize; j++)
			img[i*xsize+j] = mGr.data[i*xsize+j];

	mainLoop(&marker_info,&marker_num,img);
	//~ marker_info2 = *marker_info1;
	cout << "\n No. of Markers Detected " << marker_num<<endl;
	
	ofstream myfile("transMat.txt");
	initialElements.clear();
	for(int i = 0; i < objectnum; i++ ) {
		int k = -1;
		for(int j = 0; j < marker_num; j++ ) {
			if( object[i].id == marker_info[j].id) {

				/* you've found a pattern */
				printf("\nFound pattern: %d CF : %f\nMarker coordinates with Dir : %d\n",marker_info[j].id,marker_info[j].cf,marker_info[j].dir);
				//~ (*markerNum)++;
				//~ glColor3f( 0.0, 1.0, 0.0 );
				//~ argDrawSquare(marker_info[j].vertex,0,0);

				if( k == -1 && marker_info[j].cf > 0.7) k = j;
				else /* make sure you have the best pattern (highest confidence factor) */
					if( marker_info[k].cf < marker_info[j].cf && marker_info[j].cf > 0.7) k = j;
			}
		}
		if( k == -1 ) {
			object[i].visible = 0;
			continue;
		}
		double transMat[4][4];
		memset(transMat,0,sizeof(double)*4*4);
		double vertices[4][2];
		for(int m = 0; m < 4; m++){
			vertices[m][0] = marker_info[k].vertex[m][0]; vertices[m][1] = marker_info[k].vertex[m][1];
			printf(" %4.3f %4.3f\n",marker_info[k].vertex[m][0],marker_info[k].vertex[m][1]);
		} 
		circle(mRgb,Point(round(vertices[0][0]),round(vertices[0][1])),5,Scalar(255,0,0),1,8);
		circle(mRgb,Point(round(vertices[1][0]),round(vertices[1][1])),5,Scalar(0,255,0),1,8);
		circle(mRgb,Point(round(vertices[2][0]),round(vertices[2][1])),5,Scalar(0,0,255),1,8);
		circle(mRgb,Point(round(vertices[3][0]),round(vertices[3][1])),5,Scalar(255,0,255),1,8);

		double error = callVerify(imWidth, imHeight,2767,transMat, vertices, marker_info[k].dir,ZVec);
		
		printf("\n Error : %f\n",error);
		
		if(error < 12){
			relativeTransMat r;
			bool flag_markerIdx_id = true;
			for(int a = 0; a < markerIdx_id.size(); a++){
				if(markerIdx_id[a] == marker_info[k].id){
					flag_markerIdx_id = false;
					r.id[0]= r.id[1] = a;
				}
			}
			if(flag_markerIdx_id){
				markerIdx_id.push_back(marker_info[k].id);
				r.id[0]= r.id[1] = markerIdx_id.size() - 1;
			}
			for(int count = 0 ; count < 4; count++){
				r.transMat[count][0] = transMat[count][0];
				r.transMat[count][1] = transMat[count][1];
				r.transMat[count][2] = transMat[count][2];
				r.transMat[count][3] = transMat[count][3];
			}
			
			initialElements.push_back(r);
			if(r.id[0] == 0 && r.id[1] == 0 && finalElements.size() == 0) finalElements.push_back(r);
			
			 

			if(myfile.is_open())
			{
				char str[500];
				sprintf(str,"TransMat MarkerID : %d \n", marker_info[k].id);
				myfile << str;
				for(int count = 0 ; count < 4; count++){
					sprintf(str,"%+4.5f %+4.5f %+4.5f %+8.5f\n", transMat[count][0], transMat[count][1], transMat[count][2], transMat[count][3]);
					myfile << str;
				}
			}
		}
		object[i].visible = 1;
	}
	myfile.close();
	imwrite("output.jpg",mRgb);
	
	//~ circle(mRgb,Point(round(vertices[0][0]),round(vertices[0][1])),5,Scalar(255,0,0),1,8);
	//~ circle(mRgb,Point(round(vertices[1][0]),round(vertices[1][1])),5,Scalar(0,255,0),1,8);
	//~ circle(mRgb,Point(round(vertices[2][0]),round(vertices[2][1])),5,Scalar(0,0,255),1,8);
	//~ circle(mRgb,Point(round(vertices[3][0]),round(vertices[3][1])),5,Scalar(255,0,255),1,8);

	//~ printf("\ncandidateName: %s, Error : %f ERROR : %f",path,error,ERROR);
	double temp1 = 0;
	//~ temp = 0;
	//~ for(int i = 0;i<4;i++)
		//~ for(int j = 0;j<4;j++)
			//~ temp += (transMat[i][j]);
			
			
	//~ getIntermediateElements();
	//~ for(int i = 0; i < initialElements.size(); i++){
		//~ printf("\n\n TransMat %d wrt %d\n",initialElements[i].id[0],initialElements[i].id[1]);
		//~ for(int count = 0 ; count < 4; count++){
			//~ printf("%+4.5f %+4.5f %+4.5f %+8.5f\n", initialElements[i].transMat[count][0], initialElements[i].transMat[count][1], initialElements[i].transMat[count][2], initialElements[i].transMat[count][3]);
		//~ }
	//~ }
//~ 
	//~ for(int i = 0; i < intermediateElements.size(); i++){
		//~ printf("\n\n TransMat %d wrt %d\n",intermediateElements[i].id[0],intermediateElements[i].id[1]);
		//~ for(int count = 0 ; count < 4; count++){
			//~ printf("%+4.5f %+4.5f %+4.5f %+8.5f\n", intermediateElements[i].transMat[count][0], intermediateElements[i].transMat[count][1], intermediateElements[i].transMat[count][2], intermediateElements[i].transMat[count][3]);
		//~ }
	//~ }
	
	imageWiseElements.push_back(initialElements);
	return TransMat;
}



void getMarkerVertices_2Dn3D(double x[12],double Marker[4][2],double transMat[4][4],int imWidth,int imHeight,double focal){
	double h = 120,w = 120;
	double D[4][2] ={{-w/2,h/2},{w/2,h/2},{w/2,-h/2},{-w/2,-h/2}};
	double X3D[4],Y3D[4],Z3D[4];
	double x2D[4],y2D[4];

	X3D[0] = x[0] = transMat[0][0]*D[0][0] + transMat[0][1]*D[0][1] + transMat[0][3];
	Y3D[0] = x[1] = transMat[1][0]*D[0][0] + transMat[1][1]*D[0][1] + transMat[1][3];
	Z3D[0] = x[2] = transMat[2][0]*D[0][0] + transMat[2][1]*D[0][1] + transMat[2][3];

	x2D[0] = focal*X3D[0]/Z3D[0]; y2D[0] = focal*Y3D[0]/Z3D[0];


	X3D[1] = x[3] = transMat[0][0]*D[1][0] + transMat[0][1]*D[1][1] + transMat[0][3];
	Y3D[1] = x[4] = transMat[1][0]*D[1][0] + transMat[1][1]*D[1][1] + transMat[1][3];
	Z3D[1] = x[5] = transMat[2][0]*D[1][0] + transMat[2][1]*D[1][1] + transMat[2][3];

	x2D[1] = focal*X3D[1]/Z3D[1]; y2D[1] = focal*Y3D[1]/Z3D[1];

	X3D[2] = x[6] = transMat[0][0]*D[2][0] + transMat[0][1]*D[2][1] + transMat[0][3];
	Y3D[2] = x[7] = transMat[1][0]*D[2][0] + transMat[1][1]*D[2][1] + transMat[1][3];
	Z3D[2] = x[8] = transMat[2][0]*D[2][0] + transMat[2][1]*D[2][1] + transMat[2][3];

	x2D[2] = focal*X3D[2]/Z3D[2]; y2D[2] = focal*Y3D[2]/Z3D[2];

	X3D[3] = x[9]  = transMat[0][0]*D[3][0] + transMat[0][1]*D[3][1] + transMat[0][3];
	Y3D[3] = x[10] = transMat[1][0]*D[3][0] + transMat[1][1]*D[3][1] + transMat[1][3];
	Z3D[3] = x[11] = transMat[2][0]*D[3][0] + transMat[2][1]*D[3][1] + transMat[2][3];

	x2D[3] = focal*X3D[3]/Z3D[3]; y2D[3] = focal*Y3D[3]/Z3D[3];
	
	Marker[0][0] = x2D[0] + imWidth/2; Marker[0][1] = y2D[0] + imHeight/2;
	Marker[1][0] = x2D[1] + imWidth/2; Marker[1][1] = y2D[1] + imHeight/2; 
	Marker[2][0] = x2D[2] + imWidth/2; Marker[2][1] = y2D[2] + imHeight/2;
	Marker[3][0] = x2D[3] + imWidth/2; Marker[3][1] = y2D[3] + imHeight/2;
}

void getMarkerVertices_2D(double Marker[4][2],double transMat[4][4],int imWidth,int imHeight,double focal){
	double h =120,w = 120;
	double D[4][2] ={{-w/2,h/2},{w/2,h/2},{w/2,-h/2},{-w/2,-h/2}};
	double X3D[4],Y3D[4],Z3D[4];
	double x2D[4],y2D[4];

	X3D[0] = transMat[0][0]*D[0][0] + transMat[0][1]*D[0][1] + transMat[0][3];
	Y3D[0] = transMat[1][0]*D[0][0] + transMat[1][1]*D[0][1] + transMat[1][3];
	Z3D[0] = transMat[2][0]*D[0][0] + transMat[2][1]*D[0][1] + transMat[2][3];

	x2D[0] = focal*X3D[0]/Z3D[0]; y2D[0] = focal*Y3D[0]/Z3D[0];


	X3D[1] = transMat[0][0]*D[1][0] + transMat[0][1]*D[1][1] + transMat[0][3];
	Y3D[1] = transMat[1][0]*D[1][0] + transMat[1][1]*D[1][1] + transMat[1][3];
	Z3D[1] = transMat[2][0]*D[1][0] + transMat[2][1]*D[1][1] + transMat[2][3];

	x2D[1] = focal*X3D[1]/Z3D[1]; y2D[1] = focal*Y3D[1]/Z3D[1];

	X3D[2] = transMat[0][0]*D[2][0] + transMat[0][1]*D[2][1] + transMat[0][3];
	Y3D[2] = transMat[1][0]*D[2][0] + transMat[1][1]*D[2][1] + transMat[1][3];
	Z3D[2] = transMat[2][0]*D[2][0] + transMat[2][1]*D[2][1] + transMat[2][3];

	x2D[2] = focal*X3D[2]/Z3D[2]; y2D[2] = focal*Y3D[2]/Z3D[2];

	X3D[3] = transMat[0][0]*D[3][0] + transMat[0][1]*D[3][1] + transMat[0][3];
	Y3D[3] = transMat[1][0]*D[3][0] + transMat[1][1]*D[3][1] + transMat[1][3];
	Z3D[3] = transMat[2][0]*D[3][0] + transMat[2][1]*D[3][1] + transMat[2][3];

	x2D[3] = focal*X3D[3]/Z3D[3]; y2D[3] = focal*Y3D[3]/Z3D[3];
	
	Marker[0][0] = x2D[0] + imWidth/2; Marker[0][1] = y2D[0] + imHeight/2;
	Marker[1][0] = x2D[1] + imWidth/2; Marker[1][1] = y2D[1] + imHeight/2; 
	Marker[2][0] = x2D[2] + imWidth/2; Marker[2][1] = y2D[2] + imHeight/2;
	Marker[3][0] = x2D[3] + imWidth/2; Marker[3][1] = y2D[3] + imHeight/2;
}


double callVerify(int imWidth, int imHeight,double focal, double transMat[4][4], double vertices[4][2],int dir, double ZVec[3]){
	cout << "\n Vertices before rearrange : \n";
	
	for(int i = 0;i<4;i++)
		printf("\nMarker Coordinates : %3.5f %3.5f ",vertices[i][0],vertices[i][1]);

	rearranngeVertices(vertices,dir);
	cout << "\n Vertices after rearrange : \n";
	
	for(int i = 0;i<4;i++)
		printf("\nMarker Coordinates : %3.5f %3.5f",vertices[i][0],vertices[i][1]);

	double tempTransMat[4][4];
	double ERROR = 15;
	memset(tempTransMat,0,sizeof(double)*4*4);
	for(int mode = 1; mode <= 4; mode++){
		double error;
		double m[4][2];
		
		
		if(mode == 1){
			double temp_marker3D[12];
			int a[4] = {0,1,2,3};
			for(int i = 0; i < 4; i++){
				m[i][0] = vertices[a[i]][0];
				m[i][1] = vertices[a[i]][1];
			}
			printf("\n\nMode : %d",mode);
			double error = verify(tempTransMat,m,ZVec,focal,imWidth,imHeight,temp_marker3D,mode);
			printf("\nError : %f",error);
			if(error < ERROR){
					for(int i = 0; i < 4; i++)
						for(int j = 0; j < 4; j++)
							transMat[i][j] = tempTransMat[i][j];
					ERROR = error;
			}
		}
		if(mode == 2){
			double temp_marker3D[12];
			int a[4] = {3,0,1,2};
			for(int i = 0; i < 4; i++){
				m[i][0] = vertices[a[i]][0];
				m[i][1] = vertices[a[i]][1];
			}
			printf("\n\nMode : %d",mode);
			double error = verify(tempTransMat,m,ZVec,focal,imWidth,imHeight,temp_marker3D,mode);
			printf("\nError : %f",error);
			if(error < ERROR){
					for(int i = 0; i < 4; i++)
						for(int j = 0; j < 4; j++)
							transMat[i][j] = tempTransMat[i][j];
					ERROR = error;
			}
		}
		if(mode == 3){
			double temp_marker3D[12];
			int a[4] = {2,3,0,1};
			for(int i = 0; i < 4; i++){
				m[i][0] = vertices[a[i]][0];
				m[i][1] = vertices[a[i]][1];
			}
			printf("\n\nMode : %d",mode);
			double error = verify(tempTransMat,m,ZVec,focal,imWidth,imHeight,temp_marker3D,mode);
			printf("\nError : %f",error);
			if(error < ERROR){
					for(int i = 0; i < 4; i++)
						for(int j = 0; j < 4; j++)
							transMat[i][j] = tempTransMat[i][j];
					ERROR = error;
			}
		}
		if(mode == 4){
			double temp_marker3D[12];
			int a[4] = {1,2,3,0};
			for(int i = 0; i < 4; i++){
				m[i][0] = vertices[a[i]][0];
				m[i][1] = vertices[a[i]][1];
			}
			printf("\n\nMode : %d",mode);
			double error = verify(tempTransMat,m,ZVec,focal,imWidth,imHeight,temp_marker3D,mode);
			printf("\nError : %f",error);
			if(error < ERROR){
					for(int i = 0; i < 4; i++)
						for(int j = 0; j < 4; j++)
							transMat[i][j] = tempTransMat[i][j];
					ERROR = error;
			}
		}
	}
	return ERROR;
}

void rearranngeVertices(double vertices[4][2],int dir){
	//~ int lowerMost = 0;
	//~ double lowerMost_y = 0;
	double tempVertices[4][2];
	for(int i = 0 ; i < 4; i++){
		tempVertices[i][0] = vertices[(i+4-dir)%4][0]; tempVertices[i][1] = vertices[(i+4-dir)%4][1];
		//~ if(vertices[i][1] > lowerMost_y){
			//~ lowerMost = i;
			//~ lowerMost_y = vertices[i][1];
		//~ }
	}
	//~ printf("\nLowermost : %d, lowermost_y : %f\n",lowerMost,lowerMost_y);
	for(int i = 0; i < 4; i++){
		vertices[i][0] = tempVertices[i][0];
		vertices[i][1] = tempVertices[i][1];
	}
}

/* main loop */
static void mainLoop(ARMarkerInfo    **marker_info1,int *markerNum,ARUint8 *img)
{
    ARUint8         *dataPtr;
    ARMarkerInfo    *marker_info;
    int             marker_num;
    int             i,j,k;

    /* grab a video frame */
    //~ if( (dataPtr = (ARUint8 *)arVideoGetImage()) == NULL ) {
        //~ arUtilSleep(2);
        //~ return;
    //~ }
	if(img != NULL) dataPtr = img;
	else perror("\n Image pointer is null \n");
	
    //~ if( count == 0 ) arUtilTimerReset();  
    //~ count++;

	/*draw the video*/
    //~ argDrawMode2D();
    //~ argDispImage( dataPtr, 0,0 );

	//~ glColor3f( 1.0, 0.0, 0.0 );
	//~ glLineWidth(6.0);

	/* detect the markers in the video frame */ 
	if(arDetectMarker(dataPtr, thresh, &marker_info, &marker_num) < 0 ) {
		cleanup(); 
		exit(0);
	}
	*markerNum = marker_num;
	//~ for( i = 0; i < marker_num; i++ ) {
		//~ argDrawSquare(marker_info[i].vertex,0,0);
	//~ }

	/* check for known patterns */
    for( i = 0; i < objectnum; i++ ) {
		k = -1;
		for( j = 0; j < marker_num; j++ ) {
	        if( object[i].id == marker_info[j].id) {

				/* you've found a pattern */
				printf("Found pattern: %d \n",marker_info[j].id);
				//~ (*markerNum)++;

				if( k == -1 ) k = j;
		        else /* make sure you have the best pattern (highest confidence factor) */
					if( marker_info[k].cf < marker_info[j].cf ) k = j;
			}
		}
		if( k == -1 ) {
			object[i].visible = 0;
			continue;
		}
		
		/* calculate the transform for each marker */
		if( object[i].visible == 0 ) {
            arGetTransMat(&marker_info[k],
                          object[i].marker_center, object[i].marker_width,
                          object[i].trans);
        }
        else {
            arGetTransMatCont(&marker_info[k], object[i].trans,
                          object[i].marker_center, object[i].marker_width,
                          object[i].trans);
        }
        object[i].visible = 1;
	}
	*marker_info1 = marker_info;
	//~ arVideoCapNext();

	/* draw the AR graphics */
    //~ draw( object, objectnum );

	/*swap the graphics buffers*/
	//~ argSwapBuffers();
}
static void init( void )
{
	ARParam  wparam;

    /* open the video path */
    //~ if( arVideoOpen( vconf ) < 0 ) exit(0);
    /* find the size of the window */
    //~ if( arVideoInqSize(&xsize, &ysize) < 0 ) exit(0);
    printf("Image size (x,y) = (%d,%d)\n", xsize, ysize);

    /* set the initial camera parameters */
    if( arParamLoad(cparam_name, 1, &wparam) < 0 ) {
        printf("Camera parameter load error !!\n");
        exit(0);
    }
    arParamChangeSize( &wparam, xsize, ysize, &cparam );
    arInitCparam( &cparam );
    printf("*** Camera Parameter ***\n");
    arParamDisp( &cparam );

	/* load in the object data - trained markers and associated bitmap files */
    if( (object=read_ObjData(model_name, &objectnum)) == NULL ) exit(0);
    printf("Objectfile num = %d\n", objectnum);

    /* open the graphics window */
    //~ argInit( &cparam, 2.0, 0, 0, 0, 0 );
}

/* cleanup function called when program exits */
static void cleanup(void)
{
	//~ arVideoCapStop();
	//~ arVideoClose();
	argCleanup();
}

static char* strCopy(char src[],int strt,int end){
	char *dst = (char*)malloc(sizeof(char)*(end-strt+1)); // +1 since last character will be null and end index is excluded
	for(int i = strt; i < end; i++)
		dst[i-strt] = src[i];
	dst[end-strt] = '\0';
	return dst;
}


/* 
** ARToolKit object parsing function 
**   - reads in object data from object file in Data/object_data
**
** Format:
** <obj_num>
**
** <obj_name>
** <obj_pattern filename>
** <marker_width>
** <centerX centerY>
**
** ...
**
**	eg
**
**	#pattern 1
**	Hiro
**	Data/hiroPatt 
**	80.0
**	0.0 0.0
**
*/

ObjectData_T *read_ObjData( char *name, int *objectnum )
{
    FILE          *fp;
    ObjectData_T  *object;
    char           buf[256], buf1[256];
    int            i;

	printf("Opening Data File %s\n",name);

    if( (fp=fopen(name, "r")) == NULL ) {
		printf("Can't find the file - quitting \n");
		return(0);
	}

    get_buff(buf, 256, fp);
    if( sscanf(buf, "%d", objectnum) != 1 ) {fclose(fp); return(0);}

	printf("About to load %d Models\n",*objectnum);

    object = (ObjectData_T *)malloc( sizeof(ObjectData_T) * *objectnum );
    if( object == NULL ) return(0);

    for( i = 0; i < *objectnum; i++ ) {
		object[i].visible = 0;        
		
		get_buff(buf, 256, fp);
        if( sscanf(buf, "%s", object[i].name) != 1 ) {
            fclose(fp); free(object); return(0);
        }

		printf("Read in No.%d \n", i+1);

        get_buff(buf, 256, fp);
        if( sscanf(buf, "%s", buf1) != 1 ) {
          fclose(fp); free(object); return(0);}
        
        if( (object[i].id = arLoadPatt(buf1)) < 0 )
            {fclose(fp); free(object); return(0);}

        get_buff(buf, 256, fp);
        if( sscanf(buf, "%lf", &object[i].marker_width) != 1 ) {
            fclose(fp); free(object); return(0);
        }

        get_buff(buf, 256, fp);
        if( sscanf(buf, "%lf %lf", &object[i].marker_center[0],
            &object[i].marker_center[1]) != 2 ) {
            fclose(fp); free(object); return(0);
        }
        
    }

    fclose(fp);

    return( object );
}

static char *get_buff( char *buf, int n, FILE *fp )
{
    char *ret;

    for(;;) {
        ret = fgets( buf, n, fp );
        if( ret == NULL ) return(NULL);
        if( buf[0] != '\n' && buf[0] != '#' ) return(ret);
    }
}






