#include <stdio.h>
#include <GL/glut.h>
#include <GL/gl.h>
void renderer(void);
void initialSetup(void);
void changeSize(GLsizei w, GLsizei h); //New Function Prototype

static int prev_x = 0;
static int prev_y = 0;

float translate_x = 0;
float translate_y = 0;
float translate_z = 0;

float scale = 1;
static GLfloat theta[] = {0.0, 0.0, 0.0}; // Rotation (X,Y,Z)
static GLint axis = 2; // Changed by the Mouse Callback Routine


void argConvGlpara( float para[3][4], float gl_para[16] )
{
    int     i, j;

    for( j = 0; j < 3; j++ ) {
        for( i = 0; i < 4; i++ ) {
            gl_para[i*4+j] = para[j][i];
        }
    }
    gl_para[0*4+3] = gl_para[1*4+3] = gl_para[2*4+3] = 0.0;
    gl_para[3*4+3] = 1.0;
}
void motionCallback(int x, int y){
	
	//~ printf("\nMouse Pos : %d %d",x,y);
	//~ theta[axis] += 0.5;
	//~ if( theta[axis] > 360.0 ) theta[axis] -= 360.0;

	if(prev_x == prev_y && prev_x == 0){ prev_x = x; prev_y = y;}
	else{
		if(x - prev_x > 0){
			theta[axis] += 0.25;
			if( theta[axis] > 360.0 ) theta[axis] -= 360.0;
		}
		if(x - prev_x < 0){
			theta[axis] -= 0.25;
			//~ if( theta[axis] > 360.0 ) theta[axis] -= 360.0;
			if( theta[axis] < 360.0 ) theta[axis] += 360.0;
		}
	}

}
void mouseCallBack(int btn, int state, int x, int y)
{
	// Changes the rotation axis depending on the mouse button pressed.
	if(btn==GLUT_LEFT_BUTTON && state == GLUT_UP) {
		
		//~ axis = 0;
		prev_x = prev_y = 0;
		//~ printf("\nMouse Pos : %d %d",x,y);
	}
	//~ printf("\nMouse Pos : %d %d",x,y);
	if(btn==GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) translate_z += 50;
	if(btn==GLUT_RIGHT_BUTTON && state == GLUT_UP) {
		//~ axis = 2;
		translate_z -= 50;
	}
	if(btn == 3) {
		scale += 0.025; 
	}
	if(btn == 4){
		scale -= 0.025;
		if(scale <= 0.025) scale =0.025;
	}
}
void mouseWheel(int button, int dir, int x, int y){
	if(dir > 0) scale += 0.1;
	else if(dir < 0) scale -= 0.1; 
}
void keyBoardCallBack(int key,int x, int y){
	if(key == GLUT_KEY_UP) translate_y -= 50;
	else if(key == GLUT_KEY_DOWN) translate_y += 50;
	else if(key == GLUT_KEY_RIGHT) translate_x += 50;
	else if(key == GLUT_KEY_LEFT) translate_x -= 50;
}
void idleCallBack()
{
	//~ // Spins the cube around the set axis.
	//~ theta[axis] += 0.5;
	//~ if( theta[axis] > 360.0 ) theta[axis] -= 360.0;
	glutPostRedisplay();
}


int main (int argc, const char * argv[])
{
	glutInit(&argc, (char **)argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutCreateWindow("First Program");
	glutDisplayFunc(renderer);
	glutReshapeFunc(changeSize);
	glutMouseFunc(mouseCallBack);
	glutSpecialFunc(keyBoardCallBack);
	glutMouseWheelFunc(mouseWheel);
	glutMotionFunc(motionCallback);
	glutIdleFunc(idleCallBack);
	
	initialSetup();
	glutMainLoop();
return 0;
}

//~ void renderer(void)
//~ {
	//~ glClear(GL_COLOR_BUFFER_BIT);
	//~ glColor3f(1.0,0.0,0.0);
	//~ //Set the color to red
	//~ glRectf(100.0, 150.0, 150.0, 100.0);
	//~ //Draw a rectangle from 100,150 to 150, 100
	//~ glFlush();
//~ }

void renderer()
{
	int last=0;
	float ang;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//~ glRotatef(45,1,1,0);
	//~ glColor3f(0.0,1.0,0.0);
	glPushMatrix();
	//Draw a Cube Centered at origin
	//~ glutWireCube(80);
	//~ //Draw a Cube Centered at -80, -90, 0
	//~ glPushMatrix();
	//~ glTranslatef(0, 0, 0);
	//~ glutWireCube(70);
	//~ glPopMatrix();
	//~ //Draw a Cube Centered at -80, 90, 0
	//~ glPushMatrix();
	//~ glTranslatef(-80, 90, 0);
	//~ glutWireCube(60);
	//~ glPopMatrix();
	glRotatef(theta[0], 1.0, 0.0, 0.0);
	glRotatef(theta[1], 0.0, 1.0, 0.0);
	glRotatef(theta[2], 0.0, 0.0, 1.0);

	glTranslatef(translate_x, translate_y, translate_z);
	glScalef(scale, scale, scale);
	glColor3f(1,0,0);
	glBegin(GL_LINES);
 glVertex3f(-910.000000,0.000000,0.000000);
 glVertex3f(1077.000000,0.000000,0.000000);
 glVertex3f(-526.000000,0.000000,101.000000);
 glVertex3f(1114.000000,0.000000,101.000000);
 glVertex3f(-1509.000000,0.000000,2195.000000);
 glVertex3f(-484.000000,0.000000,2195.000000);
 glVertex3f(-213.000000,0.000000,2165.000000);
 glVertex3f(838.000000,0.000000,2165.000000);
 glVertex3f(-188.000000,0.000000,1213.000000);
 glVertex3f(754.000000,0.000000,1213.000000);
 glVertex3f(232.000000,0.000000,2517.000000);
 glVertex3f(1159.000000,0.000000,2517.000000);
 glVertex3f(-82.000000,0.000000,2046.000000);
 glVertex3f(712.000000,0.000000,2046.000000);
 glVertex3f(-49.000000,0.000000,1916.000000);
 glVertex3f(702.000000,0.000000,1916.000000);
 glVertex3f(-96.000000,0.000000,1509.000000);
 glVertex3f(617.000000,0.000000,1509.000000);
 glVertex3f(5.000000,0.000000,1723.000000);
 glVertex3f(674.000000,0.000000,1723.000000);
 glVertex3f(30.000000,0.000000,1612.000000);
 glVertex3f(576.000000,0.000000,1612.000000);
 glVertex3f(1120.000000,0.000000,2510.000000);
 glVertex3f(1120.000000,0.000000,10.000000);
 glVertex3f(-500.000000,0.000000,120.000000);
 glVertex3f(-500.000000,0.000000,2160.000000);
 glVertex3f(-1420.000000,0.000000,2190.000000);
 glVertex3f(-1420.000000,0.000000,170.000000);
 glVertex3f(800.000000,0.000000,2170.000000);
 glVertex3f(800.000000,0.000000,1160.000000);
 glVertex3f(-130.000000,0.000000,2140.000000);
 glVertex3f(-130.000000,0.000000,1180.000000);
 glVertex3f(60.000000,0.000000,2060.000000);
 glVertex3f(60.000000,0.000000,1280.000000);
 glVertex3f(680.000000,0.000000,2050.000000);
 glVertex3f(680.000000,0.000000,1280.000000);
 glVertex3f(300.000000,0.000000,2050.000000);
 glVertex3f(300.000000,0.000000,1290.000000);
 glVertex3f(400.000000,0.000000,1600.000000);
 glVertex3f(400.000000,0.000000,1970.000000);
 glVertex3f(-240.000000,0.000000,1410.000000);
 glVertex3f(-240.000000,0.000000,1710.000000);
 glVertex3f(500.000000,0.000000,1530.000000);
 glVertex3f(500.000000,0.000000,1280.000000);
 glVertex3f(160.000000,0.000000,2060.000000);
 glVertex3f(160.000000,0.000000,1820.000000);
 //Marker ID : 1
 glVertex3f(-1440.249675,-2088.823471,0.000000);
 glVertex3f(-1396.481977,1170.882710,-0.000000);
 glVertex3f(-1425.387454,-981.923243,2697.000000);
 glVertex3f(-1403.611010,639.930569,2697.000000);
 glVertex3f(-1428.569339,-1218.901883,2585.000000);
 glVertex3f(-1407.316496,363.955444,2585.000000);
 glVertex3f(-1443.431560,-2325.802110,2091.000000);
 glVertex3f(-1430.059588,-1329.891878,2091.000000);
 glVertex3f(-1404.658212,561.937599,2191.000000);
 glVertex3f(-1396.522254,1167.882981,2191.000000);
 glVertex3f(-1431.012811,-1400.885479,-10.000000);
 glVertex3f(-1431.012811,-1400.885479,2190.000000);
 glVertex3f(-1432.489635,-1510.875565,40.000000);
 glVertex3f(-1432.489635,-1510.875565,2150.000000);
 glVertex3f(-1443.632945,-2340.800759,2180.000000);
 glVertex3f(-1443.632945,-2340.800759,170.000000);
 glVertex3f(-1447.257877,-2610.776424,500.000000);
 glVertex3f(-1447.257877,-2610.776424,1430.000000);
 glVertex3f(-1447.123621,-2600.777325,1450.000000);
 glVertex3f(-1447.123621,-2600.777325,2210.000000);
 glVertex3f(-1433.832203,-1610.866552,140.000000);
 glVertex3f(-1433.832203,-1610.866552,560.000000);
 glVertex3f(-1409.128962,228.967611,2200.000000);
 glVertex3f(-1409.128962,228.967611,2590.000000);
 glVertex3f(-1441.350581,-2170.816080,600.000000);
 glVertex3f(-1441.350581,-2170.816080,830.000000);
// Marker ID : 2
 glVertex3f(1046.508150,118.963660,110.000000);
 glVertex3f(1033.371695,-3523.012649,110.000000);
 glVertex3f(1046.486509,112.963699,-0.000000);
 glVertex3f(1033.999302,-3349.013781,0.000000);
 glVertex3f(1038.421403,-2123.021756,1850.000000);
 glVertex3f(1034.107510,-3319.013976,1850.000000);
 glVertex3f(1038.446652,-2116.021802,2186.000000);
 glVertex3f(1034.233752,-3284.014204,2186.000000);
 glVertex3f(1037.635092,-2341.020338,1493.000000);
 glVertex3f(1034.789220,-3130.015206,1493.000000);
 glVertex3f(1046.771457,191.963185,2644.000000);
 glVertex3f(1044.315134,-489.032385,2644.000000);
 glVertex3f(1035.759486,-2861.016955,1709.000000);
 glVertex3f(1034.987602,-3075.015563,1709.000000);
 glVertex3f(1037.198653,-2462.019551,1737.000000);
 glVertex3f(1036.455624,-2668.018211,1737.000000);
 glVertex3f(1046.681283,166.963347,2650.000000);
 glVertex3f(1046.681283,166.963347,10.000000);
 glVertex3f(1034.958746,-3083.015511,2200.000000);
 glVertex3f(1034.958746,-3083.015511,1470.000000);
 glVertex3f(1035.752272,-2863.016942,2200.000000);
 glVertex3f(1035.752272,-2863.016942,1460.000000);
 glVertex3f(1037.411462,-2403.019935,2200.000000);
 glVertex3f(1037.411462,-2403.019935,1440.000000);
 glVertex3f(1036.581867,-2633.018439,2200.000000);
 glVertex3f(1036.581867,-2633.018439,1480.000000);
 glVertex3f(1038.421403,-2123.021756,2210.000000);
 glVertex3f(1038.421403,-2123.021756,1840.000000);
 glVertex3f(1036.185104,-2743.017723,1460.000000);
 glVertex3f(1036.185104,-2743.017723,1800.000000);
 glVertex3f(1036.942560,-2533.019089,1830.000000);
 glVertex3f(1036.942560,-2533.019089,2170.000000);
 glVertex3f(1038.024641,-2233.021041,1910.000000);
 glVertex3f(1038.024641,-2233.021041,2120.000000);
// Marker ID : 4
//~ glColor3f(0,1,0);
 glVertex3f(-2708.147237,-2634.675661,-0.000000);
 glVertex3f(-1374.554035,-2667.617602,0.000000);
 glVertex3f(-2574.188100,-2637.984672,921.000000);
 glVertex3f(-1497.516527,-2664.580227,921.000000);
 glVertex3f(-2407.239026,-2642.108588,104.000000);
 glVertex3f(-1370.555255,-2667.716379,104.000000);
 glVertex3f(-2513.206702,-2639.491012,1466.000000);
 glVertex3f(-1612.481458,-2661.740404,1466.000000);
 glVertex3f(-1355.559829,-2668.086790,160.000000);
 glVertex3f(-1355.559829,-2668.086790,1420.000000);
 glVertex3f(-1515.511038,-2664.135733,980.000000);
 glVertex3f(-1515.511038,-2664.135733,1430.000000);
 glVertex3f(-2775.126806,-2633.021156,540.000000);
 glVertex3f(-2775.126806,-2633.021156,-0.000000);
 glVertex3f(-2515.206092,-2639.441624,1110.000000);
 glVertex3f(-2515.206092,-2639.441624,1490.000000);

 glColor3f(0,1,0);
 
 //Marker ID : 0

 glVertex3f(-1509.000000,0.000000,2195.000000);
 glVertex3f(-484.000000,0.000000,2195.000000);
 glVertex3f(-213.000000,0.000000,2165.000000);
 glVertex3f(838.000000,0.000000,2165.000000);
 glVertex3f(-188.000000,0.000000,1213.000000);
 glVertex3f(754.000000,0.000000,1213.000000);
 glVertex3f(-82.000000,0.000000,2046.000000);
 glVertex3f(712.000000,0.000000,2046.000000);
 glVertex3f(-500.000000,0.000000,120.000000);
 glVertex3f(-500.000000,0.000000,2160.000000);
 glVertex3f(-1420.000000,0.000000,2190.000000);
 glVertex3f(-1420.000000,0.000000,170.000000);
 glVertex3f(800.000000,0.000000,2170.000000);
 glVertex3f(800.000000,0.000000,1160.000000);
 glVertex3f(-130.000000,0.000000,2140.000000);
 glVertex3f(-130.000000,0.000000,1180.000000);
//Marker ID : 1
 glVertex3f(-1443.431560,-2325.802110,2091.000000);
 glVertex3f(-1430.059588,-1329.891878,2091.000000);
 glVertex3f(-1431.012811,-1400.885479,-10.000000);
 glVertex3f(-1431.012811,-1400.885479,2190.000000);
 glVertex3f(-1443.632945,-2340.800759,2180.000000);
 glVertex3f(-1443.632945,-2340.800759,170.000000);
 glVertex3f(-1432.489635,-1510.875565,40.000000);
 glVertex3f(-1432.489635,-1510.875565,2150.000000);

//Marker ID : 2
//Marker ID : 3

//Marker ID : 4

 glVertex3f(-2574.188100,-2637.984672,921.000000);
 glVertex3f(-1497.516527,-2664.580227,921.000000);
 glVertex3f(-2513.206702,-2639.491012,1466.000000);
 glVertex3f(-1612.481458,-2661.740404,1466.000000);
 glVertex3f(-1515.511038,-2664.135733,980.000000);
 glVertex3f(-1515.511038,-2664.135733,1430.000000);
 glVertex3f(-2515.206092,-2639.441624,1110.000000);
 glVertex3f(-2515.206092,-2639.441624,1490.000000);

	glEnd();
	glPopMatrix();
	//~ glPushMatrix();
	//~ glScalef(scale, scale, scale);
	//~ glTranslatef(translate_x, translate_y, translate_z);
	//~ //Draw a Cube Centered at origin
	//~ glutWireCube(80);
	//~ glPopMatrix();
	glutSwapBuffers();
}

void initialSetup(void)
{
	glClearColor(0.0, 0.0, .5, 1.0);
}

//~ void changeSize(GLsizei w, GLsizei h) //New Function for resizing
//~ {
	//~ if(h==0)
	//~ h = 1;
	//~ glViewport(0,0,w,h);
	//~ glMatrixMode(GL_PROJECTION);
	//~ glLoadIdentity();
	//~ if(w <= h)
	//~ glOrtho(0, 250, 0, 250.0 * h/w, 1, -1);
	//~ else
	//~ glOrtho(0,250.0 * w/h, 0, 250, 1, -1);
	//~ glMatrixMode(GL_MODELVIEW);
	//~ glLoadIdentity();
//~ }

void changeSize(GLsizei w, GLsizei h) {
	//~ GLfloat sideRange = 200;
	glViewport(0,0,1640,1230);
	//~ glMatrixMode(GL_PROJECTION);
	//~ glLoadIdentity();
	//~ glOrtho(-sideRange, sideRange, -sideRange, sideRange, sideRange, -sideRange);
	//~ glMatrixMode(GL_MODELVIEW);
	//~ glLoadIdentity();
	//~ 
	
	float glCameraMatrix[16] = {0};

	float near = 50,far = 5000,right = 3280*(float)1.12/(2*1000), left = -3280*(float)1.12/(2*1000), top = -2460*(float)1.12/(2*1000), bottom = 2460*(float)1.12/(2*1000);
	glCameraMatrix[0*4+0] = (float)2767/(float)1640/*(2*near)/((right-left)*10)*/;
	glCameraMatrix[2*4+0] = -(right+left)/(right-left);
	glCameraMatrix[1*4+1] = (float)-2767/(float)1230/*(2*near)/((top-bottom)*10)*/;
	glCameraMatrix[2*4+1] = -(top+bottom)/(top-bottom);
	glCameraMatrix[2*4+2] = (far+near)/(far-near);
	glCameraMatrix[3*4+2] = -2*far*near/(far-near);
	glCameraMatrix[2*4+3] = 1;

	//~ float transMat[3][4] = {{0.73808,0.67402,0.03061,-703.58578},{0.43628,-0.44216,-0.78368,185.50222},{-0.51468,0.59177,-0.62041,1985.46434}};
	float transMat[3][4] = {{1,0,0,0},{0,0,-1,0},{0,1,0,1985.46434}};
	//~ float transMat[3][4] = {{+0.67463,-0.73752,+0.03061,-266.96900},{-0.44179,-0.43665,-0.78368,+343.57436},{+0.59134,+0.51517,-0.62041,+1850.99456},};
	float glPara[16] = {0};
	//~ memset(glPara,0,sizeof(glPara[0][0]*4*4));
	argConvGlpara(transMat,glPara);
	//argDrawMode3D
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//argDraw3dCamera
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
//            GLU.gluPerspective(gl,80.0f,(float)2048/(float)1440,0.1f,100.0f);
	glLoadMatrixf( glCameraMatrix );
//		    gl.gl
	glMatrixMode(GL_MODELVIEW);
	
	glLoadMatrixf(glPara);

}
