#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

/////////////////////////////////////////// Vertex Verification ////////////////////////////////
void new_marker(double marker[4][2],int imWidth,int imHeight){

	int i;

	for( i = 0 ; i < 4; i++){
//		marker[i][0] = ((marker[i][0] - imWidth/2)*1024)/1640;
//		marker[i][1] = ((marker[i][1] - imHeight/2)*720)/1230;
		marker[i][0] = (marker[i][0] - imWidth/2);
		marker[i][1] = (marker[i][1] - imHeight/2);
	}
	return;
}

void normalize(double x[3]){
	double temp = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]);
	x[0] = x[0]/temp; x[1] = x[1]/temp; x[2] = x[2]/temp;
	return;
}

double dot(double x[3],double y[3]){
	return (x[0]*y[0] + x[1]*y[1] + x[2]*y[2]);
}

void cross(double x[3],double y[3],double z[3]){
	z[0] = x[1]*y[2]-x[2]*y[1];
	z[1] = -x[0]*y[2]+x[2]*y[0];
	z[2] =  x[0]*y[1]-x[1]*y[0];
	return;
}

double verify(double rotMat[4][4],double marker[4][2], double z[3], double focal,int imWidth,int imHeight,double marker3D[12],int mode){

//	printf("\n------------------------------------       START          --------------------------------\n");
	//~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","------------------------------------       START          -------------------------------- focal : %f",focal);
	double tmp[4],phi,theta;
	double X[4],Y[4],Z[4];
	double x[4],y[4];
	double a1,a2,b1,b2,c1,c2,l1,l2,m1,m2,n1,n2;
	double a,b;
	double h = 120,w = 120;
/*	double D[3][2] ={{w/2,-h/2},{-w/2,-h/2},{-w/2,h/2}};*/


    if(mode == 1){
        double D[4][2] ={{-w/2,h/2},{w/2,h/2},{w/2,-h/2},{-w/2,-h/2}};
    //	double rotMat[3][3];
        double co,si;
        int i,j,k;
        // double temp,tempAngle;
        double diff_D_x,diff_D_y,diff_Marker_y,diff_Marker_x;
        double t[3];
        double XVec[3],YVec[3],ZVec[3],dotProd;

        double psi[2];
        double error = 9999;

    //	focal = focal*0.6;

        //////////////////////////////////// calculating theta & phi //////////////////////

        new_marker(marker,imWidth,imHeight);


        for( i = 0 ; i < 4; i++){
            //~ printf("\n%3.5f %3.5f",marker[i][0],marker[i][1]);
            //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},",marker[i][0]+(imWidth/2),marker[i][1]+(imHeight/2));
        }
    //    __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},",marker[0][0]+imWidth/2,marker[0][1]+imHeight/2,marker[1][0]+imWidth/2,marker[1][1]+imHeight/2,marker[2][0]+imWidth/2,marker[2][1]+imHeight/2,marker[3][0]+imWidth/2,marker[3][1]+imHeight/2);
        //~ printf("\n");

        tmp[0] = z[2];
        theta = acos(z[2]);
        phi = atan2(z[0],-z[1]);
        // phi += M_PI;

        ///////////////////////////////////////////////////////////////////////////////////

    //	printf("theta  = %3.5f phi = %3.5f \n", theta*180/M_PI,phi*180/M_PI);
    //	__android_log_print(ANDROID_LOG_VERBOSE,"AR native","theta  = %3.5f phi = %3.5f", theta*180/M_PI,phi*180/M_PI);

        tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

        a1 = cos(phi); a2 = -cos(theta)*sin(phi);
        b1 = sin(phi); b2 = cos(theta)*cos(phi);
        c1 = 0; 	   c2 = sin(theta);

        l1 = -cos(theta)*sin(phi); l2 = -cos(phi);
        m1 =  cos(phi)*cos(theta); m2 = -sin(phi);
        n1 = sin(theta); 		   n2 = 0;

        diff_D_x = D[0][0] - D[1][0]; diff_D_y = D[0][1] - D[1][1];
        diff_Marker_x = marker[0][0] - marker[1][0]; diff_Marker_y = marker[0][1] - marker[1][1];

        /////////// calculating a,b coefficients for equation a*cos(psi)+b*sin(psi) = 0;

        a = diff_Marker_y*(a1*diff_D_x + l1*diff_D_y +
        c1*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n1*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b1*diff_D_x +
        m1*diff_D_y + c1*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n1*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        b = diff_Marker_y*(a2*diff_D_x + l2*diff_D_y +
        c2*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n2*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b2*diff_D_x +
        m2*diff_D_y + c2*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n2*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        psi[0] = atan2(a,-b);
        psi[1] = M_PI + psi[0];

        for(i = 0 ; i < 2; i++){
    //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@@@        START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@@@       LOOP START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			if( diff_Marker_y != 0){
				t[2] = ((cos(psi[i])*b1 + sin(psi[i])*b2)*diff_D_x + (cos(psi[i])*m1 + sin(psi[i])*m2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal) * focal/diff_Marker_y;
			}
			else if(diff_Marker_x != 0){
				t[2] = ((cos(psi[i])*a1 + sin(psi[i])*a2)*diff_D_x + (cos(psi[i])*l1 + sin(psi[i])*l2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;
			}

            if(t[2] > 0){
                rotMat[0][0] = (cos(psi[i])*a1 + sin(psi[i])*a2);
                rotMat[0][1] = (cos(psi[i])*l1 + sin(psi[i])*l2);
                rotMat[0][2] = z[0];

                rotMat[1][0] = (cos(psi[i])*b1 + sin(psi[i])*b2);
                rotMat[1][1] = (cos(psi[i])*m1 + sin(psi[i])*m2);
                rotMat[1][2] = z[1];


                rotMat[2][0] = (cos(psi[i])*c1 + sin(psi[i])*c2);
                rotMat[2][1] = (cos(psi[i])*n1 + sin(psi[i])*n2);
                rotMat[2][2] = z[2];

                //~ t[2] = (rotMat[0][0]*diff_D_x + rotMat[0][1]*diff_D_y + rotMat[2][0]*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + rotMat[2][1]*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;

                Z[0] = rotMat[2][0]*D[0][0] + rotMat[2][1]*D[0][1] + t[2];

                t[1] = Z[0]*marker[0][1]/focal- rotMat[1][0]*D[0][0] - rotMat[1][1]*D[0][1] ;
                t[0] = Z[0]*marker[0][0]/focal- rotMat[0][0]*D[0][0] - rotMat[0][1]*D[0][1] ;

        //		printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
        //	    __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);

                rotMat[0][3] = t[0];
                rotMat[1][3] = t[1];
                rotMat[2][3] = t[2];

                rotMat[3][3] = 1;
                rotMat[3][0] = rotMat[3][1] = rotMat[3][2] = 0;

                X[0] = rotMat[0][0]*D[0][0] + rotMat[0][1]*D[0][1] + t[0];
                Y[0] = rotMat[1][0]*D[0][0] + rotMat[1][1]*D[0][1] + t[1];


                x[0] = focal*X[0]/Z[0]; y[0] = focal*Y[0]/Z[0];


                X[1] = rotMat[0][0]*D[1][0] + rotMat[0][1]*D[1][1] + t[0];
                Y[1] = rotMat[1][0]*D[1][0] + rotMat[1][1]*D[1][1] + t[1];
                Z[1] = rotMat[2][0]*D[1][0] + rotMat[2][1]*D[1][1] + t[2];

                x[1] = focal*X[1]/Z[1]; y[1] = focal*Y[1]/Z[1];



                // t[0] = marker[1][0]*Z[1]/focal - rotMat[0][0]*D[1][0] - rotMat[0][1]*D[1][1];
                // t[1] = marker[1][1]*Z[1]/focal - rotMat[1][0]*D[1][0] - rotMat[1][1]*D[1][1];
                // printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);



                X[2] = rotMat[0][0]*D[2][0] + rotMat[0][1]*D[2][1] + t[0];
                Y[2] = rotMat[1][0]*D[2][0] + rotMat[1][1]*D[2][1] + t[1];
                Z[2] = rotMat[2][0]*D[2][0] + rotMat[2][1]*D[2][1] + t[2];

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];

                X[3] = rotMat[0][0]*D[3][0] + rotMat[0][1]*D[3][1] + t[0];
                Y[3] = rotMat[1][0]*D[3][0] + rotMat[1][1]*D[3][1] + t[1];
                Z[3] = rotMat[2][0]*D[3][0] + rotMat[2][1]*D[3][1] + t[2];

                x[3] = focal*X[3]/Z[3]; y[3] = focal*Y[3]/Z[3];

        //		printf("\n Z's \n%3.5f %3.5f %3.5f %3.5f\n", Z[0],Z[1],Z[2],Z[3]);
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                //~ printf("\n re - projected \n%3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n", x[0],y[0], x[1],y[1], x[2],y[2], x[3],y[3]);

                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","re-projected Coordinates : ");
                //~ {
                    //~ int i;
                    //~ for(i = 0; i < 4; i++){
                        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "{%3.5f,%3.5f},", x[i]+imWidth/2,y[i]+imHeight/2);
                    //~ }
                //~ }
                printf("\nre-projected Coordinates : \n");
                {
                    int i;
                    for(i = 0; i < 4; i++){
                        printf("\n{%3.5f,%3.5f},", x[i]+imWidth/2,y[i]+imHeight/2);
                    }
                }

				//~ printf("\n{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},", x[0]+imWidth/2,y[0]+imHeight/2, x[1]+imWidth/2,y[1]+imHeight/2, x[2]+imWidth/2,y[2]+imHeight/2, x[3]+imWidth/2,y[3]+imHeight/2);
                // printf("\n error: %3.5f ",marker[0][0]*Z[0]/focal - cons[0] - marker[0][0]*t[2]/focal);		//error = 0;

                // printf("\n side1  : %3.5f ", sqrt((X[0]-X[1])*(X[0]-X[1])+(Y[0]-Y[1])*(Y[0]-Y[1])+(Z[0]-Z[1])*(Z[0]-Z[1])) );
                // printf("\n side2 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n side3  : %3.5f ", sqrt((X[0]-X[3])*(X[0]-X[3])+(Y[0]-Y[3])*(Y[0]-Y[3])+(Z[0]-Z[3])*(Z[0]-Z[3])) );
                // printf("\n side4 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n error: %3.5f ",rotMat[0][0]*D[0][0]+rotMat[0][1]*D[0][1] + t[0] - marker[0][0]*Z[0]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[0][0]+rotMat[1][1]*D[0][1] + t[1] - marker[0][1]*Z[0]/focal);

                // printf("\n error: %3.5f ",rotMat[0][0]*D[1][0]+rotMat[0][1]*D[1][1] + t[0]- marker[1][0]*Z[1]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[1][0]+rotMat[1][1]*D[1][1] + t[1]- marker[1][1]*Z[1]/focal);

                XVec[0] = X[1]-X[0];XVec[1] = Y[1]-Y[0];XVec[2] = Z[1]-Z[0];
                YVec[0] = X[1]-X[2];YVec[1] = Y[1]-Y[2];YVec[2] = Z[1]-Z[2];


                // t[1] = (marker[1][1]*((rotMat[0][0] * D[1][0] +
                normalize(XVec); normalize(YVec);

                dotProd = dot(XVec,YVec);

                cross(XVec,YVec,ZVec);
                //~ printf("\n dotProd : %3.5f \nZVec : %3.5f %3.5f %3.5f \n",dotProd,ZVec[0],ZVec[1],ZVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","XVec : %3.5f %3.5f %3.5f ",XVec[0],XVec[1],XVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","YVec : %3.5f %3.5f %3.5f ",YVec[0],YVec[1],YVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","ZVec : %3.5f %3.5f %3.5f ",ZVec[0],ZVec[1],ZVec[2]);

        //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@           END                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@ LOOP END @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                //~ error = sqrt((x[2] - marker[2][0])*(x[2] - marker[2][0]) + (y[2] - marker[2][1])*(y[2] - marker[2][1]));
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "MarkerPoint :  %3.5f,%3.5f,%3.5f ",X[0],Y[0],Z[0]);
                //~ if(error < 15){
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
                //~ }
                marker3D[0] = X[0];marker3D[0+3*1] = X[1];marker3D[0+3*2] = X[2];marker3D[0+3*3] = X[3];
                marker3D[1] = Y[0];marker3D[1+3*1] = Y[1];marker3D[1+3*2] = Y[2];marker3D[1+3*3] = Y[3];
                marker3D[2] = Z[0];marker3D[2+3*1] = Z[1];marker3D[2+3*2] = Z[2];marker3D[2+3*3] = Z[3];
                
                //~ Error calculation 

                X[2] = rotMat[0][0]*(D[2][0]+D[3][0])/2 + rotMat[0][1]*(D[2][1]+D[3][1])/2 + t[0];
                Y[2] = rotMat[1][0]*(D[2][0]+D[3][0])/2 + rotMat[1][1]*(D[2][1]+D[3][1])/2 + t[1];
                Z[2] = rotMat[2][0]*(D[2][0]+D[3][0])/2 + rotMat[2][1]*(D[2][1]+D[3][1])/2 + t[2];

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];
                error = sqrt((x[2] - (marker[2][0]+marker[3][0])/2 )*(x[2] - (marker[2][0]+marker[3][0])/2) + (y[2] - (marker[2][1]+marker[3][1])/2)*(y[2] - (marker[2][1]+marker[3][1])/2));

            }
        }
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Error : %f",error);
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","--------------------------------- END -------------------------------------");
        return error;
	}

	else if(mode == 2){
        double D[4][2] ={{-w/2,-h/2},{-w/2,h/2},{w/2,h/2},{w/2,-h/2}};
    //	double rotMat[3][3];
        double co,si;
        int i,j,k;
        // double temp,tempAngle;
        double diff_D_x,diff_D_y,diff_Marker_y,diff_Marker_x;
        double t[3];
        double XVec[3],YVec[3],ZVec[3],dotProd;

        double psi[2];
        double error = 9999;

    //	focal = focal*0.6;

        //////////////////////////////////// calculating theta & phi //////////////////////

        new_marker(marker,imWidth,imHeight);


        for( i = 0 ; i < 4; i++){
            //~ printf("\n%3.5f %3.5f",marker[i][0],marker[i][1]);
            //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},",marker[i][0]+(imWidth/2),marker[i][1]+(imHeight/2));
        }
    //    __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},",marker[0][0]+imWidth/2,marker[0][1]+imHeight/2,marker[1][0]+imWidth/2,marker[1][1]+imHeight/2,marker[2][0]+imWidth/2,marker[2][1]+imHeight/2,marker[3][0]+imWidth/2,marker[3][1]+imHeight/2);
        //~ printf("\n");

        tmp[0] = z[2];
        theta = acos(z[2]);
        phi = atan2(z[0],-z[1]);
        // phi += M_PI;

        ///////////////////////////////////////////////////////////////////////////////////

    //	printf("theta  = %3.5f phi = %3.5f \n", theta*180/M_PI,phi*180/M_PI);
    //	__android_log_print(ANDROID_LOG_VERBOSE,"AR native","theta  = %3.5f phi = %3.5f", theta*180/M_PI,phi*180/M_PI);

        tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

        a1 = cos(phi); a2 = -cos(theta)*sin(phi);
        b1 = sin(phi); b2 = cos(theta)*cos(phi);
        c1 = 0; 	   c2 = sin(theta);

        l1 = -cos(theta)*sin(phi); l2 = -cos(phi);
        m1 =  cos(phi)*cos(theta); m2 = -sin(phi);
        n1 = sin(theta); 		   n2 = 0;

        diff_D_x = D[0][0] - D[1][0]; diff_D_y = D[0][1] - D[1][1];
        diff_Marker_x = marker[0][0] - marker[1][0]; diff_Marker_y = marker[0][1] - marker[1][1];

        /////////// calculating a,b coefficients for equation a*cos(psi)+b*sin(psi) = 0;

        a = diff_Marker_y*(a1*diff_D_x + l1*diff_D_y +
        c1*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n1*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b1*diff_D_x +
        m1*diff_D_y + c1*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n1*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        b = diff_Marker_y*(a2*diff_D_x + l2*diff_D_y +
        c2*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n2*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b2*diff_D_x +
        m2*diff_D_y + c2*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n2*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        psi[0] = atan2(a,-b);
        psi[1] = M_PI + psi[0];

        for(i = 0 ; i < 2; i++){
    //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@@@        START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@@@       LOOP START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			if( diff_Marker_y != 0){
				t[2] = ((cos(psi[i])*b1 + sin(psi[i])*b2)*diff_D_x + (cos(psi[i])*m1 + sin(psi[i])*m2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal) * focal/diff_Marker_y;
			}
			else if(diff_Marker_x != 0){
				t[2] = ((cos(psi[i])*a1 + sin(psi[i])*a2)*diff_D_x + (cos(psi[i])*l1 + sin(psi[i])*l2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;
			}

            if(t[2] > 0){
                rotMat[0][0] = (cos(psi[i])*a1 + sin(psi[i])*a2);
                rotMat[0][1] = (cos(psi[i])*l1 + sin(psi[i])*l2);
                rotMat[0][2] = z[0];

                rotMat[1][0] = (cos(psi[i])*b1 + sin(psi[i])*b2);
                rotMat[1][1] = (cos(psi[i])*m1 + sin(psi[i])*m2);
                rotMat[1][2] = z[1];


                rotMat[2][0] = (cos(psi[i])*c1 + sin(psi[i])*c2);
                rotMat[2][1] = (cos(psi[i])*n1 + sin(psi[i])*n2);
                rotMat[2][2] = z[2];

                //~ t[2] = (rotMat[0][0]*diff_D_x + rotMat[0][1]*diff_D_y + rotMat[2][0]*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + rotMat[2][1]*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;

                Z[0] = rotMat[2][0]*D[0][0] + rotMat[2][1]*D[0][1] + t[2];

                t[1] = Z[0]*marker[0][1]/focal- rotMat[1][0]*D[0][0] - rotMat[1][1]*D[0][1] ;
                t[0] = Z[0]*marker[0][0]/focal- rotMat[0][0]*D[0][0] - rotMat[0][1]*D[0][1] ;

        //		printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
        //	    __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);

                rotMat[0][3] = t[0];
                rotMat[1][3] = t[1];
                rotMat[2][3] = t[2];

                rotMat[3][3] = 1;
                rotMat[3][0] = rotMat[3][1] = rotMat[3][2] = 0;

                X[0] = rotMat[0][0]*D[0][0] + rotMat[0][1]*D[0][1] + t[0];
                Y[0] = rotMat[1][0]*D[0][0] + rotMat[1][1]*D[0][1] + t[1];


                x[0] = focal*X[0]/Z[0]; y[0] = focal*Y[0]/Z[0];


                X[1] = rotMat[0][0]*D[1][0] + rotMat[0][1]*D[1][1] + t[0];
                Y[1] = rotMat[1][0]*D[1][0] + rotMat[1][1]*D[1][1] + t[1];
                Z[1] = rotMat[2][0]*D[1][0] + rotMat[2][1]*D[1][1] + t[2];

                x[1] = focal*X[1]/Z[1]; y[1] = focal*Y[1]/Z[1];



                // t[0] = marker[1][0]*Z[1]/focal - rotMat[0][0]*D[1][0] - rotMat[0][1]*D[1][1];
                // t[1] = marker[1][1]*Z[1]/focal - rotMat[1][0]*D[1][0] - rotMat[1][1]*D[1][1];
                // printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);



                X[2] = rotMat[0][0]*D[2][0] + rotMat[0][1]*D[2][1] + t[0];
                Y[2] = rotMat[1][0]*D[2][0] + rotMat[1][1]*D[2][1] + t[1];
                Z[2] = rotMat[2][0]*D[2][0] + rotMat[2][1]*D[2][1] + t[2];;

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];

                X[3] = rotMat[0][0]*D[3][0] + rotMat[0][1]*D[3][1] + t[0];
                Y[3] = rotMat[1][0]*D[3][0] + rotMat[1][1]*D[3][1] + t[1];
                Z[3] = rotMat[2][0]*D[3][0] + rotMat[2][1]*D[3][1] + t[2];;

                x[3] = focal*X[3]/Z[3]; y[3] = focal*Y[3]/Z[3];

        //		printf("\n Z's \n%3.5f %3.5f %3.5f %3.5f\n", Z[0],Z[1],Z[2],Z[3]);
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                //~ printf("\n re - projected \n%3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n", x[0],y[0], x[1],y[1], x[2],y[2], x[3],y[3]);

                printf("\nre-projected Coordinates : \n");
                {
                    int i;
                    for(i = 0; i < 4; i++){
                        printf("\n{%3.5f,%3.5f},", x[i]+imWidth/2,y[i]+imHeight/2);
                    }
                }
    //            __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},", x[0]+imWidth/2,y[0]+imHeight/2, x[1]+imWidth/2,y[1]+imHeight/2, x[2]+imWidth/2,y[2]+imHeight/2, x[3]+imWidth/2,y[3]+imHeight/2);
                // printf("\n error: %3.5f ",marker[0][0]*Z[0]/focal - cons[0] - marker[0][0]*t[2]/focal);		//error = 0;

                // printf("\n side1  : %3.5f ", sqrt((X[0]-X[1])*(X[0]-X[1])+(Y[0]-Y[1])*(Y[0]-Y[1])+(Z[0]-Z[1])*(Z[0]-Z[1])) );
                // printf("\n side2 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n side3  : %3.5f ", sqrt((X[0]-X[3])*(X[0]-X[3])+(Y[0]-Y[3])*(Y[0]-Y[3])+(Z[0]-Z[3])*(Z[0]-Z[3])) );
                // printf("\n side4 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n error: %3.5f ",rotMat[0][0]*D[0][0]+rotMat[0][1]*D[0][1] + t[0] - marker[0][0]*Z[0]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[0][0]+rotMat[1][1]*D[0][1] + t[1] - marker[0][1]*Z[0]/focal);

                // printf("\n error: %3.5f ",rotMat[0][0]*D[1][0]+rotMat[0][1]*D[1][1] + t[0]- marker[1][0]*Z[1]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[1][0]+rotMat[1][1]*D[1][1] + t[1]- marker[1][1]*Z[1]/focal);

                XVec[0] = X[1]-X[0];XVec[1] = Y[1]-Y[0];XVec[2] = Z[1]-Z[0];
                YVec[0] = X[1]-X[2];YVec[1] = Y[1]-Y[2];YVec[2] = Z[1]-Z[2];


                // t[1] = (marker[1][1]*((rotMat[0][0] * D[1][0] +
                normalize(XVec); normalize(YVec);

                dotProd = dot(XVec,YVec);

                cross(XVec,YVec,ZVec);
                //~ printf("\n dotProd : %3.5f \nZVec : %3.5f %3.5f %3.5f \n",dotProd,ZVec[0],ZVec[1],ZVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","XVec : %3.5f %3.5f %3.5f ",XVec[0],XVec[1],XVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","YVec : %3.5f %3.5f %3.5f ",YVec[0],YVec[1],YVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","ZVec : %3.5f %3.5f %3.5f ",ZVec[0],ZVec[1],ZVec[2]);

        //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@           END                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@ LOOP END @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                //~ error = sqrt((x[2] - marker[2][0])*(x[2] - marker[2][0]) + (y[2] - marker[2][1])*(y[2] - marker[2][1]));
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "MarkerPoint :  %3.5f,%3.5f,%3.5f ",X[0],Y[0],Z[0]);
                //~ if(error < 12){
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
                //~ }
                marker3D[0+3*3] = X[0];marker3D[0+3*0] = X[1];marker3D[0+3*1] = X[2];marker3D[0+3*2] = X[3];
                marker3D[1+3*3] = Y[0];marker3D[1+3*0] = Y[1];marker3D[1+3*1] = Y[2];marker3D[1+3*2] = Y[3];
                marker3D[2+3*3] = Z[0];marker3D[2+3*0] = Z[1];marker3D[2+3*1] = Z[2];marker3D[2+3*2] = Z[3];
                //~ Error calculation 

                X[2] = rotMat[0][0]*(D[2][0]+D[3][0])/2 + rotMat[0][1]*(D[2][1]+D[3][1])/2 + t[0];
                Y[2] = rotMat[1][0]*(D[2][0]+D[3][0])/2 + rotMat[1][1]*(D[2][1]+D[3][1])/2 + t[1];
                Z[2] = rotMat[2][0]*(D[2][0]+D[3][0])/2 + rotMat[2][1]*(D[2][1]+D[3][1])/2 + t[2];

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];
                error = sqrt((x[2] - (marker[2][0]+marker[3][0])/2 )*(x[2] - (marker[2][0]+marker[3][0])/2) + (y[2] - (marker[2][1]+marker[3][1])/2)*(y[2] - (marker[2][1]+marker[3][1])/2));

            }
        }
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Error : %f",error);
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","--------------------------------- END -------------------------------------");
        return error;
	}

	else if(mode == 3){
        double D[4][2] ={{w/2,-h/2},{-w/2,-h/2},{-w/2,h/2},{w/2,h/2}};
    //	double rotMat[3][3];
        double co,si;
        int i,j,k;
        // double temp,tempAngle;
        double diff_D_x,diff_D_y,diff_Marker_y,diff_Marker_x;
        double t[3];
        double XVec[3],YVec[3],ZVec[3],dotProd;

        double psi[2];
        double error = 9999;

    //	focal = focal*0.6;

        //////////////////////////////////// calculating theta & phi //////////////////////

        new_marker(marker,imWidth,imHeight);


        for( i = 0 ; i < 4; i++){
            //~ printf("\n%3.5f %3.5f",marker[i][0],marker[i][1]);
            //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},",marker[i][0]+(imWidth/2),marker[i][1]+(imHeight/2));
        }
    //    __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},",marker[0][0]+imWidth/2,marker[0][1]+imHeight/2,marker[1][0]+imWidth/2,marker[1][1]+imHeight/2,marker[2][0]+imWidth/2,marker[2][1]+imHeight/2,marker[3][0]+imWidth/2,marker[3][1]+imHeight/2);
        //~ printf("\n");

        tmp[0] = z[2];
        theta = acos(z[2]);
        phi = atan2(z[0],-z[1]);
        // phi += M_PI;

        ///////////////////////////////////////////////////////////////////////////////////

    //	printf("theta  = %3.5f phi = %3.5f \n", theta*180/M_PI,phi*180/M_PI);
    //	__android_log_print(ANDROID_LOG_VERBOSE,"AR native","theta  = %3.5f phi = %3.5f", theta*180/M_PI,phi*180/M_PI);

        tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

        a1 = cos(phi); a2 = -cos(theta)*sin(phi);
        b1 = sin(phi); b2 = cos(theta)*cos(phi);
        c1 = 0; 	   c2 = sin(theta);

        l1 = -cos(theta)*sin(phi); l2 = -cos(phi);
        m1 =  cos(phi)*cos(theta); m2 = -sin(phi);
        n1 = sin(theta); 		   n2 = 0;

        diff_D_x = D[0][0] - D[1][0]; diff_D_y = D[0][1] - D[1][1];
        diff_Marker_x = marker[0][0] - marker[1][0]; diff_Marker_y = marker[0][1] - marker[1][1];

        /////////// calculating a,b coefficients for equation a*cos(psi)+b*sin(psi) = 0;

        a = diff_Marker_y*(a1*diff_D_x + l1*diff_D_y +
        c1*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n1*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b1*diff_D_x +
        m1*diff_D_y + c1*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n1*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        b = diff_Marker_y*(a2*diff_D_x + l2*diff_D_y +
        c2*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n2*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b2*diff_D_x +
        m2*diff_D_y + c2*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n2*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        psi[0] = atan2(a,-b);
        psi[1] = M_PI + psi[0];

        for(i = 0 ; i < 2; i++){
    //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@@@        START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@@@       LOOP START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			
			if( diff_Marker_y != 0){
				t[2] = ((cos(psi[i])*b1 + sin(psi[i])*b2)*diff_D_x + (cos(psi[i])*m1 + sin(psi[i])*m2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal) * focal/diff_Marker_y;
			}
			else if(diff_Marker_x != 0){
				t[2] = ((cos(psi[i])*a1 + sin(psi[i])*a2)*diff_D_x + (cos(psi[i])*l1 + sin(psi[i])*l2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;
			}
			
            if(t[2] > 0){
                rotMat[0][0] = (cos(psi[i])*a1 + sin(psi[i])*a2);
                rotMat[0][1] = (cos(psi[i])*l1 + sin(psi[i])*l2);
                rotMat[0][2] = z[0];

                rotMat[1][0] = (cos(psi[i])*b1 + sin(psi[i])*b2);
                rotMat[1][1] = (cos(psi[i])*m1 + sin(psi[i])*m2);
                rotMat[1][2] = z[1];


                rotMat[2][0] = (cos(psi[i])*c1 + sin(psi[i])*c2);
                rotMat[2][1] = (cos(psi[i])*n1 + sin(psi[i])*n2);
                rotMat[2][2] = z[2];

                //~ t[2] = (rotMat[0][0]*diff_D_x + rotMat[0][1]*diff_D_y + rotMat[2][0]*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + rotMat[2][1]*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;

                Z[0] = rotMat[2][0]*D[0][0] + rotMat[2][1]*D[0][1] + t[2];

                t[1] = Z[0]*marker[0][1]/focal- rotMat[1][0]*D[0][0] - rotMat[1][1]*D[0][1] ;
                t[0] = Z[0]*marker[0][0]/focal- rotMat[0][0]*D[0][0] - rotMat[0][1]*D[0][1] ;

        //		printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
        //	    __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);

                rotMat[0][3] = t[0];
                rotMat[1][3] = t[1];
                rotMat[2][3] = t[2];

                rotMat[3][3] = 1;
                rotMat[3][0] = rotMat[3][1] = rotMat[3][2] = 0;

                X[0] = rotMat[0][0]*D[0][0] + rotMat[0][1]*D[0][1] + t[0];
                Y[0] = rotMat[1][0]*D[0][0] + rotMat[1][1]*D[0][1] + t[1];


                x[0] = focal*X[0]/Z[0]; y[0] = focal*Y[0]/Z[0];


                X[1] = rotMat[0][0]*D[1][0] + rotMat[0][1]*D[1][1] + t[0];
                Y[1] = rotMat[1][0]*D[1][0] + rotMat[1][1]*D[1][1] + t[1];
                Z[1] = rotMat[2][0]*D[1][0] + rotMat[2][1]*D[1][1] + t[2];

                x[1] = focal*X[1]/Z[1]; y[1] = focal*Y[1]/Z[1];



                // t[0] = marker[1][0]*Z[1]/focal - rotMat[0][0]*D[1][0] - rotMat[0][1]*D[1][1];
                // t[1] = marker[1][1]*Z[1]/focal - rotMat[1][0]*D[1][0] - rotMat[1][1]*D[1][1];
                // printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);



                X[2] = rotMat[0][0]*D[2][0] + rotMat[0][1]*D[2][1] + t[0];
                Y[2] = rotMat[1][0]*D[2][0] + rotMat[1][1]*D[2][1] + t[1];
                Z[2] = rotMat[2][0]*D[2][0] + rotMat[2][1]*D[2][1] + t[2];;

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];

                X[3] = rotMat[0][0]*D[3][0] + rotMat[0][1]*D[3][1] + t[0];
                Y[3] = rotMat[1][0]*D[3][0] + rotMat[1][1]*D[3][1] + t[1];
                Z[3] = rotMat[2][0]*D[3][0] + rotMat[2][1]*D[3][1] + t[2];;

                x[3] = focal*X[3]/Z[3]; y[3] = focal*Y[3]/Z[3];

        //		printf("\n Z's \n%3.5f %3.5f %3.5f %3.5f\n", Z[0],Z[1],Z[2],Z[3]);
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                //~ printf("\n re - projected \n%3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n", x[0],y[0], x[1],y[1], x[2],y[2], x[3],y[3]);

                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","re-projected Coordinates : ");
                //~ {
                    //~ int i;
                    //~ for(i = 0; i < 4; i++){
                        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "{%3.5f,%3.5f},", x[i]+imWidth/2,y[i]+imHeight/2);
                    //~ }
                //~ }
                printf("\nre-projected Coordinates : \n");
                {
                    int i;
                    for(i = 0; i < 4; i++){
                        printf("\n{%3.5f,%3.5f},", x[i]+imWidth/2,y[i]+imHeight/2);
                    }
                }

    //            __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},", x[0]+imWidth/2,y[0]+imHeight/2, x[1]+imWidth/2,y[1]+imHeight/2, x[2]+imWidth/2,y[2]+imHeight/2, x[3]+imWidth/2,y[3]+imHeight/2);
                // printf("\n error: %3.5f ",marker[0][0]*Z[0]/focal - cons[0] - marker[0][0]*t[2]/focal);		//error = 0;

                // printf("\n side1  : %3.5f ", sqrt((X[0]-X[1])*(X[0]-X[1])+(Y[0]-Y[1])*(Y[0]-Y[1])+(Z[0]-Z[1])*(Z[0]-Z[1])) );
                // printf("\n side2 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n side3  : %3.5f ", sqrt((X[0]-X[3])*(X[0]-X[3])+(Y[0]-Y[3])*(Y[0]-Y[3])+(Z[0]-Z[3])*(Z[0]-Z[3])) );
                // printf("\n side4 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n error: %3.5f ",rotMat[0][0]*D[0][0]+rotMat[0][1]*D[0][1] + t[0] - marker[0][0]*Z[0]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[0][0]+rotMat[1][1]*D[0][1] + t[1] - marker[0][1]*Z[0]/focal);

                // printf("\n error: %3.5f ",rotMat[0][0]*D[1][0]+rotMat[0][1]*D[1][1] + t[0]- marker[1][0]*Z[1]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[1][0]+rotMat[1][1]*D[1][1] + t[1]- marker[1][1]*Z[1]/focal);

                XVec[0] = X[1]-X[0];XVec[1] = Y[1]-Y[0];XVec[2] = Z[1]-Z[0];
                YVec[0] = X[1]-X[2];YVec[1] = Y[1]-Y[2];YVec[2] = Z[1]-Z[2];


                // t[1] = (marker[1][1]*((rotMat[0][0] * D[1][0] +
                normalize(XVec); normalize(YVec);

                dotProd = dot(XVec,YVec);

                cross(XVec,YVec,ZVec);
                //~ printf("\n dotProd : %3.5f \nZVec : %3.5f %3.5f %3.5f \n",dotProd,ZVec[0],ZVec[1],ZVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","XVec : %3.5f %3.5f %3.5f ",XVec[0],XVec[1],XVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","YVec : %3.5f %3.5f %3.5f ",YVec[0],YVec[1],YVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","ZVec : %3.5f %3.5f %3.5f ",ZVec[0],ZVec[1],ZVec[2]);

        //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@           END                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@ LOOP END @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                //~ error = sqrt((x[2] - marker[2][0])*(x[2] - marker[2][0]) + (y[2] - marker[2][1])*(y[2] - marker[2][1]));
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "MarkerPoint :  %3.5f,%3.5f,%3.5f ",X[0],Y[0],Z[0]);
                //~ if(error < 12){
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
                //~ }
                marker3D[0+3*2] = X[0];marker3D[0+3*3] = X[1];marker3D[0+3*0] = X[2];marker3D[0+3*1] = X[3];
                marker3D[1+3*2] = Y[0];marker3D[1+3*3] = Y[1];marker3D[1+3*0] = Y[2];marker3D[1+3*1] = Y[3];
                marker3D[2+3*2] = Z[0];marker3D[2+3*3] = Z[1];marker3D[2+3*0] = Z[2];marker3D[2+3*1] = Z[3];
                //~ Error calculation 

                X[2] = rotMat[0][0]*(D[2][0]+D[3][0])/2 + rotMat[0][1]*(D[2][1]+D[3][1])/2 + t[0];
                Y[2] = rotMat[1][0]*(D[2][0]+D[3][0])/2 + rotMat[1][1]*(D[2][1]+D[3][1])/2 + t[1];
                Z[2] = rotMat[2][0]*(D[2][0]+D[3][0])/2 + rotMat[2][1]*(D[2][1]+D[3][1])/2 + t[2];

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];
                error = sqrt((x[2] - (marker[2][0]+marker[3][0])/2 )*(x[2] - (marker[2][0]+marker[3][0])/2) + (y[2] - (marker[2][1]+marker[3][1])/2)*(y[2] - (marker[2][1]+marker[3][1])/2));

            }
        }
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Error : %f",error);
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","--------------------------------- END -------------------------------------");
        return error;
	}
	else if(mode == 4){
        double D[4][2] ={{w/2,h/2},{w/2,-h/2},{-w/2,-h/2},{-w/2,h/2}};
    //	double rotMat[3][3];
        double co,si;
        int i,j,k;
        // double temp,tempAngle;
        double diff_D_x,diff_D_y,diff_Marker_y,diff_Marker_x;
        double t[3];
        double XVec[3],YVec[3],ZVec[3],dotProd;

        double psi[2];
        double error = 9999;

    //	focal = focal*0.6;

        //////////////////////////////////// calculating theta & phi //////////////////////

        new_marker(marker,imWidth,imHeight);


        for( i = 0 ; i < 4; i++){
            //~ printf("\n%3.5f %3.5f",marker[i][0],marker[i][1]);
            //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},",marker[i][0]+(imWidth/2),marker[i][1]+(imHeight/2));
        }
    //    __android_log_print(ANDROID_LOG_VERBOSE,"AR native","{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},",marker[0][0]+imWidth/2,marker[0][1]+imHeight/2,marker[1][0]+imWidth/2,marker[1][1]+imHeight/2,marker[2][0]+imWidth/2,marker[2][1]+imHeight/2,marker[3][0]+imWidth/2,marker[3][1]+imHeight/2);
        //~ printf("\n");

        tmp[0] = z[2];
        theta = acos(z[2]);
        phi = atan2(z[0],-z[1]);
        // phi += M_PI;

        ///////////////////////////////////////////////////////////////////////////////////

    //	printf("theta  = %3.5f phi = %3.5f \n", theta*180/M_PI,phi*180/M_PI);
    //	__android_log_print(ANDROID_LOG_VERBOSE,"AR native","theta  = %3.5f phi = %3.5f", theta*180/M_PI,phi*180/M_PI);

        tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

        a1 = cos(phi); a2 = -cos(theta)*sin(phi);
        b1 = sin(phi); b2 = cos(theta)*cos(phi);
        c1 = 0; 	   c2 = sin(theta);

        l1 = -cos(theta)*sin(phi); l2 = -cos(phi);
        m1 =  cos(phi)*cos(theta); m2 = -sin(phi);
        n1 = sin(theta); 		   n2 = 0;

        diff_D_x = D[0][0] - D[1][0]; diff_D_y = D[0][1] - D[1][1];
        diff_Marker_x = marker[0][0] - marker[1][0]; diff_Marker_y = marker[0][1] - marker[1][1];

        /////////// calculating a,b coefficients for equation a*cos(psi)+b*sin(psi) = 0;

        a = diff_Marker_y*(a1*diff_D_x + l1*diff_D_y +
        c1*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n1*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b1*diff_D_x +
        m1*diff_D_y + c1*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n1*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        b = diff_Marker_y*(a2*diff_D_x + l2*diff_D_y +
        c2*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n2*(marker[1][0]*D[
        1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b2*diff_D_x +
        m2*diff_D_y + c2*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
        n2*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

        psi[0] = atan2(a,-b);
        psi[1] = M_PI + psi[0];

        for(i = 0 ; i < 2; i++){
    //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@@@        START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@@@       LOOP START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			if( diff_Marker_y != 0){
				t[2] = ((cos(psi[i])*b1 + sin(psi[i])*b2)*diff_D_x + (cos(psi[i])*m1 + sin(psi[i])*m2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal) * focal/diff_Marker_y;
			}
			else if(diff_Marker_x != 0){
				t[2] = ((cos(psi[i])*a1 + sin(psi[i])*a2)*diff_D_x + (cos(psi[i])*l1 + sin(psi[i])*l2)*diff_D_y + (cos(psi[i])*c1 + sin(psi[i])*c2)*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + (cos(psi[i])*n1 + sin(psi[i])*n2)*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;
			}

            if(t[2] > 0){
                rotMat[0][0] = (cos(psi[i])*a1 + sin(psi[i])*a2);
                rotMat[0][1] = (cos(psi[i])*l1 + sin(psi[i])*l2);
                rotMat[0][2] = z[0];

                rotMat[1][0] = (cos(psi[i])*b1 + sin(psi[i])*b2);
                rotMat[1][1] = (cos(psi[i])*m1 + sin(psi[i])*m2);
                rotMat[1][2] = z[1];


                rotMat[2][0] = (cos(psi[i])*c1 + sin(psi[i])*c2);
                rotMat[2][1] = (cos(psi[i])*n1 + sin(psi[i])*n2);
                rotMat[2][2] = z[2];

                //~ t[2] = (rotMat[0][0]*diff_D_x + rotMat[0][1]*diff_D_y + rotMat[2][0]*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + rotMat[2][1]*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;

                Z[0] = rotMat[2][0]*D[0][0] + rotMat[2][1]*D[0][1] + t[2];

                t[1] = Z[0]*marker[0][1]/focal- rotMat[1][0]*D[0][0] - rotMat[1][1]*D[0][1] ;
                t[0] = Z[0]*marker[0][0]/focal- rotMat[0][0]*D[0][0] - rotMat[0][1]*D[0][1] ;

        //		printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
        //	    __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);

                rotMat[0][3] = t[0];
                rotMat[1][3] = t[1];
                rotMat[2][3] = t[2];

                rotMat[3][3] = 1;
                rotMat[3][0] = rotMat[3][1] = rotMat[3][2] = 0;

                X[0] = rotMat[0][0]*D[0][0] + rotMat[0][1]*D[0][1] + t[0];
                Y[0] = rotMat[1][0]*D[0][0] + rotMat[1][1]*D[0][1] + t[1];


                x[0] = focal*X[0]/Z[0]; y[0] = focal*Y[0]/Z[0];


                X[1] = rotMat[0][0]*D[1][0] + rotMat[0][1]*D[1][1] + t[0];
                Y[1] = rotMat[1][0]*D[1][0] + rotMat[1][1]*D[1][1] + t[1];
                Z[1] = rotMat[2][0]*D[1][0] + rotMat[2][1]*D[1][1] + t[2];

                x[1] = focal*X[1]/Z[1]; y[1] = focal*Y[1]/Z[1];



                // t[0] = marker[1][0]*Z[1]/focal - rotMat[0][0]*D[1][0] - rotMat[0][1]*D[1][1];
                // t[1] = marker[1][1]*Z[1]/focal - rotMat[1][0]*D[1][0] - rotMat[1][1]*D[1][1];
                // printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);



                X[2] = rotMat[0][0]*D[2][0] + rotMat[0][1]*D[2][1] + t[0];
                Y[2] = rotMat[1][0]*D[2][0] + rotMat[1][1]*D[2][1] + t[1];
                Z[2] = rotMat[2][0]*D[2][0] + rotMat[2][1]*D[2][1] + t[2];;

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];

                X[3] = rotMat[0][0]*D[3][0] + rotMat[0][1]*D[3][1] + t[0];
                Y[3] = rotMat[1][0]*D[3][0] + rotMat[1][1]*D[3][1] + t[1];
                Z[3] = rotMat[2][0]*D[3][0] + rotMat[2][1]*D[3][1] + t[2];;

                x[3] = focal*X[3]/Z[3]; y[3] = focal*Y[3]/Z[3];

        //		printf("\n Z's \n%3.5f %3.5f %3.5f %3.5f\n", Z[0],Z[1],Z[2],Z[3]);
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                //~ printf("\n re - projected \n%3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n", x[0],y[0], x[1],y[1], x[2],y[2], x[3],y[3]);

                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","re-projected Coordinates : ");
                //~ {
                    //~ int i;
                    //~ for(i = 0; i < 4; i++){
                        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "{%3.5f,%3.5f},", x[i]+imWidth/2,y[i]+imHeight/2);
                    //~ }
                //~ }
                printf("\nre-projected Coordinates : \n");
                {
                    int i;
                    for(i = 0; i < 4; i++){
                        printf("\n{%3.5f,%3.5f},", x[i]+imWidth/2,y[i]+imHeight/2);
                    }
                }

    //            __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},{%3.5f,%3.5f},", x[0]+imWidth/2,y[0]+imHeight/2, x[1]+imWidth/2,y[1]+imHeight/2, x[2]+imWidth/2,y[2]+imHeight/2, x[3]+imWidth/2,y[3]+imHeight/2);
                // printf("\n error: %3.5f ",marker[0][0]*Z[0]/focal - cons[0] - marker[0][0]*t[2]/focal);		//error = 0;

                // printf("\n side1  : %3.5f ", sqrt((X[0]-X[1])*(X[0]-X[1])+(Y[0]-Y[1])*(Y[0]-Y[1])+(Z[0]-Z[1])*(Z[0]-Z[1])) );
                // printf("\n side2 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n side3  : %3.5f ", sqrt((X[0]-X[3])*(X[0]-X[3])+(Y[0]-Y[3])*(Y[0]-Y[3])+(Z[0]-Z[3])*(Z[0]-Z[3])) );
                // printf("\n side4 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

                // printf("\n error: %3.5f ",rotMat[0][0]*D[0][0]+rotMat[0][1]*D[0][1] + t[0] - marker[0][0]*Z[0]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[0][0]+rotMat[1][1]*D[0][1] + t[1] - marker[0][1]*Z[0]/focal);

                // printf("\n error: %3.5f ",rotMat[0][0]*D[1][0]+rotMat[0][1]*D[1][1] + t[0]- marker[1][0]*Z[1]/focal);
                // printf("\n error: %3.5f ",rotMat[1][0]*D[1][0]+rotMat[1][1]*D[1][1] + t[1]- marker[1][1]*Z[1]/focal);

                XVec[0] = X[1]-X[0];XVec[1] = Y[1]-Y[0];XVec[2] = Z[1]-Z[0];
                YVec[0] = X[1]-X[2];YVec[1] = Y[1]-Y[2];YVec[2] = Z[1]-Z[2];


                // t[1] = (marker[1][1]*((rotMat[0][0] * D[1][0] +
                normalize(XVec); normalize(YVec);

                dotProd = dot(XVec,YVec);

                cross(XVec,YVec,ZVec);
                //~ printf("\n dotProd : %3.5f \nZVec : %3.5f %3.5f %3.5f \n",dotProd,ZVec[0],ZVec[1],ZVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","XVec : %3.5f %3.5f %3.5f ",XVec[0],XVec[1],XVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","YVec : %3.5f %3.5f %3.5f ",YVec[0],YVec[1],YVec[2]);
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","ZVec : %3.5f %3.5f %3.5f ",ZVec[0],ZVec[1],ZVec[2]);

        //		printf("\n@@@@@@@@@@@@@@@@@@@@@@@           END                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        //		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","@@@@@@@@@@@@@@@@@@@@@@@ LOOP END @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                //~ error = sqrt((x[2] - marker[2][0])*(x[2] - marker[2][0]) + (y[2] - marker[2][1])*(y[2] - marker[2][1]));
                //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "MarkerPoint :  %3.5f,%3.5f,%3.5f ",X[0],Y[0],Z[0]);
                //~ if(error < 12){
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Z's : %3.5f %3.5f %3.5f %3.5f", Z[0],Z[1],Z[2],Z[3]);
                    //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
                //~ }
                marker3D[0+3*1] = X[0];marker3D[0+3*2] = X[1];marker3D[0+3*3] = X[2];marker3D[0+3*0] = X[3];
                marker3D[1+3*1] = Y[0];marker3D[1+3*2] = Y[1];marker3D[1+3*3] = Y[2];marker3D[1+3*0] = Y[3];
                marker3D[2+3*1] = Z[0];marker3D[2+3*2] = Z[1];marker3D[2+3*3] = Z[2];marker3D[2+3*0] = Z[3];
                //~ Error calculation 

                X[2] = rotMat[0][0]*(D[2][0]+D[3][0])/2 + rotMat[0][1]*(D[2][1]+D[3][1])/2 + t[0];
                Y[2] = rotMat[1][0]*(D[2][0]+D[3][0])/2 + rotMat[1][1]*(D[2][1]+D[3][1])/2 + t[1];
                Z[2] = rotMat[2][0]*(D[2][0]+D[3][0])/2 + rotMat[2][1]*(D[2][1]+D[3][1])/2 + t[2];

                x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];
                error = sqrt((x[2] - (marker[2][0]+marker[3][0])/2 )*(x[2] - (marker[2][0]+marker[3][0])/2) + (y[2] - (marker[2][1]+marker[3][1])/2)*(y[2] - (marker[2][1]+marker[3][1])/2));
           
            }
        }
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Error : %f",error);
        //~ __android_log_print(ANDROID_LOG_VERBOSE,"AR native","--------------------------------- END -------------------------------------");
        return error;
	}
}
