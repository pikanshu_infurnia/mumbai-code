void new_marker(double marker[4][2],int imWidth,int imHeight);
void normalize(double x[3]);
double dot(double x[3],double y[3]);
void cross(double x[3],double y[3],double z[3]);
double verify(double rotMat[4][4],double marker[4][2], double z[3], double focal,int imWidth,int imHeight,double marker3D[12],int mode);
