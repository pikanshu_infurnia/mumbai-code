#include "global_lineDetection.h"

void CannyThreshold(Mat Image,Mat& Edges,int lowThresh,int ratio,int ApertureSize){
	GaussianBlur(Image,Edges, Size(5,5),sqrt(2));
	Canny(Edges,Edges, lowThresh, lowThresh*ratio, ApertureSize,false);
//	imshow("Edges",Edges);
}
