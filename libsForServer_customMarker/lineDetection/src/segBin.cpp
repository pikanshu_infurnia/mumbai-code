#include "global_lineDetection.h"
/// Sub matrix extraction code/////
void binExtract(int& WinR,vector<double>& row, vector<double>& col, int rowSize, int colSize, int const WinT, SCell S, const double Theta, const double Rho,double RhoRes){
//	cout<<endl<<"                   /////////////PRINTING IN binExtract///////////////                           "<<endl;
	int RhoIndex = S.RhoIndex;
	int ThetaIndex = S.ThetaIndex;
//	cout<<"Location : ("<<RhoIndex<<","<<ThetaIndex<<")"<<endl;
	double S1 = S.StartPt[0], S2 = S.StartPt[1];
	double SlopeS,SlopeE,PsiS,PsiE;
	double distS,distE;
	double E1 = S.EndPt[0], E2 = S.EndPt[1];
	double c,c2,r1,r2,r0,c0;
	double a,b,x,d,m1,m2;
	double LenS,LenE,TPS,TPE;
	//Mat out = Mat::zeros(16*(2*WinR+1),16*(2*WinT+1),CV_8UC3);
	PsiS = atan2(S1,S2); PsiE =  atan2(E1,E2);
	LenS = S1*cos(Theta)-S2*sin(Theta);
	LenE = E1*cos(Theta)-E2*sin(Theta);
	TPS = (double)atan2(LenS,abs(Rho));
	TPE = (double)atan2(LenE,abs(Rho));
//	cout<< "PsiS " << PsiS<<" "<<"PsiE "<<PsiE<<endl;
//	cout<< "LenS " << LenS<<" "<<"LenE "<<LenE<<endl;
//	cout<< "TPS " << TPS<<" "<<"TPE "<<TPE<<endl;
//	cout<< "DiffS " << (double)Theta-PsiS<<" "<<"DiffE "<<(double)Theta-PsiE<<endl;
//	cout<< "("<<S1<<","<<S2<<")"<<"   "<< "("<<E1<<","<<E2<<")"<<endl;
	distS = sqrt(S1*S1 + S2*S2); distE = sqrt(E1*E1 + E2*E2);
//	cout<< "distS " << distS<<" "<<"distE "<<distE<<endl;
	SlopeS = ((distS*PI*sin((double)Theta-PsiS))/(RhoRes*180)); SlopeE = ((distE*PI*sin((double)Theta-PsiE))/(RhoRes*180));           
//	cout<<"SlopeS : " <<SlopeS<<endl<<"SlopeE : " <<SlopeE<<endl; 

	for(int c1=-WinT;c1<=WinT;c1 = c1+WinT+WinT){
		int temp1,temp2;
		bool flag =0;
		temp1 = round(c1*SlopeS);
		temp2 = round(c1*SlopeE);
		if(temp1 > temp2){
			flag = 1;
		}
		
		r1 = ((temp1 > temp2) ? temp2:temp1) - 2; 
		r2 = ((temp1 > temp2) ? temp1:temp2) + 2;

		c2 = ThetaIndex+c1;r2=RhoIndex-r2; r1=RhoIndex-r1;

		row.push_back(r2);col.push_back(c2); row.push_back(r1);col.push_back(c2);
		
		WinR = ((abs(RhoIndex-r1) > abs(RhoIndex-r2)) ? abs(RhoIndex-r1) : abs(RhoIndex-r2));
		
		if(flag == 0) { m1 = -SlopeS; m2 = -SlopeE;}
		else {m2 = -SlopeS; m1 = -SlopeE;}
		
		a = m1; b = m2; x = r1 - m1*c2; d = r2 - m2*c2;
//		a = m1; b = m2;x = c2 - m1*r1 ; d = c2 - m2*r2;
		
		c0 = round((d - x)/(a - b));
		r0 = round((d*a - x*b)/(a-b));  
//		cout<<endl;  
		row.push_back(r0);col.push_back(c0);  
		//cout<<"("<<r2<<","<<r1<<","<<c2<<","<<r0<<","<<c0<<")";          
	}
	
	//row.push_back(RhoIndex);col.push_back(ThetaIndex);
//	cout<<"\tROW\t"<<"COL\n";
//	for(int i =0 ; i < row.size(); i++) cout<< "\t"<<row[i]<<"\t"<<col[i]<<endl;
//	cout<<endl;
//	cout<<WinR<<endl;
//	cout<<"                   /////////////PRINTING IN binExtract END///////////////                           "<<endl<<endl;
}

	
	

////// plotData(Extration Algo. code here)/////////
// input-> (row,col,Label),Segment, H-to verify qualification, rho and theta(in radians) of segment
// output-> row,col,Label -if segment qualified for merging, if not then its empty
// output-> boolean vector, true-> should be merged, false-> ignore it for a while
bool verifyBin(SCell S, Mat& H, const double Rho, const double Theta,double& WRho,double& WTheta,double RhoRes){
//	cout<<"Address H :"<< &H<< endl;
//	cout<<endl<<"                   /////////////PRINTING IN verifyBin///////////////                           "<<endl;
	double S1 = S.StartPt[0],S2 = S.StartPt[1];//cout<<"Starting Pt: ("<<S1<<","<<S2<<")"<<endl;
	double E1 = S.EndPt[0],E2 = S.EndPt[1];//cout<<"Ending Pt: ("<<E1<<","<<E2<<")"<<endl;
	double Angle;
	double PsiM,TPDiff,PsiS,PsiE;
	double LenS,LenE,TPS,TPE,Int;
	if(Rho!=0){
//		PsiS =(double)Theta-atan2(S1,S2); PsiE = (double)Theta-atan2(E1,E2);
		PsiM = (double)(atan2(S1,S2)+atan2(E1,E2))/2;//cout<<"PsiM : "<<PsiM<<endl;
		LenS = S1*cos(Theta)-S2*sin(Theta);
		LenE = E1*cos(Theta)-E2*sin(Theta);
		TPS = (double)atan2(LenS,abs(Rho));//cout<<"LenS : "<<LenS<<"; TPS : "<<TPS<<endl;
		TPE = (double)atan2(LenE,abs(Rho));//cout<<"LenE : "<<LenE<<"; TPE : "<<TPE<<endl;
		Int = log(((1/cos(TPS))+tan(TPS))/((1/cos(TPE))+tan(TPE)))/(TPS-TPE);//cout<<"Integral : "<<Int<<endl;
		
		TPDiff = Theta - PsiM;
		//cout<<"TPDiff = "<<TPDiff;
		if(TPDiff>PI/2)TPDiff = TPDiff - PI;
		if(TPDiff<-PI/2)TPDiff = TPDiff + PI;
		//cout<<"; After = "<<TPDiff<<endl;
		Angle = atan2(((Int*Rho*PI*sin(TPDiff))/180),RhoRes);
	}
	else{
		Angle = 0; // Bad approximation
//		cout<<"Rho is zero.........."<<endl;
		PsiM = (double)(atan2(S1,S2)+atan2(E1,E2))/2;//cout<<"PsiM : "<<PsiM<<endl;
		TPDiff = Theta - PsiM;
		double DistM = sqrt((S1+E1)*(S1+E1) + (S2+E2)*(S2+E2))/2;
		if(TPDiff>PI/2)TPDiff = TPDiff - PI;
		if(TPDiff<-PI/2)TPDiff = TPDiff + PI;
		Angle = atan2(DistM*(PI/180)*sin(TPDiff),RhoRes);
	}
	bool flagNAN = isnan(Angle);
	bool RatioPass = false;
	if(flagNAN==false){
//		cout<<"Int : "<<Int <<" "<<"TPDiff : "<<TPDiff<<" "<<"Angle(rad) : "<< Angle<<" "<<" Rho "<< Rho <<"Theta"<<Theta<<endl;
//		cout<<"Angle(deg) = "<<Angle*180/PI<<endl;
		// Test for peak - antisymmetry test
		int RhoIdx = S.RhoIndex;int ThetaIdx = S.ThetaIndex;int ThetaPos = 0;
		int RowSize = H.rows;int ColSize = H.cols;int RhoPos = 0;
		int Size = 3;
		vector <double> ExtH (2*Size+1,0),Temp_ExtH (2*Size+1,0);
//		cout<<"Extracted Votes along the angle: [";
		for(int sx=0;sx<ExtH.size();++sx){
			RhoPos = RhoIdx;
			ThetaPos = ThetaIdx - Size + sx;
			RhoPos = RhoIdx - round((sx-Size)*tan(Angle));
			if(ThetaPos<0){
				ThetaPos = ColSize + ThetaPos;
				RhoPos = RowSize - RhoPos - 1;
			}
			else if(ThetaPos>ColSize-1){
				ThetaPos = ThetaPos - ColSize;
				RhoPos = RowSize - RhoPos - 1;
			}
			Temp_ExtH[sx] = ((H.at<int>(RhoPos-1,ThetaPos) > H.at<int>(RhoPos,ThetaPos)) ? H.at<int>(RhoPos-1,ThetaPos) : H.at<int>(RhoPos,ThetaPos));
			Temp_ExtH[sx] = ((H.at<int>(RhoPos+1,ThetaPos) > Temp_ExtH[sx]) ? H.at<int>(RhoPos+1,ThetaPos) : Temp_ExtH[sx]);
			ExtH[sx] = (H.at<int>(RhoPos-1,ThetaPos)+H.at<int>(RhoPos,ThetaPos)+H.at<int>(RhoPos+1,ThetaPos))/Size;
//			cout<<  H.at<int>(RhoPos-1,ThetaPos) <<" "<<H.at<int>(RhoPos,ThetaPos)<<" "<<H.at<int>(RhoPos+1,ThetaPos);
//			cout<<" "<<ExtH[sx]<<endl;
		}
//		cout<<"]"<<endl;
		// giving output flag whether to consider for merging or not
		int Count = 0;float Ratio = 0;int CountComp = 2;int f = 0;
		for(int rx=0;rx<Size;++rx){
			Ratio = ExtH[rx]/ExtH[ExtH.size()-1-rx];
			//if(ExtH[Size]>0.9*(ExtH[rx]+ExtH[ExtH.size()-1-rx])/2)CountComp+=1;
			if(Ratio>.5 && Ratio<2)Count+=1;
			else if(rx == (Size-1)) f = 1;
		}
		if (f==1) Count = 0;
		if(Count>=1 && CountComp>1){
//			cout<<"Allowed "<<Count<<endl;
//			cout<<Temp_ExtH[Size-1]<<" "<<Temp_ExtH[Size]<<" "<<Temp_ExtH[Size+1]<<" "<< WRho<< " "<<WTheta<<endl;
			RatioPass = true;
		}
		else{
//			cout<<"Rejected"<<endl;
			RatioPass = false;
			return RatioPass;
		}
		//transform(col.begin(),col.end(),col.begin(),bind1st(multiplies<float>()),3));//tan(Angle)*col;
		
	}
	else{
		RatioPass = false;
	}
//	cout<<"                   /////////////PRINTING IN verifyBin END///////////////                           "<<endl<<endl;
	return RatioPass;
}
