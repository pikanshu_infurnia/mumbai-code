#include "global_lineDetection.h"
#include "lineSegments.h"

void joinSeg_modified(vector<SCell>& Line,vector<SCell>& Segments,vector<SCell>& raw_Segment, vector<int>& done, double Theta_Margin,double Rho_Margin,int seg_gap_thresh,vector<SCell>& Seg_in_range,bool HORIZONTAL,bool ON_RAW_SEGMENTS){
 
 	double Rho0,Theta0;

 	int initial_len_done_Seg_in_range;
 	vector<int> done_Seg_in_range;
	SCell temp_Seg;
	vector<SCell> temp_vec_SCell;
	double S1,E1,S2,E2;

 	int seg_gap;
 	
	for(int i = 0 ; i< Segments.size();i++){
	if( (ON_RAW_SEGMENTS && (done[i])) || ~(done[i]) ){

		//~ double S1 = Segments[i].StartPt[0],S2 = Segments[i].StartPt[1];
		//~ double E1 = Segments[i].EndPt[0],E2 = Segments[i].EndPt[1];
		//~ double slop_baseline = atan((round(S1) - round(E1))/(round(S2) - round(E2)));

		Theta0= Segments[i].slop*180/PI ;
		Rho0=   Segments[i].rho_new ;
		//~ printf("\n JOINSEG RHO0 %f Theta0 %f ",Rho0,Theta0);
		//~ double Rho1,Rho2;
		//~ Rho1 = S2*cos(slop_baseline- PI/2)+S1*sin(slop_baseline- PI/2);
		//~ Rho2 = S2*cos(slop_baseline+ PI/2)+S1*sin(slop_baseline+ PI/2);
		//~ double Theta1 = fabs(Rho1-Rho0)>fabs(Rho2-Rho0)?(slop_baseline+ PI/2):(slop_baseline- PI/2);
		//~ 
		//~ printf("\n JOINSEG RHO0 %f Theta0 %f ",Rho0,Theta1);
		//~ 
		if(ON_RAW_SEGMENTS) Seg_in_range = merge_in_range(raw_Segment,Theta0,Rho0,Theta_Margin,Rho_Margin,done);
		else  Seg_in_range = merge_in_range(Segments,Theta0,Rho0,Theta_Margin,Rho_Margin,done);

		initial_len_done_Seg_in_range = Seg_in_range.size();
		done_Seg_in_range.clear();
		done_Seg_in_range.resize(initial_len_done_Seg_in_range,0);
					
		for(int k = 0; k < initial_len_done_Seg_in_range-1;k++){
			if(done_Seg_in_range[k] != 1){
				for(int j = k+1; j < initial_len_done_Seg_in_range;j++){
					if(done_Seg_in_range[j] != 1){
    					if(HORIZONTAL){
    						S1 = Seg_in_range[j].StartPt[1]; E1 = Seg_in_range[j].EndPt[1];
    						S2 = Seg_in_range[k].StartPt[1]; E2 = Seg_in_range[k].EndPt[1];
    					}
    					else{
    						S1 = Seg_in_range[j].StartPt[0]; E1 = Seg_in_range[j].EndPt[0];
    						S2 = Seg_in_range[k].StartPt[0]; E2 = Seg_in_range[k].EndPt[0];
    					}
    					
    					seg_gap = 999;
    					if(abs(E2 - S1) < seg_gap) seg_gap = abs(E2 - S1);

    					if(abs(E1 - S2) < seg_gap) seg_gap = abs(E1 - S2);

    					if(((E1 > S2) && (E1 < E2)) || ((E2 > S1) && (E2 < E1))) seg_gap = 0;
    					else if(((E1 < S2) && (E1 > E2)) || ((E2 < S1) && (E2 > E1))) seg_gap = 0;
    					else if(((S1 > S2) && (S1 < E2)) || ((S2 > S1) && (S2 < E1))) seg_gap = 0;
    					else if(((S1 < S2) && (S1 > E2)) || ((S2 < S1) && (S2 > E1))) seg_gap = 0;

						if(seg_gap < seg_gap_thresh){
							done_Seg_in_range[j] = 1;
							done_Seg_in_range[k] = 1;
							temp_vec_SCell.push_back(Seg_in_range[k]);
							temp_vec_SCell.push_back(Seg_in_range[j]);
							temp_Seg = joinSeg(temp_vec_SCell);
							Seg_in_range[k] = temp_Seg;
							temp_vec_SCell.clear();
							break;
						}
					}	
				}
						
				if(done_Seg_in_range[k] == 1){done_Seg_in_range[k] = 2; k--;}
				else if(done_Seg_in_range[k] == 0){ Line.push_back(raw_Segment[Seg_in_range[k].SegIndex]);done_Seg_in_range[k] = 1;}
				else if(done_Seg_in_range[k] == 2) Line.push_back(Seg_in_range[k]);
			}
		}
		for(int k = 0; k < Seg_in_range.size();k++){
			if(done_Seg_in_range[k] == 0){
					if(k < initial_len_done_Seg_in_range) Line.push_back(raw_Segment[Seg_in_range[k].SegIndex]);
					else Line.push_back(Seg_in_range[k]);
			}
		}
	}
	}
}

