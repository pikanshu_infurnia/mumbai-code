#include "global_lineDetection.h"

///////// Bubble Sort /////////

void bubbleSort(vector <SCell>& Segments){
	int Len = Segments.size();
	SCell temp1;
	for(int ax=0;ax<Len;++ax){
		for(int sx=Len-1;sx>=ax+1;--sx){
			if(Segments[sx].Votes < Segments[sx-1].Votes){
				temp1 = Segments[sx];
				Segments[sx] = Segments[sx-1];
				Segments[sx-1] = temp1;
			}
		}
	}
}
////////// Quick Sort Algo //////////
void quickSort(vector<SCell>& Segments, int const len)
{
  int const lenD = len;
  SCell pivot;
  int ind = lenD/2;
  int i,j = 0,k = 0;
  if(lenD>1){
    vector<SCell> L(lenD), R(lenD);
    pivot = Segments[ind];
    for(i=0;i<lenD;i++){
      if(i!=ind){
        if(Segments[i].Votes > pivot.Votes){
          L[j] = Segments[i];
          j++;
        }
        else{
          R[k] = Segments[i];
          k++;
        }
      }
    }
    quickSort(L,j);
    quickSort(R,k);
    for(int cnt=0;cnt<lenD;cnt++){
      if(cnt<j){
        Segments[cnt] = L[cnt];
      }
      else if(cnt==j){
        Segments[cnt] = pivot;
      }
      else{
        Segments[cnt] = R[cnt-(j+1)];
      }
    }
  }
}

void quickSortNum(vector<double>& vec,int const len){
	int const lenD = len;
	double pivot;
	int ind = lenD/2;
	int i,j=0,k=0;
	if(lenD>1){
		vector<double> L(lenD),R(lenD);
		pivot = vec[ind];
		for(i = 0;i<lenD;i++){
			if(i != ind){
				if(vec[i]>pivot){
					L[j]= vec[i];
					j++;
				}
				else{
				R[k] = vec[i];
				k++;
				}
			}
		}
		quickSortNum(L,j);
		quickSortNum(R,k);
		for(int cnt = 0;cnt<lenD;cnt++){
			if(cnt<j){
				vec[cnt] = L[cnt];
			}
			else if(cnt == j){
				vec[cnt] = pivot;
			}
			else{
				vec[cnt] = R[cnt-(j+1)];
			}
		}
	}
}
