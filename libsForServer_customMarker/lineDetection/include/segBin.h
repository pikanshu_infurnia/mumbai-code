void binExtract(int& WinR,vector<double>& row, vector<double>& col, int rowSize, int colSize, int const WinT, SCell S, const double Theta, const double Rho,double RhoRes);

bool verifyBin(SCell S, Mat& H, const double Rho, const double Theta,double& WRho,double& WTheta,double RhoRes);
