//#ifndef GLOBAL_VARIABLES
//#define GLOBAL_VARIABLES

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "highgui.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <cmath>
#include <bits/stdc++.h>
using namespace cv;
using namespace std;


//SCell will store information about a particular linesegment
struct SCell{
		// SegmentCell = struct('StartPt',[],'EndPt',[],'Votes',[],'RhoIndex',[],'ThetaIndex',[],'Index',[]);
		//vector<unsigned int> StartPt;
		//vector<unsigned int> EndPt;
		double StartPt[2],EndPt[2];
		int Votes;
		int RhoIndex;
		int ThetaIndex;
		int SegIndex;
		double slop;
		double rho_new;
	};

struct LCell{
	double StartPt[2],EndPt[2];
	//int Votes;
};
//vector<SCell> Segment;


/// Global Vars ///
//extern Mat Image_inp,Image,Edges;
//extern bool Hist;
//extern int Type; // 1 -> photographed images !1-> synthetic images
// Hough Parameters //
//extern double RhoRes;
//extern double ThetaRes;
//extern int Threshold;
//extern int cannyThreshold; // Prev 335/450/520/350/280-GaussBlur - 6sq(2)
//extern float FillGap;
//extern int LenR,LenT;

#define PI 3.14159265
//extern double PERCENTILE ;
//extern vector<SCell> Segment;
//#endif
