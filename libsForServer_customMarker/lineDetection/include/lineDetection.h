#include "global_lineDetection.h"
#include "canny.h"
#include "hough.h"
#include "houghpeaks.h"
#include "canny_threshold.h"
#include "lineSegments.h"
#include "mainBaseSeg.h"
#include "joinSeg.h"

/// Global Vars ///
//Mat Image_inp,Image,Edges;
//bool Hist = true;
//int Type = 1; // 1 -> photographed images !1-> synthetic images
// Hough Parameters //
//double RhoRes = 1;
//double ThetaRes = 1;
//int Threshold =30;
//int cannyThreshold = 320 ; // Prev 335/450/520/350/280-GaussBlur - 6sq(2)
//float FillGap = 8;
//int LenR,LenT;
//double PERCENTILE = 50;

