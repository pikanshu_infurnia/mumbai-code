    float focal = 540;
    float ppx = 320;
    float ppy = 240;
    float aspect = 4/3;
    Mat rot_mat4 = (Mat_<double>(3,3) <<4.9090385E-4,-0.0077387393,-0.99996984,-0.9999771,-0.0067415237,-4.388392E-4,-0.0067377985,0.9999473,-0.0077420175);
    Mat rot_mat3 = (Mat_<double>(3,3) <<0.49238104,-0.003050238,-0.87037444,-0.8703364,0.008260354,-0.4923884,0.00869146,0.99996126,0.0014125258);
    Mat rot_mat2 = (Mat_<double>(3,3) <<0.86416847,-0.0013136268,-0.5032011,-0.5031918,0.004374787,-0.86416376,0.003336534,0.9999896,0.003119558);
    Mat rot_mat1 = (Mat_<double>(3,3) <<0.99996215,0.0028048614,-0.00822857,-0.008208492,-0.0070966,-0.9999411,-0.002863091,0.9999709,-0.007073308);

    rot_mat.push_back(rot_mat1);
    rot_mat.push_back(rot_mat2);
    rot_mat.push_back(rot_mat3);
    rot_mat.push_back(rot_mat4);
    rot_mat.push_back(rot_mat5);
    
    ./create_pano /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_171417.jpeg /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_171421.jpeg /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_171425.jpeg /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_171430.jpeg
