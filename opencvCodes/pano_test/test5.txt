    float focal = 540;
    float ppx = 320;
    float ppy = 240;
    float aspect = 4/3;
    Mat rot_mat5 = (Mat_<double>(3,3) <<0.5024112,0.052643955,-0.8630247,-0.8620347,-0.0467636,-0.5046874,-0.0669269,0.9975178,0.021886379);
    Mat rot_mat4 = (Mat_<double>(3,3) <<0.012745559,0.012373,-0.9998422,-0.99828374,-0.05700326,-0.013431072,-0.057160437,0.99829733,0.01162529);
    Mat rot_mat3 = (Mat_<double>(3,3) <<-0.5091046,-0.042372793,-0.8596609,-0.85730827,-0.063676715,0.51084995,-0.07638651,0.99707055,-0.0039084554);
    Mat rot_mat2 = (Mat_<double>(3,3) <<-0.85915387,-0.040976986,-0.5100741,-0.509568,-0.022755146,0.8601293,-0.04685223,0.99890083,-0.0013307333);
    Mat rot_mat1 = (Mat_<double>(3,3) <<-0.9988528,-0.045169756,0.015901908,0.015664088,0.005614102,0.99986154,-0.04525278,0.99896353,-0.004900217);

    rot_mat.push_back(rot_mat1);
    rot_mat.push_back(rot_mat2);
    rot_mat.push_back(rot_mat3);
    rot_mat.push_back(rot_mat4);
    rot_mat.push_back(rot_mat5);
    
    ./create_pano /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_161922.jpeg /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_161926.jpeg /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_161933.jpeg /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_161936.jpeg /home/garvita/Panorama_infurnia/sample_images/test/IMG_20160520_161940.jpeg
