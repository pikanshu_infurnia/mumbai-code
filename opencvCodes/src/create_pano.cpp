
#include <iostream>
#include <fstream>
#include <string>
#include "opencv2/opencv_modules.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/stitching/detail/autocalib.hpp"
#include "opencv2/stitching/detail/blenders.hpp"
#include "opencv2/stitching/detail/timelapsers.hpp"
#include "opencv2/stitching/detail/camera.hpp"
#include "opencv2/stitching/detail/exposure_compensate.hpp"
#include "opencv2/stitching/detail/matchers.hpp"
#include "opencv2/stitching/detail/motion_estimators.hpp"
#include "opencv2/stitching/detail/seam_finders.hpp"
#include "opencv2/stitching/detail/util.hpp"
#include "opencv2/stitching/detail/warpers.hpp"
#include "opencv2/stitching/warpers.hpp"

using namespace std;
using namespace cv;
using namespace cv::detail;

vector<String> img_names;
bool preview = false;
bool try_cuda = false;
double work_megapix = 0.6;
double seam_megapix = 0.1;
double compose_megapix = -1;
float conf_thresh = 1.f;
string ba_refine_mask = "xxxxx";
bool do_wave_correct = true;
WaveCorrectKind wave_correct = detail::WAVE_CORRECT_HORIZ;
int expos_comp_type = ExposureCompensator::GAIN_BLOCKS;
float match_conf = 0.4f;        //only 0.3 or 0.4
int blend_type = Blender::MULTI_BAND;
int timelapse_type = Timelapser::AS_IS;
float blend_strength = 5;
string result_name = "result.jpg";
bool timelapse = false;


static int parseCmdArgs(int argc, char** argv)
{
	if (argc == 1)
	{
		return -1;
		cout<<argc<<endl;
	}
	for (int i = 1; i < argc; ++i){
		if (string(argv[i]) == "--result")
		{
			result_name = argv[i + 1];
			i++;
		}
		else
		{
			img_names.push_back(argv[i]);
		}

	}
}



int main(int argc, char* argv[])
{

	int retval = parseCmdArgs(argc, argv);
	// if (retval)
	//     return retval;

	// Check if have enough images
	int num_images = static_cast<int>(img_names.size());
	if (num_images < 2)
	{
		LOGLN("Need more images");
		return -1;
	}

	double work_scale = 1, seam_scale = 1, compose_scale = 1;
	bool is_work_scale_set = false, is_seam_scale_set = false, is_compose_scale_set = false;


	Ptr<FeaturesFinder> finder;
	finder = makePtr<OrbFeaturesFinder>();
	Mat full_img, img;
	vector<ImageFeatures> features(num_images);
	vector<Mat> images(num_images);
	vector<Size> full_img_sizes(num_images);
	double seam_work_aspect = 1;
	cout << " ++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	for (int i = 0; i < num_images; ++i)
	{
		cout << " Image : " << i << endl;
		full_img = imread(img_names[i]);
		full_img_sizes[i] = full_img.size();
		cout << " FullImg.size() : " <<  full_img.size() << endl;
		if (full_img.empty())
		{
			LOGLN("Can't open image " << img_names[i]);
			return -1;
		}
		if (work_megapix < 0)
		{
			img = full_img;
			work_scale = 1;
			is_work_scale_set = true;
		}
		else
		{
			if (!is_work_scale_set)
			{
				work_scale = min(1.0, sqrt(work_megapix * 1e6 / full_img.size().area()));
				is_work_scale_set = true;
			}
			resize(full_img, img, Size(), work_scale, work_scale);
			cout << "\n		FullImg.size(),area() : " <<  full_img.size().area() << "\n		ele : " << sqrt(work_megapix * 1e6 / full_img.size().area()) <<  " \n		work_scale : " <<  work_scale << endl;
		}
		if (!is_seam_scale_set)
		{
			seam_scale = min(1.0, sqrt(seam_megapix * 1e6 / full_img.size().area()));
			seam_work_aspect = seam_scale / work_scale;
			is_seam_scale_set = true;
			cout << "\n		FullImg.size(),area() : " <<  full_img.size().area() << "\n		ele :" << sqrt(seam_megapix * 1e6 / full_img.size().area()) <<  "\n		seam_scale :" <<  seam_scale << "\n		seam_work_aspect : " << seam_scale / work_scale << endl;
		}
		cout << " \n feature_img.size() : " << img.size() << endl;
		(*finder)(img, features[i]);
		features[i].img_idx = i;
		LOGLN("Features in image #" << i+1 << ": " << features[i].keypoints.size());

		resize(full_img, img, Size(), seam_scale, seam_scale);
		images[i] = img.clone();
	}
	cout << " ++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	finder->collectGarbage();
	full_img.release();
	img.release();


   // range_width = 2;
	vector<MatchesInfo> pairwise_matches;
	BestOf2NearestMatcher matcher(try_cuda, match_conf);
	matcher(features, pairwise_matches);
	matcher.collectGarbage();

	// Leave only images we are sure are from the same panorama
	vector<int> indices = leaveBiggestComponent(features, pairwise_matches, conf_thresh);
	// for(int i = 0; i<num_images; i++){
	//     indices.push_back(features[i].img_idx);
	// }
	cout<<"number of input images:  "<<num_images<<endl;
	cout<<"number of images in panorama: "<<indices.size()<<endl;
	vector<Mat> img_subset;
	vector<String> img_names_subset;
	vector<Size> full_img_sizes_subset;
	for (size_t i = 0; i < indices.size(); ++i)
	{
		img_names_subset.push_back(img_names[indices[i]]);
		img_subset.push_back(images[indices[i]]);
		full_img_sizes_subset.push_back(full_img_sizes[indices[i]]);
	}

	images = img_subset;
	img_names = img_names_subset;
	full_img_sizes = full_img_sizes_subset;

	// Check if we still have enough images
	num_images = static_cast<int>(img_names.size());
	cout << "\n No. of images For panaroma : " << num_images << endl;
	for( int i = 0; i < num_images; i++)
		cout << "\n Image.size() :  " << images[i].size() << endl;
	if (num_images < 2)
	{
		LOGLN("Need more images");
		return -1;
	}

	HomographyBasedEstimator estimator;
	vector<CameraParams> cameras;
	if (!estimator(features, pairwise_matches, cameras))
	{
		cout << "Homography estimation failed.\n";
		return -1;
	}

	for (size_t i = 0; i < cameras.size(); ++i)
	{
		Mat R;
		cameras[i].R.convertTo(R, CV_32F);
		cameras[i].R = R;
		cout<<"camera parameters \n  "<<cameras[i].R<< " \n" <<cameras[i].focal<<" ppx = "<<cameras[i].ppx<<"  ppy= " <<cameras[i].ppy<<endl;

	}
	Ptr<detail::BundleAdjusterBase> adjuster;
	adjuster = makePtr<detail::BundleAdjusterRay>();
	adjuster->setConfThresh(conf_thresh);
	Mat_<uchar> refine_mask = Mat::zeros(3, 3, CV_8U);
	if (ba_refine_mask[0] == 'x') refine_mask(0,0) = 1;
	if (ba_refine_mask[1] == 'x') refine_mask(0,1) = 1;
	if (ba_refine_mask[2] == 'x') refine_mask(0,2) = 1;
	if (ba_refine_mask[3] == 'x') refine_mask(1,1) = 1;
	if (ba_refine_mask[4] == 'x') refine_mask(1,2) = 1;
	adjuster->setRefinementMask(refine_mask);
	if (!(*adjuster)(features, pairwise_matches, cameras))
	{
		cout << "Camera parameters adjusting failed.\n";
	   // return -1;
	}
	cout<<"after BundleAdjusterRay"<<endl;
	for (size_t i = 0; i < cameras.size(); ++i)
	{

		cout<<"camera parameters  \n  "<<cameras[i].R<< " \n" <<cameras[i].focal<<" ppx = "<<cameras[i].ppx<<"  ppy= " <<cameras[i].ppy<<endl;

	}
	// Find median focal length

	vector<double> focals;
	for (size_t i = 0; i < cameras.size(); ++i)
	{
		LOGLN("Camera #" << indices[i]+1 << ":\n" << cameras[i].K());
		focals.push_back(cameras[i].focal);
	}

	sort(focals.begin(), focals.end());
	float warped_image_scale;
	if (focals.size() % 2 == 1)
		warped_image_scale = static_cast<float>(focals[focals.size() / 2]);
	else
		warped_image_scale = static_cast<float>(focals[focals.size() / 2 - 1] + focals[focals.size() / 2]) * 0.5f;

	if (do_wave_correct)
	{
		vector<Mat> rmats;
		for (size_t i = 0; i < cameras.size(); ++i)
			rmats.push_back(cameras[i].R.clone());
		waveCorrect(rmats, wave_correct);
		for (size_t i = 0; i < cameras.size(); ++i)
			cameras[i].R = rmats[i];
	}

	vector<Point> corners(num_images);
	vector<UMat> masks_warped(num_images);
	vector<UMat> images_warped(num_images);
	vector<Size> sizes(num_images);
	vector<UMat> masks(num_images);

	// Preapre images masks
	for (int i = 0; i < num_images; ++i)
	{
		masks[i].create(images[i].size(), CV_8U);
		masks[i].setTo(Scalar::all(255));
	}

	// Warp images and their masks

	Ptr<WarperCreator> warper_creator;
	warper_creator = makePtr<cv::SphericalWarper>();


	if (!warper_creator)
	{
		cout << "Can't create the following warper '" <<"'\n";
		return 1;
	}

	Ptr<RotationWarper> warper = warper_creator->create(static_cast<float>(warped_image_scale * seam_work_aspect));

	for (int i = 0; i < num_images; ++i)
	{
		Mat_<float> K;
		cameras[i].K().convertTo(K, CV_32F);
		float swa = (float)seam_work_aspect;
		K(0,0) *= swa; K(0,2) *= swa;
		K(1,1) *= swa; K(1,2) *= swa;

		corners[i] = warper->warp(images[i], K, cameras[i].R, INTER_LINEAR, BORDER_REFLECT, images_warped[i]);
		sizes[i] = images_warped[i].size();

		warper->warp(masks[i], K, cameras[i].R, INTER_NEAREST, BORDER_CONSTANT, masks_warped[i]);
		cout << "\n Warped_Img.size() : " << images_warped[i].size() << endl;
	}

	vector<UMat> images_warped_f(num_images);
	for (int i = 0; i < num_images; ++i)
		images_warped[i].convertTo(images_warped_f[i], CV_32F);


	Ptr<ExposureCompensator> compensator = ExposureCompensator::createDefault(expos_comp_type);
	compensator->feed(corners, images_warped, masks_warped);

	Ptr<SeamFinder> seam_finder;
	seam_finder = makePtr<detail::GraphCutSeamFinder>(GraphCutSeamFinderBase::COST_COLOR_GRAD);
	 //   seam_finder = makePtr<detail::DpSeamFinder>(DpSeamFinder::COLOR_GRAD);

	if (!seam_finder)
	{
		cout << "Can't create the following seam finder '" << "'\n";
		return 1;
	}

	seam_finder->find(images_warped_f, corners, masks_warped);

	// Release unused memory
	images.clear();
	images_warped.clear();
	images_warped_f.clear();
	masks.clear();


	Mat img_warped, img_warped_s;
	Mat dilated_mask, seam_mask, mask, mask_warped;
	Ptr<Blender> blender;
	Ptr<Timelapser> timelapser;
	//double compose_seam_aspect = 1;
	double compose_work_aspect = 1;

	cout << "\n ReWarping Images +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ " << endl;
	cout << "\n		Num_images : " << num_images << endl;
    for (int img_idx = 0; img_idx < num_images; ++img_idx)
    {
		cout << "\n		Image : " << img_idx<< endl;
        // Read image and resize it if necessary
        full_img = imread(img_names[img_idx]);
			cout << "\n			Image.size() : " << full_img.size() << endl;
        if (!is_compose_scale_set)
        {
			cout << "\n			is_compose_scale_set : " << is_compose_scale_set << endl;
            if (compose_megapix > 0)
                compose_scale = min(1.0, sqrt(compose_megapix * 1e6 / full_img.size().area()));
            is_compose_scale_set = true;
			cout << "\n			compose_scale: " << compose_scale<< endl;
            // Compute relative scales
            //compose_seam_aspect = compose_scale / seam_scale;
            compose_work_aspect = compose_scale / work_scale;
			cout << "\n			compose_work_aspect : " << compose_work_aspect << endl;
            // Update warped image scale
            warped_image_scale *= static_cast<float>(compose_work_aspect);
            warper = warper_creator->create(warped_image_scale);
			cout << "\n			warped_image_scale : " << warped_image_scale << endl;
            // Update corners and sizes
            cout << "*********************** inner loop ***********************" << endl;
            for (int i = 0; i < num_images; ++i)
            {
                // Update intrinsics
				cout << "img_ " << i << endl;
				cout << "cameras[i].focal_old :" << cameras[i].focal << "		";
                cameras[i].focal *= compose_work_aspect;
                cout << "cameras[i].focal_new :" << cameras[i].focal << "\n";
				cout << "cameras[i].ppx_old :" << cameras[i].ppx << "		";
                cameras[i].ppx *= compose_work_aspect;
                cout << "cameras[i].ppx_new :" << cameras[i].ppx << "\n";
                cout << "cameras[i].ppy_old :" << cameras[i].ppy << "		";
                cameras[i].ppy *= compose_work_aspect;
                cout << "cameras[i].ppy_new :" << cameras[i].ppy << "\n";

				// Update corner and size
				
				Size sz = full_img_sizes[i];
				cout << "imgSize_old :" << sz << "		";
				if (std::abs(compose_scale - 1) > 1e-1)
				{
					sz.width = cvRound(full_img_sizes[i].width * compose_scale);
					sz.height = cvRound(full_img_sizes[i].height * compose_scale);
				}
				cout << "imgSize_new :" << sz << "\n";
				Mat K;
				cameras[i].K().convertTo(K, CV_32F);
				Rect roi = warper->warpRoi(sz, K, cameras[i].R);
				cout << "\n Corners : " << roi.tl() ;
				cout << "\n Sizes   : " << roi.size();
				cout << "\n Kmatrix : " << cameras[i].K();
				//~ Mat K = _K.getMat(), R = _R.getMat(), T = _T.getMat();
//~ 
				//~ CV_Assert(K.size() == Size(3, 3) && K.type() == CV_32F);
				//~ CV_Assert(R.size() == Size(3, 3) && R.type() == CV_32F);

				cout << "\n RK_inv  : " << cameras[i].R * K.inv() ;
				corners[i] = roi.tl();
				sizes[i] = roi.size();
			}
		}
		if (abs(compose_scale - 1) > 1e-1)
			resize(full_img, img, Size(), compose_scale, compose_scale);
		else
			img = full_img;
		cout << "\n			 resized_img_size :" << img.size() << "\n";
		full_img.release();
		Size img_size = img.size();

		Mat K;
		cameras[img_idx].K().convertTo(K, CV_32F);

		// Warp the current image
		warper->warp(img, K, cameras[img_idx].R, INTER_LINEAR, BORDER_REFLECT, img_warped);
		char str[200];
		sprintf(str,"Warped_Img_%d.png",img_idx);
		imwrite(str,img_warped);
		cout << "\n			 warped_img_size :" << img_warped.size() << "\n";
		// Warp the current image mask
		mask.create(img_size, CV_8U);
		mask.setTo(Scalar::all(255));
		warper->warp(mask, K, cameras[img_idx].R, INTER_NEAREST, BORDER_CONSTANT, mask_warped);

		// Compensate exposure
		compensator->apply(img_idx, corners[img_idx], img_warped, mask_warped);

		img_warped.convertTo(img_warped_s, CV_16S);
		img_warped.release();
		img.release();
		mask.release();

		dilate(masks_warped[img_idx], dilated_mask, Mat());
		resize(dilated_mask, seam_mask, mask_warped.size());
		mask_warped = seam_mask & mask_warped;

		if (!blender && !timelapse)
		{
			blender = Blender::createDefault(blend_type, try_cuda);
			Size dst_sz = resultRoi(corners, sizes).size();
			float blend_width = sqrt(static_cast<float>(dst_sz.area())) * blend_strength / 100.f;
			if (blend_width < 1.f)
				blender = Blender::createDefault(Blender::NO, try_cuda);
			else if (blend_type == Blender::MULTI_BAND)
			{
				MultiBandBlender* mb = static_cast<MultiBandBlender*>(blender.get());
				mb->setNumBands(static_cast<int>(ceil(log(blend_width)/log(2.)) - 1.));
				LOGLN("Multi-band blender, number of bands: " << mb->numBands());
			}
			else if (blend_type == Blender::FEATHER)
			{
				FeatherBlender* fb = static_cast<FeatherBlender*>(blender.get());
				fb->setSharpness(1.f/blend_width);
				LOGLN("Feather blender, sharpness: " << fb->sharpness());
			}
			blender->prepare(corners, sizes);
		}
		else if (!timelapser && timelapse)
		{
			timelapser = Timelapser::createDefault(timelapse_type);
			timelapser->initialize(corners, sizes);
		}

		// Blend the current image
		if (timelapse)
		{
			timelapser->process(img_warped_s, Mat::ones(img_warped_s.size(), CV_8UC1), corners[img_idx]);
			// String fixedFileName;
			// size_t pos_s = String(img_names[img_idx]).find_last_of("/\\");
			// if (pos_s == String::npos)
			// {
			//     fixedFileName = "fixed_" + img_names[img_idx];
			// }
			// else
			// {
			//     fixedFileName = "fixed_" + String(img_names[img_idx]).substr(pos_s + 1, String(img_names[img_idx]).length() - pos_s);
			// }
			// imwrite(fixedFileName, timelapser->getDst());
		}
		else
		{
			blender->feed(img_warped_s, mask_warped, corners[img_idx]);
		}
	}

	if (!timelapse)
	{
		Mat result, result_mask;
		blender->blend(result, result_mask);


		imwrite("create_panoout2.png", result);
	}

	return 0;
}
