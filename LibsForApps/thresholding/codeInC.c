#include <cv.h>
#include "highgui.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(){
    
    CvMemStorage* storage1 = cvCreateMemStorage(0);
    CvSeq* ptseq = cvCreateSeq( CV_SEQ_KIND_GENERIC|CV_32SC2, sizeof(CvContour),sizeof(CvPoint), storage1 );
    CvSeq* hull;
    int i;
    
    CvPoint pt;
    
    pt.x = 50; pt.y = 50; cvSeqPush(ptseq,&pt);
    pt.x = 75; pt.y = 25; cvSeqPush(ptseq,&pt);
    pt.x = 50; pt.y = 150; cvSeqPush(ptseq,&pt);
    pt.x = 150; pt.y = 50; cvSeqPush(ptseq,&pt);
    pt.x = 150; pt.y = 150; cvSeqPush(ptseq,&pt);  
    
    printf("\n reached 1");
    hull = cvConvexHull2( ptseq, 0, CV_CLOCKWISE, 1 );
    int hullcount = hull->total;      

    printf("\n reached 2 ");
    for(i = 0; i < hullcount; i++ )
    {
        CvPoint* p = (CvPoint*)cvGetSeqElem ( hull, i );
/*        CvPoint pt = **CV_GET_SEQ_ELEM( CvPoint*, hull, i );*/
        printf("(%d,%d) \n",(*p).x,(*p).y);
/*        int index = **CV_GET_SEQ_ELEM( int, hull, i );*/
/*        printf("(%d,%d) \n",index,index);*/
    }
    
    
        
    return 0;
}
