LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

OPENCVROOT := $(OpenCV_SDKHome)
OPENCV_LIB_TYPE:=SHARED
include ${OPENCVROOT}/sdk/native/jni/OpenCV.mk


LOCAL_MODULE := thresholding 

LOCAL_LDLIBS +=  -llog -ldl 
LOCAL_SRC_FILES := src/thresholding.cpp 

LOCAL_C_INCLUDES += $(LOCAL_PATH)/include

include $(BUILD_SHARED_LIBRARY)
