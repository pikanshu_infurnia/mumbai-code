#include<iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>

using namespace cv;
using namespace std;


void FindBlobs(const Mat &binary,vector<vector<Point2i> > &blobs)
{
	blobs.clear();

	// Fill the label_image with the blobs
	// 0  - background
	// 1  - unlabelled foreground
	// 2+ - labelled foreground

	Mat label_image;
	binary.convertTo(label_image, CV_32SC1);

	int label_count = 2; // starts at 2 because 0,1 are used already

	for(int y=0; y < label_image.rows; y++) {
		int *row = (int*)label_image.ptr(y);
		for(int x=0; x < label_image.cols; x++) {
			if(row[x] != 1) {
				continue;
			}

			Rect rect;
			floodFill(label_image, Point(x,y), label_count, &rect, 0, 0, 8);

			vector <Point2i> blob;

			for(int i=rect.y; i < (rect.y+rect.height); i++) {
				int *row2 = (int*)label_image.ptr(i);
				for(int j=rect.x; j < (rect.x+rect.width); j++) {
					if(row2[j] != label_count) {
						continue;
					}

					blob.push_back(Point2i(j,i));
				}
			}

			blobs.push_back(blob);

			label_count++;
		}
	}
}


void thresholding(const Mat &imGray , int count){
	Mat imBW,binary;
	char str[100];
	int len;
	int height,width;
	int h = 201,w = 201;
	height = imGray.rows; width = imGray.cols;                    // dimension of input image

	if(! imGray.data ) {                                                   // checking image data
		cout <<  "Could not open or find the image" << std::endl ;
		return;
	}

	adaptiveThreshold(imGray,imBW,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY_INV,21,0); // adaptive thresholding or local thresholding

	// loop for erasing the unused part of image , top one-third rows and , first and last one-fourth columns
	for(int i = 0 ; i < height; i++)
		for(int j = 0; j < width; j++)
			if((i < height/3) || (j < width/4) || (j > (width*3)/4)) imBW.data[i*width+j] = 0;

	vector < vector<Point2i > > blobs;                          // output variable for blob detection

	threshold(imBW, binary, 0, 1.0, THRESH_BINARY);             // converting binary image 0-255 to 0-1

	imBW = Mat(binary.size(),CV_8UC1,Scalar(255));
	FindBlobs(binary, blobs);	                                // blob detection

	imBW.release();
	binary.release();

	vector<Point2i> centroids;                    // centroids for blobs

	Mat output = Mat::zeros(imGray.size(), CV_8UC1);           // image for output connected component

	for(int i=0 ; i < blobs.size(); i++) {
		double tempx = 0,tempy = 0;
		Point2i centre;

		if(blobs[i].size() < 9000 && blobs[i].size() > 500){
			Mat crpIMGray,crpIMBW,crpEdge,crpBinary;
			crpIMGray = Mat(Size(w,h),CV_8UC1); crpIMBW = Mat(Size(w,h),CV_8UC1);      // initializing

			for(int j=0; j < blobs[i].size(); j++) {
				int x = blobs[i][j].x; int y = blobs[i][j].y;
				tempx += x; tempy += y;
				output.at<uchar>(y,x) = 255;
			}

			centre.x = int(tempx/blobs[i].size()); centre.y = int(tempy/blobs[i].size());
			centroids.push_back(centre);
			// x = col = width, y = row = height;

			int offsetRow = centre.y - 100, offsetCol = centre.x - 100;

			for(int m = 0; m < h ; m++){
				for(int n = 0; n < w; n++){
					if(m+offsetRow >=0 && m+offsetRow < height && n+offsetCol >=0 && n+offsetCol < width){
						crpIMGray.data[m*w + n] = imGray.data[(m+offsetRow)*width+(n+offsetCol)];
						crpIMBW.data[m*w+n] = output.data[((m+offsetRow)*width+(n+offsetCol))];
					}
					else crpIMGray.data[m*w + n] = crpIMBW.data[m*w+n] = 0;
				}
			}
			len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_1.jpg",count,int(centroids.size()));
			imwrite(str,crpIMGray);

			GaussianBlur(crpIMGray,crpIMGray, Size(3,3),sqrt(2));
			Canny(crpIMGray,crpEdge, 0.1*255, 0.5*255, 3,false);
			crpIMGray.release();

			vector < vector<Point2i > > cannyBlobs;
			threshold(crpEdge, crpBinary, 0, 1.0, THRESH_BINARY);             // converting binary image 0-255 to 0-1
			vector<Point2i> cannyCentroids;                                        // centroids for cannyblobs
			FindBlobs(crpBinary, cannyBlobs);
			crpBinary.release();

			for(int a=0 ; a < cannyBlobs.size(); a++) {
				double cannyTempx = 0,cannyTempy = 0;
				Point2i cannyCentre;

				for(int b=0; b < cannyBlobs[a].size(); b++) {
					int cannyX = cannyBlobs[a][b].x;
					int cannyY = cannyBlobs[a][b].y;
					cannyTempx += cannyX; cannyTempy += cannyY;
				}
				cannyCentre.x = int(cannyTempx/cannyBlobs[a].size()); cannyCentre.y = int(cannyTempy/cannyBlobs[a].size());

				if(((cannyCentre.x < 50 || cannyCentre.x > 150) || (cannyCentre.y < 50 || cannyCentre.y > 150))){
					for(int b=0; b < cannyBlobs[a].size(); b++) {
						crpEdge.data[cannyBlobs[a][b].y*w + cannyBlobs[a][b].x] = 0;
					}
				}
				len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_2.jpg",count,int(centroids.size()));
				imwrite(str,crpEdge);

			}

			for(int m = 0; m < h ; m++){
				for(int n = 0; n < w; n++){
					crpIMBW.data[m*w + n] += crpEdge.data[m*w+n];
				}
			}
			crpEdge.release();

			threshold(crpIMBW, crpIMBW, 0, 255, THRESH_BINARY_INV);

			Mat tmpImg = Mat :: ones(427,320,CV_8UC1);
			for(int i = 0 ,m = 427-250; i < h; i++,m++){
				for(int j = 0,n = 320 - 250; j < w; j++,n++){
					tmpImg.data[m*320+n] = crpIMBW.data[i*w+j];
				}
			}
			
			len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_3.jpg",count,int(centroids.size()));
			imwrite(str,crpIMBW);

			crpIMBW.release();

			threshold(tmpImg, tmpImg, 0, 255, THRESH_BINARY);
			len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/test_%03d_%03d_3.png",count,int(centroids.size()));
			imwrite(str,tmpImg);
			tmpImg.release();
		}
	}
	output.release();
}


int main(int argc, char** argv) {

//	if(argc != 2 ){
//		cout << "No. of input arguments should be 1.\n Provide an input image with full path\n";
//	}

	Mat inputIM,imGray,imBW,binary;         // Image variables
	Mat crpIMGray,crpIMBW,crpBinary,crpEdge;          // crop Image variables
	int strt,end;                           // image no. to operate on
	strt = atoi(argv[1]);
	end = atoi(argv[2]);


	char str[100];                          // variable for writing path
	int len;
	int height, width;                      // input image dimension
	int w = 201,h = 201;                    // crop window dimension
	int strtRow,endRow,strtCol,endCol,offsetRow,offsetCol;      // start and end indices for cropped images

	crpIMGray = Mat(Size(w,h),CV_8UC1); crpIMBW = Mat::zeros(Size(w,h),CV_8UC1);      // initializing

	VideoCapture cap("/home/pika/Desktop/pika_marker_images/%03d.png");             // for reading images from a folder

	int count= 1;

	if(!(cap.isOpened())) cout<< "\nreached 5\n";

	while(cap.isOpened() && count <= end)                  // loop for traveresing through images
	{
		cap.read(inputIM);                                              // getting one image at a time

		if(count >= strt && count <= end){
		//	inputIM = imread(argv[1],CV_LOAD_IMAGE_COLOR);
			height = inputIM.rows; width = inputIM.cols;                    // dimension of input image

			if(! inputIM.data ) {                                                   // checking image data
				cout <<  "Could not open or find the image" << std::endl ;
				return -1;
			}

			cvtColor(inputIM,imGray,CV_BGR2GRAY);                       // image conversion from BGR to GRAY

			//~ /*
			len = sprintf(str,"/home/pika/Desktop/outputOpenCV/Gray_%03d.jpg",count);
			imwrite(str,imGray);


	//        namedWindow("Input Image(Gray)",WINDOW_NORMAL);
	//        imshow("Input Image(Gray)",imGray);
	//
	//        waitKey(0);

			adaptiveThreshold(imGray,imBW,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,21,0); // adaptive thresholding or local thresholding


			// loop for erasing the unused part of image , top one-third rows and , first and last one-fourth columns
			for(int i = 0 ; i < height; i++){
				for(int j = 0; j < width; j++){

					if((i < height/3) || (j < width/4) || (j > (width*3)/4)) imBW.data[i*width+j] = 0;
				}
			}



		//    namedWindow("Thrasholded Image(Binary)",WINDOW_NORMAL);
		//    imshow("Thrasholded Image(Binary)",imBW);

			Mat output = Mat::zeros(inputIM.size(), CV_8UC3);           // image for output connected component

			vector < vector<Point2i > > blobs;                          // output variable for blob detection

			threshold(imBW, binary, 0, 1.0, THRESH_BINARY);             // converting binary image 0-255 to 0-1
			//~ threshold(imBW, binary, 0, 1.0, THRESH_BINARY_INV);             // converting binary image 0-255 to 0-1
			
			len = sprintf(str,"/home/pika/Desktop/outputOpenCV/pika_1_%03d.jpg",count);
			cout<<"\n"<< str<<endl;
            imwrite(str,imBW);

			//~ len = sprintf(str,"/home/pika/Desktop/outputOpenCV/pika_2_%03d.jpg",count);
			//~ cout<<"\n"<< str<<endl;
            //~ imwrite(str,binary);

			imBW = Mat(binary.size(),CV_8UC1,Scalar(255));

			FindBlobs(binary, blobs);	                                // blob detection
	//        cout<<"\nsize of blobs :" << blobs.size()<<endl;
			// Randomy color the blobs
			vector<Point2i> centroids;                    // centroids for blobs

			for(int i=0 ; i < blobs.size(); i++) {
				double tempx = 0,tempy = 0;
				Point2i centre;
				crpIMBW = Mat::zeros(Size(w,h),CV_8UC1);
				if(blobs[i].size() < 9000 && blobs[i].size() > 500){
					for(int j=0; j < blobs[i].size(); j++) {
						int x = blobs[i][j].x; int y = blobs[i][j].y;
						tempx += x; tempy += y;
						output.at<Vec3b>(y,x)[0] = 255; output.at<Vec3b>(y,x)[1] = 255; output.at<Vec3b>(y,x)[2] = 255;
					}

					centre.x = int(tempx/blobs[i].size()); centre.y = int(tempy/blobs[i].size());
					bool flag = true;
					for(int j = 0; j < blobs[i].size(); j++){
						int x = blobs[i][j].x; int y = blobs[i][j].y;
						if(abs(x-centre.x)>60 || abs(y-centre.y)>60){
							flag = false;
							break;
						} 
					}
					if(flag == false) continue;
					centroids.push_back(centre);
//                    circle(output,centre,10,Scalar(0,0,255),-1,8);          // plotting centroids

					// x = col = width, y = row = height;

					offsetRow = centre.y - 100; offsetCol = centre.x - 100;

					for(int j=0; j < blobs[i].size(); j++) {
						int x = blobs[i][j].x; int y = blobs[i][j].y;
						//~ tempx += x; tempy += y;
						if((x-offsetCol) >= 0 && (y-offsetRow) >= 0)
							crpIMBW.data[(y-offsetRow)*w+(x-offsetCol)] = 255;
					}

					for(int m = 0; m < h ; m++){
						for(int n = 0; n < w; n++){
							if(m+offsetRow >=0 && m+offsetRow < height && n+offsetCol >=0 && n+offsetCol < width){
								crpIMGray.data[m*w + n] = imGray.data[(m+offsetRow)*width+(n+offsetCol)];
								//~ if(m >= h/4 && m < (3*h)/4 && n >= w/4 && n < (3*w)/4)
								//~ if(
									//~ crpIMBW.data[m*w+n] = output.data[3*((m+offsetRow)*width+(n+offsetCol))];
								//~ else crpIMBW.data[m*w+n] = 0;
							}
							else crpIMGray.data[m*w + n] /*= crpIMBW.data[m*w+n]*/ = 0;
						}
					}
//                    len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_1.jpg",count,int(centroids.size()));
//                    imwrite(str,crpIMGray);
					GaussianBlur(crpIMGray,crpIMGray, Size(3,3),sqrt(2));
					Canny(crpIMGray,crpEdge, 0.1*255, 0.5*255, 3,false);
					Mat cannyBlobOutput = Mat(crpIMGray.size(), CV_8UC3,Scalar(255,255,255));
					vector < vector<Point2i > > cannyBlobs;
					threshold(crpEdge, crpBinary, 0, 1.0, THRESH_BINARY);             // converting binary image 0-255 to 0-1
					vector<Point2i> cannyCentroids;                                        // centroids for cannyblobs
					FindBlobs(crpBinary, cannyBlobs);
					int totalSize = 0;
					for(int a=0 ; a < cannyBlobs.size(); a++) {
						double cannyTempx = 0,cannyTempy = 0;
						Point2i cannyCentre;
						unsigned char r = 255 * (rand()/(1.0 + RAND_MAX));
						unsigned char g = 255 * (rand()/(1.0 + RAND_MAX));
						unsigned char b = 255 * (rand()/(1.0 + RAND_MAX));
						
						bool flag2 = true;
						for(int b=0; b < cannyBlobs[a].size(); b++) {

							int cannyX = cannyBlobs[a][b].x;
							int cannyY = cannyBlobs[a][b].y;
							if(abs(cannyX - 100) > 50 || abs(cannyY - 100) > 50) {
								flag2 = false;
								break;
							}
							cannyBlobOutput.at<cv::Vec3b>(cannyY,cannyX)[0] = b;
							cannyBlobOutput.at<cv::Vec3b>(cannyY,cannyX)[1] = g;
							cannyBlobOutput.at<cv::Vec3b>(cannyY,cannyX)[2] = r;

							cannyTempx += cannyX; cannyTempy += cannyY;
						}
						cannyCentre.x = int(cannyTempx/cannyBlobs[a].size()); cannyCentre.y = int(cannyTempy/cannyBlobs[a].size());

//                        int dis = sqrt((cannyCentre.x - 100)*(cannyCentre.x - 100)+(cannyCentre.y - 100)*(cannyCentre.y - 100));
//                        cout<<"("<<cannyCentre.x<<","<<cannyCentre.y<<","<<((cannyCentre.x < 50 || cannyCentre.x > 150) || (cannyCentre.y < 50 || cannyCentre.y > 150))<<")"<<endl;
						if(((cannyCentre.x < 50 || cannyCentre.x > 150) || (cannyCentre.y < 50 || cannyCentre.y > 150) || flag2 == false)){
							for(int b=0; b < cannyBlobs[a].size(); b++) {
								crpEdge.data[cannyBlobs[a][b].y*w + cannyBlobs[a][b].x] = 0;
							}
						}
						else totalSize += (int)cannyBlobs[a].size();
					
					}
					printf("\n Count %d,totalSize %d \n",int(centroids.size()),totalSize);
					if(totalSize > 20){
						len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_2.jpg",count,int(centroids.size()));
						//~ imwrite(str,crpEdge);
					}
					else continue;
//                    len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_4.jpg",count,int(centroids.size()));
//                    cout<<"\n"<< str<<endl;
//                    imwrite(str,cannyBlobOutput);
//
					for(int m = 0; m < h ; m++){
						for(int n = 0; n < w; n++){
						   crpIMBW.data[m*w + n] += crpEdge.data[m*w+n];
						}
					}

					threshold(crpIMBW, crpIMBW, 0, 255, THRESH_BINARY_INV);
					len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/Crop_%03d_%03d_3.jpg",count,int(centroids.size()));
                    imwrite(str,crpIMBW);

					Mat tmpImg = Mat :: ones(427,320,CV_8UC1);
					for(int i = 0 ,m = 427-250; i < h; i++,m++){
						for(int j = 0,n = 320 - 250; j < w; j++,n++){
							tmpImg.data[m*320+n] = crpIMBW.data[i*w+j];
						}
					}
					threshold(tmpImg, tmpImg, 0, 255, THRESH_BINARY);
					len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/test_%03d_%03d_3.png",count,int(centroids.size()));
					imwrite(str,tmpImg);

					for(int m = 0; m < h ; m++){
						for(int n = 0; n < w; n++){
							if(m+offsetRow >=0 && m+offsetRow < height && n+offsetCol >=0 && n+offsetCol < width){
								imBW.data[(m+offsetRow)*width+(n+offsetCol)] = crpIMBW.data[m*w+n] ;
							}
						}
					}

				}

			}

			len = sprintf(str,"/home/pika/Desktop/outputOpenCV/cropNedges/final_%03d.jpg",count);
            imwrite(str,imBW);

			len = sprintf(str,"/home/pika/Desktop/outputOpenCV/%03d.jpg",count);
			cout<<"\n"<< str<<endl;
            imwrite(str,output);
			//~ */
			//~ thresholding(imGray,count);
		}
		cout << count << endl;
		count++;

	}

//    namedWindow("blobs",WINDOW_NORMAL);
//    imshow("blobs",output);
//
//	waitKey(0);
	return 0;

}
