#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <fstream>
#include <iostream>
#define LINE_AA 8
#define FILLED -1
using namespace cv;
using namespace std;
static void help()
{
    cout << "\nThis sample program demonstrates the use of the convexHull() function\n"
         << "Call:\n"
         << "./convexhull\n" << endl;
}
int main( int /*argc*/, char** /*argv*/ )
{

    Mat imGray,imBW,imRGB;
    
    
    imGray = imread("/home/pika/Desktop/outputOpenCV/cropNedges/Crop_031_007_3.jpg",CV_LOAD_IMAGE_GRAYSCALE);
    
    if(! imGray.data ) {                                                   // checking image data
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
    
    imRGB = Mat(imGray.size(),CV_8UC3,Scalar(255,255,255));
    
    threshold(imGray,imBW,10,255,THRESH_BINARY_INV);
    namedWindow("aisehi",WINDOW_NORMAL);
    imshow("aisehi",imBW);
       
//    waitKey(0);
    
    Mat img(500, 500, CV_8UC3);
    RNG& rng = theRNG();
    help();
    for(;;)
    {
        char key;
        int i, count = (unsigned)rng%100 + 1;
        vector<Point> points;
//        for( i = 0; i < count; i++ )
//        {
//            Point pt;
//            pt.x = rng.uniform(img.cols/4, img.cols*3/4);
//            pt.y = rng.uniform(img.rows/4, img.rows*3/4);
//            points.push_back(pt);
//        }

        for(int m  = 0; m < imBW.rows; m++){
            for(int n = 0; n < imBW.cols; n++){
                
                if(imBW.data[m*(imBW.cols)+n] != 0){
                    imRGB.at<Vec3b>(m,n)[0] = 0; imRGB.at<Vec3b>(m,n)[1] = 0; imRGB.at<Vec3b>(m,n)[2] = 0;
                    Point pt;
                    pt.x = n; pt.y = m;
                    points.push_back(pt);
                }
            }
        }
     
        cout<< "size of points : "<<points.size()<<endl;
        vector<int> hull;
        convexHull(Mat(points), hull, true);
        img = Scalar::all(0);
//        for( i = 0; i < count; i++ )
//            circle(imRGB, points[i], 3, Scalar(0, 0, 255), FILLED, LINE_AA);
        int hullcount = (int)hull.size();
        Point pt0 = points[hull[hullcount-1]];
        for( i = 0; i < hullcount; i++ )
        {
            Point pt = points[hull[i]];
            circle(imRGB, pt0, 1, Scalar(0, 0, 255), FILLED, LINE_AA);
            line(imRGB, pt0, pt, Scalar(0, 255, 0), 1,LINE_AA);
            pt0 = pt;
        }
        namedWindow("hull",WINDOW_NORMAL);
        imshow("hull", imRGB);
        key = (char)waitKey();
        if( key == 27 || key == 'q' || key == 'Q' ) // 'ESC'
            break;
    }
    return 0;
}

