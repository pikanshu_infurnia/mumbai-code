LOCAL_PATH := $(call my-dir)
LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

include $(CLEAR_VARS)
LOCAL_MODULE    := ceres
LOCAL_SRC_FILES := libceres.a

include $(PREBUILT_STATIC_LIBRARY)


include $(CLEAR_VARS)

SRC := src
#SRC := src

LOCAL_C_INCLUDES += /usr/include/eigen3
LOCAL_C_INCLUDES += $(CeresSolverHome)/include
LOCAL_C_INCLUDES += $(CeresSolverHome)/internal/ceres/miniglog
LOCAL_C_INCLUDES += $(CeresSolverHome)/config
LOCAL_C_INCLUDES += $(LibsForApps)/spaceFinder/include
LOCAL_C_INCLUDES += $(CeresSolverHome)/internal
LOCAL_C_INCLUDES += $(CeresSolverHome)/internal/ceres
#LOCAL_C_INCLUDES += /usr/include

LOCAL_MODULE    := spaceFinder
LOCAL_STATIC_LIBRARIES  = ceres
LOCAL_LDLIBS +=  -llog -ldl 


LOCAL_SRC_FILES := $(SRC)/angleSweep.cpp $(SRC)/equations.cpp $(SRC)/getAxes.cpp $(SRC)/sort.cpp $(SRC)/transformations.cpp


include $(BUILD_SHARED_LIBRARY)
