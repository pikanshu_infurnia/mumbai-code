#include <stdio.h>
#include <math.h>

void new_marker(double marker[3][2],int imWidth,int imHeight){

	int i;

	for( i = 0 ; i < 3; i++){
		marker[i][0] = marker[i][0] - imWidth;
		marker[i][1] = marker[i][1] - imHeight;
	}
	return;
}

void normalize(double x[3]){
	double temp = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]);
	x[0] = x[0]/temp; x[1] = x[1]/temp; x[2] = x[2]/temp;
	return;
}

double dot(double x[3],double y[3]){
	return (x[0]*y[0] + x[1]*y[1] + x[2]*y[2]); 
}

void cross(double x[3],double y[3],double z[3]){
	z[0] = x[1]*y[2]-x[2]*y[1];
	z[1] = -x[0]*y[2]+x[2]*y[0];
	z[2] =  x[0]*y[1]-x[1]*y[0];
	return;
}

int verify(double marker[4][2], double z[3], double focal,int imWidth,int imHeight){

	printf("\n------------------------------------       START          --------------------------------\n");
	double tmp[4],phi,theta;
	double X[4],Y[4],Z[4];
	double x[4],y[4];
	double a1,a2,b1,b2,c1,c2,l1,l2,m1,m2,n1,n2;
	double a,b;
	double h = 85.72,w = 53.98;
/*	double D[3][2] ={{w/2,-h/2},{-w/2,-h/2},{-w/2,h/2}};*/
	double D[4][2] ={{-w/2,h/2},{w/2,h/2},{w/2,-h/2},{-w/2,-h/2}};
	double rotMat[3][3];
	double co,si;
	int i,j,k;
	// double temp,tempAngle;
	double diff_D_x,diff_D_y,diff_Marker_y,diff_Marker_x;
	double t[3];
	double XVec[3],YVec[3],ZVec[3],dotProd;

	double psi[2];

	//////////////////////////////////// calculating theta & phi //////////////////////

	new_marker(marker,imWidth,imHeight);


	for( i = 0 ; i < 3; i++){
		printf("\n%3.5f %3.5f",marker[i][0],marker[i][1]);
	}
	
	printf("\n");

	tmp[0] = z[2]; 
	theta = acos(z[2]);
	phi = atan2(z[0],-z[1]);
	// phi += M_PI;
	
	///////////////////////////////////////////////////////////////////////////////////
	
	printf("theta  = %3.5f phi = %3.5f \n", theta*180/M_PI,phi*180/M_PI);
	
	tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

	a1 = cos(phi); a2 = -cos(theta)*sin(phi);
	b1 = sin(phi); b2 = cos(theta)*cos(phi);
	c1 = 0; 	   c2 = sin(theta);

	l1 = -cos(theta)*sin(phi); l2 = -cos(phi);
	m1 =  cos(phi)*cos(theta); m2 = -sin(phi);
	n1 = sin(theta); 		   n2 = 0;

	diff_D_x = D[0][0] - D[1][0]; diff_D_y = D[0][1] - D[1][1];
	diff_Marker_x = marker[0][0] - marker[1][0]; diff_Marker_y = marker[0][1] - marker[1][1];

	/////////// calculating a,b coefficients for equation a*cos(psi)+b*sin(psi) = 0;

	a = diff_Marker_y*(a1*diff_D_x + l1*diff_D_y +
	c1*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n1*(marker[1][0]*D[
	1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b1*diff_D_x +
	m1*diff_D_y + c1*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
	n1*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

	b = diff_Marker_y*(a2*diff_D_x + l2*diff_D_y +
	c2*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + n2*(marker[1][0]*D[
	1][1]-marker[0][0]*D[0][1])/focal) -  diff_Marker_x*(b2*diff_D_x +
	m2*diff_D_y + c2*(marker[1][1]*D[1][0]-marker[0][1]*D[0][0])/focal +
	n2*(marker[1][1]*D[1][1]-marker[0][1]*D[0][1])/focal);

	psi[0] = atan2(a,-b);
	psi[1] = M_PI + psi[0];

	for(i = 0 ; i < 2; i++){
		printf("\n@@@@@@@@@@@@@@@@@@@@@@@@@        START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");

		rotMat[0][0] = cos(psi[i])*a1 + sin(psi[i])*a2;
		rotMat[0][1] = cos(psi[i])*l1 + sin(psi[i])*l2;
		rotMat[0][2] = z[0];

		rotMat[1][0] = cos(psi[i])*b1 + sin(psi[i])*b2;
		rotMat[1][1] = cos(psi[i])*m1 + sin(psi[i])*m2;
		rotMat[1][2] = z[1];


		rotMat[2][0] = cos(psi[i])*c1 + sin(psi[i])*c2;
		rotMat[2][1] = cos(psi[i])*n1 + sin(psi[i])*n2;
		rotMat[2][2] = z[2];

		t[2] = (rotMat[0][0]*diff_D_x + rotMat[0][1]*diff_D_y + rotMat[2][0]*(marker[1][0]*D[1][0]-marker[0][0]*D[0][0])/focal + rotMat[2][1]*(marker[1][0]*D[1][1]-marker[0][0]*D[0][1])/focal) * focal/diff_Marker_x;

		Z[0] = rotMat[2][0]*D[0][0] + rotMat[2][1]*D[0][1] + t[2];

		t[1] = Z[0]*marker[0][1]/focal- rotMat[1][0]*D[0][0] - rotMat[1][1]*D[0][1] ;
		t[0] = Z[0]*marker[0][0]/focal- rotMat[0][0]*D[0][0] - rotMat[0][1]*D[0][1] ;

		printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);

		X[0] = rotMat[0][0]*D[0][0] + rotMat[0][1]*D[0][1] + t[0];
		Y[0] = rotMat[1][0]*D[0][0] + rotMat[1][1]*D[0][1] + t[1];
		

		x[0] = focal*X[0]/Z[0]; y[0] = focal*Y[0]/Z[0];


		X[1] = rotMat[0][0]*D[1][0] + rotMat[0][1]*D[1][1] + t[0];
		Y[1] = rotMat[1][0]*D[1][0] + rotMat[1][1]*D[1][1] + t[1];
		Z[1] = rotMat[2][0]*D[1][0] + rotMat[2][1]*D[1][1] + t[2];

		x[1] = focal*X[1]/Z[1]; y[1] = focal*Y[1]/Z[1];



		// t[0] = marker[1][0]*Z[1]/focal - rotMat[0][0]*D[1][0] - rotMat[0][1]*D[1][1];
		// t[1] = marker[1][1]*Z[1]/focal - rotMat[1][0]*D[1][0] - rotMat[1][1]*D[1][1];
		// printf("\n array T :  %3.5f %3.5f %3.5f ",t[0],t[1],t[2]);
		
		

		X[2] = rotMat[0][0]*D[2][0] + rotMat[0][1]*D[2][1] + t[0];
		Y[2] = rotMat[1][0]*D[2][0] + rotMat[1][1]*D[2][1] + t[1];
		Z[2] = rotMat[2][0]*D[2][0] + rotMat[2][1]*D[2][1] + t[2];;

		x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];

		X[3] = rotMat[0][0]*D[3][0] + rotMat[0][1]*D[3][1] + t[0];
		Y[3] = rotMat[1][0]*D[3][0] + rotMat[1][1]*D[3][1] + t[1];
		Z[3] = rotMat[2][0]*D[3][0] + rotMat[2][1]*D[3][1] + t[2];;

		x[3] = focal*X[3]/Z[3]; y[3] = focal*Y[3]/Z[3];

		printf("\n Z's \n%3.5f %3.5f %3.5f %3.5f\n", Z[0],Z[1],Z[2],Z[3]);
		printf("\n re - projected \n%3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n %3.5f %3.5f \n", x[0],y[0], x[1],y[1], x[2],y[2], x[3],y[3]);
		// printf("\n error: %3.5f ",marker[0][0]*Z[0]/focal - cons[0] - marker[0][0]*t[2]/focal);		//error = 0;

		// printf("\n side1  : %3.5f ", sqrt((X[0]-X[1])*(X[0]-X[1])+(Y[0]-Y[1])*(Y[0]-Y[1])+(Z[0]-Z[1])*(Z[0]-Z[1])) );
		// printf("\n side2 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

		// printf("\n side3  : %3.5f ", sqrt((X[0]-X[3])*(X[0]-X[3])+(Y[0]-Y[3])*(Y[0]-Y[3])+(Z[0]-Z[3])*(Z[0]-Z[3])) );
		// printf("\n side4 : %3.5f ", sqrt((X[2]-X[1])*(X[2]-X[1])+(Y[2]-Y[1])*(Y[2]-Y[1])+(Z[2]-Z[1])*(Z[2]-Z[1])) );

		// printf("\n error: %3.5f ",rotMat[0][0]*D[0][0]+rotMat[0][1]*D[0][1] + t[0] - marker[0][0]*Z[0]/focal);
		// printf("\n error: %3.5f ",rotMat[1][0]*D[0][0]+rotMat[1][1]*D[0][1] + t[1] - marker[0][1]*Z[0]/focal);

		// printf("\n error: %3.5f ",rotMat[0][0]*D[1][0]+rotMat[0][1]*D[1][1] + t[0]- marker[1][0]*Z[1]/focal);
		// printf("\n error: %3.5f ",rotMat[1][0]*D[1][0]+rotMat[1][1]*D[1][1] + t[1]- marker[1][1]*Z[1]/focal);

		XVec[0] = X[1]-X[0];XVec[1] = Y[1]-Y[0];XVec[2] = Z[1]-Z[0];
		YVec[0] = X[1]-X[2];YVec[1] = Y[1]-Y[2];YVec[2] = Z[1]-Z[2];


		// t[1] = (marker[1][1]*((rotMat[0][0] * D[1][0] + 
		normalize(XVec); normalize(YVec);

		dotProd = dot(XVec,YVec);

		cross(XVec,YVec,ZVec);
		printf("\n dotProd : %3.5f \nZVec : %3.5f %3.5f %3.5f \n",dotProd,ZVec[0],ZVec[1],ZVec[2]);

		printf("\n@@@@@@@@@@@@@@@@@@@@@@@           END                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");	
	}
	return 0;
}
