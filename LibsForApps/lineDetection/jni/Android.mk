LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

OPENCVROOT := $(OpenCV_SDKHome)
OPENCV_LIB_TYPE:=SHARED
include ${OPENCVROOT}/sdk/native/jni/OpenCV.mk


LOCAL_MODULE := linedetection 
LOCAL_SRC_FILES := src/lineSegments.cpp src/canny_threshold.cpp src/canny.cpp src/hough.cpp src/segBin.cpp src/mergeLines.cpp src/sort.cpp src/houghpeaks.cpp src/mainBaseSeg.cpp src/joinSeg.cpp

#
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include
#LOCAL_C_INCLUDES += /usr/include/
#LOCAL_C_INCLUDES += /usr/include/opencv

include $(BUILD_SHARED_LIBRARY)
