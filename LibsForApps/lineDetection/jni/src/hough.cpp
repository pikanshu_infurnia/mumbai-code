#include "global_lineDetection.h"

vector <SCell> hough(Mat Edges, vector<double> rho, vector<double> theta,
	float FillGap, int Threshold, Mat& H, int& CountSeg,int& Margin){
//	cout<<endl<<"////////////////////////////////PRINTING IN HAUGH //////////////////////////////////////////"<<endl;
	vector <SCell> Segment;
	//Mat LabelStruct(LenR,LenT,DataType<LCell>::type);
	int LenT = theta.size(),LenR = rho.size();
	Mat CurrDist(LenR,LenT,DataType<double>::type);
	Mat SegVotes = Mat::zeros(LenR,LenT,DataType<int>::type);
	Mat Starti = Mat::zeros(LenR,LenT,DataType<double>::type);
	Mat Startj = Mat::zeros(LenR,LenT,DataType<double>::type);
	Mat Endi = Mat::zeros(LenR,LenT,DataType<double>::type);
	Mat Endj = Mat::zeros(LenR,LenT,DataType<double>::type);

	Mat mi = Mat::zeros(LenR,LenT,DataType<double>::type);
	Mat mj = Mat::zeros(LenR,LenT,DataType<double>::type);

	
	double m,r,m_temp,r_temp;
	Mat prev_slop = Mat::zeros(LenR,LenT,DataType<double>::type);
	Mat prev_rho = Mat::zeros(LenR,LenT,DataType<double>::type);
	//Mat CpH(LenR,LenT,DataType<int>::type);
	SCell SegmentCell;// Will be used for pushing back into Segment and then return Segment at the end
	CountSeg=0;
	int Rows = Edges.rows;
	int Cols = Edges.cols;
	//Mat matSub;
	// Storing values of Sin(theta) and cos(theta) for further use
	double SinArray[LenT],CosArray[LenT];
	for(int l=0;l<LenT;++l){
		SinArray[l] = sin(theta[l]*PI/180);
		CosArray[l] = cos(theta[l]*PI/180);
	}
	int i;
	//cout<<"["<<SinArray[0]<<","<<SinArray[1]<<","<<SinArray[2]<<","<<"..."<<SinArray[theta.size()-1]<<"]"<<endl;
	double slope_r = (double)(LenR-1)/(rho[LenR-1]-rho[0]);
	//cout<<"Slope R = "<<slope_r<<endl;
	double slope_t = (double)(LenT-1)/(theta[LenT-1]-theta[0]);
	//cout<<"Slope T = "<<slope_t<<endl;
	double RhoVal;double Distance,PrevDist,DiffDist;int rhoIdx;int CurrVotes;
	for(int c=0;c<Cols+Rows;++c){
		for(int j = ((c < Rows-1) ? c:Rows-1) ; j>=0 ; j--){
			i = c - j;
			if(i > Cols-1){break;}
			if(Edges.at<bool>(j,i)>0){
				//cout<<"("<<i<<","<<j<<") \t";
				for(int ThetaID=0;ThetaID < LenT;++ThetaID){
					RhoVal = i*CosArray[ThetaID] + j*SinArray[ThetaID];//(j,i) is the pixel location
					rhoIdx = (int)round(slope_r*(RhoVal - rho[0]));
					//cout<<"RhoVal: "<<RhoVal<<", rhoIdx: "<<rhoIdx<<endl;
					Distance = i*SinArray[ThetaID] - j*CosArray[ThetaID];// 1D distance from projection of the origin
					H.at<int>(rhoIdx,ThetaID) += 1 ;
					//matSub = H(Rect(rhoIdx,ThetaID,3,3));
					//cout<<"("<<rhoIdx<<","<<ThetaID<<");";//<<endl;
					//CurrVotes = LabelStruct.at<LCell>(rhoIdx,ThetaID).Votes;
					CurrVotes = SegVotes.at<int>(rhoIdx,ThetaID);
					//cout<<CurrVotes<<"\t"; 
					if(CurrVotes==0){
						Startj.at<double>(rhoIdx,ThetaID) = j;
						Starti.at<double>(rhoIdx,ThetaID) = i;
						Endj.at<double>(rhoIdx,ThetaID) = j;
						Endi.at<double>(rhoIdx,ThetaID) = i;
						mi.at<double>(rhoIdx,ThetaID) = i;
						mj.at<double>(rhoIdx,ThetaID) = j;
						//LabelStruct.at<LCell>(rhoIdx,ThetaID).Votes = 1;
						SegVotes.at<int>(rhoIdx,ThetaID) = 1;
						CurrDist.at<double>(rhoIdx,ThetaID) = Distance;
					}
					else{
						PrevDist = CurrDist.at<double>(rhoIdx,ThetaID);
						//cout<<"Storage : "<<CurrDist.at<float>(rhoIdx,ThetaID)<<"; Out : "<<PrevDist<<endl;
						DiffDist = abs(Distance-PrevDist);
						CurrDist.at<double>(rhoIdx,ThetaID) = Distance;
						//cout<<"DiffDist : "<<DiffDist;
						if (DiffDist<FillGap){
						//cout<<"; Inside...."<<endl;
							
								mi.at<double>(rhoIdx,ThetaID) = (mi.at<double>(rhoIdx,ThetaID)*(SegVotes.at<int>(rhoIdx,ThetaID))+Endi.at<double>(rhoIdx,ThetaID))/double(SegVotes.at<int>(rhoIdx,ThetaID)+1);
								mj.at<double>(rhoIdx,ThetaID) = (mj.at<double>(rhoIdx,ThetaID)*(SegVotes.at<int>(rhoIdx,ThetaID))+Endj.at<double>(rhoIdx,ThetaID))/double(SegVotes.at<int>(rhoIdx,ThetaID)+1);
							if(SegVotes.at<int>(rhoIdx,ThetaID) > 10){
								
								m = atan((mj.at<double>(rhoIdx,ThetaID) - j) / (mi.at<double>(rhoIdx,ThetaID) - i));
								if( m <= 0 ) m = PI/2 - abs(m);
								else m = abs(m) - PI/2;
								if(m < -1) m += PI; 
								r = (double)i*cos(m)+(double)j*sin(m);
								m_temp = m; r_temp = r;
								r = (prev_rho.at<double>(rhoIdx,ThetaID)*(SegVotes.at<int>(rhoIdx,ThetaID)-1-10) + r)/(SegVotes.at<int>(rhoIdx,ThetaID)-10);
								m = (prev_slop.at<double>(rhoIdx,ThetaID)*(SegVotes.at<int>(rhoIdx,ThetaID)-1-10) + m)/(SegVotes.at<int>(rhoIdx,ThetaID)-10);
							
								if((rhoIdx == 924) && (ThetaID == 177)){
									//cout<<"m_inst : " <<m_temp*180/PI<<"\t"<<"r_inst : "<<r_temp<<endl;
//									cout<<"m : " <<m*180/PI<<"\t"<<"r : "<<r<<"\t"<<"rho"<<rhoIdx<<"\t"<<"ThetaID"<<ThetaID<<"\t"; 

								}
								prev_slop.at<double>(rhoIdx,ThetaID) = m;
								prev_rho.at<double>(rhoIdx,ThetaID) = r;
							}
							if((rhoIdx == 924) && (ThetaID == 177)){
//								cout<<"("<<j<<","<<i<<"),"<<"\t"<<"("<<mj.at<double>(rhoIdx,ThetaID)<<","<<mi.at<double>(rhoIdx,ThetaID)<<"),"<<endl;
							}
								Endj.at<double>(rhoIdx,ThetaID) = (double)j;
								Endi.at<double>(rhoIdx,ThetaID) = (double)i;
								//LabelStruct.at<LCell>(rhoIdx,ThetaID).Votes += 1;
								SegVotes.at<int>(rhoIdx,ThetaID) += 1;
							
							
						}
						else{
						//cout<<"; OutSide...."<<endl;
							if(CurrVotes>Threshold){
								CountSeg +=1;
								SegmentCell.SegIndex = CountSeg;
								SegmentCell.StartPt[0] = Startj.at<double>(rhoIdx,ThetaID);
								SegmentCell.StartPt[1] = Starti.at<double>(rhoIdx,ThetaID);
								SegmentCell.EndPt[0] = Endj.at<double>(rhoIdx,ThetaID);
								SegmentCell.EndPt[1] = Endi.at<double>(rhoIdx,ThetaID);
								SegmentCell.Votes = CurrVotes;
				                SegmentCell.RhoIndex = rhoIdx;
				                SegmentCell.ThetaIndex = ThetaID;
				                SegmentCell.slop = prev_slop.at<double>(rhoIdx,ThetaID);
				                SegmentCell.rho_new = prev_rho.at<double>(rhoIdx,ThetaID);
				                SegVotes.at<int>(rhoIdx,ThetaID) = 0;
				                if(ThetaID > Margin && ThetaID < (LenT-Margin)){ Segment.push_back(SegmentCell);
				                //LabelStruct.at<LCell>(rhoIdx,ThetaID).Votes = 0;
				                
//				                if((rhoIdx == 924) && (ThetaID == 177)){
//				            	cout<<"Seg. NO.: "<<SegmentCell.SegIndex<<";CurrVotes: "<<CurrVotes<<"; Position: ("<<rhoIdx<<","<<ThetaID<<") ";
//				            	cout<<",H = "<<H.at<int>(rhoIdx,ThetaID);
//				            	cout<<"		sPT:"<<"("<<SegmentCell.StartPt[0]<<","<<SegmentCell.StartPt[1]<<")";
//				            	cout<<"		ePT:"<<"("<<SegmentCell.EndPt[0]<<","<<SegmentCell.EndPt[1]<<")"<<endl;
				            	}
				            	prev_slop.at<double>(rhoIdx,ThetaID) = 0;
				            	prev_rho.at<double>(rhoIdx,ThetaID) = 0;
							}
							
							else{
								Startj.at<double>(rhoIdx,ThetaID) = (double)j;
								Starti.at<double>(rhoIdx,ThetaID) = (double)i;
								Endj.at<double>(rhoIdx,ThetaID) = (double)j;
								Endi.at<double>(rhoIdx,ThetaID) = (double)i;
								//CurrDist.at<float>(rhoIdx,ThetaID) = Distance;
								//LabelStruct.at<LCell>(rhoIdx,ThetaID).Votes = 1;
								SegVotes.at<int>(rhoIdx,ThetaID) = 1;
							}
						}
					}
				}
			}
		}
	}
//	cout<<" \n Post processing after all the points being done \n";
	// Post processing after all the points being done
	for(int r=0;r<LenR;++r){
		for(int t=0;t<LenT;++t){
			//CurrVotes = LabelStruct.at<LCell>(r,t).Votes;
			CurrVotes = SegVotes.at<int>(r,t);
			if(CurrVotes>Threshold){
				CountSeg +=1;
				SegmentCell.SegIndex = CountSeg;
				SegmentCell.StartPt[0] = Startj.at<double>(r,t);
				SegmentCell.StartPt[1] = Starti.at<double>(r,t);
				SegmentCell.EndPt[0] = Endj.at<double>(r,t);
				SegmentCell.EndPt[1] = Endi.at<double>(r,t);
				SegmentCell.Votes = CurrVotes;
				SegmentCell.RhoIndex = r;
				SegmentCell.ThetaIndex = t;
				SegmentCell.slop = prev_slop.at<double>(r,t);
				SegmentCell.rho_new = prev_rho.at<double>(r,t);
				if(t > Margin && t < (LenT-Margin)){ Segment.push_back(SegmentCell);
				//cout<<"Printing... Seg. No."<<CountSeg<<" -> "<<"("<<Segment[CountSeg-1].Votes<<","<<Segment[CountSeg-1].RhoIndex<<","<<Segment[CountSeg-1].ThetaIndex<<") -> ("<<Segment[CountSeg-1].StartPt[0]<<","<<Segment[CountSeg-1].StartPt[1]<<") -> ("<<Segment[CountSeg-1].EndPt[0]<<","<<Segment[CountSeg-1].EndPt[1]<<");"<<endl;
//				cout<<"Seg. NO.: "<<SegmentCell.SegIndex<<";CurrVotes: "<<CurrVotes<<"; Position: ("<<r<<","<<t<<") ";
//				cout<<",H = "<<H.at<int>(r,t); cout<<",slop = "<<SegmentCell.slop;
//				cout<<"	sPT:"<<"("<<SegmentCell.StartPt[0]<<","<<SegmentCell.StartPt[1]<<")";
//				cout<<"	ePT:"<<"("<<SegmentCell.EndPt[0]<<","<<SegmentCell.EndPt[1]<<")"<<endl;
				}
				prev_slop.at<double>(r,t) = 0;
				//prev_rho.at<double>(r,t) = 0;
			}
		}
	}
//	cout<<endl;
	//H = CpH;
	//SegmentCell.release();
	//LabelStruct.release();
	CurrDist.release();
	SegVotes.release();
	Starti.release();
	Startj.release();
	Endj.release();
	Endi.release();
	prev_slop.release();
	prev_rho.release();
//	cout<<"////////////////////////////////PRINTING IN HAUGH END///////////////////////////////////////"<<endl<<endl;
	return Segment;
}

///// Hough End /////////////
