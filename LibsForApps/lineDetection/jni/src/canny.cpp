#include "global_lineDetection.h"

void CannyThreshold(Mat Image,Mat& Edges,double *thresh,int ApertureSize){
	GaussianBlur(Image,Edges, Size(5,5),sqrt(2));
	int lowThresh = (int)thresh[1];
	int highThresh = (int)thresh[0];
	//~ Canny(Edges,Edges, lowThresh, lowThresh*ratio, ApertureSize,false);
	Canny(Edges,Edges, lowThresh, highThresh, ApertureSize,false);
//	imshow("Edges",Edges);
}
