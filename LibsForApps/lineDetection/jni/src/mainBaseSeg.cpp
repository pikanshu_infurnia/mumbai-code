#include <global_lineDetection.h>

double distance(Point P1,Point P2,Point P,double& len){

	len = sqrt((P1.x - P2.x)*(P1.x - P2.x) + (P1.y - P2.y)*(P1.y - P2.y));
	double dis = abs((P1.y - P2.y)*(P2.x - P.x) - (P1.x - P2.x)*(P2.y - P.y))/len;
	return dis;

}

SCell nearestSeg(vector<SCell>& Segments,vector<double>& SegLen,vector<double>& dis,Point pt,double lenThresh,double slop_marker)
{
	double temp,temp1;
	Point P1,P2;
	int min_index = 0,flag = 0,final_index=0;

	double SLOP;
	//~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"Slop of marker %f and margin is %f", slop_marker, 40*PI/180);
	for(int i  = 0; i<Segments.size(); i++){
		P1.y = Segments[i].StartPt[0]; P1.x = Segments[i].StartPt[1];
		P2.y = Segments[i].EndPt[0]; P2.x = Segments[i].EndPt[1];
		SLOP = atan((Segments[i].StartPt[0] - Segments[i].EndPt[0])/(Segments[i].StartPt[1] - Segments[i].EndPt[1]));
		double intercept = P2.y - tan(SLOP)*P2.x;
		double side  = pt.y - tan(SLOP)*pt.x - intercept;
		//~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"Slop of segment %f ", SLOP);

		temp1 = distance(P1,P2,pt,temp);
		dis.push_back(temp1);
		SegLen.push_back(temp);
		if(((SLOP > (slop_marker-20*PI/180)) && (SLOP < (slop_marker+20*PI/180))) && ((((pt.x - P1.x) * (pt.x - P2.x)) < 0) || (((pt.y - P1.y) * (pt.y - P2.y)) < 0 )) && (side > 0)){
			if(i == 0){ flag = 1;}

			else if(min_index == 0 && flag != 2){
			    if(flag == 1){
			        flag = 2;
			        //~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"dis[i] %f and dis[min_index] %f ", dis[i],dis[min_index]);
			    	min_index =(dis[i] <= dis[min_index]) ? i : min_index;
			    	//~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"SegLen[min_index] %f ", SegLen[min_index]);
			    	if(SegLen[min_index] > lenThresh) {final_index = min_index;}

			    }
			    if(flag == 0) {min_index = i; final_index = i;flag = 2;}
			}
			else{
			    //~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"dis[i] %f and dis[min_index] %f ", dis[i],dis[min_index]);
				 min_index =(dis[i] <= dis[min_index]) ? i : min_index;
				 //~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"SegLen[min_index] %f ", SegLen[min_index]);
				if(SegLen[min_index] > lenThresh) {final_index = min_index;}
			}
			//~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"min_index %d and final_index %d ", min_index,final_index);

		}

        //~ __android_log_print(ANDROID_LOG_VERBOSE,APPNAME,"****************************************** ");
	}
	return Segments[final_index];

}
