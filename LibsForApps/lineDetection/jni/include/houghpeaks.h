bool within_triangle(Point& A,Point& B,Point& C,Point& p);
void print_block(vector<int>& row,vector<int>& col,Mat& H, int RhoIndex, int ThetaIndex, int WinT, int WinR ,int vote,int Threshold);
void PlotSeg(vector<SCell>& Segments,Mat image,char *string,double r,double g,double b);
vector<SCell> houghpeaks(Mat& H, vector<double> rho, vector<double> theta, vector<SCell>& Segments, int CountSeg, Mat& Image, char out_str[],int Threshold,double RhoRes);
void plotSeg(vector<SCell>& Segments,Mat& image,double r,double g,double b);
