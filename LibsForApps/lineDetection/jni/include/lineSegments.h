vector <double> linespace(double Low, double High, double Resol);
vector<SCell> GetLineSeg(Mat Image_inp,Mat& Edges,double Theta0,int ThetaRng,int Margin,char out_str[],vector<SCell>& Segment,int Threshold,float FillGap,double RhoRes,double ThetaRes,int PERCENTILE,vector<Point2f> marker,Mat &H);
SCell joinSeg(vector<SCell>& Proj_Seg);
vector<SCell> merge_in_range(vector<SCell>& FinalSegments,double Theta0,double Rho0,double Theta_Margin,double Rho_Margin,vector<int>& done_horizontal);
