#include "global_lineDetection.h"

#include "segBin.h"
#include "mergeLines.h"
#include "sort.h"

#define BRIGHT 1
#define RED 31
#define GREEN 32
#define BLACK 30
#define BG_WHITE 47


bool within_triangle(Point& A,Point& B,Point& C,Point& p){
	double a,b,c,d,e,f,x,y;
	a = B.x - A.x; //d = (float)col[1] - col1[0] = 0;
	b = C.x - A.x; e = C.y - A.y;
	c = p.x - A.x ; f =p.y - A.y;
	y = f/e;x = (c*e - b*f)/(a*e);
	if((0<=x && 0<=y && (x + y) <= 1)){
		
		return true;
	}
	else return false;
}
void print_block(vector<double>& row,vector<double>& col,Mat& H, int RhoIndex, int ThetaIndex, int WinT, int WinR ,int vote,int Threshold){
	bool TriA,TriB;
	for(int i = RhoIndex-WinR; i<=RhoIndex+WinR ; i++ ){
		for(int j = ThetaIndex-WinT; j<=ThetaIndex+WinT ; j++ ){
							int aj,ind = (0<=j && j < H.cols-1);
							if( j < 0) aj = H.cols + j;
							else if(j > H.cols-1) aj = j - H.cols;
							else aj = j;
							
							int ai = (ind*i+(1-ind)*(H.rows-1-i)); 
							int k = H.at<int>(ai,aj) ;
							if(k <= Threshold) printf("%c[%d;%d;%dm", 0x1B, BRIGHT,RED+4,BG_WHITE);
							else printf("%c[%d;%d;%dm", 0x1B, BRIGHT,RED,BG_WHITE);
							
							Point A,B,C,p;
							A.x = row[0]; A.y = col[0];
							B.x = row[1]; B.y = col[1];
							C.x = row[2]; C.y = col[2];
							p.x = i ;     p.y = j;
							TriA = within_triangle(A,B,C,p);

							A.x = row[3]; A.y = col[3];
							B.x = row[4]; B.y = col[4];
							C.x = row[5]; C.y = col[5];
							p.x = i ;     p.y = j;
							TriB = within_triangle(A,B,C,p);

							if(i == RhoIndex && j == ThetaIndex) printf("%c[%d;%d;%dm%3d  ", 0x1B, BRIGHT,RED+5,BG_WHITE,k);
							else if(TriB){
									printf("%3d  ",k);
									//cout<< i <<" "<<j<< " "<<x<<" "<<y<<" "<<endl;
							}
							else if(TriA){
									printf("%3d  ",k);
									//cout<< i <<" "<<j<< " "<<x1<<" "<<y1<<" "<<endl;
							}
							else printf("%c[%d;%d;%dm%3d  ", 0x1B, BRIGHT,GREEN,BG_WHITE,k);
			}
			cout<<endl;
		}
		printf("%c[%dm", 0x1B, 0);
		
}

 void plotSeg(vector<SCell>& Segments,Mat& image,double r,double g,double b){

 	double S1,S2,E1,E2;
 	//Mat out;
 	//image.copyTo(out);
 //	cout<<string<<" :::::::: "<<endl<<endl;
 	for(int ax=0;ax<Segments.size();++ax){
 		SCell S = Segments[ax];
 		S1 = S.StartPt[0],S2 = S.StartPt[1];
 		E1 = S.EndPt[0],E2 = S.EndPt[1];
 		line(image,Point(round(S2),round(S1)),Point(round(E2),round(E1)),Scalar(r,g,b),1,8);
 		printf("\nEnd points are (%f,%f) (%f,%f) ", S1,S2,E1,E2);
 //		cout<<endl<<"\t"<<Segments[ax].SegIndex<<"\t"<<"("<<S1<<","<<S2<<")"<<"\t"<<"("<<E1<<","<<E2<<")"<<"\t";
 //		cout<<"\t"<<Segments[ax].rho_new<<"\t"<<Segments[ax].slop<<endl;
 //		imshow(string,out);
 //		waitKey(0);

 	}

 	//out.release();
 }

void PlotSeg(vector<SCell>& Segments,Mat image,char *string,double r,double g,double b){

	double S1,S2,E1,E2;
	Mat out;
	image.copyTo(out);
	cout<<string<<" :::::::: "<<endl<<endl;
	for(int ax=0;ax<Segments.size();++ax){
		SCell S = Segments[ax];
		S1 = S.StartPt[0],S2 = S.StartPt[1];
		E1 = S.EndPt[0],E2 = S.EndPt[1];
		line(out,Point(round(S2),round(S1)),Point(round(E2),round(E1)),Scalar(r,g,b),1,8);
		cout<<endl<<"\t"<<Segments[ax].SegIndex<<"\t"<<"("<<S1<<","<<S2<<")"<<"\t"<<"("<<E1<<","<<E2<<")"<<"\t";
		cout<<"\t"<<Segments[ax].rho_new<<"\t"<<Segments[ax].slop<<endl;
		imshow(string,out);
		waitKey(0);
		
	}

	out.release();
}


///// HoughPeaks begin //////
// input -> H, rho, theta, Segments, CountSeg
// output -> So far nothing ;)
vector<SCell> houghpeaks(Mat& H, vector<double> rho, vector<double> theta, vector<SCell>& Segments, int CountSeg, Mat& Image, char out_str[],int Threshold,double RhoRes){

//	cout<<endl<<"////////////////////////////////PRINTING IN HAUGHPEAKS//////////////////////////////////////////"<<endl;

	vector<bool> Done (CountSeg,false);
	int p, q; // var for indiv rho and theta
	vector<int> indices;
	vector<SCell>  FinalSegments; 
	int WinR,WinT,count=0;
	float Factor;
	double S1,S2,E1,E2;
	char String[] = "Segments";
	
//	PlotSeg(Segments,Image,"Segment1",0,255,0);
	int len = Segments.size();
	quickSort(Segments, Segments.size());
//	cout<<"After Sorting..."<<endl;
//	cout<<endl<<"Seg Index\tSeg Votes\tRho Index\tTheta Index";
	for(int i=0; i<Segments.size();i++) {
		Segments[i].SegIndex = i;
//		cout<<endl<<"\t"<<Segments[i].SegIndex<<"\t"<<Segments[i].Votes<<"\t\t"<<Segments[i].RhoIndex<<"\t\t"<<Segments[i].ThetaIndex;
		//H.at<int>(Segments[i].RhoIndex,Segments[i].ThetaIndex) = Segments[i].Votes;
	}
//	PlotSeg(Segments,Image,"Segment2",0,255,0);
//	cout<<endl<<"CountSeg: "<<CountSeg<<endl;

	int index = 0, i=0, Merged=0;

	Mat Mergeout ;//= imread(argv[1]);
	Image.copyTo(Mergeout);
	for(i=0; i<len; i++){
		
		if(Done[i]==false){

			vector<double> row,col,Label;
			int NumMerge = 0;
			Factor = sqrt(2) ; //*cos((abs(theta[Segments[i].ThetaIndex])-45)*PI/180);
			WinT = round(asin(RhoRes/(Threshold*Factor))*180/PI)+2;
			
			int rowSize = H.rows;
			int colSize = H.cols;
//			cout<<"/////////////////////////////// NEW SEGMENT//////////////////////////////////"<<endl;
				
//			cout<<"Segment No.- "<<i+1<<" "<<Segments[i].SegIndex<< endl;
//			cout<<"Votes - "<<Segments[i].Votes<<endl;
//			cout<<"WinT = "<< WinT<<endl<<endl;
			double WRho =0,WTheta =0;
			
			bool SegPass = verifyBin(Segments[i],H,rho[Segments[i].RhoIndex],theta[Segments[i].ThetaIndex]*PI/180,WRho,WTheta,RhoRes);
			binExtract(WinR,row, col, rowSize, colSize, WinT,Segments[i],theta[Segments[i].ThetaIndex]*PI/180,rho[Segments[i].RhoIndex],RhoRes);
//			print_block(row,col,H,Segments[i].RhoIndex, Segments[i].ThetaIndex, WinT, WinR,Segments[i].Votes,Threshold);
//			waitKey(0);
		
			if(SegPass==true){ // Anti-symmetry condition
				vector <int> MergeSeg;
				SCell MainSeg = Segments[i];
				SCell FinalSeg;
				double rho_temp = rho[MainSeg.RhoIndex];
				double theta_temp = theta[MainSeg.ThetaIndex]*PI/180;
				count++;
				
				//waitKey(0);
				//int pika=0;
				S1 = MainSeg.StartPt[0],S2 = MainSeg.StartPt[1];
				E1 = MainSeg.EndPt[0],E2 = MainSeg.EndPt[1];
				
//				line(Image,Point(round(S2),round(S1)),Point(round(E2),round(E1)),Scalar(0,255,0),1,8);
				//imshow("MainSegments",Image);
				int temp_RhoIndex, temp_ThetaIndex,temp;
					//cout<<temp_index ;
				//namedWindow("selected_bin_segments",WINDOW_AUTOSIZE);
				Point A,B,C,p;bool TriA,TriB;
				for(int j=0; j < Segments.size(); ++j){
					if( j != i){
						if(Done[j]==false){
							
//							if(col[0] < 0){
////								cout<<"entered 1 ";
//								temp = H.cols + col[0] ;
////								cout<<temp<<" "<<Segments[j].ThetaIndex<<" ";
//								
//								if(Segments[j].ThetaIndex >= temp) {
////									cout<<"entered 1 " << H.cols <<" "<<H.rows<<" ";
//									temp_ThetaIndex = Segments[j].ThetaIndex - H.cols;
////									cout<<temp_ThetaIndex<<" ";
//									temp_RhoIndex = (H.rows-1-Segments[j].RhoIndex);
////									cout<<temp_RhoIndex;
//								}
//								else{ 
//									temp_ThetaIndex = Segments[j].ThetaIndex;
//									temp_RhoIndex = Segments[j].RhoIndex;
//									}
////								cout<<endl;
//							}
//							else if( col[0] > (H.cols-1)){
////								cout<<"entered 2 ";
//								temp = col[0] - H.cols;
//								if(Segments[j].ThetaIndex <= temp) {
//									temp_ThetaIndex =H.cols + Segments[j].ThetaIndex;
//									temp_RhoIndex = (H.rows-1-Segments[j].RhoIndex);
//								}
//								else{ 
//									temp_ThetaIndex = Segments[j].ThetaIndex;
//									temp_RhoIndex = Segments[j].RhoIndex;
//									}
////								cout<<endl;
//							}
//							else {
//								cout<<"entered 3 ";
								temp_ThetaIndex = Segments[j].ThetaIndex;
								temp_RhoIndex = Segments[j].RhoIndex;
//								cout<<endl;
//							}
							
//							intersection( v,row[1], row[0], col[1], MainSeg.RhoIndex, MainSeg.ThetaIndex);
							A.x = row[0]; A.y = col[0];
							B.x = row[1]; B.y = col[1];
							C.x = row[2]; C.y = col[2];
							p.x = temp_RhoIndex ;     p.y =temp_ThetaIndex ;
							TriA = within_triangle(A,B,C,p);


//							cout<< Segments[j].RhoIndex << " " <<Segments[j].ThetaIndex <<" "<< temp_RhoIndex << " " << temp_ThetaIndex<<" "<<x<<" "<<y<<endl;

//							if(col[3]<0){
////								cout<<"entered 4 ";
//								temp = col[3]+ H.cols;
//								if(Segments[j].ThetaIndex >= temp) {
//									temp_ThetaIndex = Segments[j].ThetaIndex - H.cols;
//									temp_RhoIndex = (H.rows-1-Segments[j].RhoIndex);
//								}
//								else{ 
//									temp_ThetaIndex = Segments[j].ThetaIndex;
//									temp_RhoIndex = Segments[j].RhoIndex;
//									}
////								cout<<endl;
//							}
//							else if( col[3] > (H.cols-1)){
////								cout<<"entered 5 ";
//								temp = H.cols - col[3];
//								if(Segments[j].ThetaIndex <= temp) {
//									temp_ThetaIndex =H.cols + Segments[j].ThetaIndex;
//									temp_RhoIndex = (H.rows-1-Segments[j].RhoIndex);
//								}
//								else{ 
//									temp_ThetaIndex = Segments[j].ThetaIndex;
//									temp_RhoIndex = Segments[j].RhoIndex;
//									}
////								cout<<endl;

//							}
//							else {
//								cout<<"entered 6 ";
//								temp_ThetaIndex = Segments[j].ThetaIndex;
//								temp_RhoIndex = Segments[j].RhoIndex;
//								cout<<endl;
//							} 

							A.x = row[3]; A.y = col[3];
							B.x = row[4]; B.y = col[4];
							C.x = row[5]; C.y = col[5];
							p.x = temp_RhoIndex ;     p.y =temp_ThetaIndex ;
							TriB = within_triangle(A,B,C,p);

							if(TriA || TriB){
								
								MergeSeg.push_back(Segments[j].SegIndex);
								//cout<<Segments[j].Votes<<" ";
								
							}
						}
					}
				} 

//				cout<<"Num. of lines to be merged : "<<MergeSeg.size()<<endl;

//				cout<<endl;
				
				int MergeCount = 0;
				
				MergeLines(MergeCount,FinalSeg,MergeSeg,MainSeg,Done,Segments,rho_temp,theta_temp,WRho,WTheta);
				
				Done[i] = 1;
				// and, struct FinalSeg
//				cout<<"No. of lines Merged = "<<MergeCount<<endl;
				S1 = FinalSeg.StartPt[0],S2 = FinalSeg.StartPt[1];
				E1 = FinalSeg.EndPt[0],E2 = FinalSeg.EndPt[1];
//				line(Mergeout,Point(round(S2),round(S1)),Point(round(E2),round(E1)),Scalar(0,0,255),1,8);
				//imshow("Final Processed Segments",Mergeout);
				//FinalSeg.SegIndex = count;
				FinalSegments.push_back(FinalSeg);

				Merged += MergeCount;
				Merged++;
			
//				for(int lx = 0;lx<Done.size();lx++) cout<< Done[lx] <<endl;
						
			// Merge Code ends here....
			// Put comment Here
//				printf("Merged %i lines, now %i remaining \n",Merged,(int)Done.size()-Merged);
			}
			else {
				S1 = Segments[i].StartPt[0],S2 = Segments[i].StartPt[1];
				E1 = Segments[i].EndPt[0],E2 = Segments[i].EndPt[1];
//				line(Image,Point(round(S2),round(S1)),Point(round(E2),round(E1)),Scalar(255,0,0),1,8);
				//imshow("MainSegments",Image);
				Done[i] = 1;	// if cond.
			}
			//waitKey(0);
		}	// for loop
	}
	
//	cout<<"count"<<count<<endl;
//	cout<<"////////////////////////////////PRINTING IN HAUGHPEAKS END///////////////////////////////////////"<<endl<<endl;
//	cout<<"Reached here:";
//	imwrite(out_str,Mergeout);
	Mergeout.release();

	return FinalSegments;
	
}

