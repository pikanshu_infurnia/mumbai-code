#include "global_lineDetection.h"

////// MergeLine Code ///////
void MergeLines(int& MergeCount, SCell& FinalSeg, vector<int>& MergeSeg, SCell& MainSeg, vector<bool>& Done, vector<SCell>& Segments,const double Rho, const double Theta,const double WRho, const double WTheta){
//		cout<<endl<<"                   /////////////PRINTING IN MergeLine///////////////                           "<<endl;
		double Teta;
		Teta =  MainSeg.slop;
	double S0j = MainSeg.StartPt[0],S0i = MainSeg.StartPt[1];
	double E0j = MainSeg.EndPt[0],E0i = MainSeg.EndPt[1];

//	cout<<"Main LIne: ";
//	cout<<"RHo,Teta = ("<<Rho<<","<<Theta*180/PI<<"); ";
//	cout<<"StartPt = ("<<S0j<<","<<S0i<<"); ";cout<<"EndPt = ("<<E0j<<","<<E0i<<"); ";




	double Rho1;
	//float rho_new;
	//float rho_new = E0i*cos(Teta)+ E0j*sin(Teta);
		Rho1 = S0i*sin(Teta) - S0j*cos(Teta);
//		cout<< "("<< S0j<<","<<S0i<<","<<Rho1<<"), ";
		S0i = cos(Teta)*MainSeg.rho_new + sin(Teta)*Rho1;
		S0j = sin(Teta)*MainSeg.rho_new - cos(Teta)*Rho1;
///		cout<< "("<< S0j<<","<<S0i<<"), ";
//		cout<< "("<< E0j<<","<<E0i<<"), ";
		Rho1 = E0i*sin(Teta) - E0j*cos(Teta);
		E0i = cos(Teta)*MainSeg.rho_new + sin(Teta)*Rho1;
		E0j = sin(Teta)*MainSeg.rho_new - cos(Teta)*Rho1;
///		cout<< "("<< E0j<<","<<E0i<<"), ";
//		cout<< "("<< MainSeg.rho_new<<","<<Teta*180/PI<<"), ";
//		cout<<"\n";

	
	double DistHS = sin(Teta)*(S0i) - cos(Teta)*(S0j); // Distance of start point from the perpendicular point 
	double DistHE = sin(Teta)*(E0i) - cos(Teta)*(E0j); // Distance of end point from the perpendicular point
	double DistMPS,DistMPE,DistMHS,DistMHE;int SSbit = 0,SEbit = 0,ESbit = 0,EEbit = 0;
//	cout<<"DistHS,DistHE = ("<<DistHS<<","<<DistHE<<"); "<<endl;
	double NewS1 = S0j, NewS2 = S0i, NewE1 = E0j, NewE2 = E0i,temp=0;bool flag = false;
	if(DistHE<DistHS){
		//rho_new = E0i*cos(Teta)+ E0j*sin(Teta);

		NewS1 = E0j;NewS2 = E0i;
		NewE1 = S0j;NewE2 = S0i;
		temp = DistHS;
		DistHS = DistHE;
		DistHE = temp;
	}
	int VoteCount = MainSeg.Votes;
	for(int i=0;i<MergeSeg.size(); ++i){
		double Sj = Segments[MergeSeg[i]].StartPt[0],Si = Segments[MergeSeg[i]].StartPt[1];
		double Ej = Segments[MergeSeg[i]].EndPt[0],Ei = Segments[MergeSeg[i]].EndPt[1];
		DistMPS = abs(cos(Teta)*Si + sin(Teta)*Sj-MainSeg.rho_new); //perpendicular distance of point (Si,Sj) from the main line 
		DistMPE = abs(cos(Teta)*Ei + sin(Teta)*Ej-MainSeg.rho_new); //perpendicular distance of point (Ei,Ej) from the main line
		//cout<<"Merge Seg StartPt = ("<<Sj<<","<<Si<<"); ";cout<<"EndPt = ("<<Ej<<","<<Ei<<"); ";
		//cout<<"DistMPS,DistMPE = ("<<DistMPS<<","<<DistMPE<<"); ";
		double ProjS1, ProjS2, ProjE1, ProjE2;
		if(DistMPS < 6 && DistMPE < 6){
			DistMHS = sin(Teta)*Si - cos(Teta)*Sj;
			DistMHE = sin(Teta)*Ei - cos(Teta)*Ej;
			//cout<<"DistMHS,DistMHE = ("<<DistMHS<<","<<DistMHE<<"); ";
			SSbit = signbit(DistMHS-DistHS);SEbit = signbit(DistMHS-DistHE);
			ESbit = signbit(DistMHE-DistHS);EEbit = signbit(DistMHE-DistHE);
			// This condition will be true only when mgx segment does not at all intersect with the main segment 
			if(SSbit==SEbit && SEbit==ESbit && ESbit==EEbit){
				Done[MergeSeg[i]] = false;
			}
			else{
				Done[MergeSeg[i]] = true;
				MergeCount++;
				VoteCount += Segments[MergeSeg[i]].Votes;
//				Rho1 = Si*sin(WTheta) - Sj*cos(WTheta);
//				ProjS2 = cos(WTheta)*WRho + sin(WTheta)*Rho1;
//				ProjS1 = sin(WTheta)*WRho - cos(WTheta)*Rho1;
//				Rho1 = Ei*sin(WTheta) - Ej*cos(WTheta);
//				ProjE2 = cos(WTheta)*WRho + sin(WTheta)*Rho1;
//				ProjE1 = sin(WTheta)*WRho - cos(WTheta)*Rho1;
				//rho_new = E0i*cos(Teta)+ E0j*sin(Teta);
				Rho1 = Si*sin(Teta) - Sj*cos(Teta);
				ProjS2 = cos(Teta)*MainSeg.rho_new + sin(Teta)*Rho1;
				ProjS1 = sin(Teta)*MainSeg.rho_new - cos(Teta)*Rho1;
				Rho1 = Ei*sin(Teta) - Ej*cos(Teta);
				ProjE2 = cos(Teta)*MainSeg.rho_new + sin(Teta)*Rho1;
				ProjE1 = sin(Teta)*MainSeg.rho_new - cos(Teta)*Rho1;
				//if(MainSeg.rho_new == -258){
				
				//}
				//cout<<"Projections -> S:("<<ProjS1<<","<<ProjS2<<"); E:("<<ProjE1<<","<<ProjE2<<");";
				if(DistMHE<DistMHS){
					temp = DistMHS;
					DistMHS = DistMHE;
					DistMHE = temp;
					flag = true;
				}
				else{
					flag = false;
				}
				if(DistMHS<=DistHS){
					DistHS = DistMHS;
					if(flag==true){NewS1 = ProjE1;NewS2 = ProjE2;}
					else{NewS1 = ProjS1;NewS2 = ProjS2;}
				}
				if(DistMHE>=DistHE){
					DistHE = DistMHE;
					if(flag==true){NewE1 = ProjS1;NewE2 = ProjS2;}
					else{NewE1 = ProjE1;NewE2 = ProjE2;}
				}
			}
		}
		else Done[MergeSeg[i]] = false;
		//cout<<endl;
//		cout<<"StartPt = ("<<NewS1<<","<<NewS2<<"); EndPt = ("<<NewE1<<","<<NewE2<<"); Votes = "<<VoteCount<<" Updated DistHS,DistHE = ("<<DistHS<<","<<DistHE<<"); "<<endl;
	}
//	cout<<endl;
	FinalSeg.StartPt[0] =NewS1; //round(NewS1);
	FinalSeg.StartPt[1] =NewS2; //round(NewS2);
	FinalSeg.EndPt[0] =NewE1 ;//round(NewE1);
	FinalSeg.EndPt[1] =NewE2 ;//round(NewE2);
	FinalSeg.Votes = VoteCount;
	FinalSeg.RhoIndex = MainSeg.RhoIndex;
	FinalSeg.ThetaIndex = MainSeg.ThetaIndex;
	FinalSeg.SegIndex = MainSeg.SegIndex;
	FinalSeg.slop = MainSeg.slop;
	FinalSeg.rho_new = MainSeg.rho_new;
	
//	cout<<"Final Line: ";
//	cout<<"RHo,Teta = ("<<MainSeg.rho_new<<","<<Teta*180/PI<<"); ";
//	cout<<" StartPt = ("<<NewS1<<","<<NewS2<<"); EndPt = ("<<NewE1<<","<<NewE2<<"); Votes = "<<VoteCount<<endl;
//		cout<<" StartPt = ("<<(int)(NewS1)<<","<<int(NewS2)<<"); EndPt = ("<<int(NewE1)<<","<<int(NewE2)<<"); Votes = "<<VoteCount<<endl;
//	cout<<endl<<"                   /////////////PRINTING IN MergeLine END///////////////                           "<<endl<<endl;
	//waitKey(0);
}
