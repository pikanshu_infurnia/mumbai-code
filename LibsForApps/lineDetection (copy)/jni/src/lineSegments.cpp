#include "global_lineDetection.h"

#include "canny.h"
#include "hough.h"
#include "houghpeaks.h"
#include "canny_threshold.h"
int counter_for_canny = 0;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

vector <double> linespace(double Low, double High, double Resol){
	int Length = floor((High-Low)/Resol)+1;
	vector<double> vec;
	for(unsigned int i=0;i<Length;++i){
		vec.push_back(Low + i*Resol);
	}
	return vec;
}

vector<SCell> GetLineSeg(Mat Image_inp,double Theta0,int ThetaRng,int Margin,char out_str[],vector<SCell>& Segment,int Threshold,float FillGap,double RhoRes,double ThetaRes,int PERCENTILE)
{
	int Type=1,Hist = 1;
	int Rows = Image_inp.rows;
	int Cols = Image_inp.cols;
	
	Mat Image,Edges;
	int cannyThreshold;
	int LenT,LenR;
	int ratio = 2;
	int ApertureSize = 5;

	Image = Image_inp;
		
	double sum = cannyThresh(Image,PERCENTILE); 

	cannyThreshold = (int)sum/ratio; 
	CannyThreshold(Image,Edges,cannyThreshold,ratio,ApertureSize);
	//~ imwrite("debugging/canny.jpg",Edges);

								///////////////////////////CANNY END////////////////////////
	
	Mat img_rgb(Edges.size(), CV_8UC3);
	cvtColor(Image, img_rgb, CV_GRAY2RGB);

								////////////////Preparing Variables For Haugh/////////////////

	vector<double> ThetaRange(2);
	ThetaRange[0] = Theta0 - ThetaRng;
	ThetaRange[1] = Theta0 + ThetaRng;
//	cout << "Edges.size() : "<<Edges.size() <<"  "<<endl;
	vector<double> Theta = linespace((floor(ThetaRange[0]-abs(Margin))),(ceil(ThetaRange[1]+abs(Margin))),ThetaRes);
//	cout << "Edges.size() : "<<Edges.size() <<"  "<<endl;
	double D = sqrt(Rows*Rows + Cols*Cols);
	int q = ceil(D/RhoRes);
	vector<double> Rho = linespace(-q*RhoRes,q*RhoRes,RhoRes);
//	cout << "Edges.size() : "<<Edges.size() <<"  "<<endl;
	LenT = Theta.size(),LenR = Rho.size();
	
//	cout<<"Rho = ["<<Rho[0]<<Rho[1]<<Rho[2]<<"..."<<Rho[Rho.size()-1]<<"]"<<LenR<<endl;
//	cout<<"Theta = ["<<Theta[0]<<Theta[1]<<Theta[2]<<"..."<<Theta[Theta.size()-1]<<"]"<<LenT<<endl;

	Mat H = Mat::zeros(LenR,LenT,DataType<int>::type);
	int CountSeg = 0;
	Edges = Edges > 128;
//	imshow("Edges1",Edges);waitKey(0);
								////////////////Preparing Variables For Haugh END/////////////////

	Segment = hough(Edges,Rho,Theta,FillGap,Threshold,H,CountSeg,Margin);
//	PlotSeg(Segment,argv,"Segments",0,0,255);
	
	double maxVal;
	double minVal;
	Point minLoc, maxLoc;
	minMaxLoc(H,&minVal,&maxVal);
	
	vector<SCell> FinalSegments = houghpeaks(H,Rho,Theta,Segment,CountSeg,img_rgb,out_str,Threshold,RhoRes);
//	PlotSeg(Segment,argv,"Segment",0,255,0);
//	cout<<"Reached End Of GetLineSeg:";
	H.release();
	img_rgb.release();
	return FinalSegments;
}

SCell joinSeg(vector<SCell>& Proj_Seg){
	double Temp_StartPt[2];
	double Temp_EndPt[2];
	int Votes = 0;
	double max=-9999,min=9999;
	double Rho1; double S1,S2,E1,E2;
	double Teta = Proj_Seg[0].slop;
	SCell LineSeg;
	for(int i = 0; i < Proj_Seg.size(); i++){
		//cout<<"\n"<<i;
		S1 = Proj_Seg[i].StartPt[0];	S2 = Proj_Seg[i].StartPt[1];
		E1 = Proj_Seg[i].EndPt[0];		E2 = Proj_Seg[i].EndPt[1];
		Rho1 = S2*sin(Teta) - S1*cos(Teta);
		//cout<<"  "<<Rho1<<"  "<<min<<"  "<<max;
		if(Rho1<min){ min = Rho1; Temp_StartPt[0] = S1; Temp_StartPt[1] = S2;}
		if(Rho1>max){ max = Rho1; Temp_EndPt[0] = S1; Temp_EndPt[1] = S2;}
		Rho1 = E2*sin(Teta) - E1*cos(Teta);
		//cout<<"  "<<Rho1<<"  "<<min<<"  "<<max;
		if(Rho1<min){ min = Rho1; Temp_StartPt[0] = E1; Temp_StartPt[1] = E2;}
		if(Rho1>max){ max = Rho1; Temp_EndPt[0] = E1; Temp_EndPt[1] = E2;}
		Votes += Proj_Seg[i].Votes;
		LineSeg.RhoIndex = Proj_Seg[i].RhoIndex;
		LineSeg.ThetaIndex = Proj_Seg[i].ThetaIndex;
		LineSeg.rho_new = Proj_Seg[i].rho_new;
		LineSeg.SegIndex = Proj_Seg[i].SegIndex;
	}
	//cout<<endl;
	LineSeg.StartPt[0] =Temp_StartPt[0]; //round(NewS1);
	LineSeg.StartPt[1] =Temp_StartPt[1]; //round(NewS2);
	LineSeg.EndPt[0] =Temp_EndPt[0];//round(NewE1);
	LineSeg.EndPt[1] =Temp_EndPt[1];//round(NewE2);
	LineSeg.Votes =Votes;
	LineSeg.slop = Teta;
	return LineSeg;
	
}

// Theta0 and Theta_Margin in Degrees
vector<SCell> merge_in_range(vector<SCell>& FinalSegments,double Theta0,double Rho0,double Theta_Margin,double Rho_Margin,vector<int>& done_horizontal){
	double Teta = Theta0*PI/180; Theta_Margin *= PI/180; 
	double Rho1; double S1,S2,E1,E2,M1,M2,ProjE1,ProjE2,ProjS1,ProjS2,dist;
	vector<SCell> Proj_Seg;
	SCell line;
	int j = 0;
	double temp_slop;
	//cout<<endl<<"////////////////////////////// Printing in merge_in_range////////////////////"<<endl;
	for(int i = 0; i < FinalSegments.size();i++){
		//if(FinalSegments[i].slop < -1) {FinalSegments[i].slop += PI;FinalSegments[i].rho_new = -FinalSegments[i].rho_new;} 
		if((FinalSegments[i].slop <= Teta+abs(Theta_Margin)) && (FinalSegments[i].slop > Teta-abs(Theta_Margin)) && done_horizontal[i] == 0){
			S1 = FinalSegments[i].StartPt[0];	S2 = FinalSegments[i].StartPt[1];
			E1 = FinalSegments[i].EndPt[0];		E2 = FinalSegments[i].EndPt[1];
			M1 = (S1+E1)/2; M2 = (S2+E2)/2;
			dist = abs(Rho0 - (M2*cos(Teta)+M1*sin(Teta)));
			//~ //cout<<"\n"<< i << " " << Rho0 << " " << M2*cos(Teta)+M1*sin(Teta) << endl;
			//~ printf("\n Dist %d : %f ",i,dist);
			if((dist <= abs(Rho_Margin))){
				done_horizontal[i] = 1;
				S1 = FinalSegments[i].StartPt[0];	S2 = FinalSegments[i].StartPt[1];
				E1 = FinalSegments[i].EndPt[0];		E2 = FinalSegments[i].EndPt[1];
				Rho1 = S2*sin(Teta) - S1*cos(Teta);
				ProjS2 = cos(Teta)*Rho0 + sin(Teta)*Rho1;
				ProjS1 = sin(Teta)*Rho0 - cos(Teta)*Rho1;
				Rho1 = E2*sin(Teta) - E1*cos(Teta);
				ProjE2 = cos(Teta)*Rho0 + sin(Teta)*Rho1;
				ProjE1 = sin(Teta)*Rho0 - cos(Teta)*Rho1;
				line.StartPt[0] =ProjS1; //round(NewS1);
				line.StartPt[1] =ProjS2; //round(NewS2);
				line.EndPt[0] =ProjE1 ;//round(NewE1);
				line.EndPt[1] =ProjE2 ;//round(NewE2);
				line.Votes = FinalSegments[i].Votes;
				line.RhoIndex = FinalSegments[i].RhoIndex;
				line.ThetaIndex = FinalSegments[i].ThetaIndex;
				line.SegIndex = FinalSegments[i].SegIndex;
				line.slop = Teta;
				line.rho_new = Rho0;
				Proj_Seg.push_back(line);
				j++;
			}
		}
	}
	return Proj_Seg;
}


