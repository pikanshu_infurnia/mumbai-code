#include "global_lineDetection.h"
#include "sort.h"



double cannyThresh(Mat Image,int PERCENTILE){
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;
	Mat grad_x, grad_y,Edges,grad;
	Mat abs_grad_x, abs_grad_y;
//	Image.copyTo(Edges);
	GaussianBlur(Image,Edges, Size(5,5),sqrt(2));

  /// Gradient X
	Sobel(Edges, grad_x, ddepth, 1, 0, 5, scale, delta, cv::BORDER_REPLICATE );
  /// Gradient Y
	Sobel( Edges, grad_y, ddepth, 0, 1, 5, scale, delta, cv::BORDER_REPLICATE );

	grad_x.convertTo(abs_grad_x, CV_32F);
	grad_y.convertTo(abs_grad_y, CV_32F);

	grad = Mat(abs_grad_x.size(), abs_grad_x.type());
	double sum = 0;
	double max = 0;
	int index_col = ceil((abs_grad_x.cols-1)*PERCENTILE/100); //cout<<"INDEX_Col : "<<index_col<<endl;
	int index_row = ceil((abs_grad_x.rows-1)*PERCENTILE/100); //cout<<"INDEX_Row : "<<index_row<<endl;

	vector<double> grad_vec_col(abs_grad_x.cols);
	vector<double> grad_vec_row(abs_grad_x.rows);

	for (int i = 0; i < (abs_grad_x.rows) ; i++){
		for (int j = 0; j < (abs_grad_x.cols) ; j++){
			grad.at<float>(i,j) = abs(abs_grad_x.at<float>(i,j))+abs(abs_grad_y.at<float>(i,j));
			grad_vec_col[j]=(double) grad.at<float>(i,j); 
//			grad_vec_col[j]=abs_grad_x.at<double>(i,j)*abs_grad_x.at<double>(i,j)+abs_grad_y.at<double>(i,j)*abs_grad_y.at<double>(i,j);
		}
		quickSortNum(grad_vec_col,abs_grad_x.cols);
		if(max < grad_vec_col[0]) max = grad_vec_col[0];
		grad_vec_row[i] = grad_vec_col[index_col];
//		cout<<grad_vec_row[i]<<" ";
		//sum = ((sum*(i-1)) + grad_vec[index])/i;
	}
	quickSortNum(grad_vec_row,abs_grad_x.rows);
	printf("\n calculating threshold for canny : \n");
	//~ for(int i=0;i<abs_grad_x.rows;i++){ cout<<grad_vec_row[i]<<" "; }
	int counter = 0;
	int count = 1;
	for(int i = 0; i < abs_grad_x.rows-1;i++){
		if(grad_vec_row[i] == grad_vec_row[i+1]){
			counter++;
		}
		else{
			//~ cout << count << "." << grad_vec_row[i] << " ----- " << counter+1<<endl;
			counter = 0;
			count++;
		}
	} 
	//~ cout<< endl;
	sum = grad_vec_row[index_row];
	cout << "\nSum : "<<sum <<"  "<< "index_row : "<< abs_grad_x.rows << " "<<index_row << endl;//"  "<<"AVG : "<< sum/counter<<endl;
	
	int histSize = round(max)/5+1;
	
	float range[] = { 0,(float)round(max)  } ; //the upper boundary is exclusive
	const float* histRange = { range };
	bool uniform = true; bool accumulate = false;
	
	Mat hist;
	calcHist( &grad, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );
	int hist_w = histSize; int hist_h = histSize;
	int bin_w = cvRound( (double) hist_w/histSize );

	
	Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );
	
	normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
	
	float gaussianKernel[] = {0.006, 0.061, 0.242, 0.383, 0.242, 0.061, 0.006};
	//~ int bin_w = 1;
	/// Draw for each channel
	
	//~ cout << "\n Plotting points Histogram : \n";
	for( int i = 1; i < histSize; i++ )
	{
		line( histImage, Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ) ,
						 Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
						 Scalar( 255, 0, 0), 2, 8, 0  );
		//~ printf("(%d,%d) (%d,%d) \n",bin_w*(i-1),hist_h - cvRound(hist.at<float>(i-1)),bin_w*(i), hist_h - cvRound(hist.at<float>(i)));
	}
	
	Mat smoothHist = Mat(hist.size(),hist.type());
	
	for(int i = 0; i < histSize; i++){
		if(i < 6 || i > histSize - 6) smoothHist.at<float>(i) = hist.at<float>(i);
		else{
			smoothHist.at<float>(i) = hist.at<float>(i+0)*gaussianKernel[0] + hist.at<float>(i+1)*gaussianKernel[1] + hist.at<float>(i+2)*gaussianKernel[2] +hist.at<float>(i+3)*gaussianKernel[3] + hist.at<float>(i+4)*gaussianKernel[4] +hist.at<float>(i+5)*gaussianKernel[5] + hist.at<float>(i+6)*gaussianKernel[6];
		}
	}

	for( int i = 1; i < histSize; i++ )
	{
		line( histImage, Point( bin_w*(i-1), hist_h - cvRound(smoothHist.at<float>(i-1)) ) ,
						 Point( bin_w*(i), hist_h - cvRound(smoothHist.at<float>(i)) ),
						 Scalar( 0, 255, 0), 2, 8, 0  );
	}

	vector<float> slope(histSize-2);
	float error = 999;
	float threshold;
	//~ cout<<"\n slopes \n :";
	for(int i = 1; i < histSize-1; i++){
		slope[i-1] = (smoothHist.at<float>(i+1) - smoothHist.at<float>(i-1))/2;
		//~ cout<< slope[i-1] << " \n";
	}

	if(slope[round(sum/5)] < -1){
		for(int i = round(sum/5)-1 ; i < histSize-1; i++){
			if(slope[i-1] < -1 && slope[i] > -1){
				threshold = (i+0.5)*5;
				break;
			}
		}
	}
	else if(slope[round(sum/5)] > -1){
		for(int i = round(sum/5)+1 ; i > 2; i--){
			if(slope[i-1] < -1 && slope[i] > -1){
				threshold = (i+0.5)*5;
				break;
			}
		}
	}
	//~ namedWindow("calcHist Demo", CV_WINDOW_NORMAL );
	//~ imshow("calcHist Demo", histImage );
	//~ imwrite("debugging/hist.jpg",histImage);

	//~ waitKey(0);

	hist.release();
	histImage.release();
	smoothHist.release();
	grad_x.release();
	grad_y.release();
	abs_grad_x.release();
	abs_grad_y.release();
	Edges.release();
	return (double)threshold;
	//~ return sum;
}
