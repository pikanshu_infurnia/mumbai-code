#include <stdio.h>
#include <math.h>

void new_marker(double marker[3][2]){

	int i;
	double x,y;
/*	x = 960/2; y  = 720/2;*/
/*	x = 500 ; y = 500;*/
	x = 0;y = 0;
	for( i = 0 ; i < 3; i++){
		marker[i][0] = marker[i][0] - x;
		marker[i][1] = marker[i][1] - y;
	}

	return;
}

int main(){
		printf("\n@@@@@@@@@@@@@@@@@@@@@@@@@        START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	double tmp[4],phi,theta;
	double X[3],Y[3],Z[3];
	double x[3],y[3];
	double a1,a2,b1,b2,l1,l2,m1,m2,C,N;
	double alpha[3],beta[3];
	double c[3],s[3];
	double p,b;
	double h = 85.72,w = 53.98;
/*	double D[3][2] ={{w/2,-h/2},{-w/2,-h/2},{-w/2,h/2}};*/
	double D[3][2] ={{-w/2,h/2},{w/2,h/2},{w/2,-h/2}};
	double focal = 750;
	double rotMat[3][3];
	int i,j,k;
	
/*	double z[3] = {-0.055675,-0.554732,-0.830164};*/
/*	double z[3] = {0.044528,-0.497368,-0.866396};*/
	double z[3] = {-0.032421,-0.612784,-0.789585};
/*	double marker[3][2] = {{498.151580,422.302318},{519.091958,438.997736},{491.760590,470.468214}};*/
	double marker[3][2] = {{463.885786,380.375660},{532.087324,379.118211},{533.161559,474.987764}};
	double psi[2];
	double psi1;

	//////////////////////////////////// calculating theta & phi //////////////////////

	new_marker(marker);
	tmp[0] = z[2]; 
	theta = acos(z[2]);
	phi = atan(-z[0]/z[1]);
	
	///////////////////////////////////////////////////////////////////////////////////
	
	printf("theta  = %3.5f phi = %3.5f \n", theta*180/M_PI,phi*180/M_PI);
	
	tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

	a1 = tmp[0];	 	a2 = tmp[2]*tmp[1];
	b1 = tmp[2]*tmp[1];	 b2 = tmp[0];
	l1 = tmp[1]; 		l2 =tmp[2]*tmp[0];
	m1 = tmp[2]*tmp[0];	m2 = tmp[1];
	C = tmp[1]*tmp[3];	N = -tmp[3]*tmp[0];
	
	for( i = 0 ; i < 3; i++){
		printf("\n%3.5f %3.5f",marker[i][0],marker[i][1]);
	}
	
	printf("\n");

	
	for(i = 0 ; i < 3; i++){
		alpha[i] = 1/marker[i][0];
		beta[i] = 1/marker[i][1];
		
		c[i] = a1*D[i][0]*alpha[i] + l1*D[i][1]*alpha[i] + b1*D[i][0]*beta[i] - m1*D[i][1]*beta[i];
		s[i] = -a2*D[i][0]*alpha[i] + l2*D[i][1]*alpha[i] + b2*D[i][0]*beta[i] + m2*D[i][1]*beta[i];
	}

	p = (c[1-1]*alpha[2-1] - c[2-1]*alpha[1-1])*(beta[1-1]*alpha[3-1] - beta[3-1]*alpha[1-1]) - (c[1-1]*alpha[3-1] - c[3-1]*alpha[1-1])*(beta[1-1]*alpha[2-1] - beta[2-1]*alpha[1-1]);
	
	b = (s[1-1]*alpha[2-1] - s[2-1]*alpha[1-1])*(beta[1-1]*alpha[3-1] - beta[3-1]*alpha[1-1]) - (s[1-1]*alpha[3-1] - s[3-1]*alpha[1-1])*(beta[1-1]*alpha[2-1] - beta[2-1]*alpha[1-1]);

	psi[0] = atan2(p,-b);
	psi[1] = M_PI + psi[0];
/*	printf(" \nerror : %f \n",cos(psi[1])*p+sin(psi[1])*b);*/
/*	printf("\n coefficients : %.30f %.30f %.30f\n",p,-b,p/-b);*/
/*	printf("\n angles : %f %f\n",psi[0]*180/M_PI,psi[1]*180/M_PI);*/
	
	for(i = 0 ; i < 2; i++){
		printf("\n@@@@@@@@@@@@@@@@@@@@@@@@@        START           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");

		psi1 = psi[i];
		printf(" \n angle psi = %f \n",psi1*180/M_PI);

		rotMat[0][0] = cos(psi1)*a1 - sin(psi1)*a2;
		rotMat[0][1] = cos(psi1)*l1 + sin(psi1)*l2;
		rotMat[0][2] = sin(psi1)*sin(theta);

		rotMat[1][0] = -cos(psi1)*b1 - sin(psi1)*b2;
		rotMat[1][1] = cos(psi1)*m1 - sin(psi1)*m2;
		rotMat[1][2] = cos(psi1)*sin(theta);

		rotMat[2][0] = C;
		rotMat[2][1] = N;
		rotMat[2][2] = z[2];

		tmp[2-1] = (cos(psi1)*(c[1-1]*alpha[3-1] - c[3-1]*alpha[1-1]) + sin(psi1)*(s[1-1]*alpha[3-1] - s[3-1]*alpha[1-1]))/((beta[1-1]*alpha[3-1] - beta[3-1]*alpha[1-1]));
		printf("\n %f",tmp[1]);
		tmp[2-1] = (cos(psi1)*(c[1-1]*alpha[2-1] - c[2-1]*alpha[1-1]) + sin(psi1)*(s[1-1]*alpha[2-1] - s[2-1]*alpha[1-1]))/((beta[1-1]*alpha[2-1] - beta[2-1]*alpha[1-1]));
		printf(" %f\n",tmp[1]);
		
		tmp[1-1] = (tmp[2-1]*beta[1-1] - cos(psi1)*c[1-1] - sin(psi1)*s[1-1])/alpha[1-1];
		printf("\n %f",tmp[0]);
		tmp[1-1] = (tmp[2-1]*beta[2-1] - cos(psi1)*c[2-1] - sin(psi1)*s[2-1])/alpha[2-1];
		printf(" %f",tmp[0]);
		tmp[1-1] = (tmp[2-1]*beta[3-1] - cos(psi1)*c[3-1] - sin(psi1)*s[3-1])/alpha[3-1];
		printf(" %f\n",tmp[0]);

		Z[0] =((cos(psi1)*a1 - sin(psi1)*a2)*D[0][0] + (cos(psi1)*l1 + sin(psi1)*l2)*D[0][1] + tmp[1-1])*alpha[1-1]*focal;
		printf("\n %f",Z[0]);
		Z[0] =((-cos(psi1)*b1 - sin(psi1)*b2)*D[0][0] + (cos(psi1)*m1 - sin(psi1)*m2)*D[0][1] + tmp[2-1])*beta[1-1]*focal;
		printf(" %f\n",Z[0]);

		Z[1] =((cos(psi1)*a1 - sin(psi1)*a2)*D[1][0] + (cos(psi1)*l1 + sin(psi1)*l2)*D[1][1] + tmp[1-1])*alpha[2-1]*focal;
		printf("\n %f",Z[1]);
		Z[1] =((-cos(psi1)*b1 - sin(psi1)*b2)*D[1][0] + (cos(psi1)*m1 - sin(psi1)*m2)*D[1][1] + tmp[2-1])*beta[2-1]*focal;
		printf(" %f\n",Z[1]);

		Z[2] =((cos(psi1)*a1 - sin(psi1)*a2)*D[2][0] + (cos(psi1)*l1 + sin(psi1)*l2)*D[2][1] + tmp[1-1])*alpha[3-1]*focal;
		printf("\n %f",Z[2]);
		Z[2] =((-cos(psi1)*b1 - sin(psi1)*b2)*D[2][0] + (cos(psi1)*m1 - sin(psi1)*m2)*D[2][1] + tmp[2-1])*beta[3-1]*focal;
		printf(" %f\n",Z[2]);

		printf("\nPREVIOUS :  Zs Z[0] : %f Z[1] : %f Z[2] : %f ",Z[0],Z[1],Z[2]);
		tmp[3-1] = Z[0] - C*D[0][0] - N*D[0][1];
		printf("\nT3 :  %f",tmp[2]);
		tmp[3-1] = Z[1] - C*D[1][0] - N*D[1][1];
		printf(" %f",tmp[2]);
		tmp[3-1] = Z[2] - C*D[2][0] - N*D[2][1];
		printf(" %f\n",tmp[2]);
		printf("\n translation :%5.5f %5.5f %5.5f\n",tmp[0],tmp[1],tmp[2]);
		
/*		void error(double psi1,double c,double n,double co,double si,double t1,double t3,double focal,double x1,double x,double y)*/
/*		error(psi11,c,n,co[0][0],si[0][0],tmp[0],tmp[2],focal,x1,D[0][0],D[0][1]);*/
/*		X[0] = rotMat[0][0]*D[0][0]+rotMat[0][1]*D[0][1]+tmp[0];*/
/*		Y[0] = rotMat[1][0]*D[0][0]+rotMat[1][1]*D[0][1]+tmp[1];*/
/*		Z[0] = rotMat[2][0]*D[0][0]+rotMat[2][1]*D[0][1]+tmp[2];*/

/*		x[0] = focal*X[0]/Z[0]; y[0] = focal*Y[0]/Z[0];*/
/*		tmp[3-1] = Z[1] - C*D[1][0] - N*D[1][1];*/
/*		X[1] = rotMat[0][0]*D[1][0]+rotMat[0][1]*D[1][1]+tmp[0];*/
/*		Y[1] = rotMat[1][0]*D[1][0]+rotMat[1][1]*D[1][1]+tmp[1];*/
/*		Z[1] = rotMat[2][0]*D[1][0]+rotMat[2][1]*D[1][1]+tmp[2];*/
/*		tmp[3-1] = Z[2] - C*D[2][0] - N*D[2][1];*/
/*		x[1] = focal*X[1]/Z[1]; y[1] = focal*Y[1]/Z[1];*/
/*		X[2] = rotMat[0][0]*D[2][0]+rotMat[0][1]*D[2][1]+tmp[0];*/
/*		Y[2] = rotMat[1][0]*D[2][0]+rotMat[1][1]*D[2][1]+tmp[1];*/
/*		Z[2] = rotMat[2][0]*D[2][0]+rotMat[2][1]*D[2][1]+tmp[2];*/
/*		x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];*/

/*		printf("\nPRESENT :  Z[0] : %f Z[1] : %f Z[2] : %f \n",Z[0],Z[1],Z[2]);*/
/*		*/
/*		*/
/*		printf("\n projected coordinates :\n%3.5f %3.5f",x[0],y[0]);*/
/*		printf("\n%3.5f %3.5f",x[1],y[1]);*/
/*		printf("\n%3.5f %3.5f\n",x[2],y[2]);*/
/*		*/
/*		printf(" ROTATION MATRIX : \n");*/
/*		for( k = 0 ; k < 3 ; k++){*/
/*			for( j = 0 ; j < 3 ; j++){*/
/*				printf( "%3.5f ", rotMat[j][k]);*/
/*			}*/
/*			printf("\n");*/
/*		}*/
/*		printf("\ncheck %f %f",rotMat[0][0]*D[0][0]+rotMat[0][1]*D[0][1]+tmp[0],(cos(psi1)*a1 - sin(psi1)*a2)*D[0][0] + (cos(psi1)*l1 + sin(psi1)*l2)*D[0][1] + tmp[1-1]);*/
		printf("\n@@@@@@@@@@@@@@@@@@@@@@@           END                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	}

	
	return 0;

}
