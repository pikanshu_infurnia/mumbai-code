#include <stdio.h>
#include <math.h>

void new_marker(double marker[3][2]){

	int i;
	double x,y;
	x = 960/2; y  = 720/2;
	for( i = 0 ; i < 3; i++){
		marker[i][0] = marker[i][0] - x;
		marker[i][1] = marker[i][1] - y;
	}

	return;
}
void error(double psi,double c,double n,double co,double si,double t1,double t3,double focal,double x1,double x,double y){
	double error = cos(psi)*focal*co - sin(psi)*focal*si + t1*focal/x1 - (c*x+n*y) - t3;
	printf("\nerror : %f\n",error);
}

int main(){

	double tmp[4],phi,theta;
	double X[3],Y[3],Z[3];
	double x[3],y[3];
	double a1,a2,b1,b2,c,l1,l2,m1,m2,n;
	
	double co[3][2],si[3][2];
	double x1,y1,x2,y2,x3,y3;
	double tmpAngle[2],a[2],b[2],C[2],psi[4];
	double h = 85.72,w = 53.98;
	double alpha1minusalpha3, alpha1minusalpha5, beta2minusbeta4, beta2minusbeta6;
/*	double z[3] = {-0.06020, -0.37965, -0.92317 };{-0.067370,-0.424061,-0.903125};*/
/*	double z[3] = {0.052064 ,-0.588031 ,-0.807161};*/
	double z[3] = {-0.055675,-0.554732,-0.830164};
/*	double z[3] = {-0.07049, -0.39176 ,-0.91736  },{-0.052484,-0.453245,-0.889840};*/
/*	double marker[3][2] = {{473.633298,274.572512},{562.138308,265.908607},{577.928808,400.232025}};*/
/*	double marker[3][2] = {{483.288178 ,309.381140},{517.206093,328.250143},{481.823465,373.158143}};*/
	double marker[3][2] = {{498.151580,422.302318},{519.091958,438.997736},{491.760590,470.468214}};
	double marker3D[3][2] ={{w/2,-h/2},{-w/2,-h/2},{-w/2,h/2}}; 
/*	double marker[3][2] = {{470.544226,277.634915},{553.385153,273.611707},{562.270331,397.409823 },{473.244945,401.429645}};*/
	double focal = 750;
	double rotMat[3][3];
	int i,j;
	double psi1;
	//////////////////////// calculating theta /////////////////////
	
	new_marker(marker);
	tmp[0] = z[2]; 
	theta = acos(z[2]);
	phi = atan(-z[0]/z[1]);
	
/*	phi = atan(fabs(z[0])/fabs(z[1]));*/
/*	printf("theta  = %3.5f phi = %3.5f %3.5f \n" ,fabs(z[1]),fabs(z[0]) ,fabs(z[1])/fabs(z[0]));*/
/*	printf("theta  = %3.5f phi = %3.5f \n" , theta,phi);*/
/*	if(z[0] > 0 && z[1] > 0) phi = M_PI - phi;*/
/*	else if(z[0] < 0 && z[1] > 0 ) phi = -M_PI + phi;*/
/*	else if(z[0] < 0 && z[1] < 0 ) phi = -phi;*/

	printf("theta  = %3.5f phi = %3.5f \n", theta*180/M_PI,phi*180/M_PI);
	
	tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

/*	a1 = tmp[0]; a2 = -tmp[2]*tmp[1];*/
/*	b1 = -tmp[2]*tmp[1]; b2 = -tmp[0];*/
/*	l1 = tmp[1]; l2 =tmp[2]*tmp[0];*/
/*	m1 = tmp[2]*tmp[0];m2 = -tmp[1];*/
/*	c = tmp[1]*tmp[3];n = -tmp[3]*tmp[0];*/

/*	x1 = marker[0][0]; y1 = marker[0][1];*/
/*	x2 = marker[1][0]; y2 = marker[1][1];*/
/*	x3 = marker[2][0]; y3 = marker[2][1];*/

/*	co[0][0] = a1*marker3D[0][0]/x1 + marker3D[0][1]*l1/x1; co[0][1] = m1*marker3D[0][1]/y1 - b1*marker3D[0][0]/y1;*/
/*	si[0][0] = a2*marker3D[0][0]/x1 - marker3D[0][1]*l2/x1; si[0][1] = m2*marker3D[0][1]/y1 + b2*marker3D[0][0]/y1;*/

/*	co[1][0] = a1*marker3D[1][0]/x2 + marker3D[1][1]*l1/x2; co[1][1] = m1*marker3D[1][1]/y2 - b1*marker3D[1][0]/y2;*/
/*	si[1][0] = a2*marker3D[1][0]/x2 - marker3D[1][1]*l2/x2; si[1][1] = m2*marker3D[1][1]/y2 + b2*marker3D[1][0]/y2;*/

/*	co[2][0] = a1*marker3D[2][0]/x3 + marker3D[2][1]*l1/x3; co[2][1] = m1*marker3D[2][1]/y3 - b1*marker3D[2][0]/y3;*/
/*	si[2][0] = a2*marker3D[2][0]/x3 - marker3D[2][1]*l2/x3; si[2][1] = m2*marker3D[2][1]/y3 + b2*marker3D[2][0]/y3;*/

/*//////////////////////// final coefficients of a*cos - b* sin = C*/

/*	alpha1minusalpha3 = (1/x1 - 1/x2); alpha1minusalpha5 = (1/x1 - 1/x3);*/
/*	beta2minusbeta4 = (1/y1 - 1/y2); beta2minusbeta6 = (1/y1 - 1/y3);*/

/*	a[0] = (co[0][0] - co[1][0])*alpha1minusalpha5 - (co[0][0] - co[3][0])*alpha1minusalpha3;*/
/*	b[0] = (si[0][0] - si[1][0])*alpha1minusalpha5 - (si[0][0] - si[3][0])*alpha1minusalpha3;*/
/*	C[0] = (c*((marker3D[0][0] - marker3D[1][0])*alpha1minusalpha5 - (marker3D[0][0] - marker3D[2][0])*alpha1minusalpha3)) + (n*((marker3D[0][1] - marker3D[1][1])*alpha1minusalpha5 - (marker3D[0][1] - marker3D[2][1])*alpha1minusalpha3));*/

/*	a[1] = (co[0][1] - co[1][1])*beta2minusbeta6 - (co[0][1] - co[3][1])*beta2minusbeta4;*/
/*	b[1] = (si[0][1] - si[1][1])*beta2minusbeta6 - (si[0][1] - si[3][1])*beta2minusbeta4;*/
/*	C[1] = (c*((marker3D[0][0] - marker3D[1][0])*beta2minusbeta6 - (marker3D[0][0] - marker3D[2][0])*beta2minusbeta4)) + (n*((marker3D[0][1] - marker3D[1][1])*beta2minusbeta6 - (marker3D[0][1] - marker3D[2][1])*beta2minusbeta4));*/

/*	tmp[0] = sqrt(a[0]*a[0]+b[0]*b[0]);*/
/*	tmp[1] = sqrt(a[1]*a[1]+b[1]*b[1]);*/

/*	C[0] = tmp[0]*C[0]/focal;*/
/*	C[1] = tmp[1]*C[1]/focal;*/
/*	*/
/*	*/
/*	tmpAngle[0] = asin(fabs(a[0])/tmp[0]);	tmpAngle[1] = asin(fabs(a[1])/tmp[1]);*/
/*	*/
/*	b[0] = -b[0]; b[1] = -b[1];*/
/*	*/
/*	if(a[0] > 0 && b[0] < 0) tmpAngle[0] = M_PI - tmpAngle[0];*/
/*	else if(a[0] < 0 && b[0] < 0 ) tmpAngle[0] = -M_PI + tmpAngle[0];*/
/*	else if(a[0] < 0 && b[0] > 0 ) tmpAngle[0] = -tmpAngle[0];*/

/*	if(a[1] > 0 && b[1] < 0) tmpAngle[1] = M_PI - tmpAngle[1];*/
/*	else if(a[1] < 0 && b[1] < 0 ) tmpAngle[1] = -M_PI + tmpAngle[1];*/
/*	else if(a[1] < 0 && b[1] > 0 ) tmpAngle[1] = -tmpAngle[1];*/

/*	psi[0] = asin(C[0]);*/
/*	psi[2] = asin(C[1]);*/

/*	printf("psi[0]= %3.5f psi[2] = %3.5f\n", psi[0]*180/M_PI,psi[2]*180/M_PI);*/
/*	printf("psitmpAngle[0]= %3.5f psitmpAngle[1] = %3.5f\n", tmpAngle[0]*180/M_PI,tmpAngle[1]*180/M_PI);*/

/*	if(psi[0] > 0) psi[1] = M_PI - psi[0]-tmpAngle[0];*/
/*	else psi[1] = -M_PI - psi[0]-tmpAngle[0];*/

/*	if(psi[2] > 0) psi[3] = M_PI - psi[2]-tmpAngle[1];*/
/*	else psi[3] = -M_PI - psi[2]-tmpAngle[1];*/



	a1 = tmp[0];	 	a2 = tmp[2]*tmp[1];
	b1 = tmp[2]*tmp[1];	 b2 = tmp[0];
	l1 = tmp[1]; 		l2 =tmp[2]*tmp[0];
	m1 = tmp[2]*tmp[0];	m2 = tmp[1];
	c = tmp[1]*tmp[3];	n = -tmp[3]*tmp[0];

	x1 = marker[0][0]; y1 = marker[0][1];
	x2 = marker[1][0]; y2 = marker[1][1];
	x3 = marker[2][0]; y3 = marker[2][1];
	
	for( i = 0 ; i < 3; i++){
		printf("\n%3.5f %3.5f",marker[i][0],marker[i][1]);
	}
	
	printf("\n");
	co[0][0] = a1*marker3D[0][0]/x1 + marker3D[0][1]*l1/x1; co[0][1] = m1*marker3D[0][1]/y1 - b1*marker3D[0][0]/y1;
	si[0][0] = a2*marker3D[0][0]/x1 - marker3D[0][1]*l2/x1; si[0][1] = m2*marker3D[0][1]/y1 + b2*marker3D[0][0]/y1;

	co[1][0] = a1*marker3D[1][0]/x2 + marker3D[1][1]*l1/x2; co[1][1] = m1*marker3D[1][1]/y2 - b1*marker3D[1][0]/y2;
	si[1][0] = a2*marker3D[1][0]/x2 - marker3D[1][1]*l2/x2; si[1][1] = m2*marker3D[1][1]/y2 + b2*marker3D[1][0]/y2;

	co[2][0] = a1*marker3D[2][0]/x3 + marker3D[2][1]*l1/x3; co[2][1] = m1*marker3D[2][1]/y3 - b1*marker3D[2][0]/y3;
	si[2][0] = a2*marker3D[2][0]/x3 - marker3D[2][1]*l2/x3; si[2][1] = m2*marker3D[2][1]/y3 + b2*marker3D[2][0]/y3;

	//////////////////////// final coefficients of a*cos - b* sin = C

	alpha1minusalpha3 = (1/x1 - 1/x2); alpha1minusalpha5 = (1/x1 - 1/x3);
	beta2minusbeta4 = (1/y1 - 1/y2); beta2minusbeta6 = (1/y1 - 1/y3);

	a[0] = (co[0][0] - co[1][0])*alpha1minusalpha5 - (co[0][0] - co[2][0])*alpha1minusalpha3;
	b[0] = (si[0][0] - si[1][0])*alpha1minusalpha5 - (si[0][0] - si[2][0])*alpha1minusalpha3;
	C[0] = (c*((marker3D[0][0] - marker3D[1][0])*alpha1minusalpha5 - (marker3D[0][0] - marker3D[2][0])*alpha1minusalpha3)) + (n*((marker3D[0][1] - marker3D[1][1])*alpha1minusalpha5 - (marker3D[0][1] - marker3D[2][1])*alpha1minusalpha3));

	a[1] = (co[0][1] - co[1][1])*beta2minusbeta6 - (co[0][1] - co[2][1])*beta2minusbeta4;
	b[1] = (si[0][1] - si[1][1])*beta2minusbeta6 - (si[0][1] - si[2][1])*beta2minusbeta4;
	C[1] = (c*((marker3D[0][0] - marker3D[1][0])*beta2minusbeta6 - (marker3D[0][0] - marker3D[2][0])*beta2minusbeta4)) + (n*((marker3D[0][1] - marker3D[1][1])*beta2minusbeta6 - (marker3D[0][1] - marker3D[2][1])*beta2minusbeta4));

	tmp[0] = sqrt(a[0]*a[0]+b[0]*b[0]);
	tmp[1] = sqrt(a[1]*a[1]+b[1]*b[1]);

	C[0] = tmp[0]*C[0]/focal;
	C[1] = tmp[1]*C[1]/focal;


	tmpAngle[0] = asin(fabs(a[0])/tmp[0]);	tmpAngle[1] = asin(fabs(a[1])/tmp[1]);

	b[0] = -b[0]; b[1] = -b[1];

	if(a[0] > 0 && b[0] < 0) tmpAngle[0] = M_PI - tmpAngle[0];
	else if(a[0] < 0 && b[0] < 0 ) tmpAngle[0] = -M_PI + tmpAngle[0];
	else if(a[0] < 0 && b[0] > 0 ) tmpAngle[0] = -tmpAngle[0];

	if(a[1] > 0 && b[1] < 0) tmpAngle[1] = M_PI - tmpAngle[1];
	else if(a[1] < 0 && b[1] < 0 ) tmpAngle[1] = -M_PI + tmpAngle[1];
	else if(a[1] < 0 && b[1] > 0 ) tmpAngle[1] = -tmpAngle[1];

	psi[0] = asin(C[0]);
	psi[2] = asin(C[1]);

	printf("psi[0]= %3.5f psi[2] = %3.5f\n", psi[0]*180/M_PI,psi[2]*180/M_PI);
	printf("psitmpAngle[0]= %3.5f psitmpAngle[1] = %3.5f\n", tmpAngle[0]*180/M_PI,tmpAngle[1]*180/M_PI);
	
	psi[0] -=tmpAngle[0];
	psi[1] -=tmpAngle[1]; 
	if(psi[0] > 0) psi[1] = M_PI - psi[0]-tmpAngle[0];
	else psi[1] = -M_PI - psi[0]-tmpAngle[0];

	if(psi[2] > 0) psi[3] = M_PI - psi[2]-tmpAngle[1];
	else psi[3] = -M_PI - psi[2]-tmpAngle[1];

	printf("psi[0]  = %3.5f psi[1] = %3.5f psi[2] = %3.5f psi[3] = %3.5f \n" , psi[0]*180/M_PI,psi[1]*180/M_PI,psi[2]*180/M_PI,psi[3]*180/M_PI);

	
	
	psi1 = psi[3] ;
/*	rotMat[0][0] = cos(psi1)*cos(phi)-cos(theta)*sin(phi)*sin(psi1);*/
/*	rotMat[0][1] = cos(psi1)*sin(phi)+cos(theta)*cos(phi)*sin(psi1);*/
/*	rotMat[0][2] = sin(psi1)*sin(theta);*/
/*	rotMat[1][0] = -sin(psi1)*cos(phi)-cos(theta)*sin(phi)*cos(psi1);*/
/*	rotMat[1][1] = -sin(psi1)*sin(phi)+cos(theta)*cos(phi)*cos(psi1);*/

/*	rotMat[1][2] = cos(psi1)*sin(theta);*/

/*	rotMat[2][0] = sin(theta)*sin(phi);*/

/*	rotMat[2][1] = -sin(theta)*cos(phi);*/

/*	rotMat[2][2] = cos(theta);*/


	rotMat[0][0] = cos(psi1)*cos(phi)-cos(theta)*sin(phi)*sin(psi1);
	rotMat[1][0] = cos(psi1)*sin(phi)+cos(theta)*cos(phi)*sin(psi1);
	rotMat[2][0] = sin(psi1)*sin(theta);
	rotMat[0][1] = -sin(psi1)*cos(phi)-cos(theta)*sin(phi)*cos(psi1);
	rotMat[1][1] = -sin(psi1)*sin(phi)+cos(theta)*cos(phi)*cos(psi1);

	rotMat[2][1] = cos(psi1)*sin(theta);

	rotMat[0][2] = sin(theta)*sin(phi);

	rotMat[1][2] = -sin(theta)*cos(phi);

	rotMat[2][2] = cos(theta);

	for( i = 0 ; i < 3 ; i++){
		for( j = 0 ; j < 3 ; j++){
			printf( "%3.5f ", rotMat[i][j]);
		}
		printf("\n");
	}

	for(i = 0 ; i < 4; i++){
	
		psi1 = psi[i];
		rotMat[0][0] = cos(psi1)*cos(phi)-cos(theta)*sin(phi)*sin(psi1);
		rotMat[1][0] = cos(psi1)*sin(phi)+cos(theta)*cos(phi)*sin(psi1);
		rotMat[2][0] = sin(psi1)*sin(theta);
		rotMat[0][1] = -sin(psi1)*cos(phi)-cos(theta)*sin(phi)*cos(psi1);
		rotMat[1][1] = -sin(psi1)*sin(phi)+cos(theta)*cos(phi)*cos(psi1);

		rotMat[2][1] = cos(psi1)*sin(theta);

		rotMat[0][2] = sin(theta)*sin(phi);

		rotMat[1][2] = -sin(theta)*cos(phi);

		rotMat[2][2] = cos(theta);

		tmp[0] = (sin(psi1)*focal*(si[0][0] - si[1][0]) - cos(psi1)*focal*(co[0][0] - co[1][0]) + c*(marker3D[0][0] - marker3D[1][0]) + n*(marker3D[0][1] - marker3D[1][1]))/focal*alpha1minusalpha3;
		tmp[2] = sin(psi1)*focal*(si[0][0]) - cos(psi1)*focal*(co[0][0]) - tmp[0]*focal/x1 + c*(marker3D[0][0]) + n*(marker3D[0][1]);
		tmp[1] = (sin(psi1)*focal*(si[0][1]) - cos(psi1)*focal*(co[0][1]) + tmp[2] + c*(marker3D[0][0]) + n*(marker3D[0][1]))*y1/focal;
		printf("\n%5.5f %5.5f %5.5f\n",tmp[0],tmp[1],tmp[2]);
	
/*		void error(double psi,double c,double n,double co,double si,double t1,double t3,double focal,double x1,double x,double y)*/
		error(psi1,c,n,co[0][0],si[0][0],tmp[0],tmp[2],focal,x1,marker3D[0][0],marker3D[0][1]);
		X[0] = rotMat[0][0]*marker3D[0][0]+rotMat[0][1]*marker3D[0][1]+tmp[0];
		Y[0] = rotMat[1][0]*marker3D[0][0]+rotMat[1][1]*marker3D[0][1]+tmp[1];
		Z[0] = rotMat[2][0]*marker3D[0][0]+rotMat[2][1]*marker3D[0][1]+tmp[2];

		x[0] = focal*X[0]/Z[0]; y[0] = focal*Y[0]/Z[0];
		
		X[1] = rotMat[0][0]*marker3D[1][0]+rotMat[0][1]*marker3D[1][1]+tmp[0];
		Y[1] = rotMat[1][0]*marker3D[1][0]+rotMat[1][1]*marker3D[1][1]+tmp[1];
		Z[1] = rotMat[2][0]*marker3D[1][0]+rotMat[2][1]*marker3D[1][1]+tmp[2];
	
		x[1] = focal*X[1]/Z[1]; y[1] = focal*Y[1]/Z[1];
		X[2] = rotMat[0][0]*marker3D[2][0]+rotMat[0][1]*marker3D[2][1]+tmp[0];
		Y[2] = rotMat[1][0]*marker3D[2][0]+rotMat[1][1]*marker3D[2][1]+tmp[1];
		Z[2] = rotMat[2][0]*marker3D[2][0]+rotMat[2][1]*marker3D[2][1]+tmp[2];
		x[2] = focal*X[2]/Z[2]; y[2] = focal*Y[2]/Z[2];
		printf("\n%3.5f %3.5f",x[0],y[0]);
		printf("\n%3.5f %3.5f",x[1],y[1]);
		printf("\n%3.5f %3.5f\n",x[2],y[2]);
	}
	

	return 0;
}

