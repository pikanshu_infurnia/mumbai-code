#include <stdio.h>
#include <math.h>

void test(double (*a)[3]){
	int i,j;
	for(i = 0; i<3; i++){
		for(j = 0; j<3; j++){
//			printf("%1f \t",a[i][j]);
			printf("%1f \t",*(*(a+i)+j));

		}
		printf("\n");
	}
}

int main(){
	double currentEigenVec[3] = {0.999874, -0.003386 ,-0.015540};
	double prevEigenVec[3] = {-0.997663, -0.045939, 0.050570};
	double prevRotMat[3][3];
	double currentAngleRot = 130.84*M_PI/180;
	double prevAngleRot = 134.39*M_PI/180;

	int i,j;
	double c,s,t,x,y,z,tmp1,tmp2; 


	/*Bisector of eigenVectors*/
//	x = (currentEigenVec[0] + prevEigenVec[0]); y = (currentEigenVec[1] + prevEigenVec[1]); z = (currentEigenVec[2] + prevEigenVec[2]);
//	tmp1 = /*magnitude of bisector*/ sqrt(x*x+y*y+z*z);
//	x = x/tmp1; y = y/tmp1;z = z/tmp1;
//	c = cos((prevAngleRot+currentAngleRot)/2);
//	s = sin((prevAngleRot+currentAngleRot)/2);

	x = currentEigenVec[0]; y = currentEigenVec[1]; z = currentEigenVec[2];
	c = cos(currentAngleRot);
	s = sin(currentAngleRot);

	t = 1.0 - c;
	*(*(prevRotMat + 0) + 0)= c + x*x*t;
	*(*(prevRotMat + 1) + 1)= c + y*y*t;
	*(*(prevRotMat + 2) + 2)= c + z*z*t;
	tmp1 = x*y*t;
	tmp2 = z*s;
	*(*(prevRotMat + 1) + 0)= tmp1 + tmp2;
	*(*(prevRotMat + 0) + 1)= tmp1 - tmp2;
	tmp1 = x*z*t;
	tmp2 = y*s;
	*(*(prevRotMat + 2) + 0)= tmp1 - tmp2;
	*(*(prevRotMat + 0) + 2)= tmp1 + tmp2;
	tmp1 = y*z*t;
	tmp2 = x*s;
	*(*(prevRotMat + 2) + 1)= tmp1 + tmp2;
	*(*(prevRotMat + 1) + 2)= tmp1 - tmp2;

	for(i = 0; i < 3; i++){
		for(j = 0 ; j < 3; j++){
			printf("%2.6f ",prevRotMat[j][i]);
		}
		printf("\n");
	}
	return 0;
}
