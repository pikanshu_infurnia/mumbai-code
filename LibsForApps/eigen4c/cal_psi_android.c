
void new_marker(double marker[3][2]){

	int i;
	double x,y;
	x = 960/2; y  = 720/2;
	for( i = 0 ; i < 3; i++){
		marker[i][0] = marker[i][0] - x;
		marker[i][1] = marker[i][1] - y;
	}

	return;
}


double cal_psi(double z[3], double old_marker[4][2],int dir,double rotMat[3][3]){

	double tmp[4],phi,theta;
	double a1,a2,b1,b2,l1,l2,m1,m2,C,N;
	double alpha[3],beta[3];
	double c[3],s[3];
	double p,b;
	double h = 85.72,w = 53.98;
	double D[3][2] ={{-w/2,h/2},{w/2,h/2},{w/2,-h/2}};
	double focal = 750;
	double psi[2];
	double Z;
	int i,j,k;

	double marker[3][2];

	marker[0][0] = old_marker[(4-dir)%4][0]; marker[0][1] = old_marker[(4-dir)%4][1];
	marker[1][0] = old_marker[(5-dir)%4][0]; marker[1][1] = old_marker[(5-dir)%4][1];
	marker[2][0] = old_marker[(6-dir)%4][0]; marker[2][1] = old_marker[(6-dir)%4][1];

	z[0] = -z[0]; z[1] = -z[1]; z[2] = -z[2];

	//////////////////////////////////// calculating theta & phi //////////////////////

//	new_marker(marker);
	tmp[0] = z[2];
	theta = acos(z[2]);
	phi = atan(-z[0]/z[1]);
	__android_log_print(ANDROID_LOG_VERBOSE,"cal_psi","theta  = %3.5f phi = %3.5f ", theta*180/M_PI,phi*180/M_PI);
	///////////////////////////////////////////////////////////////////////////////////


	tmp[0] = cos(phi); tmp[1] = sin(phi); tmp[2] = cos(theta); tmp[3] = sin(theta);

	a1 = tmp[0];	 	a2 = tmp[2]*tmp[1];
	b1 = tmp[2]*tmp[1];	 b2 = tmp[0];
	l1 = tmp[1]; 		l2 =tmp[2]*tmp[0];
	m1 = tmp[2]*tmp[0];	m2 = tmp[1];
	C = tmp[1]*tmp[3];	N = -tmp[3]*tmp[0];

/*	for( i = 0 ; i < 3; i++){*/
/*		__android_log_print(ANDROID_LOG_VERBOSE,"cal_psi"," %3.5f %3.5f",marker[i][0],marker[i][1]);*/
/*	}*/

	for(i = 0 ; i < 3; i++){
		alpha[i] = 1/marker[i][0];
		beta[i] = 1/marker[i][1];

		c[i] = a1*D[i][0]*alpha[i] + l1*D[i][1]*alpha[i] + b1*D[i][0]*beta[i] - m1*D[i][1]*beta[i];
		s[i] = -a2*D[i][0]*alpha[i] + l2*D[i][1]*alpha[i] + b2*D[i][0]*beta[i] + m2*D[i][1]*beta[i];
	}

	p = (c[1-1]*alpha[2-1] - c[2-1]*alpha[1-1])*(beta[1-1]*alpha[3-1] - beta[3-1]*alpha[1-1]) - (c[1-1]*alpha[3-1] - c[3-1]*alpha[1-1])*(beta[1-1]*alpha[2-1] - beta[2-1]*alpha[1-1]);

	b = (s[1-1]*alpha[2-1] - s[2-1]*alpha[1-1])*(beta[1-1]*alpha[3-1] - beta[3-1]*alpha[1-1]) - (s[1-1]*alpha[3-1] - s[3-1]*alpha[1-1])*(beta[1-1]*alpha[2-1] - beta[2-1]*alpha[1-1]);

	psi[0] = atan2(p,-b);
	psi[1] = M_PI + psi[0];

	for(i = 0 ; i < 2; i++){

		__android_log_print(ANDROID_LOG_VERBOSE,"cal_psi"," angle psi = %f ",psi[i]*180/M_PI);

		tmp[2] = cos(psi[i]); tmp[3] = sin(psi[i]);

		tmp[2-1] = (tmp[2]*(c[1-1]*alpha[3-1] - c[3-1]*alpha[1-1]) + tmp[3]*(s[1-1]*alpha[3-1] - s[3-1]*alpha[1-1]))/((beta[1-1]*alpha[3-1] - beta[3-1]*alpha[1-1]));
		tmp[1-1] = (tmp[2-1]*beta[1-1] - tmp[2]*c[1-1] - tmp[3]*s[1-1])/alpha[1-1];

		Z =((tmp[2]*a1 - tmp[3]*a2)*D[0][0] + (tmp[2]*l1 + tmp[3]*l2)*D[0][1] + tmp[1-1])*alpha[1-1]*focal;

		if(Z > 0){

			rotMat[0][0] = tmp[2]*a1 - tmp[3]*a2;
			rotMat[0][1] = -tmp[2]*b1 - tmp[3]*b2;
			rotMat[0][2] = C;

			rotMat[1][0] = tmp[2]*l1 + tmp[3]*l2;
			rotMat[1][1] = tmp[2]*m1 - tmp[3]*m2;
			rotMat[1][2] = N;

			rotMat[2][0] = tmp[3]*sin(theta);
			rotMat[2][1] = tmp[2]*sin(theta);
			rotMat[2][2] = z[2];

			printf(" ROTATION MATRIX : ");
			for( k = 0 ; k < 3 ; k++){
					__android_log_print(ANDROID_LOG_VERBOSE,"cal_psi transMat", "%3.5f %3.5f %3.5f %3.5f", rotMat[k][0],rotMat[k][1],rotMat[k][2],tmp[k]);
			}
			break;
		}
	}
}

