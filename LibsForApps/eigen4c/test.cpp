#include <iostream>
#include <cstdio>


#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/src/Eigenvalues/EigenSolver.h>

using namespace std;
using namespace Eigen;

int main()
{

	complex<double> lambda;
	double rotMatrix[3][3] = {{0.995615, -0.064903 ,-0.067370},{-0.087457 ,-0.901401, -0.424061},{-0.033204, 0.428093, -0.90312}};
	
//	MatrixXd A = MatrixXd::Random(6,6);
//	cout << "Here is a random 6x6 matrix, A:" << endl ;//<< A << endl << endl;

	Matrix3d rotMat(3,3);
	
	for(int i = 0; i < 3; i++){
		for(int j = 0 ; j < 3; j++){
			rotMat(i,j) = rotMatrix[i][j];
		}
	}
	
	
	for(int i = 0; i < 3; i++){
		printf("%3.5f %3.5f %3.5f",rotMat(i,0),rotMat(i,1),rotMat(i,2));
	}
	
	EigenSolver<MatrixXd> es(rotMat);
	
	cout << "The eigenvalues of rotMat are:" << endl << es.eigenvalues() << endl;
	cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors() << endl << endl;
	
	
//	
//	cout << "\n\nprinting eigenvalues separately "<<endl;
//	
//	
//	for(int i = 0 ; i < 3; i++){
//	
//		lambda =es.eigenvalues()[i];
//		if(lambda.imag() == 0) cout << lambda.real() << "\t"<< lambda.imag()<<endl;
//	}
//	

//////	cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors() << endl << endl;
////	complex<double> lambda = es.eigenvalues()[0];
////	cout << "Consider the first eigenvalue, lambda = " << lambda << endl;
//	VectorXcd v = es.eigenvectors().col();
//	
//	cout << "\n\n printing the eigenvector " << endl;
//	
//	for(int i = 0 ; i < 6; i++){
//		lambda = v[i];
//		cout << lambda.real() << "\t"<< lambda.imag() << endl;
//	}

	return 0;

}
