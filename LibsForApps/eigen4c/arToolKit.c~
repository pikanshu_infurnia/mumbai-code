 /**
	Copyright (C) 2009  Tobias Domhan

    This file is part of AndOpenGLCam.

    AndObjViewer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AndObjViewer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AndObjViewer.  If not, see <http://www.gnu.org/licenses/>.
 
 */
/*
 * arToolKit.c
 * author: Tobias Domhan
 * license: GPL
 * This is the glue between the Java and the C part of AndAR.
 */

//IMPORTS
#include <GLES/gl.h>
#include <stdio.h>
//ar.h containing the logging switch (DEBUG_LOGGING)
#include <AR/ar.h>
#include <AR/param.h>
#include <../marker_info.h>
#include <android/log.h>
#include <stdlib.h>
#include <simclist.h>
#include <string.h>
#include <math.h>
#include "eigen4c.h"

//END IMPORTS


int arImXsize_cam = 0, arImYsize_cam = 0;


/////////////////////////// declared by Pika //////////////////////////
double current_vertex[4][2];
static double prev_transMat_avg[12];
static double avg_param[12];
static double ratio;
static int heightOffset, widthOffset;
static int present_state = 1,prev_state = 1;
static int marker_visibility = 0;
int numSignChanges = 0;
static double prevTransMat[12];
static double correctTransMat[12];

double currentRotMat[3][3];
static double prevRotMat[3][3];

bool print = true;
bool objectAccptFlag = false;

double threshAngle = 20;
double cosThresh ;
double dotProd;
int dir;


double arcTan2(double x, double y){
	if(x < 0 && y < 0) return atan(y/x)- M_PI;
	if(x < 0 && y >= 0) return atan(y/x) + M_PI;
	if(x > 0) return atan(y/x);
	if(x == 0 && y > 0) return M_PI/2;
	if(x == 0 && y < 0) return -M_PI/2;
	if(x == 0 && y == 0) return 100;
}
//double coord0Dis = 0, diff_WdHt = 0,THRESHOLD = 100;

//static double prevCoord0[2];
//static double prevMarker[4][2]; // stores previous marker coordinates
//double diffMarker[4][2]; // stores the diff between prev and present marker coordinates
//double maxDiffX = 0 , maxDiffY = 0 ; // stores the maximum diff. in the coordinates
//int shiftType = 1;  // denotes the type of shift in the cooordinates (translation or rotation or both)
//int changeCount = 0;
//double diffX = 0, diffY = 0; // stores the difference between x and y coordinates of the first two vertices


void avgRotmat(double *currentRotMat,double *prevRotMat){
	double currentEigenVec[3],prevEigenVec[3];
	double currentAngleRot,prevAngleRot;

	eigen4c(currentRotMat,1,currentEigenVec);
	eigen4c(prevRotMat,1,prevEigenVec);

	rotAngle(currentRotMat,currentEigenVec,currentAngleRot);
	rotAngle(prevRotMat,prevEigenVec,prevAngleRot);

	__android_log_print(ANDROID_LOG_VERBOSE,"AR native","currentAngleRot : %2.2f ,prevAngleRot : %2.2f",currentAngleRot,prevAngleRot);
}

void rotAngle(double rotMat[3][3],double eigenVec[3],double *rotAngle){
	double at90ToEigen[3];
	double newRotVec[3];
	int i;
	double aDotb; double modADotmodB;
	at90ToEigen[0] = 0; at90ToEigen[1] = eigenVec[2]; at90ToEigen[2] = -eigenVec[1];

	for(i = 0; i < 3; i++){
		newRotVec[i] = /*rotMat[i][0]*at90ToEigen[0] = 0*/ rotMat[i][1]*at90ToEigen[1] + rotMat[i][2]*at90ToEigen[2];
	}

	aDotb = at90ToEigen[1]*newRotVec[1]+at90ToEigen[2]*newRotVec[2];
	modADotmodB = sqrt(at90ToEigen[1]*at90ToEigen[1]+at90ToEigen[2]*at90ToEigen[2])*sqrt(newRotVec[0]*newRotVec[0]+newRotVec[1]*newRotVec[1]+newRotVec[2]*newRotVec[2]);
	*rotAngle = acos(aDotb/modADotmodB);
}
///////////////////////////////////////////////////////////////////////




//DATASTRUCTURES
/**
 * Represents an AR Object. There is one Java object that corresponds to each of those.
 * The data structure will contain all information that is needed in the native code.
 */


typedef struct {
    int       name;
    int        id;
//    double     marker_coord[4][2];//not needed anymore -> using the array of the corresponding java object
//    double     trans[3][4];//not needed anymore -> using the array of the corresponding java object
    int contF;
    double     marker_width;
    double     marker_center[2];
	jobject objref;
} Object;

/**
 * Data structure representing a pattern ID. Used for caching the pattern IDs.
 * Contains the filename of the pattern file, e.g. patt.hiro, and the the according ID.
 */
typedef struct {
	int id;
	char* filename;
} patternID;

//size of the camera images
int             xsize, ysize;
//Binarization threshold
int             thresh = 100;
int             count = 0;
//camera distortion parameters
ARParam         cparam;

//the opengl para matrix
extern float   gl_cpara[16];
//A list of AR objects
list_t objects;
//A list of cached pattern IDs
list_t* patternIDs = NULL;

//END DATASTRUCTURES

/*
 * compare an AR object with an ID
 * used to search the list of objects for an particular object.
 * return 0 if the objects equal
 * and 1 otherwise
 */
int objectcomparator(const void *a, const void *b) {
	/* compare areas */
	const Object *A = (Object *) a;
	const int *B = (int *) b;
	if(A->name == *B)
		return 0;
	else
		return 1;
}

/**
 * This function will search the list of cached pattern IDs for a pattern ID according to the given filename.
 * If it finds one, the found ID will be returned.
 * If not, -1 will be returned.
 */
int getPatternIDFromList(const char *filename) {
	int id = -1;
#ifdef DEBUG_LOGGING
		__android_log_write(ANDROID_LOG_INFO,"AR native","trying to retrieve pattern from list");
#endif
	if(patternIDs == NULL) {
#ifdef DEBUG_LOGGING
		__android_log_write(ANDROID_LOG_INFO,"AR native","list of patterns is null!!");
#endif	
		return -1;
	}
	list_iterator_start(patternIDs);
	while (list_iterator_hasnext(patternIDs)) { 
		patternID * currPatt = (patternID *)list_iterator_next(patternIDs);
#ifdef DEBUG_LOGGING
		__android_log_print(ANDROID_LOG_INFO,"AR native","current pattern fiel: %s",currPatt->filename);
#endif
		if(strcmp(filename, currPatt->filename)==0) {
#ifdef DEBUG_LOGGING
		__android_log_write(ANDROID_LOG_INFO,"AR native","found pattern in list");
#endif
			id = currPatt->id;
			break;
		}
	}
	list_iterator_stop(patternIDs); 
	
#ifdef DEBUG_LOGGING
		if(id==-1)
		__android_log_print(ANDROID_LOG_INFO,"AR native","found no pattern in the list for file %s",filename);
#endif
	return id;
}

/**
 * Do some basic initialization, like creating data structures.
 */
/*
 * Class:     edu_dhbw_andar_ARToolkit
 * Method:    artoolkit_init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_edu_dhbw_andar_ARToolkit_artoolkit_1init__
  (JNIEnv * env, jobject object) {
#ifdef DEBUG_LOGGING
		__android_log_write(ANDROID_LOG_INFO,"AR native","initializing artoolkit");
#endif
	//initialize the list of objects
	list_init(&objects);
	//set the comperator function:
	list_attributes_comparator(&objects, objectcomparator);
	//Intialize the list of pattern IDs
	//It might already be initialized, as the native library doesn't get unloaded after the java application finished
	//The pattern IDs will be cached during multiple invocation of the Java application(AndAR)
	if(patternIDs==NULL) {
		patternIDs = (list_t*) malloc(sizeof(list_t));
		if(patternIDs == NULL) {
#ifdef DEBUG_LOGGING
		__android_log_write(ANDROID_LOG_INFO,"AR native","list of patterns is null after init!!");
#endif	
		} else {
			list_init(patternIDs);
		}
	}	
#ifdef DEBUG_LOGGING
		__android_log_write(ANDROID_LOG_INFO,"AR native","finished initializing ARToolkit");
#endif
  }

/**
 * Register a object to the native library. From now on the detection function will determine
 * if the given object is visible on a marker, and set the transformation matrix accordingly.
 * @param id a unique ID of the object
 * @param patternName the fileName of the pattern
 * @param markerWidth the width of the object
 * @param markerCenter the center of the object
 */  
/*
 * Class:     edu_dhbw_andar_ARToolkit
 * Method:    addObject
 * Signature: (ILedu/dhbw/andar/ARObject;Ljava/lang/String;D[D)V
 */
JNIEXPORT void JNICALL Java_edu_dhbw_andar_ARToolkit_addObject
  (JNIEnv * env, jobject artoolkit, jint name, jobject obj, jstring patternFile, jdouble width, jdoubleArray center) {
	Object* newObject;
	jdouble* centerArr;
	centerArr = (*env)->GetDoubleArrayElements(env, center, NULL);
	const char *cPatternFile = (*env)->GetStringUTFChars( env, patternFile, NULL ); 
	if (centerArr == NULL) {
		//could not retrieve the java array
		jclass exc = (*env)->FindClass( env, "edu/dhbw/andar/exceptions/AndARException" );  
		if ( exc != NULL ) 
			(*env)->ThrowNew( env, exc, "could not retrieve array of the marker center in native code." ); 
	}
	if((newObject = (Object *)malloc(sizeof(Object))) == NULL) {
		//something went wrong with allocating -> throw error
		jclass exc = (*env)->FindClass( env, "edu/dhbw/andar/exceptions/AndARException" );  
		if ( exc != NULL ) 
			(*env)->ThrowNew( env, exc, "could not allocate memory for new object." ); 
	} else {
		//ok object allocated, now fill the struct with data
#ifdef DEBUG_LOGGING
		__android_log_print(ANDROID_LOG_INFO,"AR native","registering object with name %d", name);
#endif
		newObject->name = (int) name;
		newObject->marker_width = (double) width;
		newObject->contF = 0;
		newObject->marker_center[0] = (double) centerArr[0];
		newObject->marker_center[1] = (double) centerArr[1];
		newObject->objref = (*env)->NewGlobalRef(env, obj);
		//search the list of pattern IDs for a pattern matching the given filename
		//this is needed as the native library doesn't get unloaded after the Java application finished
		//and multiple invocations of arLoadPatt with the same pattern file will result in incorrect IDs
		if( (newObject->id = getPatternIDFromList(cPatternFile)) < 0 ) {
			if( (newObject->id = arLoadPatt(cPatternFile)) < 0 ) {
				//failed to read the pattern file
				//release the object and throw an exception
				free(newObject);
				jclass exc = (*env)->FindClass( env, "edu/dhbw/andar/exceptions/AndARException" );  
				if ( exc != NULL ) 
					(*env)->ThrowNew( env, exc, "could not read pattern file for object." );
			} else {
	#ifdef DEBUG_LOGGING
			__android_log_print(ANDROID_LOG_INFO,"AR native","loaded marker with ID %d from file: %s", newObject->id, cPatternFile);
	#endif
				//add object to the list
				list_append(&objects, newObject);
				patternID* newPatternID = (patternID *)malloc(sizeof(patternID));
				if(newPatternID != NULL) {
					newPatternID->filename = strdup(cPatternFile);
					newPatternID->id = newObject->id;
					list_append(patternIDs, newPatternID);
				}
			}
		} else {
#ifdef DEBUG_LOGGING
			__android_log_print(ANDROID_LOG_INFO,"AR native","loaded marker with ID %d from cached pattern ID", newObject->id, cPatternFile);
	#endif
				//add object to the list
				list_append(&objects, newObject);
		}
	}
	//release the marker center array
	(*env)->ReleaseDoubleArrayElements(env, center, centerArr, 0);
	(*env)->ReleaseStringUTFChars( env, patternFile, cPatternFile);
  }
  
/**
 * Remove the object from the list of registered objects.
 * @param id the id of the object.
 */
/*
 * Class:     edu_dhbw_andar_ARToolkit
 * Method:    removeObject
 * Signature: (I)V
 */
 //TODO release globalref
JNIEXPORT void JNICALL Java_edu_dhbw_andar_ARToolkit_removeObject
  (JNIEnv *env, jobject artoolkit, jint objectID) {
	if(list_delete(&objects,&objectID) != 0) {
		//failed to delete -> throw error
		jclass exc = (*env)->FindClass( env, "edu/dhbw/andar/exceptions/AndARException" );  
		if ( exc != NULL ) 
			(*env)->ThrowNew( env, exc, "could not delete object from native array" );
	}
#ifdef DEBUG_LOGGING
	__android_log_write(ANDROID_LOG_INFO,"AR native","array of objects still containing the following elements:");
	list_iterator_start(&objects);        /* starting an iteration "session" */
    while (list_iterator_hasnext(&objects)) { /* tell whether more values available */
        Object* curObject = (Object *)list_iterator_next(&objects);     /* get the next value */
		__android_log_print(ANDROID_LOG_INFO,"AR native","name: %s", curObject->name);
		__android_log_print(ANDROID_LOG_INFO,"AR native","id: %s", curObject->id);
	}
    list_iterator_stop(&objects);
#endif
}

/**
 * Do initialization specific to the image/screen dimensions.
 * @param imageWidth width of the image data
 * @param imageHeight height of the image data
 * @param screenWidth width of the screen
 * @param screenHeight height of the screen
 */
/*
 * Class:     edu_dhbw_andar_ARToolkit
 * Method:    artoolkit_init
 * Signature: (Ljava/lang/String;IIII)V
 */
JNIEXPORT void JNICALL Java_edu_dhbw_andar_ARToolkit_artoolkit_1init__Ljava_lang_String_2IIII
  (JNIEnv *env, jobject object, jstring calibFile, jint imageWidth, jint imageHeight, jint screenWidth, jint screenHeight) {
    ARParam  wparam;
	const char *cparam_name = (*env)->GetStringUTFChars( env, calibFile, NULL ); 

	
    xsize = imageWidth;
    ysize = imageHeight;
    printf("Image size (x,y) = (%d,%d)\n", xsize, ysize);

    /* set the initial camera parameters */
    if( arParamLoad(cparam_name, 1, &wparam) < 0 ) {
	__android_log_write(ANDROID_LOG_ERROR,"AR native","Camera parameter load error !!");
	    jclass exc = (*env)->FindClass( env, "edu/dhbw/andar/exceptions/AndARRuntimeException" );  
		if ( exc != NULL ) 
			(*env)->ThrowNew( env, exc, "Camera parameter load error !!" ); 
        //exit(EXIT_FAILURE);
    }
#ifdef DEBUG_LOGGING
    else {
        __android_log_write(ANDROID_LOG_INFO,"AR native","Camera parameter loaded successfully !!");
    }
#endif
    arParamChangeSize( &wparam, imageWidth, imageHeight, &cparam );
    arInitCparam( &cparam );
    printf("*** Camera Parameter ***\n");
    arParamDisp( &cparam );

    //initialize openGL stuff
    argInit( &cparam, 1.0, 0, screenWidth, screenHeight, 0 );
	
	//gl_cpara
	jclass arObjectClass = (*env)->FindClass(env, "edu/dhbw/andar/ARObject");
	if (arObjectClass != NULL) {
		jfieldID glCameraMatrixFieldID = (*env)->GetStaticFieldID(env, arObjectClass, "glCameraMatrix", "[F");
		if (glCameraMatrixFieldID != NULL) {
			jobject glCameraMatrixObj = (*env)->GetStaticObjectField(env, arObjectClass, glCameraMatrixFieldID);
			if(glCameraMatrixObj != NULL) {
				float *glCamMatrix = (*env)->GetFloatArrayElements(env, glCameraMatrixObj, JNI_FALSE);
				int i=0;
				for(i=0;i<16;i++)
					glCamMatrix[i] = gl_cpara[i];
				(*env)->ReleaseFloatArrayElements(env, glCameraMatrixObj, glCamMatrix, 0); 
			}
		}
	}
	
	(*env)->ReleaseStringUTFChars( env, calibFile, cparam_name);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * detect the markers in the given frame.
 * @param in the image
 * @param matrix the transformation matrix for each marker, will be locked right before the trans matrix will be altered
 * @return number of markers
 */
/*
 * Class:     edu_dhbw_andar_MarkerInfo
 * Method:    artoolkit_detectmarkers
 * Signature: ([B[D)I
 */
JNIEXPORT jdoubleArray JNICALL Java_edu_dhbw_andar_ARToolkit_artoolkit_1detectmarkers
  (JNIEnv *env, jobject object, jbyteArray image, jobject transMatMonitor,jdoubleArray MARKER_old,jbyteArray selectedRegion, jintArray imgDim,jdoubleArray Marker_Center,jdoubleArray gravity) {


	cosThresh = cos(threshAngle*M_PI/180);
    ARUint8         *dataPtr,*dataPtr_Modified;
    ARUint8 *selectedRegionPtr;
    ARMarkerInfo    *marker_info;
    double 	    *matrixPtr;
    int             marker_num;
    int             j, k=-1;
    double tempWidth , tempHeight;

    jdouble *Gravity = (*env)->GetDoubleArrayElements(env,gravity,0);

	__android_log_print(ANDROID_LOG_VERBOSE, "AR native","@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"); //printing
    __android_log_print(ANDROID_LOG_VERBOSE,"AR native", "-------------GRAVITY VECTOR : [ %1.5f %1.5f %1.5f ]",Gravity[0],Gravity[1],Gravity[2]); //printing


	Object* curObject;

	jint *imgDim_cam = (*env)->GetIntArrayElements(env,imgDim,0);

	arImXsize_cam = imgDim_cam[1];/////// Pika
	arImYsize_cam = imgDim_cam[0];/////// Pika

	heightOffset = (arImYsize_cam - arImYsize)/2;

//	__android_log_print(ANDROID_LOG_VERBOSE,"AR native", "heigthOffset %d ", heightOffset );

    (*env)->ReleaseIntArrayElements(env,imgDim,imgDim_cam,0);

	jsize arrLength = (*env)->GetArrayLength(env,MARKER_old);
    jdoubleArray MARKER = (*env)->NewDoubleArray(env,arrLength);
    jdouble *temp_marker = NULL;
    temp_marker = (*env)->GetDoubleArrayElements(env,MARKER_old, 0);


    //if (NULL == temp_marker) return NULL;
	int i = 0;
	int a [4];// ={7-marker_info->dir,8-marker_info->dir,5-marker_info->dir,6-marker_info->dir};


    /* grab a vide frame */
    dataPtr = (*env)->GetByteArrayElements(env, image, JNI_FALSE);
    selectedRegionPtr = (*env)->GetByteArrayElements(env, selectedRegion, JNI_FALSE);

//    dataPtr = dataPtr +  (arImXsize-1) * heightOffset; /////// Pika
    dataPtr_Modified = dataPtr +  (arImXsize) * heightOffset; /////// Pika

    if( count == 0 ) arUtilTimerReset();
    count++;
    //__android_log_print(ANDROID_LOG_INFO,"AR native","reached detectmarker");
    /* detect the markers in the video frame */

//    __android_log_print(ANDROID_LOG_VERBOSE, "ARdetectmarker"," Reached artoolkit.c");

    if( arDetectMarker(dataPtr_Modified, thresh, &marker_info, &marker_num,selectedRegionPtr) < 0 ) {
	__android_log_write(ANDROID_LOG_ERROR,"AR native","arDetectMarker failed!!");
		jclass exc = (*env)->FindClass( env, "edu/dhbw/andar/exceptions/AndARException" );
		if ( exc != NULL )
			(*env)->ThrowNew( env, exc, "failed to detect marker" );
    }

    __android_log_print(ANDROID_LOG_INFO,"AR native","marker_num : %d",marker_num); //printing
    //__android_log_print(ANDROID_LOG_INFO,"AR native","after detectmarker");

#ifdef DEBUG_LOGGING
   __android_log_print(ANDROID_LOG_INFO,"AR native","detected %d markers",marker_num);
#endif


    //lock the matrix
    /*(*env)->MonitorEnter(env, transMatMonitor);
    cur_marker_id = k;
    argConvGlpara(patt_trans, gl_para);
    (*env)->MonitorExit(env, transMatMonitor);*/

	static jfieldID visibleField = NULL;
	static jfieldID glMatrixField = NULL;
	static jfieldID transMatField = NULL;
	jclass arObjectClass = NULL;
	jfloatArray glMatrixArrayObj = NULL;
	jdoubleArray transMatArrayObj = NULL;

#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","done detecting markers, going to iterate over markers now");
#endif
	//iterate over objects:
	list_iterator_start(&objects);        /* starting an iteration "session" */
	int itCount = 0;
    while (list_iterator_hasnext(&objects)) { /* tell whether more values available */
        curObject = (Object *)list_iterator_next(&objects);     /* get the next value */
#ifdef DEBUG_LOGGING
		__android_log_print(ANDROID_LOG_INFO,"AR native","now handling object with id %d, in %d iteration",curObject->name, itCount);
#endif
		itCount++;
		// //get field ID'
		if(visibleField == NULL) {
			if(arObjectClass == NULL) {
				if(curObject->objref != NULL)
					arObjectClass = (*env)->GetObjectClass(env, curObject->objref);
			}
			if(arObjectClass != NULL) {
				visibleField = (*env)->GetFieldID(env, arObjectClass, "visible", "Z");//Z means boolean
			}
		}
		if(glMatrixField == NULL) {
			if(arObjectClass == NULL) {
				if(curObject->objref != NULL)
					arObjectClass = (*env)->GetObjectClass(env, curObject->objref);
			}
			if(arObjectClass != NULL) {
				glMatrixField = (*env)->GetFieldID(env, arObjectClass, "glMatrix", "[F");//[F means array of floats
			}
		}

		if(transMatField == NULL) {
			if(arObjectClass == NULL) {
				if(curObject->objref != NULL)
					arObjectClass = (*env)->GetObjectClass(env, curObject->objref);
			}
			if(arObjectClass != NULL) {
				transMatField = (*env)->GetFieldID(env, arObjectClass, "transMat", "[D");//[D means array of doubles
			}
		}

		if(visibleField == NULL || glMatrixField == NULL || transMatField == NULL) {
			//something went wrong..
#ifdef DEBUG_LOGGING
		__android_log_write(ANDROID_LOG_INFO,"AR native","error: either visibleField or glMatrixField or transMatField null");
#endif
			continue;
		}

		 // check for object visibility
		k = -1;
//		__android_log_print(ANDROID_LOG_INFO,"AR native","marker_num in arDetector: %d",marker_num);

		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		for( j = 0; j < marker_num; j++ ) {

#ifdef DEBUG_LOGGING
			__android_log_print(ANDROID_LOG_INFO,"AR native","marker with id: %d", marker_info[j].id);
#endif
			//_______________________________________________________________________________________________________
			if(marker_info[j].id == 0){

				tempWidth = ((marker_info[j].vertex[1][0] - marker_info[j].vertex[2][0])*(marker_info[j].vertex[1][0] - marker_info[j].vertex[2][0]) + (marker_info[j].vertex[1][1] - marker_info[j].vertex[2][1])*(marker_info[j].vertex[1][1] - marker_info[j].vertex[2][1]));
//				__android_log_print(ANDROID_LOG_VERBOSE,"AR native" , "markerwidth^2 : %f markerwidth : %f" , tempWidth,sqrt(tempWidth)); //printing
				tempHeight = (marker_info[j].vertex[1][0] - marker_info[j].vertex[0][0])*(marker_info[j].vertex[1][0] - marker_info[j].vertex[0][0]) + (marker_info[j].vertex[1][1] - marker_info[j].vertex[0][1])*(marker_info[j].vertex[1][1] - marker_info[j].vertex[0][1]);
//				__android_log_print(ANDROID_LOG_VERBOSE,"AR native" , "markerheight^2 : %f markerheight : %f" , tempHeight,sqrt(tempHeight)); //printing
				if(tempWidth > tempHeight){
//                if(fabs(marker_info[j].vertex[0][1] - marker_info[j].vertex[1][1]) < fabs(marker_info[j].vertex[0][1] - marker_info[j].vertex[3][1])){
					a[0] = 2;
					a[1] = 3;
					a[2] = 0;
					a[3] = 1;
					marker_info[j].dir = 2;
					dir = marker_info[j].dir;
					prev_state = 1;
				}
				else{
					a[0] = 1;
					a[1] = 2;
					a[2] = 3;
					a[3] = 0;
					marker_info[j].dir = 3;
					dir = marker_info[j].dir;
					prev_state = 0;
				}
				for(i = 0;i<4;i++){

                        current_vertex[i][0] = marker_info[j].vertex[i][0];
                        current_vertex[i][1] = marker_info[j].vertex[i][1];
//                     __android_log_print(ANDROID_LOG_VERBOSE,"AR native","Marker Coordinates : %5f %5f ",marker_info[j].vertex[i][0],marker_info[j].vertex[i][1]);
//                       __android_log_print(ANDROID_LOG_INFO,"AR native","%5f   %5f ,%d",marker_info[j].vertex[(a[i])%4][0],marker_info[j].vertex[(a[i])%4][1],(a[i])); //printing

                    }
				k=j;
              }

			//_______________________________________________________________________________________________________

//			__android_log_print(ANDROID_LOG_INFO,"AR native","detected object %d with marker %d and object marker %d",curObject->name,j,curObject->id);//printing
			/*if( curObject->id == marker_info[j].id ) {

				if( k == -1 ) {
					k = j;
#ifdef DEBUG_LOGGING
					__android_log_print(ANDROID_LOG_INFO,"AR native","detected object %d with marker %d and object marker %d --------- entered",curObject->name,k,curObject->id); //printing
#endif
//                    }
//                    temp_marker[8] = marker_num;
//                    temp_marker[9] = marker_info[j].dir;

				}
				else if( marker_info[k].cf < marker_info[j].cf )  {
#ifdef DEBUG_LOGGING
					__android_log_print(ANDROID_LOG_INFO,"AR native","detected better object %d with marker %d and object marker %d ------------- ENTERED 2",curObject->name,k,curObject->id); //printing
#endif
					k = j;
				}
			}*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}

		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		if( k == -1 ) {
			//object not visible
			curObject->contF = 0;
			(*env)->SetBooleanField(env, curObject->objref, visibleField, JNI_FALSE);
#ifdef DEBUG_LOGGING
			__android_log_print(ANDROID_LOG_INFO,"AR native","object %d  not visible, with marker ID %d",curObject->name,curObject->id);
#endif
			continue;
		}
		//object visible

		//lock the object
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","locking object");
#endif
		(*env)->MonitorEnter(env, curObject->objref);
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","done locking object...obtaining arrays");
#endif
		//access the arrays of the current object
		glMatrixArrayObj = (*env)->GetObjectField(env, curObject->objref, glMatrixField);
		transMatArrayObj = (*env)->GetObjectField(env, curObject->objref, transMatField);
		if(transMatArrayObj == NULL || glMatrixArrayObj == NULL) {
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","failed to fetch the matrix arrays objects");
#endif
			continue;//something went wrong
		}
		float *glMatrix = (*env)->GetFloatArrayElements(env, glMatrixArrayObj, JNI_FALSE);
		if(glMatrix == NULL ) {
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","failed to fetch the matrix arrays");
#endif
			continue;//something went wrong
		}
		double* transMat = (*env)->GetDoubleArrayElements(env, transMatArrayObj, JNI_FALSE);
		if(transMat == NULL) {
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","failed to fetch the matrix arrays");
#endif
			continue;//something went wrong
		}
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","calculating trans mat now");
#endif

		// get the transformation between the marker and the real camera


		//@@@@@@@@@@@@@@@@@@@@@@@@@_____________________ TRANSFORMATION MATRIX PART __________________________@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


		__android_log_print(ANDROID_LOG_INFO,"AR native","-------------------------------value of k : %d",k); //printing

		for(i=0;i<3;i++){
			prevTransMat[4*i+0] = transMat[4*i+0];
			prevTransMat[4*i+1] = transMat[4*i+1];
			prevTransMat[4*i+2] = transMat[4*i+2];
			prevTransMat[4*i+3] = transMat[4*i+3];
		}


		if(/* curObject->contF == 0 original */ marker_visibility == 0) {
			arGetTransMat(&marker_info[k], curObject->marker_center, curObject->marker_width, transMat);
		} else {
			arGetTransMatCont(&marker_info[k], transMat, curObject->marker_center, curObject->marker_width, transMat);
		}

		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ADDED CODE INFURNIA @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

		dotProd = transMat[4*0+2]*Gravity[0]+transMat[4*1+2]*Gravity[1]+transMat[4*2+2]*Gravity[2];
		__android_log_print(ANDROID_LOG_VERBOSE,"AR native","dot product : %5f ",dotProd); //printing
		
		if(dotProd < -cosThresh){
		
			
			__android_log_print(ANDROID_LOG_VERBOSE,"AR native","-----------------------------------------     ACCEPTED"); //printing
			objectAccptFlag = true;

		}
		else{
			__android_log_print(ANDROID_LOG_VERBOSE,"AR native","-----------------------------------------     REJECTED,dir : %d",dir); //printing
			objectAccptFlag = false;

			if(dir == 2) {marker_info[k].dir = 3; dir = 3;}
			else if(dir == 3) {marker_info[k].dir = 2; dir = 2;}

			if(/* curObject->contF == 0 original */ marker_visibility == 0) {
				arGetTransMat(&marker_info[k], curObject->marker_center, curObject->marker_width, transMat);
			} else {
				arGetTransMatCont(&marker_info[k], prevTransMat, curObject->marker_center, curObject->marker_width, transMat);
			}

			dotProd = transMat[4*0+2]*Gravity[0]+transMat[4*1+2]*Gravity[1]+transMat[4*2+2]*Gravity[2];
			__android_log_print(ANDROID_LOG_VERBOSE,"AR native","dot product : %5f ",dotProd); //printing
			
			if(dotProd < -cosThresh){
				__android_log_print(ANDROID_LOG_VERBOSE,"AR native","-----------------------------------------     ACCEPTED ------------ 2");//printing
				objectAccptFlag = true;
			}
			else {
				__android_log_print(ANDROID_LOG_VERBOSE,"AR native","-----------------------------------------     REJECTED ------------- 2 time");//printing
				objectAccptFlag = false;

				for(i=0;i<3;i++){
					correctTransMat[4*i+3] = transMat[4*i+3];
				}
			}
		}

		if(objectAccptFlag == true){

			for(i=0;i<3;i++){
				currentRotMat[i][0] = correctTransMat[4*i+0] = transMat[4*i+0];
				currentRotMat[i][1] = correctTransMat[4*i+1] = transMat[4*i+1];
				currentRotMat[i][2] = correctTransMat[4*i+2] = transMat[4*i+2];
				correctTransMat[4*i+3] = transMat[4*i+3];
			}

			for(i = 0;i<4;i++){
				temp_marker[i*2] = current_vertex[(4-dir+i)%4][0];
				temp_marker[(i*2)+1] = current_vertex[(4-dir+i)%4][1];
			}
			temp_marker[8] = marker_num;
			temp_marker[9] = dir;

			if(prevRotMat[0][0] == prevRotMat[0][1] && prevRotMat[0][2] == prevRotMat[0][1]){
				for( i = 0; i < 3; i++){
					prevRotMat[i][0] = transMat[4*i+0];
					prevRotMat[i][1] = transMat[4*i+1];
					prevRotMat[i][2] = transMat[4*i+2];
				}
			}
			else{
				avgRotmat
				
			}
		}

		numSignChanges = 0;
		for(i = 0 ; i<3 ; i++){
			if(prevTransMat[4*i+0] * transMat[4*i+0] < 0 ) numSignChanges++;
			if(prevTransMat[4*i+1] * transMat[4*i+1] < 0 ) numSignChanges++;
			if(prevTransMat[4*i+2] * transMat[4*i+2] < 0 ) numSignChanges++;
			if(prevTransMat[4*i+3] * transMat[4*i+3] < 0 ) numSignChanges++;
		}


        __android_log_print(ANDROID_LOG_INFO,"ARToolKit.c","Number of sign changes in transformation matrix : %d" , numSignChanges); //printing
		__android_log_print(ANDROID_LOG_VERBOSE, "AR native","@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"); //printing

		if(k < 0){
			marker_visibility = 0;
		}
		else marker_visibility = 1;

//		argConvGlpara(prev_transMat_avg, glMatrix);	//  Called when avg of transMat or avg of transMat parameters is applied
		argConvGlpara(correctTransMat, glMatrix);	//  Called when avg of marker vertices is applied

		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ END (ADDED CODE INFURNIA) @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



		curObject->contF = 1;
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","calculating OpenGL trans mat now");
#endif

//		argConvGlpara(transMat, glMatrix);
		//argConvGlpara(patt_trans, gl_para);
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","releasing arrays");
#endif
		(*env)->ReleaseFloatArrayElements(env, glMatrixArrayObj, glMatrix, 0);
		(*env)->ReleaseDoubleArrayElements(env, transMatArrayObj, transMat, 0);

		(*env)->SetBooleanField(env, curObject->objref, visibleField, JNI_TRUE);
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","releasing lock");
#endif
		//release the lock on the object
		(*env)->MonitorExit(env, curObject->objref);
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","done releasing lock");
#endif
    }
    list_iterator_stop(&objects);         /* starting the iteration "session" */
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","releasing image array");
#endif
    (*env)->ReleaseByteArrayElements(env, image, dataPtr, 0);
#ifdef DEBUG_LOGGING
        __android_log_write(ANDROID_LOG_INFO,"AR native","releasing image array");
#endif


    temp_marker[8] = marker_num;												// INFURNIA
    (*env)->SetDoubleArrayRegion(env,MARKER, 0,arrLength,temp_marker);			// INFURNIA
    (*env)->ReleaseDoubleArrayElements(env, MARKER_old, temp_marker, 0);		// INFURNIA
    (*env)->ReleaseDoubleArrayElements(env,gravity,Gravity,0);					// INFURNIA
    return MARKER;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Class:     edu_dhbw_andar_ARToolkit
 * Method:    arUtilMatInv
 * Signature: ([D[D)I
 */
JNIEXPORT jint JNICALL Java_edu_dhbw_andar_ARToolkit_arUtilMatInv
  (JNIEnv *env, jclass this, jdoubleArray mat1, jdoubleArray mat2) {
	double 	    *mat1Ptr;
	double 	    *mat2Ptr;
	int retval;
	mat1Ptr = (*env)->GetDoubleArrayElements(env, mat1, JNI_FALSE);
	mat2Ptr = (*env)->GetDoubleArrayElements(env, mat2, JNI_FALSE);
	retval = arUtilMatInv(mat1Ptr,mat2Ptr);
	(*env)->ReleaseDoubleArrayElements(env, mat1, mat1Ptr, 0);
	(*env)->ReleaseDoubleArrayElements(env, mat2, mat2Ptr, 0);
	return retval;
  }

/*
 * Class:     edu_dhbw_andar_ARToolkit
 * Method:    arUtilMatMul
 * Signature: ([D[D[D)I
 */
JNIEXPORT jint JNICALL Java_edu_dhbw_andar_ARToolkit_arUtilMatMul
  (JNIEnv *env, jclass this, jdoubleArray mat1, jdoubleArray mat2, jdoubleArray result) {
	double 	    *mat1Ptr;
	double 	    *mat2Ptr;
	double 	    *resPtr;
	int retval;
	mat1Ptr = (*env)->GetDoubleArrayElements(env, mat1, JNI_FALSE);
	mat2Ptr = (*env)->GetDoubleArrayElements(env, mat2, JNI_FALSE);
	resPtr = (*env)->GetDoubleArrayElements(env, result, JNI_FALSE);
	retval = arUtilMatMul(mat1Ptr,mat2Ptr,resPtr);
	(*env)->ReleaseDoubleArrayElements(env, mat1, mat1Ptr, 0);
	(*env)->ReleaseDoubleArrayElements(env, mat2, mat2Ptr, 0);
	(*env)->ReleaseDoubleArrayElements(env, result, resPtr, 0);
	return retval;
  }
