#include <stdio.h>

void test(double **a){
	int i,j;
	for(i = 0; i<3; i++){
		for(j = 0; j<3; j++){
			printf("%1f \t",a[i][j]);
		}
		printf("\n");
	}
}

int main(){
	double arr[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
	test((double**)arr);
	return 0;
}
