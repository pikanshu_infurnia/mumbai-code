LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)


LOCAL_MODULE := eigen4c 

LOCAL_C_INCLUDES += /usr/include/eigen3

LOCAL_SRC_FILES := eigen4c.cpp

LOCAL_LDLIBS +=  -llog -ldl

include $(BUILD_SHARED_LIBRARY)
