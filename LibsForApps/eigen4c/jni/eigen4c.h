#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void eigen4c(double rotMatrix[3][3], int print, double *eigenVec);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

