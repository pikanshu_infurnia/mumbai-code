#include "eigen4c.h"
#include <iostream>
#include <complex>
#include <cmath>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/src/Eigenvalues/EigenSolver.h>


#include <android/log.h>
#define EPSILON 1.0E-8
using namespace std;
using namespace Eigen;

//extern "C" {

	void eigen4c(double rotMatrix[3][3],int print, double *eigenVec){

		complex<double> lambda;
		VectorXcd v;
		Matrix3d rotMat(3,3);
		
		for(int i = 0; i < 3; i++){
			for(int j = 0 ; j < 3; j++){
				rotMat(i,j) = rotMatrix[i][j];
			}
		}

//		if(print == 1){
//			for(int i = 0; i < 3; i++){
//				__android_log_print(ANDROID_LOG_VERBOSE," EIGEN4C", " %1.5f %1.5f %1.5f", rotMat(i,0),rotMat(i,1),rotMat(i,2));
//			}
//		}

		EigenSolver<Matrix3d> es(rotMat);

		for(int i = 0 ; i < 3; i++){
			lambda = es.eigenvalues()[i];
			if(print == 1) __android_log_print(ANDROID_LOG_VERBOSE," EIGEN4C", " %1.5f %1.5f",lambda.real(),lambda.imag());
			if((round(lambda.imag() == 0 && round(lambda.real()) == 1)) || (fabs(lambda.imag()) < EPSILON && fabs(lambda.real() - 1.0) < EPSILON)){
				v = es.eigenvectors().col(i);
				for(int j = 0 ; j < 3; j++ ){
					lambda = v[j];
					eigenVec[j] = lambda.real(); 
				}
				if(print == 1) __android_log_print(ANDROID_LOG_VERBOSE," EIGEN4C", " %1.5f %1.5f %1.5f",eigenVec[0],eigenVec[1],eigenVec[2]);
			}
		}

	}

//}
