LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)


LOCAL_MODULE := smartCards 
LOCAL_SRC_FILES := src/calibrateSC.cpp src/indices.cpp src/regions.cpp src/threshold.cpp src/utils.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/smartCards_includes

LOCAL_CFLAGS = -std=gnu++11

LOCAL_LDLIBS +=  -llog

include $(BUILD_SHARED_LIBRARY)
