void getHistogram(uint8_t *window[][2], int windowWidth, int windowHeight, int lumaBlockSize, int chromaBlockSize, vector<int> &histData);
void sortIndices(vector<int>& data, vector<int>& index);
int thresholdIndices(vector<int>& histData, vector<int>& indices, float percentile);

int sortIndices_MODIFIED(vector<int>& histData, vector<int>& index, float percentile);
