void getRegions(vector<int>& indices, int lumaSize, int chromaSize, int lumaMax, int chromaMax, int chromaBlockSize, vector<vector<uint8_t> >& regions);
void mergeRegions(vector<vector<uint8_t> >& regions, float upThreshold);
int cutRegion(vector<uint8_t> &fromRegion, vector<uint8_t> &cutRegion,vector<vector<uint8_t> > &leftOverRegions);
void upscaleRegions(vector<vector<uint8_t> > &leftOverRegions, int lumaBlockSize, int chromaBlockSize, vector<vector<uint8_t> > &selectedRegions);
