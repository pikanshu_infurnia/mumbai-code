void thresholdImage(uint8_t *image, int width, int height,  vector<vector<uint8_t> > &selectedRegions);
void chromaThresholdImage(uint8_t *image, int width, int height, vector<vector<uint8_t> > &selectedRegions);

void thresholdImage_new(uint8_t *image, int width, int height,  vector<vector<uint8_t> > &selectedRegions,vector<vector<uint8_t> > &removedRegions);
