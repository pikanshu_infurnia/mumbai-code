void printRegions(vector<vector <uint8_t> > &regions);
void printIndices(vector<int>& indices, vector<int>& histData, int thresholdIndex, int lumaBlockSize, int chromaBlockSize);

void PrintRegions(vector<vector <uint8_t> > &regions,char str[]);
void PrintIndices(vector<int>& indices, vector<int>& histData, int thresholdIndex, int lumaBlockSize, int chromaBlockSize,char str[]);
