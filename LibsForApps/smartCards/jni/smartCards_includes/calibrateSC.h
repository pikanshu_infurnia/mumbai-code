void calibrateSC(uint8_t *image, int width, int height, int lumaBlockSize, int chromaBlockSize, float surroundPercentile, float SCPercentile,vector<vector<uint8_t> > &selectedRegions,vector<vector<uint8_t> > &removedRegions,bool PRINTFLAG);
//void getCalibrationWindows(uint8_t *image, int width, int height, int windowStartX, int windowStartY, int windowWidth, int windowHeight, uint8_t *SC[][2], uint8_t *left[][2], uint8_t *top[][2], uint8_t *right[][2], uint8_t *bottom[][2]);


void getCalibrationWindows(uint8_t *image, int width, int height, int windowStartX, int windowStartY,int SCWindowWidth, int SCWindowHeight,int sideWindowWidth,int sideWindowHeight,int topBottomWindowWidth,int topBottomWindowHeight, uint8_t *SC[][2], uint8_t *left[][2], uint8_t *top[][2], uint8_t *right[][2], uint8_t *bottom[][2]);
