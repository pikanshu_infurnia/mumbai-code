//#include<stdint.h>
//#include<vector>
//#include<iostream>

//using namespace std;

//#include "calibrateSC.h"
//#include "indices.h"
//#include "regions.h"
//#include "threshold.h"
//#include "utils.h"

//void calibrateSC(uint8_t *image, int width, int height, int lumaBlockSize, int chromaBlockSize, float surroundPercentile, float SCPercentile,vector<vector<uint8_t> > &selectedRegions,vector<vector<uint8_t> > &removedRegions,bool PRINTFLAG) {

//	int count = 0,i=0,j=0;
//	bool duplicate;

//	//Intentionally keepng windowWidth, windowHeight, windowStartX and windowY even so that it is convinient to access UV data from interleved plane of image in YUV420 format
////	int windowWidth = 2*((width/3)/2);
////	int windowHeight = 2*((height/5)/2);
////	int windowStartX = 2*((width/3)/2);
////	int windowStartY = 2*(height/5);
//	
//	int windowWidth = 2*((width/3)/2);
//	int windowHeight = 2*((height/3)/2);
//	int windowStartX = 2*((width/3)/2);
//	int windowStartY = 2*((height/3)/2);

//	

//	//Creating array of pointers which will be appointed appropriate address by getCalibrationSegments function
//	uint8_t *SC[windowHeight][2], *left[windowHeight][2], *top[windowHeight][2], *right[windowHeight][2], *bottom[windowHeight][2];

//	int sizeOfColorSpace = 256/lumaBlockSize * 256/chromaBlockSize * 256/chromaBlockSize;

//	vector<int> SCHistData(sizeOfColorSpace);
//	vector<int> leftHistData(sizeOfColorSpace);
//	vector<int> topHistData(sizeOfColorSpace);
//	vector<int> rightHistData(sizeOfColorSpace);
//	vector<int> bottomHistData(sizeOfColorSpace);

//	vector<int> SCDecIdx(sizeOfColorSpace);
//	vector<int> leftDecIdx(sizeOfColorSpace);
//	vector<int> topDecIdx(sizeOfColorSpace);
//	vector<int> rightDecIdx(sizeOfColorSpace);
//	vector<int> bottomDecIdx(sizeOfColorSpace);

//	int SCThresholdIndex, leftThresholdIndex, topThresholdIndex, rightThresholdIndex, bottomThresholdIndex;

//	getCalibrationWindows(image, width, height, windowStartX, windowStartY, windowWidth, windowHeight, SC, left, top, right, bottom);
//	cout<<"Got segements.."<<endl;

////	getHistogram(SC, windowWidth, windowHeight, lumaBlockSize, chromaBlockSize, SCHistData);
//////	sortIndices(SCHistData, SCDecIdx);
//////	SCThresholdIndex = thresholdIndices(SCHistData, SCDecIdx, SCPercentile);
////	SCThresholdIndex = sortIndices_MODIFIED(SCHistData, SCDecIdx, SCPercentile);
////	
//	getHistogram(left, windowWidth, windowHeight, lumaBlockSize, chromaBlockSize, leftHistData);
////	sortIndices(leftHistData, leftDecIdx);
////	leftThresholdIndex = thresholdIndices(leftHistData, leftDecIdx, surroundPercentile);
//	leftThresholdIndex = sortIndices_MODIFIED(leftHistData, leftDecIdx, surroundPercentile);
//	
//	getHistogram(top, windowWidth, windowHeight, lumaBlockSize, chromaBlockSize, topHistData);
////	sortIndices(topHistData, topDecIdx);
////	topThresholdIndex = thresholdIndices(topHistData, topDecIdx, surroundPercentile);
//	topThresholdIndex = sortIndices_MODIFIED(topHistData, topDecIdx, surroundPercentile);

//	getHistogram(right, windowWidth, windowHeight, lumaBlockSize, chromaBlockSize, rightHistData);
////	sortIndices(rightHistData, rightDecIdx);
////	rightThresholdIndex = thresholdIndices(rightHistData, rightDecIdx, surroundPercentile);
//	rightThresholdIndex = sortIndices_MODIFIED(rightHistData, rightDecIdx, surroundPercentile);

//	getHistogram(bottom, windowWidth, windowHeight, lumaBlockSize, chromaBlockSize, bottomHistData);
////	sortIndices(bottomHistData, bottomDecIdx);
////	bottomThresholdIndex = thresholdIndices(bottomHistData, bottomDecIdx, surroundPercentile);
//	bottomThresholdIndex = sortIndices_MODIFIED(bottomHistData, bottomDecIdx, surroundPercentile);

////	cout<<"Got Indices"<<endl;
////	cout<<"Printing SCIndices"<<endl;
////	printIndices(SCDecIdx, SCHistData, SCThresholdIndex, lumaBlockSize, chromaBlockSize);
////	
////	if (PRINTFLAG) PrintIndices(SCDecIdx, SCHistData, SCThresholdIndex, lumaBlockSize, chromaBlockSize,"1st_SC_Indices");


//	//Creating another vector of thresholded SC index
////	vector<int> SCIndices;
////	
////	for (i=0; i < SCThresholdIndex; i++) {
////		SCIndices.push_back(SCDecIdx[i]);
////		count++;
////	}

//	
//	count = 0;
//	//Concatenate left, top, right and bottom index arrays excluding duplicates
//	vector<int> removeIndices;
//	
//	for (i=0; i < leftThresholdIndex; i++) {
//		removeIndices.push_back(leftDecIdx[i]);
//		count++;
//	}
//	for (i=0; i < topThresholdIndex; i++) {
//		duplicate = false;
//		for (j=0; j<count; j++) {
//			if (topDecIdx[i]==removeIndices[j]) {
//				duplicate = true;
//			}
//		}
//		if (duplicate == false) {
//			removeIndices.push_back(topDecIdx[i]);
//			count++;
//		}
//	}
//	for (i=0; i < rightThresholdIndex; i++) {
//		duplicate = false;
//		for (j=0; j<count; j++) {
//			if (rightDecIdx[i]==removeIndices[j]) {
//				duplicate = true;
//			}
//		}
//		if (duplicate == false) {
//			removeIndices.push_back(rightDecIdx[i]);
//			count++;
//		}
//	}
//	for (i=0; i < bottomThresholdIndex; i++) {
//		duplicate = false;
//		for (j=0; j<count; j++) {
//			if (bottomDecIdx[i]==removeIndices[j]) {
//				duplicate = true;
//			}
//		}
//		if (duplicate == false) {
//			removeIndices.push_back(bottomDecIdx[i]);
//			count++;
//		}
//	}

//	cout<<"Got unique removeIndices list"<<endl;

//	//Convert Indices to regions and merge them to the extent possible
//	vector<vector<uint8_t> > removeRegions;
//	getRegions(removeIndices, 3, 3, 256/lumaBlockSize, 256/chromaBlockSize, chromaBlockSize, removeRegions);

//	if (PRINTFLAG) 	PrintRegions(removeRegions,"1st_Remove_Region");

//	mergeRegions(removeRegions, 1);
//	cout<<"Printing removeRegions..."<<endl;
//	printRegions(removeRegions);
//	
//	if (PRINTFLAG) 	PrintRegions(removeRegions,"Remove_Region_after_merge");

////////////////////////////////////////////////////
////	vector<vector<uint8_t> > SCRegions;
////	getRegions(SCIndices, 3, 3, 256/lumaBlockSize, 256/chromaBlockSize, chromaBlockSize, SCRegions);
////	
////	if (PRINTFLAG) 	PrintRegions(SCRegions,"1st_SC_Region");
////	
////	mergeRegions(SCRegions, 1.4);
////	cout<<"Printing SCRegions..."<<endl;
////	printRegions(SCRegions);

////	if (PRINTFLAG) 	PrintRegions(SCRegions,"SC_Region_after_merge");

////////////////////////////////////////
////	cout<<"Merged removeRegions and SCRegions"<<endl;

////	int cutValue = 0;
////	//Cut out mergedRemoveRegions from mergedSCRegions
////	vector<vector<uint8_t> >::iterator SCRegion = SCRegions.begin();
////	vector<vector<uint8_t> >::iterator removeRegion = removeRegions.begin();
////	while(SCRegion != SCRegions.end()) {
////		removeRegion = removeRegions.begin();
////		while(removeRegion != removeRegions.end()) {
////			//remove region A from B
////			cutValue = cutRegion(*SCRegion, *removeRegion, SCRegions);
////			if (cutValue ==2) {
////				SCRegion = SCRegions.erase(SCRegion);
////				break;
////			}
////		removeRegion++;
////		cutValue = 0;
////		}
////		if (cutValue!=2) {
////			SCRegion++;
////		}
////	}

////	cout<<"Printing cutted out SCRegions..."<<endl;
////	printRegions(SCRegions);
////	
////	if (PRINTFLAG) 	PrintRegions(SCRegions,"SC_Region_after_cut");
////	
////	
////	cout<<"Cutted out removeRegions from SCRegions"<<endl;

////	

////	upscaleRegions(SCRegions, lumaBlockSize, chromaBlockSize, selectedRegions);
//	upscaleRegions(removeRegions,lumaBlockSize, chromaBlockSize, removedRegions);
//	
////	
////	cout<<"Printing selectedRegions..."<<endl;
////	printRegions(selectedRegions);
////	cout <<"Got upscaled selected regions"<<endl ;

////	thresholdImage(image, width, height, selectedRegions);
//	
//	thresholdImage_new(image, width, height, selectedRegions,removedRegions);



//	cout<<"Thresholded image on the basis of selectedRegions"<<endl;
//	
//}


//void getCalibrationWindows(uint8_t *image, int width, int height, int windowStartX, int windowStartY, int windowWidth, int windowHeight, uint8_t *SC[][2], uint8_t *left[][2], uint8_t *top[][2], uint8_t *right[][2], uint8_t *bottom[][2]) {
//	int y;
//	for (y=0; y<=windowHeight; y++) {
//		SC[y][0] = &image[(windowStartY + y)*width + windowStartX];
//		left[y][0] = &image[(windowStartY + y)*width + windowStartX - windowWidth];
//		top[y][0] = &image[(windowStartY - windowHeight + y)*width + windowStartX];
//		right[y][0] = &image[(windowStartY + y)*width + windowStartX + windowWidth];
//		bottom[y][0] = &image[(windowStartY + windowHeight + y)*width + windowStartX];

//		SC[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX];
//		left[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX - windowWidth];
//		top[y][1] = &image[width*height + (((windowStartY + y - windowHeight)/2))*width + windowStartX];
////		right[y][1] = &image[width*height + ((windowStartY + y + windowStartX)/2)*width + windowWidth];
////		right[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowWidth];		
//		right[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX + windowWidth];
//		bottom[y][1] = &image[width*height + (((windowStartY + y + windowHeight)/2))*width + windowStartX];
//	}
//}

#include<stdint.h>
#include<vector>
#include<iostream>

using namespace std;

#include "calibrateSC.h"
#include "indices.h"
#include "regions.h"
#include "threshold.h"
#include "utils.h"

void calibrateSC(uint8_t *image, int width, int height, int lumaBlockSize, int chromaBlockSize, float surroundPercentile, float SCPercentile,vector<vector<uint8_t> > &selectedRegions,vector<vector<uint8_t> > &removedRegions,bool PRINTFLAG) {

	int count = 0,i=0,j=0;
	bool duplicate;


	//Intentionally keepng windowWidth, windowHeight, windowStartX and windowY even so that it is convinient to access UV data from interleved plane of image in YUV420 format
	int SCWindowWidth = 2*((width/3)/2);
	int SCWindowHeight = 2*((height/3)/2);
	int windowStartX = 2*((width/3)/2);
	int windowStartY = 2*((height/3)/2);

	int sideWindowWidth = 2*((width/6)/2);
	int sideWindowHeight = 2*((2*height/3)/2);

	int topBottomWindowWidth = 2*((width/3)/2);
	int topBottomWindowHeight = 2*((height/6)/2);

	//Creating array of pointers which will be appointed appropriate address by getCalibrationSegments function
	uint8_t *SC[SCWindowHeight][2], *left[sideWindowHeight][2], *top[topBottomWindowHeight][2], *right[sideWindowHeight][2], *bottom[topBottomWindowHeight][2];

	int sizeOfColorSpace = 256/lumaBlockSize * 256/chromaBlockSize * 256/chromaBlockSize;

	vector<int> SCHistData(sizeOfColorSpace);
	vector<int> leftHistData(sizeOfColorSpace);
	vector<int> topHistData(sizeOfColorSpace);
	vector<int> rightHistData(sizeOfColorSpace);
	vector<int> bottomHistData(sizeOfColorSpace);

	vector<int> SCDecIdx(sizeOfColorSpace);
	vector<int> leftDecIdx(sizeOfColorSpace);
	vector<int> topDecIdx(sizeOfColorSpace);
	vector<int> rightDecIdx(sizeOfColorSpace);
	vector<int> bottomDecIdx(sizeOfColorSpace);

	int SCThresholdIndex, leftThresholdIndex, topThresholdIndex, rightThresholdIndex, bottomThresholdIndex;

	getCalibrationWindows(image,width,height,windowStartX,windowStartY,SCWindowWidth,SCWindowHeight,sideWindowWidth,sideWindowHeight,topBottomWindowWidth, topBottomWindowHeight,SC, left, top, right, bottom);
	cout<<"Got segements.."<<endl;

//	getHistogram(SC, windowWidth, windowHeight, lumaBlockSize, chromaBlockSize, SCHistData);
////	sortIndices(SCHistData, SCDecIdx);
////	SCThresholdIndex = thresholdIndices(SCHistData, SCDecIdx, SCPercentile);
//	SCThresholdIndex = sortIndices_MODIFIED(SCHistData, SCDecIdx, SCPercentile);
//	
	getHistogram(left, sideWindowWidth, sideWindowHeight, lumaBlockSize, chromaBlockSize, leftHistData);
//	sortIndices(leftHistData, leftDecIdx);
//	leftThresholdIndex = thresholdIndices(leftHistData, leftDecIdx, surroundPercentile);
	leftThresholdIndex = sortIndices_MODIFIED(leftHistData, leftDecIdx, surroundPercentile);
	
	getHistogram(top, topBottomWindowWidth, topBottomWindowHeight, lumaBlockSize, chromaBlockSize, topHistData);
//	sortIndices(topHistData, topDecIdx);
//	topThresholdIndex = thresholdIndices(topHistData, topDecIdx, surroundPercentile);
	topThresholdIndex = sortIndices_MODIFIED(topHistData, topDecIdx, surroundPercentile);

	getHistogram(right, sideWindowWidth, sideWindowHeight, lumaBlockSize, chromaBlockSize, rightHistData);
//	sortIndices(rightHistData, rightDecIdx);
//	rightThresholdIndex = thresholdIndices(rightHistData, rightDecIdx, surroundPercentile);
	rightThresholdIndex = sortIndices_MODIFIED(rightHistData, rightDecIdx, surroundPercentile);

	getHistogram(bottom, topBottomWindowWidth, topBottomWindowHeight, lumaBlockSize, chromaBlockSize, bottomHistData);
//	sortIndices(bottomHistData, bottomDecIdx);
//	bottomThresholdIndex = thresholdIndices(bottomHistData, bottomDecIdx, surroundPercentile);
	bottomThresholdIndex = sortIndices_MODIFIED(bottomHistData, bottomDecIdx, surroundPercentile);

//	cout<<"Got Indices"<<endl;
//	cout<<"Printing SCIndices"<<endl;
//	printIndices(SCDecIdx, SCHistData, SCThresholdIndex, lumaBlockSize, chromaBlockSize);
//	
//	if (PRINTFLAG) PrintIndices(SCDecIdx, SCHistData, SCThresholdIndex, lumaBlockSize, chromaBlockSize,"1st_SC_Indices");


	//Creating another vector of thresholded SC index
//	vector<int> SCIndices;
//	
//	for (i=0; i < SCThresholdIndex; i++) {
//		SCIndices.push_back(SCDecIdx[i]);
//		count++;
//	}

	
	count = 0;
	//Concatenate left, top, right and bottom index arrays excluding duplicates
	vector<int> removeIndices;
	
	for (i=0; i < leftThresholdIndex; i++) {
		removeIndices.push_back(leftDecIdx[i]);
		count++;
	}
	for (i=0; i < topThresholdIndex; i++) {
		duplicate = false;
		for (j=0; j<count; j++) {
			if (topDecIdx[i]==removeIndices[j]) {
				duplicate = true;
			}
		}
		if (duplicate == false) {
			removeIndices.push_back(topDecIdx[i]);
			count++;
		}
	}
	for (i=0; i < rightThresholdIndex; i++) {
		duplicate = false;
		for (j=0; j<count; j++) {
			if (rightDecIdx[i]==removeIndices[j]) {
				duplicate = true;
			}
		}
		if (duplicate == false) {
			removeIndices.push_back(rightDecIdx[i]);
			count++;
		}
	}
	for (i=0; i < bottomThresholdIndex; i++) {
		duplicate = false;
		for (j=0; j<count; j++) {
			if (bottomDecIdx[i]==removeIndices[j]) {
				duplicate = true;
			}
		}
		if (duplicate == false) {
			removeIndices.push_back(bottomDecIdx[i]);
			count++;
		}
	}

	cout<<"Got unique removeIndices list"<<endl;

	//Convert Indices to regions and merge them to the extent possible
	vector<vector<uint8_t> > removeRegions;
	getRegions(removeIndices, 3, 3, 256/lumaBlockSize, 256/chromaBlockSize, chromaBlockSize, removeRegions);

	if (PRINTFLAG) 	PrintRegions(removeRegions,"1st_Remove_Region");

	mergeRegions(removeRegions, 1);
	cout<<"Printing removeRegions..."<<endl;
	printRegions(removeRegions);
	
	if (PRINTFLAG) 	PrintRegions(removeRegions,"Remove_Region_after_merge");

//////////////////////////////////////////////////
//	vector<vector<uint8_t> > SCRegions;
//	getRegions(SCIndices, 3, 3, 256/lumaBlockSize, 256/chromaBlockSize, chromaBlockSize, SCRegions);
//	
//	if (PRINTFLAG) 	PrintRegions(SCRegions,"1st_SC_Region");
//	
//	mergeRegions(SCRegions, 1.4);
//	cout<<"Printing SCRegions..."<<endl;
//	printRegions(SCRegions);

//	if (PRINTFLAG) 	PrintRegions(SCRegions,"SC_Region_after_merge");

//////////////////////////////////////
//	cout<<"Merged removeRegions and SCRegions"<<endl;

//	int cutValue = 0;
//	//Cut out mergedRemoveRegions from mergedSCRegions
//	vector<vector<uint8_t> >::iterator SCRegion = SCRegions.begin();
//	vector<vector<uint8_t> >::iterator removeRegion = removeRegions.begin();
//	while(SCRegion != SCRegions.end()) {
//		removeRegion = removeRegions.begin();
//		while(removeRegion != removeRegions.end()) {
//			//remove region A from B
//			cutValue = cutRegion(*SCRegion, *removeRegion, SCRegions);
//			if (cutValue ==2) {
//				SCRegion = SCRegions.erase(SCRegion);
//				break;
//			}
//		removeRegion++;
//		cutValue = 0;
//		}
//		if (cutValue!=2) {
//			SCRegion++;
//		}
//	}

//	cout<<"Printing cutted out SCRegions..."<<endl;
//	printRegions(SCRegions);
//	
//	if (PRINTFLAG) 	PrintRegions(SCRegions,"SC_Region_after_cut");
//	
//	
//	cout<<"Cutted out removeRegions from SCRegions"<<endl;

//	

//	upscaleRegions(SCRegions, lumaBlockSize, chromaBlockSize, selectedRegions);
	upscaleRegions(removeRegions,lumaBlockSize, chromaBlockSize, removedRegions);
	
//	
//	cout<<"Printing selectedRegions..."<<endl;
//	printRegions(selectedRegions);
//	cout <<"Got upscaled selected regions"<<endl ;

//	thresholdImage(image, width, height, selectedRegions);
	
	thresholdImage_new(image, width, height, selectedRegions,removedRegions);



	cout<<"Thresholded image on the basis of selectedRegions"<<endl;
	
}


//void getCalibrationWindows(uint8_t *image, int width, int height, int windowStartX, int windowStartY, int windowWidth, int windowHeight, uint8_t *SC[][2], uint8_t *left[][2], uint8_t *top[][2], uint8_t *right[][2], uint8_t *bottom[][2]) {
//	int y;
//	for (y=0; y<=windowHeight; y++) {
//		SC[y][0] = &image[(windowStartY + y)*width + windowStartX];
//		left[y][0] = &image[(windowStartY + y)*width + windowStartX - windowWidth];
//		top[y][0] = &image[(windowStartY - windowHeight + y)*width + windowStartX];
//		right[y][0] = &image[(windowStartY + y)*width + windowStartX + windowWidth];
//		bottom[y][0] = &image[(windowStartY + windowHeight + y)*width + windowStartX];

//		SC[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX];
//		left[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX - windowWidth];
//		top[y][1] = &image[width*height + (((windowStartY + y - windowHeight)/2))*width + windowStartX];
////		right[y][1] = &image[width*height + ((windowStartY + y + windowStartX)/2)*width + windowWidth];
////		right[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowWidth];		
//		right[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX + windowWidth];
//		bottom[y][1] = &image[width*height + (((windowStartY + y + windowHeight)/2))*width + windowStartX];
//	}
//}



void getCalibrationWindows(uint8_t *image, int width, int height, int windowStartX, int windowStartY,int SCWindowWidth, int SCWindowHeight,int sideWindowWidth,int sideWindowHeight,int topBottomWindowWidth,int topBottomWindowHeight, uint8_t *SC[][2], uint8_t *left[][2], uint8_t *top[][2], uint8_t *right[][2], uint8_t *bottom[][2]) {
	int y;
//	for (y=0; y<=windowHeight; y++) {
//		SC[y][0] = &image[(windowStartY + y)*width + windowStartX];
//		left[y][0] = &image[(windowStartY + y)*width + windowStartX - windowWidth];
//		top[y][0] = &image[(windowStartY - topBottomWindowHeight + y)*width + windowStartX];
//		right[y][0] = &image[(windowStartY + y)*width + windowStartX + windowWidth];
//		bottom[y][0] = &image[(windowStartY + windowHeight + y)*width + windowStartX];

//		SC[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX];
//		left[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX - windowWidth];
//		top[y][1] = &image[width*height + (((windowStartY + y - windowHeight)/2))*width + windowStartX];
//		right[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX + windowWidth];
//		bottom[y][1] = &image[width*height + (((windowStartY + y + windowHeight)/2))*width + windowStartX];
//	}
//	
	for (y=0; y<=SCWindowHeight; y++) {

		SC[y][0] = &image[(windowStartY + y)*width + windowStartX];
		SC[y][1] = &image[width*height + ((windowStartY + y)/2)*width + windowStartX];

	}

	for (y=0; y<=sideWindowHeight; y++) {

		left[y][0] = &image[(windowStartY + y-sideWindowHeight/4)*width + windowStartX - sideWindowWidth];
		right[y][0] = &image[(windowStartY + y-sideWindowHeight/4)*width + windowStartX + SCWindowWidth];

		left[y][1] = &image[width*height + ((windowStartY + y-sideWindowHeight/4)/2)*width + windowStartX - sideWindowWidth];
		right[y][1] = &image[width*height + ((windowStartY + y-sideWindowHeight/4)/2)*width + windowStartX + SCWindowWidth];
	}

	for (y=0; y<=topBottomWindowHeight; y++) {
		top[y][0] = &image[(windowStartY - topBottomWindowHeight + y)*width + windowStartX];
		bottom[y][0] = &image[(windowStartY + SCWindowHeight + y)*width + windowStartX];

		top[y][1] = &image[width*height + (((windowStartY + y - topBottomWindowHeight)/2))*width + windowStartX];
		bottom[y][1] = &image[width*height + (((windowStartY + y + SCWindowHeight)/2))*width + windowStartX];
	}

}

