#include<stdint.h>
#include<vector>
#include<iostream>

using namespace std;

void thresholdImage(uint8_t *image, int width, int height,  vector<vector<uint8_t> > &selectedRegions) {
	int i,j,k;
	for (i=0; i< height; i++) {
//		cout<<"Processing row: "<<i<<endl;
		for (j=0; j<width; j++) {
			for (k=0; k<selectedRegions.size(); k++) {
				if ( image[i*width + j] >= selectedRegions[k][0] && image[i*width +j] <= selectedRegions[k][3] ) {
					if (image[width*height + (i/2)*width + (j/2)*2] >= selectedRegions[k][1] && image[width*height + (i/2)*width + (j/2)*2] <= selectedRegions[k][4] ) {
						if (image[width*height + (i/2)*width + (j/2)*2 + 1] >= selectedRegions[k][2] && image[width*height + (i/2)*width + (j/2)*2 + 1] <= selectedRegions[k][5] ) {
							image[i*width + j] = 0;
						}
					}
				}
			}
		}
	}
	return;
}


void thresholdImage_new(uint8_t *image, int width, int height,  vector<vector<uint8_t> > &selectedRegions,vector<vector<uint8_t> > &removedRegions) {
	int i,j,k;
	for (i=0; i< height; i++) {
//		cout<<"Processing row: "<<i<<endl;
		for (j=0; j<width; j++) {
//			for (k=0; k<selectedRegions.size(); k++) {
//				if ( image[i*width + j] >= selectedRegions[k][0] && image[i*width +j] < selectedRegions[k][3] ) {
//					if (image[width*height + (i/2)*width + (j/2)*2] >= selectedRegions[k][1] && image[width*height + (i/2)*width + (j/2)*2] < selectedRegions[k][4] ) {
//						if (image[width*height + (i/2)*width + (j/2)*2 + 1] >= selectedRegions[k][2] && image[width*height + (i/2)*width + (j/2)*2 + 1] < selectedRegions[k][5] ) {
//							image[i*width + j] = 0;
//						}
//					}
//				}
//			}

			for (k=0; k<removedRegions.size(); k++) {
				if ( image[i*width + j] >= removedRegions[k][0] && image[i*width +j] <= removedRegions[k][3] ) {
					if (image[width*height + (i/2)*width + (j/2)*2] >= removedRegions[k][1] && image[width*height + (i/2)*width + (j/2)*2] <= removedRegions[k][4] ) {
						if (image[width*height + (i/2)*width + (j/2)*2 + 1] >= removedRegions[k][2] && image[width*height + (i/2)*width + (j/2)*2 + 1] <= removedRegions[k][5] ) {
							image[i*width + j] = 1;
						}
						else image[i*width + j] = 0;
					}
				}
			}


		}
	}
	return;
}

void chromaThresholdImage(uint8_t *image, int width, int height, vector<vector<uint8_t> > &selectedRegions) {
	int i,j,k;
	for (i=0; i< height; i++) {
//		cout<<"Processing row: "<<i<<endl;
		for (j=0; j<width; j++) {
			for (k=0; k<selectedRegions.size(); k++) {
				if (image[width*height + (i/2)*width + (j/2)*2] >= selectedRegions[k][1] && image[width*height + (i/2)*width + (j/2)*2] < selectedRegions[k][4] ) {
					if (image[width*height + (i/2)*width + (j/2)*2 + 1] >= selectedRegions[k][2] && image[width*height + (i/2)*width + (j/2)*2 + 1] < selectedRegions[k][5] ) {
						image[i*width + j] = 0;
					}
				}
			}
		}
	}
	return;
}
