#include<vector>
#include<stdint.h>
#include<stdlib.h>

using namespace std;

void getRegions(vector<int>& indices, int lumaSize, int chromaSize, int lumaMax, int chromaMax, int chromaBlockSize, vector<vector<uint8_t> >& regions) {
	vector<int>::iterator v=indices.begin();
	vector<uint8_t>* newVec;
	int YIndex, UIndex, VIndex;
	int xPos, yPos, zPos;
	int xPosEnd, yPosEnd, zPosEnd;

	while (v!=indices.end()) {
		YIndex = *v/((256/chromaBlockSize)*(256/chromaBlockSize));
		UIndex = (*v % ((256/chromaBlockSize)*(256/chromaBlockSize)) )/(256/chromaBlockSize);
		VIndex = *v % (256/chromaBlockSize);

		xPos = max(YIndex - (lumaSize-1)/2, 0);
		yPos = max(UIndex - (chromaSize-1)/2, 0);
		zPos = max(VIndex - (chromaSize-1)/2, 0);
		xPosEnd = min(YIndex + (lumaSize-1)/2, lumaMax);
		yPosEnd = min(UIndex + (chromaSize-1)/2, chromaMax);
		zPosEnd = min(VIndex + (chromaSize-1)/2, chromaMax);
    
		newVec = new vector<uint8_t>;
		newVec->push_back(xPos);
		newVec->push_back(yPos);
		newVec->push_back(zPos);
		newVec->push_back(xPosEnd);
		newVec->push_back(yPosEnd);
		newVec->push_back(zPosEnd);

		regions.push_back(*newVec);
		v++;
	}
}


void mergeRegions(vector<vector<uint8_t> >& regions, float upThreshold) {
	int YCenter0, UCenter0, VCenter0;
	int YCenter1, UCenter1, VCenter1;
	int YSize0, USize0, VSize0;
	int YSize1, USize1, VSize1;
	int newXPos, newYPos, newZPos;
	int newXPosEnd, newYPosEnd, newZPosEnd;
	int newVolume, originalVolume;

	vector<vector<uint8_t> >::iterator v0 = regions.begin();
	while (v0 != regions.end()) {
		vector<vector<uint8_t> >::iterator v1 = regions.begin();
		while (v1 != regions.end()) {
			if (v0!= v1) {
				YCenter0 = ((*v0)[0] + (*v0)[3] )/2;
				UCenter0 = ((*v0)[1] + (*v0)[4] )/2;
				VCenter0 = ((*v0)[2] + (*v0)[5] )/2;

				YCenter1 = ((*v1)[0] + (*v1)[3] )/2;
				UCenter1 = ((*v1)[1] + (*v1)[4] )/2;
				VCenter1 = ((*v1)[2] + (*v1)[5] )/2;

				YSize0 = abs((*v0)[0] - (*v0)[3]);
				USize0 = abs((*v0)[1] - (*v0)[4]);
				VSize0 = abs((*v0)[2] - (*v0)[5]);

				YSize1 = abs((*v1)[0] - (*v1)[3]);
				USize1 = abs((*v1)[1] - (*v1)[4]);
				VSize1 = abs((*v1)[2] - (*v1)[5]);
				
				if ( abs(YCenter0 - YCenter1) < 0.5* (YSize0 + YSize1) && abs(UCenter0 - UCenter1) < 0.5* (USize0 + USize1) && abs(VCenter0 - VCenter1) < 0.5* (VSize0 + VSize1) ) {
					newXPos = min(min((*v0)[0], (*v0)[3]), min((*v1)[0], (*v1)[3]));
					newXPosEnd = max(max((*v0)[0], (*v0)[3]), max((*v1)[0], (*v1)[3]));
					newYPos = min(min((*v0)[1], (*v0)[4]), min((*v1)[1], (*v1)[4]));
					newYPosEnd = max(max((*v0)[1], (*v0)[4]), max((*v1)[1], (*v1)[4]));
					newZPos = min(min((*v0)[2], (*v0)[5]), min((*v1)[2], (*v1)[5]));
					newZPosEnd = max(max((*v0)[2], (*v0)[5]), max((*v1)[2], (*v1)[5]));
					
					newVolume = abs((newXPosEnd - newXPos)*(newYPosEnd - newYPos)*(newZPosEnd - newZPos));
					originalVolume = YSize0*USize0*VSize0 + YSize1*USize1*VSize1;
					
					if ((float)(newVolume)/originalVolume <= upThreshold) {
						(*v0)[0] = newXPos;
						(*v0)[1] = newYPos;
						(*v0)[2] = newZPos;
						(*v0)[3] = newXPosEnd;
						(*v0)[4] = newYPosEnd;
						(*v0)[5] = newZPosEnd;
						regions.erase(v1);
						v0--;
						break;
					}
				}
			}
			v1++;
		}
		v0++;
	}
}


int cutRegion(vector<uint8_t> &fromRegion, vector<uint8_t> &cutRegion,vector<vector<uint8_t> > &leftOverRegions) {
	vector<uint8_t> *leftOverRegion;
	if ( (cutRegion[0] > fromRegion[3] || cutRegion[3] < fromRegion[0]) || (cutRegion[1] > fromRegion[4] || cutRegion[4] < fromRegion[1]) || (cutRegion[2] > fromRegion[5] || cutRegion[5] < fromRegion[2]) ) {
//		leftOverRegion = new vector<uint8_t>(fromRegion);
//		leftOverRegions.push_back(*leftOverRegion);
		return 1;
	}
	
	if (cutRegion[0] > fromRegion[0] && cutRegion[0] <= fromRegion[3] ) {
		leftOverRegion = new vector<uint8_t>(fromRegion);
		(*leftOverRegion)[3] = cutRegion[0]-1;
		leftOverRegions.push_back(*leftOverRegion);
		fromRegion[0] = cutRegion[0];
	}
	
	if (cutRegion[1] > fromRegion[1] && cutRegion[1] <= fromRegion[4] ) {
		leftOverRegion = new vector<uint8_t>(fromRegion);
		(*leftOverRegion)[4] = cutRegion[1]-1;
		leftOverRegions.push_back(*leftOverRegion);
		fromRegion[1] = cutRegion[1];
	}
	
	if (cutRegion[2] > fromRegion[2] && cutRegion[2] <= fromRegion[5] ) {
		leftOverRegion = new vector<uint8_t>(fromRegion);
		(*leftOverRegion)[5] = cutRegion[2]-1;
		leftOverRegions.push_back(*leftOverRegion);
		fromRegion[2] = cutRegion[2];
	}
	
	if (cutRegion[3] >= fromRegion[0] && cutRegion[3] < fromRegion[3] ) {
		leftOverRegion = new vector<uint8_t>(fromRegion);
		(*leftOverRegion)[0] = cutRegion[3]+1;
		leftOverRegions.push_back(*leftOverRegion);
		fromRegion[3] = cutRegion[3];
	}
	
	if (cutRegion[4] >= fromRegion[1] && cutRegion[4] < fromRegion[4] ) {
		leftOverRegion = new vector<uint8_t>(fromRegion);
		(*leftOverRegion)[1] = cutRegion[4]+1;
		leftOverRegions.push_back(*leftOverRegion);
		fromRegion[4] = cutRegion[4];
	}
	
	if (cutRegion[5] >= fromRegion[2] && cutRegion[5] < fromRegion[5] ) {
		leftOverRegion = new vector<uint8_t>(fromRegion);
		(*leftOverRegion)[2] = cutRegion[5]+1;
		leftOverRegions.push_back(*leftOverRegion);
		fromRegion[5] = cutRegion[5];
	}

//	leftOverRegions.erase(fromRegion);

	return 2;

}


void upscaleRegions(vector<vector<uint8_t> > &leftOverRegions, int lumaBlockSize, int chromaBlockSize, vector<vector<uint8_t> > &selectedRegions) {
	vector<uint8_t> *selectedRegion;
	vector<vector<uint8_t> >::iterator v = leftOverRegions.begin();
	while (v != leftOverRegions.end()) {
		selectedRegion = new vector<uint8_t>(*v);
		(*selectedRegion)[0] = lumaBlockSize*((*selectedRegion)[0]);
		(*selectedRegion)[1] = chromaBlockSize*((*selectedRegion)[1]);
		(*selectedRegion)[2] = chromaBlockSize*((*selectedRegion)[2]);
		(*selectedRegion)[3] = lumaBlockSize*((*selectedRegion)[3]+1)-1;
		(*selectedRegion)[4] = chromaBlockSize*((*selectedRegion)[4]+1)-1;
		(*selectedRegion)[5] = chromaBlockSize*((*selectedRegion)[5]+1)-1;
		selectedRegions.push_back(*selectedRegion);
		v++;
	}

	return;

}
