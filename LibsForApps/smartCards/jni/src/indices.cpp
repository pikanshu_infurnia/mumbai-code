#include <stdint.h>
#include <numeric>      // std::iota
#include <algorithm>    // std::sort
#include <iostream>     // std::cout
#include <functional>   // std::bind
#include <iterator>
#include <vector>
//#include <algo.h>
using namespace std;

void getHistogram(uint8_t *window[][2], int windowWidth, int windowHeight, int lumaBlockSize, int chromaBlockSize, vector<int> &histData) {
	int i,j;
	int YIndex, UIndex, VIndex;
	for (i=0; i<windowHeight; i++) {
		for (j=0; j<windowWidth; j++) {
			YIndex = window[i][0][j]/lumaBlockSize;
			UIndex = window[i][1][2*(j/2)]/chromaBlockSize;
			VIndex = window[i][1][2*(j/2)+1]/chromaBlockSize;
			histData[YIndex*(256/chromaBlockSize)*(256/chromaBlockSize) + UIndex*(256/chromaBlockSize) + VIndex] += 1;
		}
	}
}


bool compare(int a, int b, vector<int>& data)
{
    return data[a]>data[b];
}

void sortIndices(vector<int>& data, vector<int>& index) {
	
	iota(index.begin(), index.end(), 0); // fill index with {0,1,2,...} This only needs to happen once
	sort(index.begin(), index.end(), bind(compare, std::placeholders::_1, std::placeholders::_2, data ));
}


int thresholdIndices(vector<int>& histData, vector<int>& indices, float percentile) {
	int i, count=0, thresh=0;
	for (vector<int>::iterator v=histData.begin(); v !=histData.end(); v++) thresh+=*v;

	for (i=0; i < indices.size(); i++) {
		count += histData[indices[i]];
		if (count>= thresh*(percentile/100)) break;
	}
	return i+1;
}


int sortIndices_MODIFIED(vector<int>& histData, vector<int>& index, float percentile){
	
	int thresh = 0,max,max_index,count = 0,temp,i;
	iota(index.begin(), index.end(), 0); // fill index with {0,1,2,...} This only needs to happen once
	
	for (vector<int>::iterator v=histData.begin(); v !=histData.end(); v++) thresh+=*v;

	for(i = 0; i < histData.size(); i++){
		max = histData[index[i]];
		for(int j = i; j< histData.size(); j++){
			if(max < histData[index[j]]){
				max = histData[index[j]];
				max_index = j;
			}
		}
		
		index[i] = temp;
		index[i] = index[max_index];
		index[max_index] = temp;
		
		count += max;
		
		if (count>= thresh*(percentile/100)) break;
	}
	
	return i+1;
}
