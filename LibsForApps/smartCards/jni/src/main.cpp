#include<iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "calibrateSC.h"
#include <string>
#include <stdlib.h>

using namespace cv;
using namespace std;

int main(int argc, char** argv) {

	if (argc<2) {
		cout<<"Usage: calibrate imageToCalibrate [lumaBlockSize chromaBlockSize surroundPercentile SCPercentile]\n";
	}
	
	uint8_t lumaBlockSize = 16;
	uint8_t chromaBlockSize = 4;

	float surroundPercentile = 90;
	float SCPercentile = 99.9;
	
	if (argc>2) {
		lumaBlockSize = *((uint8_t*)&argv[2]);
	}

	if (argc>3) {
		chromaBlockSize = *((uint8_t*)argv[3]);
	}

	if (argc>4) {
		string argv4Str(argv[4]);
		surroundPercentile = stof(argv4Str);
	}

	if (argc>5) {
		string argv5Str(argv[5]);
		SCPercentile = stof(argv5Str);
	}

	Mat inputImage, image, outputImage;
	inputImage = imread(argv[1], CV_LOAD_IMAGE_COLOR);
	
	if(! inputImage.data ) {
		cout <<  "Could not open or find the image" << std::endl ;
		return -1;
	}

	cvtColor(inputImage, image, CV_BGR2YCrCb);
	
	int i, j, width, height;
	width = image.cols;
	height = image.rows;
	uint8_t imagePointer[height*width + (height/2)*width];

	//Converting multichannel YCrCb data into YUV420
	for (i=0; i<height; i++) {
		for (j=0; j<width; j++) {
			imagePointer[i*width + j] = image.data[3*(i*width +j)];
			imagePointer[height*width + (i/2)*width + 2*(j/2)] = image.data[3*(i*width + j) + 1];
			imagePointer[height*width + (i/2)*width + 2*(j/2) + 1] = image.data[3*(i*width + j) + 2];
		}
	}

	calibrateSC(imagePointer, width, height, lumaBlockSize, chromaBlockSize, surroundPercentile, SCPercentile);

	//Copying just illuminance value from YUV420 into YCrCb
	for (i=0; i<height; i++) {
		for (j=0; j<width; j++) {
			image.data[3*(i*width +j)] = imagePointer[i*width + j];
		}
	}
	
	cvtColor(image, outputImage, CV_YCrCb2BGR);
	
	namedWindow( "Display window", WINDOW_AUTOSIZE );	// Create a window for display.

	imshow( "Display window", outputImage );                   // Show our image inside it.
	imwrite("sc0_output.png", outputImage);
	while(1) {
		waitKey(0);                                          // Wait for a keystroke in the window
	}

	return 0;

}
