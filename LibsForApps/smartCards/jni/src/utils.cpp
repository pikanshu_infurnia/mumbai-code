#include <stdint.h>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <android/log.h>
using namespace std;

void printRegions(vector<vector<uint8_t> > &regions) {
	vector<vector <uint8_t> >::iterator iRegion = regions.begin();
	vector<uint8_t>::iterator iValue;
	while (iRegion != regions.end()) {
		iValue = (*iRegion).begin();
		while (iValue != (*iRegion).end()) {
			printf("%d\t", *iValue);
			iValue++;
		}
		cout<<"\n";
		iRegion++;
	}
	return;
}


void printIndices(vector<int>& indices, vector<int>& histData, int thresholdIndex, int lumaBlockSize, int chromaBlockSize) {
	int i, YIndex, UIndex, VIndex;
	for (i=0; i <= thresholdIndex; i++) {
		YIndex = indices[i]/((256/chromaBlockSize)*(256/chromaBlockSize));
                UIndex = (indices[i] % ((256/chromaBlockSize)*(256/chromaBlockSize)) )/(256/chromaBlockSize);
                VIndex = indices[i] % (256/chromaBlockSize);
		cout<<YIndex<<"\t"<<UIndex<<"\t"<<VIndex<<"\t"<<histData[indices[i]]<<endl;
	}
}

void PrintRegions(vector<vector<uint8_t> > &regions,char str[]) {
	__android_log_print(ANDROID_LOG_VERBOSE,"calibrateSC","%s",str);
	for(int i = 0 ; i< regions.size();i++){
	    __android_log_print(ANDROID_LOG_VERBOSE,"calibrateSC"," (%d,%d,%d) (%d,%d,%d)",regions[i][0],regions[i][1],regions[i][2],regions[i][3],regions[i][4],regions[i][5]);
	}
	return;
}


void PrintIndices(vector<int>& indices, vector<int>& histData, int thresholdIndex, int lumaBlockSize, int chromaBlockSize,char str[]) {
	int i, YIndex, UIndex, VIndex;
	__android_log_print(ANDROID_LOG_VERBOSE,"calibrateSC","%s",str);
	for (i=0; i <= thresholdIndex; i++) {
		YIndex = indices[i]/((256/chromaBlockSize)*(256/chromaBlockSize));
                UIndex = (indices[i] % ((256/chromaBlockSize)*(256/chromaBlockSize)) )/(256/chromaBlockSize);
                VIndex = indices[i] % (256/chromaBlockSize);
//		cout<<YIndex<<"\t"<<UIndex<<"\t"<<VIndex<<"\t"<<histData[indices[i]]<<endl;
		__android_log_print(ANDROID_LOG_VERBOSE,"calibrateSC"," %d \t %d \t %d \t %d",YIndex,UIndex,VIndex,histData[indices[i]]);
	}
}
